ALTER TABLE dbo.tblNameField ADD city NVARCHAR(20) NULL ;
ALTER TABLE dbo.tblNameField ADD state NVARCHAR(10) NULL ;
ALTER TABLE dbo.tblNameField ADD zip NVARCHAR(10) NULL ;
ALTER TABLE dbo.tblNameField ADD email NVARCHAR(100) NULL ;
ALTER TABLE dbo.tblNameField ADD cellphone NVARCHAR(20) NULL ;
ALTER TABLE dbo.tblNameField ADD fax NVARCHAR(20) NULL ;
ALTER TABLE dbo.tblApplications ADD OutcomeType nvarchar(100);
ALTER TABLE dbo.tblApplications ADD PermitType nvarchar(100);
GO