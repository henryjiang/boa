ALTER TABLE dbo.tblApplications DROP COLUMN AssocAppeal
GO
ALTER TABLE dbo.tblApplications ADD AssocAppeal NVARCHAR(255) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN Block
GO
ALTER TABLE dbo.tblApplications ADD Block NVARCHAR(20) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN Lot
GO
ALTER TABLE dbo.tblApplications ADD Lot NVARCHAR(20) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN PostCardsMailed
GO
ALTER TABLE dbo.tblApplications ADD PostCardsMailed DateTime NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN IndexCardsFiled
GO
ALTER TABLE dbo.tblApplications ADD IndexCardsFiled DateTime NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN LetterNoticesMailed
GO
ALTER TABLE dbo.tblApplications ADD LetterNoticesMailed DateTime NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN PlanningDept
GO
ALTER TABLE dbo.tblApplications ADD PlanningDept nvarchar(100) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN JRGranted
GO
ALTER TABLE dbo.tblApplications ADD JRGranted Bit NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN JRGrantedText
GO
ALTER TABLE dbo.tblApplications ADD JRGrantedText nvarchar(100) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN Conditions
GO
ALTER TABLE dbo.tblApplications ADD Conditions Bit NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN ConditionsText
GO
ALTER TABLE dbo.tblApplications ADD ConditionsText nvarchar(100) NULL ;
GO
ALTER TABLE dbo.tblApplications DROP COLUMN Continuance
GO
ALTER TABLE dbo.tblApplications ADD Continuance NVARCHAR(1000) NULL ;
GO