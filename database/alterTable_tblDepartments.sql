ALTER TABLE dbo.tblDepartments DROP COLUMN street
GO
ALTER TABLE dbo.tblDepartments ADD street NVARCHAR(200) NULL ;
GO
ALTER TABLE dbo.tblDepartments DROP COLUMN city
GO
ALTER TABLE dbo.tblDepartments ADD city NVARCHAR(20) NULL ;
GO
ALTER TABLE dbo.tblDepartments DROP COLUMN state
GO
ALTER TABLE dbo.tblDepartments ADD state NVARCHAR(10) NULL ;
GO
ALTER TABLE dbo.tblDepartments DROP COLUMN zip
GO
ALTER TABLE dbo.tblDepartments ADD zip NVARCHAR(10) NULL ;
GO
ALTER TABLE dbo.tblDepartments DROP COLUMN phone
GO
ALTER TABLE dbo.tblDepartments ADD phone NVARCHAR(20) NULL ;
GO
ALTER TABLE dbo.tblDepartments DROP COLUMN fax
GO
ALTER TABLE dbo.tblDepartments ADD fax NVARCHAR(20) NULL ;
GO

