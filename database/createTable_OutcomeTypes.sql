USE [LicenseDB]
GO

/****** Object:  Table [dbo].[tblOutcomeTypes]    Script Date: 01/25/2011 14:15:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOutcomeTypes]') AND type in (N'U'))
DROP TABLE [dbo].tblOutcomeTypes
GO

USE [LicenseDB]
GO

/****** Object:  Table [dbo].[tblOutcomeTypes]    Script Date: 01/25/2011 14:15:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].tblOutcomeTypes(
	[OutcomeName] [nvarchar](100) NOT NULL,
	[OutcomeType] [nvarchar](20) NOT NULL,
	[AgencyID] [nvarchar](4) NOT NULL,
	[Comments] [nvarchar](1000) NULL,
 CONSTRAINT [PK_tblOutcomeTypes] PRIMARY KEY CLUSTERED 
(
	[OutcomeType] ASC,
	[AgencyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


