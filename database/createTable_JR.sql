USE [LicenseDB]
GO
/****** Object:  Table [dbo].[tblJRApplications]    Script Date: 07/27/2011 16:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblJRApplications]') AND type in (N'U'))
DROP TABLE [dbo].[tblJRApplications]
GO
CREATE TABLE [dbo].[tblJRApplications](
	[ApplicationID] [nvarchar](17) NOT NULL,
	[FileDate] [datetime] NULL,
	[Status] [nvarchar](20) NULL,
	[ExpDate] [datetime] NULL,
	[PermitNumber] [nvarchar](20) NULL,
	[RouteNumber] [nvarchar](2) NULL,
	[HearingDate] [datetime] NULL,
	[AgencyID] [nvarchar](4) NOT NULL,
	[DBA] [nchar](100) NULL,
	[Public_Comments] [nvarchar](1000) NULL,
	[Comm_Comments] [nvarchar](1000) NULL,
	[CaseNum] [nvarchar](50) NULL,
	[CaseType] [nvarchar](100) NULL,
	[Related_Agency] [nvarchar](4) NULL,
	[InitialFee] [smallmoney] NULL,
	[OutcomeType] [nvarchar](100) NULL,
	[PermitType] [nvarchar](100) NULL,
 CONSTRAINT [PK_tblJRApplications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC,
	[AgencyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
