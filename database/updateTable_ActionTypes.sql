DELETE FROM [LicenseDB].[dbo].[tblActionTypes]

INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('ISSUANCE', 'ISSUANCE', 'BOA', NULL, NULL, NULL, NULL, 'BOA')
INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('DENIAL', 'DENIAL', 'BOA', NULL, NULL, NULL, NULL, 'BOA')
INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('SUSPENSION', 'SUSPENSION', 'BOA', NULL, NULL, NULL, NULL, 'BOA')
INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('REVOCATION', 'REVOCATION', 'BOA', NULL, NULL, NULL, NULL, 'BOA')
INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('CONDITIONS', 'CONDITIONS', 'BOA', NULL, NULL, NULL, NULL, 'BOA')
INSERT INTO [LicenseDB].[dbo].[tblActionTypes] VALUES ('OTHER ACTION', 'OTHER ACTION', 'BOA', NULL, NULL, NULL, NULL, 'BOA')

