USE [LicenseDB]
GO

/****** Object:  Table [dbo].[tblPermitTypes]    Script Date: 02/14/2011 11:37:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[tblPermitTypes]
GO

CREATE TABLE [dbo].[tblPermitTypes](
	[PermitName] [nvarchar](100) NOT NULL,
	[PermitType] [nvarchar](20) NOT NULL,
	[AgencyID] [nvarchar](4) NOT NULL,
	[Comments] [nvarchar](1000) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Fee] [smallmoney] NULL,
	[DeptID] [nvarchar](8) NULL,
 CONSTRAINT [PK_tblPermitTypes_1] PRIMARY KEY CLUSTERED 
(
	[PermitType] ASC,
	[AgencyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


