﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ServiceModel.DomainServices.Client.ApplicationServices;
using System.ComponentModel.DataAnnotations;
using BOA_AppealApplication1.Web.Services;
using BOA_AppealApplication1.Web;
using System.ServiceModel.DomainServices.Client;


namespace BOA_AppealApplication1.LoginUI
{
    public partial class UserLogin : Page
    {
        private LoginInfo loginInfo = new LoginInfo();

        public UserLogin()
        {
            InitializeComponent();
            // Set the DataContext of this control to the
            // LoginInfo instance to allow for easy binding
            this.DataContext = this.loginInfo;
        }

        /// <summary>
        /// Handles <see cref="DataForm.AutoGeneratingField"/> to provide the PasswordAccessor.
        /// </summary>
        private void userLoginForm_AutoGeneratingField(object sender, DataFormAutoGeneratingFieldEventArgs e)
        {
            if (e.PropertyName == "Password")
            {
                PasswordBox passwordBox = (PasswordBox)e.Field.Content;
                this.loginInfo.PasswordAccessor = () => passwordBox.Password;
                passwordBox.KeyUp += new KeyEventHandler(passwordBox_KeyUp);
            }
        }

        void passwordBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.LoginButton_Click(sender, e);
            }
        }

        /// <summary>
        ///     Submits the <see cref="LoginOperation"/> to the server
        /// </summary>
        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (this.userLoginForm.ValidateItem())
            {
                //this.loginInfo.Password = ApplicationStrings.AgencyFor;
                this.loginInfo.CurrentLoginOperation = WebContext.Current.Authentication.Login(this.loginInfo.ToLoginParameters(), this.LoginOperation_Completed, null);

            }
            
        }



        /// <summary>
        /// Completion handler for a <see cref="LoginOperation"/>. If operation succeeds,
        /// it closes the window. If it has an error, we show an <see cref="ErrorWindow"/>
        /// and mark the error as handled. If it was not canceled, but login failed, it must
        /// have been because credentials were incorrect so we add a validation error to
        /// to notify the user.
        /// </summary>

        private void LoginOperation_Completed(LoginOperation loginOperation)
        {
            if (loginOperation.LoginSuccess)
            {
                this.loginInfo.CurrentLoginOperation = WebContext.Current.Authentication.Login(this.loginInfo.ToLoginParameters(), this.LoginOperation1_Completed, null);

            }
            else if (loginOperation.HasError)
            {
                ErrorWindow.CreateNew(loginOperation.Error);
                loginOperation.MarkErrorAsHandled();
            }
            else if (!loginOperation.IsCanceled)
            {
                this.loginInfo.ValidationErrors.Add(new ValidationResult(ErrorResources.ErrorBadAgency, new string[] { "UserName", "Password" }));
            }
        }

        private void LoginOperation1_Completed(LoginOperation loginOperation)
        {
            if (loginOperation.LoginSuccess)
            {
                //this.LoginLabel.Text = "You are now login!";
                this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
            }
            else if (loginOperation.HasError)
            {
                ErrorWindow.CreateNew(loginOperation.Error);
                loginOperation.MarkErrorAsHandled();
            }
            else if (!loginOperation.IsCanceled)
            {
                this.loginInfo.ValidationErrors.Add(new ValidationResult(ErrorResources.ErrorBadUserNameOrPassword, new string[] { "UserName", "Password" }));
            }
        }
        /// <summary>
        ///     If a login operation is in progress and is cancellable, cancel it.
        ///     Otherwise, closes the window
        /// </summary>
        //private void CancelButton_Click(object sender, EventArgs e)
        //{
        //    //if (this.lastLoginOperation != null && this.lastLoginOperation.CanCancel)
        //    //{
        //    //    this.lastLoginOperation.Cancel();
        //    //}
        //    //else
        //    //{
        //    //    this.parentWindow.Close();
        //    //}

        //    this.txtUserName.Text="";
        //    this.txtPassword = new PasswordBox();
        //}

    }
    //public partial class UserLogin : Page
    //{
    //    private LoginRegistrationWindow parentWindow;
    //    private LoginInfo loginInfo = new LoginInfo();
    //    private LoginOperation lastLoginOperation = null;

    //    public UserLogin()
    //    {
    //        InitializeComponent();

    //        this.loginForm.CurrentItem = this.loginInfo;
    //    }

    //    // Executes when the user navigates to this page.

    //    private void LoginForm_OnAutoGeneratingField(object sender, DataFormAutoGeneratingFieldEventArgs e)
    //    {
    //        if (e.PropertyName == "UserName")
    //        {
    //            ((TextBox)e.Field.Content).TextChanged += EnableOrDisableOKButton;
    //        }
    //        else if (e.PropertyName == "Password")
    //        {
    //            ((PasswordBox)e.Field.Content).PasswordChanged += EnableOrDisableOKButton;
    //        }
    //    }

    //    /// <summary>
    //    ///     Enables the OK button if both username and password are not empty, disable it
    //    ///     otherwise
    //    /// </summary>
    //    private void EnableOrDisableOKButton(object sender, EventArgs e)
    //    {
    //        this.loginButton.IsEnabled = !(
    //            String.IsNullOrEmpty(((TextBox)this.loginForm.Fields["UserName"].Content).Text) ||
    //            String.IsNullOrEmpty(((PasswordBox)this.loginForm.Fields["Password"].Content).Password)
    //    );
    //    }


    //    /// <summary>
    //    ///     Removes the binding that the cancel button will have to <see cref="LoginOperation.CanCancel"/>
    //    ///     while an operation is in progress and makes it enabled again
    //    /// </summary>
    //    private void ReEnableCancelButton(object sender, EventArgs eventArgs)
    //    {
    //        this.loginCancel.IsEnabled = true;
    //    }

    //    /// <summary>
    //    ///     Handles <see cref="LoginOperation.Completed"/> event. If operation
    //    ///     succeeds, it closes the window. If it has an error, we show an <see cref="ErrorWindow"/>
    //    ///     and mark the error as handled. If it was not canceled, succeded but login failed, 
    //    ///     it must have been because credentials were incorrect so we add a validation error 
    //    ///     to notify the user
    //    /// </summary>
    //    private void LoginOperation_Completed(LoginOperation loginOperation)
    //    {
    //        if (loginOperation.LoginSuccess)
    //        {
    //            this.parentWindow.Close();
    //        }
    //        else
    //        {
    //            if (loginOperation.HasError)
    //            {
    //                ErrorWindow.CreateNew(loginOperation.Error);
    //                loginOperation.MarkErrorAsHandled();
    //            }
    //            else if (!loginOperation.IsCanceled)
    //            {
    //                this.loginForm.ValidationSummary.Errors.Add(new ValidationSummaryItem(ErrorResources.ErrorBadUserNameOrPassword));
    //            }

    //            this.loginForm.BeginEdit();
    //        }
    //    }

    //    /// <summary>
    //    ///     Binds UI so that controls will look disabled and activity control will
    //    ///     be displaying while operation is in progress, and cancel button will
    //    ///     be enabled only while the login operation can be cancelled
    //    /// </summary>
    //    private void BindUIToOperation(LoginOperation loginOperation)
    //    {
    //        Binding isEnabledBinding = loginOperation.CreateOneWayBinding("IsComplete");
    //        Binding isActivityActiveBinding = loginOperation.CreateOneWayBinding("IsComplete", new NotOperatorValueConverter());
    //        Binding isCancelEnabledBinding = loginOperation.CreateOneWayBinding("CanCancel");

    //        this.activity.SetBinding(Activity.IsActiveProperty, isActivityActiveBinding);
    //        this.loginButton.SetBinding(Control.IsEnabledProperty, isEnabledBinding);
    //        this.loginForm.SetBinding(Control.IsEnabledProperty, isEnabledBinding);
    //        //this.registerNow.SetBinding(Control.IsEnabledProperty, isEnabledBinding);

    //        // The correct binding for the cancel button would be
    //        // IsEnabled = loginOperation.CanCancel || loginOperation.IsComplete
    //        //
    //        // However, this is a binding to two distinct properties which is
    //        // not supported, so we bind solely to CanCancel and remove the binding
    //        // once the operation is complete with a callback
    //        this.loginCancel.SetBinding(Control.IsEnabledProperty, isCancelEnabledBinding);
    //        loginOperation.Completed += this.ReEnableCancelButton;
    //    }

    //    protected override void OnNavigatedTo(NavigationEventArgs e)
    //    {
    //    }

    //}
}