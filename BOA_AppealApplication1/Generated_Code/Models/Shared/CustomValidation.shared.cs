﻿using System;
using System.ComponentModel.DataAnnotations;
//using BOA_AppealApplication1.Web.Models;
//using System.ComponentModel;

/// <summary>    
/// Custom validators for the BOA application
/// </summary> 
namespace BOA_AppealApplication1.Web.Models.Shared
{
    public static class CustomValidators
    {

        public static ValidationResult ConfirmPassword(string password, ValidationContext validationContext)
        {

            if (password == "/**NO***/")
            {
                return new ValidationResult(
                        "Passwords don't match",
                        new[] { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }



    }
}