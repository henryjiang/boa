﻿namespace BOA_AppealApplication1
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Navigation;
    using BOA_AppealApplication1.LoginUI;
    using System;
    using System.ServiceModel.DomainServices.Client.ApplicationServices;
    using System.ComponentModel.DataAnnotations;
    using BOA_AppealApplication1.Web.Services;
    using BOA_AppealApplication1.Web;
    using System.ServiceModel.DomainServices.Client;
    using System.Linq;

    using System.Collections.Generic;
    using System.Net;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using System.Text.RegularExpressions;

    /// <summary>
    /// <see cref="UserControl"/> class providing the main UI for the application.
    /// </summary>
    public partial class MainPage : UserControl
    {
        private static MainPage MainPageObject;
        /// <summary>
        /// Creates a new <see cref="MainPage"/> instance.
        /// </summary>
        /// 

        public MainPage()
        {
            InitializeComponent();
            MainPageObject = this;
            this.loginContainer.Child = new LoginStatus(this);

        }

        /// <summary>
        /// After the Frame navigates, ensure the <see cref="HyperlinkButton"/> representing the current page is selected
        /// </summary>
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            bool loggedIn = WebContext.Current.User.IsAuthenticated;

            string x = WebContext.Current.User.DisplayName;
            //if (e.Uri.ToString() != "/UserLogin" && !loggedIn)
            if (!loggedIn)
            {
                this.ContentFrame.Source = new Uri(String.Format("/UserLogin"), UriKind.Relative);
            }
            else if (e.Uri.ToString() == "/UserLogin" && loggedIn)
            {
                this.ContentFrame.Source = new Uri(String.Format("/Home"), UriKind.Relative);
            }
            else
            {
                foreach (UIElement child in LinksStackPanel.Children)
                {
                    HyperlinkButton hb = child as HyperlinkButton;
                    if (hb != null && hb.NavigateUri != null)
                    {
                        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
                        {
                            VisualStateManager.GoToState(hb, "ActiveLink", true);
                        }
                        else
                        {
                            VisualStateManager.GoToState(hb, "InactiveLink", true);
                        }
                    }
                }
            }
        }





        /// <summary>
        /// If an error occurs during navigation, show an error window
        /// </summary>
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            ErrorWindow.CreateNew(e.Exception);
        }

        public bool NavigateTo(string uriString, UriKind uriKind)
        {
            if (Uri.IsWellFormedUriString(uriString, uriKind))
            {
                return MainPageObject.ContentFrame.Navigate(new Uri(uriString, uriKind));
            }
            else return false;
        }
    }
}