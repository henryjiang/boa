﻿namespace BOA_AppealApplication1
{
    using System;
    using System.Runtime.Serialization;
    using System.ServiceModel.DomainServices.Client.ApplicationServices;
    using System.Windows;
    using System.Windows.Browser;
    using System.ServiceModel.DomainServices.Client;
    using BOA_AppealApplication1.Controls;

    /// <summary>
    /// Main <see cref="Application"/> class.
    /// </summary>
    public partial class App : Application
    {
        private BusyIndicator busyIndicator;

        /// <summary>
        /// Creates a new <see cref="App"/> instance.
        /// </summary>
        public App()
        {
            InitializeComponent();
            
            // Create a WebContext and add it to the ApplicationLifetimeObjects
            // collection.  This will then be available as WebContext.Current.
            WebContext webContext = new WebContext();
            webContext.Authentication = new FormsAuthentication();
            //webContext.Authentication = new WindowsAuthentication();
            this.ApplicationLifetimeObjects.Add(webContext);
            bool loggedIn = WebContext.Current.User.IsAuthenticated;
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // This will enable you to bind controls in XAML files to WebContext.Current
            // properties
            this.Resources.Add("WebContext", WebContext.Current);

            // This will automatically authenticate a user when using windows authentication
            // or when the user chose "Keep me signed in" on a previous login attempt
            WebContext.Current.Authentication.LoadUser(this.Application_UserLoaded, null);

            //// Show some UI to the user while LoadUser is in progress
            this.InitializeRootVisual();
            System.Windows.Controls.ScrollViewer sV = new System.Windows.Controls.ScrollViewer();

            ////sV.TabNavigation = System.Windows.Input.KeyboardNavigationMode.Cycle;
            sV.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
            sV.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;

            sV.Content = new MainPage();
            this.RootVisual = sV;
        }

        /// <summary>
        /// Invoked when the <see cref="LoadUserOperation"/> completes. Use this
        /// event handler to switch from the "loading UI" you created in
        /// <see cref="InitializeRootVisual"/> to the "application UI"
        /// </summary>
        private void Application_UserLoaded(LoadUserOperation operation)
        {
           
        }

        /// <summary>
        /// Initializes the <see cref="Application.RootVisual"/> property. The
        /// initial UI will be displayed before the LoadUser operation has completed
        /// (The LoadUser operation will cause user to be logged automatically if
        /// using windows authentication or if the user had selected the "keep
        /// me signed in" option on a previous login).
        /// </summary>
        protected virtual void InitializeRootVisual()
        {
            this.busyIndicator = new BusyIndicator();
            this.busyIndicator.Content = new MainPage();
            this.busyIndicator.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            this.busyIndicator.VerticalContentAlignment = VerticalAlignment.Stretch;

            //this.RootVisual = this.busyIndicator;
        }

        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // a ChildWindow control.
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                ErrorWindow.CreateNew(e.ExceptionObject);
            }

            // redirect to login on exceptions when user is not logged in
            var domainException = e.ExceptionObject as DomainOperationException;
            if (domainException != null && !WebContext.Current.User.IsAuthenticated)
            { // probably a server-side timeout has occurred.  
                e.Handled = true;
                HtmlPage.Document.Submit(); // redirect to login page
            }

        }

        private string _sessionRequestType;
        public string SessionRequestType
        {
            get
            {
                return _sessionRequestType;
            }
            set
            {
                _sessionRequestType = value;
            }
        }
        private int _sessionApplicationID;

        public int SessionApplicationID
        {
            get
            {
                return _sessionApplicationID;
            }

            set
            {
                _sessionApplicationID = value;
            }
        }

        private int _sessionCurrentUserAccessID;

        public int SessionCurrentUserAccessID
        {
            get
            {
                return _sessionCurrentUserAccessID;
            }

            set
            {
                _sessionCurrentUserAccessID = value;
            }
        }

        private string _sessionStatus;

        public string SessionStatus
        {
            get
            {
                return _sessionStatus;
            }

            set
            {
                _sessionStatus = value;
            }
        }

        private string _sessionCaseType;

        public string SessionCaseType
        {
            get
            {
                return _sessionCaseType;
            }

            set
            {
                _sessionCaseType = value;
            }
        }

        private DateTime _sessionHearingDate;

        public DateTime SessionHearingDate
        {
            get
            {
                return _sessionHearingDate;
            }

            set
            {
                _sessionHearingDate = value;
            }
        }

        private string _sessionAgencyID;

        public string SessionAgencyID
        {
            get
            {
                return _sessionAgencyID;
            }

            set
            {
                _sessionAgencyID = value;
            }
        }

        private string _sessionMainUserID;

        public string SessionMainUserID
        {
            get
            {
                return _sessionMainUserID;
            }

            set
            {
                _sessionMainUserID = value;
            }
        }

        private string _sessionBoardMemberID;

        public string SessionBoardMemberID
        {
            get
            {
                return _sessionBoardMemberID;
            }

            set
            {
                _sessionBoardMemberID = value;
            }
        }
        private string _sessionPermitType;
        public string SessionPermitType
        {
            get
            {
                return _sessionPermitType;
            }

            set
            {
                _sessionPermitType = value;
            }
        }

        private string _sessionSortHeader;
        public string SessionSortHeader
        {
            get
            {
                return _sessionSortHeader;
            }

            set
            {
                _sessionSortHeader = value;
            }
        }
        private System.ComponentModel.ListSortDirection _sessionSortOrder;
        public System.ComponentModel.ListSortDirection SessionSortOrder
        {
            get
            {
                return _sessionSortOrder;
            }

            set
            {
                _sessionSortOrder = value;
            }
        }
        private int _sessionSortPageIndex;
        public int SessionSortPageIndex
        {
            get
            {
                return _sessionSortPageIndex;
            }

            set
            {
                _sessionSortPageIndex = value;
            }
        }
        private object _sessionObject;

        public object SessionObject
        {
            get
            {
                return _sessionObject;
            }

            set
            {
                _sessionObject = value;
            }
        }
    }
}