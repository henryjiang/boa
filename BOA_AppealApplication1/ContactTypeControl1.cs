﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace BOA_AppealApplication1
{
    public class ContactTypeControl1 : ObservableCollection<ObservableContactType>
    {
        public ContactTypeControl1() : base()
        {
            Add(new ObservableContactType("Co-Owner"));
            Add(new ObservableContactType("Co-Appellant"));

        }
        
    }


    public class ObservableContactType
    {
        private string contactType;

        public ObservableContactType(string inType)
        {
            this.ContactType = inType;
        }

        public string ContactType
        {
            get { return this.contactType; }
            set { this.contactType = value; }
        }
    }
}