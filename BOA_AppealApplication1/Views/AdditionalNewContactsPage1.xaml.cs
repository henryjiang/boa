﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.Windows.Printing;


namespace BOA_AppealApplication1.Views
{
    public partial class AdditionalNewContactsPage1 : Page
    {
        public tblNameField newContact { get; set; }
        RemoveContactChildWindow1 removeContactWindow;

        //Initiating all objects
        App app = (App)Application.Current;
        //OwnerDomainService1 context = new OwnerDomainService1();
        OwnerDomainService1 _OwnerDomainServices1;

        public AdditionalNewContactsPage1()
        {
            InitializeComponent();

            //this.othersNamesGrid.ItemsSource = context.tblNameFields;
            this.appellantParameter.Value = app.SessionApplicationID;
            newContact = new tblNameField();
            this.newContactDataForm.CurrentItem = newContact;
            this.Title = ApplicationStrings.AdditionalContactPageTitle;
            this.newContactDataForm.ValidatingItem +=new EventHandler<System.ComponentModel.CancelEventArgs>(newContactDataForm_ValidatingItem);
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //ApplicationDomainService1 context = new ApplicationDomainService1();
            //this.applicationGrid.ItemsSource = context.tblApplications;
            //context.Load(context.GetTblApplicationsQuery());
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.newContactDataForm.CurrentItem != null)
            {
                newContact.NameDescription = newContact.FirstName + newContact.MiddleName + newContact.LastName;
                if (this.newContactDataForm.ValidateItem())
                { 
                    // NameDescription is required.
                    newContact.ApplicationID = app.SessionApplicationID;
                    //newContact.NameDescription = newContact.FirstName + newContact.MiddleName + newContact.LastName;
                    newContactDataForm.CommitEdit();
                    _OwnerDomainServices1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                    try
                    {
                        _OwnerDomainServices1.tblNameFields.Add(newContact);
                    }
                    catch(System.InvalidOperationException ex1) {
                        MessageBox.Show("Entry already exist, please try a different type.", "Entry already exist, please try a different type.", MessageBoxButton.OK);
                    }
                    AppellantDataSource1.SubmitChanges();
                    //context.Load(context.GetTblOthersNameFieldsQuery(app.SessionApplicationID));
                    newContact = new tblNameField();
                    this.newContactDataForm.CancelEdit();
                    this.newContactDataForm.CurrentItem = newContact;
                }
            }
        }
        private void newContactDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.newContactDataForm.ValidationSummary.Errors.Clear();
            if (this.typeSelection.SelectedValue == null)
                this.newContactDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Contact Type is required!", "Contact Type", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("typeSelection"), this.newContactDataForm.CurrentItem));
            /*if (this.FirstName.Text == "")
                this.newContactDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("First Name is required!", "First Name", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("FirstName"), this.newContactDataForm.CurrentItem));
            if (this.LastName.Text == "")
                this.newContactDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Last Name is required!", "Last Name", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("LastName"), this.newContactDataForm.CurrentItem));
             */
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            newContactDataForm.CancelEdit();
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
            app.SessionApplicationID = 0;
        }

        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.newContactDataForm.CurrentItem != null)
            {
                if (this.newContactDataForm.IsItemValid)
                {
                    newContactDataForm.CommitEdit();
                    AppellantDataSource1.SubmitChanges();
                }
            }
            this.NavigationService.Navigate(new Uri(String.Format("/EditTransactionPage1"), UriKind.Relative));
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            this.AppellantDataSource1.RejectChanges();
            if (this.newContactDataForm.CurrentItem != null)
            {
                //tblNameField contact = new tblNameField();
                //this.newContactDataForm.CancelEdit();
                //this.newContactDataForm.CurrentItem = contact;
                this.newContactDataForm.CancelEdit();
                //this.newContactDataForm.BeginEdit();
            }
        }

        private void RemovedButton_Click(object sender, RoutedEventArgs e)
        {
            //app.SessionObject = (object)(this.othersNamesGrid.SelectedItem);
            if (this.othersNamesGrid.SelectedItem != null)
            {
                app.SessionObject = (object)(this.othersNamesGrid.SelectedItem);
                this.removeContactWindow = new RemoveContactChildWindow1();
                this.removeContactWindow.Closed += new EventHandler(RemoveContactChildWindow1_Closed);
                this.removeContactWindow.Show();
            }
        }
        private void goBackButton_Click(object sender, RoutedEventArgs e)
        {
            if(app.SessionRequestType == "REG")
                this.NavigationService.Navigate(new Uri(String.Format("/EditAppeal"), UriKind.Relative));
            else
                this.NavigationService.Navigate(new Uri(String.Format("/EditAppealJR"), UriKind.Relative));
        }
        
        void RemoveContactChildWindow1_Closed(object sender, EventArgs e)
        {
            if (this.removeContactWindow.DialogResult.Value && this.othersNamesGrid.SelectedItem != null)
            {
                tblNameField deleteContact = this.othersNamesGrid.SelectedItem as tblNameField;
                _OwnerDomainServices1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                _OwnerDomainServices1.tblNameFields.Remove(deleteContact);
                _OwnerDomainServices1.SubmitChanges();

                this.CancelEdit.Visibility = System.Windows.Visibility.Collapsed;
                this.SaveEdit.Visibility = System.Windows.Visibility.Collapsed;
                this.editContactDataForm.Visibility = System.Windows.Visibility.Collapsed;
                this.NewButton.Visibility = System.Windows.Visibility.Visible;
                this.AddButton.Visibility = System.Windows.Visibility.Visible;
                this.newContactDataForm.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void AppellantDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("No Results!", "No Results!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            
            if (this.typeSelection2.SelectedValue != null && this.typeSelection2.SelectedValue.ToString() == "Other")
            {
                this.otherType2.UpdateLayout();
                this.otherType2.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.otherType2.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void AppellantDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBoxResult mbr =  MessageBox.Show("No Data Changes!", "No Data Changes!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
                //error, give new edit form
                if (mbr.ToString() == MessageBoxResult.OK.ToString())
                {
                    if (this.newContactDataForm.CurrentItem != null)
                    {
                        //tblNameField contact = new tblNameField();
                        this.newContactDataForm.CancelEdit();
                        this.AppellantDataSource1.RejectChanges();
                        //this.newContactDataForm.CurrentItem = contact;

                        //this.newContactDataForm.BeginEdit();
                    }
                }
            }
            else
            {
                if(this.AppellantDataSource1.CanLoad)
                    this.AppellantDataSource1.Load();
            }
        }

        private void othersNamesGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void othersNamesGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Click to edit the current item, detects left mouse click
            //Additional detects can be: left mouse up, mouseenter.
            if (this.othersNamesGrid.SelectedItem != null)
            {
                //setup new set of buttons
                this.NewButton.Visibility = System.Windows.Visibility.Collapsed;
                this.AddButton.Visibility = System.Windows.Visibility.Collapsed;
                this.newContactDataForm.Visibility = System.Windows.Visibility.Collapsed;
                this.newContactDataForm.CancelEdit();
                this.newContactDataForm.ValidationSummary.Errors.Clear();
                
                //reject dirty editing
                this.editContactDataForm.CancelEdit();
                if( this.AppellantDataSource1.HasChanges )
                    this.AppellantDataSource1.RejectChanges();
                //this.appellantParameter.Value = ((tblNameField)this.othersNamesGrid.SelectedItem).ApplicationID;
                //this.AppellantDataSource1.Load();

                //set item source
                if(this.typeSelection2.ItemsSource == null)
                    this.typeSelection2.ItemsSource = new ContactTypeProvider().ContactTypeList.ToList();

                //setup edit form
                this.CancelEdit.Visibility = System.Windows.Visibility.Visible;
                this.SaveEdit.Visibility = System.Windows.Visibility.Visible;
                this.editContactDataForm.Visibility = System.Windows.Visibility.Visible;
                this.typeSelection2.UpdateLayout();
            }
        }

        private void CancelEdit_Click(object sender, RoutedEventArgs e)
        {
            this.AppellantDataSource1.RejectChanges();

            this.CancelEdit.Visibility = System.Windows.Visibility.Collapsed;
            this.SaveEdit.Visibility = System.Windows.Visibility.Collapsed;
            this.editContactDataForm.Visibility = System.Windows.Visibility.Collapsed;
            this.NewButton.Visibility = System.Windows.Visibility.Visible;
            this.AddButton.Visibility = System.Windows.Visibility.Visible;
            this.newContactDataForm.Visibility = System.Windows.Visibility.Visible;

            this.newContactDataForm.CancelEdit();
            this.editContactDataForm.CancelEdit();
        }

        private void SaveEdit_Click(object sender, RoutedEventArgs e)
        {
            if (this.editContactDataForm.IsItemValid)
            {
                this.AppellantDataSource1.SubmitChanges();

                this.CancelEdit.Visibility = System.Windows.Visibility.Collapsed;
                this.SaveEdit.Visibility = System.Windows.Visibility.Collapsed;
                this.editContactDataForm.Visibility = System.Windows.Visibility.Collapsed;
                this.NewButton.Visibility = System.Windows.Visibility.Visible;
                this.AddButton.Visibility = System.Windows.Visibility.Visible;
                this.newContactDataForm.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PrintContacts printWindow = new PrintContacts();
            printWindow.Show();
        }

        private void typeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.typeSelection.SelectedValue != null)
            {
                if (this.typeSelection.SelectedValue.ToString() == "Other")
                {
                    //this.otherType.UpdateLayout();
                    this.otherType.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    this.otherType.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void typeSelection2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.typeSelection2.SelectedValue != null)
            {
                if (this.typeSelection2.SelectedValue.ToString() == "Other")
                {
                    this.otherType2.UpdateLayout();
                    this.otherType2.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    this.otherType2.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
    }

    public class ContactTypeProvider
    {
        public List<string> ContactTypeList
        {
            get
            {
                return new List<string> { 
                    "Co-Respondent", 
                    "Co-Appellant",
                    "Appellant's Agent", 
                    "Appellant's Attorney", 
                    "Determination Holder's Agent", 
                    "Determination Holder's Attorney",
                    "Other" };
            }
        }
    }
}