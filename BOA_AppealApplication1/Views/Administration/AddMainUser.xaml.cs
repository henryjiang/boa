﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.ComponentModel.DataAnnotations;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class AddMainUser : ChildWindow
    {
        private tblMainUser newMainUser { get; set; }
        private DomainDataSource mainUserDataSource;
        private MainUserAdminDomainContext mainUserDomainService;

        public AddMainUser(ref DomainDataSource dds)
        {
            InitializeComponent();

            this.Title = ApplicationStrings.MainUserAdminPageTitle;
            this.mainUserDataSource = dds;

            //Create a new entity for a new user
            newMainUser = new tblMainUser();
            newMainUser.AccessGroupID = 1;
            newMainUser.IsActive = "Active";
            this.newMainUserForm.CurrentItem = newMainUser;
            this.newMainUserForm.BeginEdit();

            this.confirmPassword.GotFocus += new RoutedEventHandler(confirmPassword_GetFocus);
            this.Password.GotFocus += new RoutedEventHandler(Password_GetFocus);
            this.GotFocus += new RoutedEventHandler(sth_GotFocus);
            this.KeyDown += new KeyEventHandler(AddMainUser_KeyDown);

            mainUserDomainService = (MainUserAdminDomainContext)this.mainUserDataSource.DomainContext;
        }

        void sth_GotFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(() => { UserID.Focus(); });
            this.GotFocus -= new RoutedEventHandler(sth_GotFocus);
        }

        void AddMainUser_KeyDown( object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter) SaveUser();
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            SaveUser();
        }

        private void SaveUser()
        {
            String pwd = ((PasswordBox)this.newMainUserForm.FindName("Password")).Password;
            String confirmPwd = ((PasswordBox)this.newMainUserForm.FindName("confirmPassword")).Password;

            //Set value used in custom validation
            if (pwd != confirmPwd)
            {
                ((PasswordBox)this.newMainUserForm.FindName("Password")).Password = "/**NO***/";
            }

            //Trigger Silverlight form validation
            this.newMainUserForm.ValidateItem();

            //get collection of validation errors
            ICollection<ValidationResult> validationResults = this.newMainUser.ValidationErrors;

            if (validationResults.Count == 0)
            {
                SaveNewUser();
            }
            else
            {

                ErroSourceSetFocus(validationResults);
                ((PasswordBox)this.newMainUserForm.FindName("Password")).Password = pwd;
                ((PasswordBox)this.newMainUserForm.FindName("confirmPassword")).Password = confirmPwd;
            }
        }


        private void mainUserAdded(SubmitOperation so)
        {
            
            if (so.HasError)
            {
               so.MarkErrorAsHandled();
               ErroSourceSetFocus(so.EntitiesInError.First().ValidationErrors);
            }

            if (!so.HasError && this.mainUserDataSource.CanLoad)
            {
                this.mainUserDataSource.Load();
                this.DialogResult = true;
            }
        }

        private void SaveNewUser()
        {
            mainUserDomainService.tblMainUsers.Add(newMainUser);
            this.newMainUserForm.CommitEdit();
            this.mainUserDataSource.DomainContext.SubmitChanges(mainUserAdded, null);
        }


        private void ErroSourceSetFocus(ICollection<ValidationResult> validationResults)
        {
            if (validationResults.Count == 0) return;

            var controlNames = validationResults.Last().MemberNames;

            if (controlNames != null)
            {
                string controlName = controlNames.First().ToString();
                if (controlName == "Password")
                {
                    PasswordBox pwdBox = (PasswordBox)this.newMainUserForm.FindName(controlName);
                    pwdBox.Focus();
                }
                else
                {
                    TextBox txtBox = (TextBox)this.newMainUserForm.FindName(controlName);
                    txtBox.Focus();
                }
            }
        
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        void confirmPassword_GetFocus(object sender, RoutedEventArgs e)
        {
            this.confirmPassword.SelectAll();
        }

        void Password_GetFocus(object sender, RoutedEventArgs e)
        {
            this.Password.SelectAll();
        }
    }
}

