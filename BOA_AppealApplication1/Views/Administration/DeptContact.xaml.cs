﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class DeptContact : ChildWindow
    {
        //Declare a new variable to hold new department information.
        public tblDeptContact newContact { get; set; }

        public DeptContact()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.DepartmentAdminPage1Title;

            //Create a new dataform for a new Department.
            newContact = new tblDeptContact();
            this.newDeptContactForm.CurrentItem = newContact;

            this.deptID.LostFocus += new RoutedEventHandler(deptID_LostFocus);
        }

        void deptID_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.deptID.Text != "")
            {
                this.deptID.Text = this.deptID.Text.ToUpper();
                checkIFDepartmentExist(this.deptID.Text.ToUpper());
            }
        }

        //Create a new department.
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            
            this.newDeptContactForm.CommitEdit();
            departmentDomainService.tblBoardMembers.Add(newContact);
            departmentDomainService.SubmitChanges();
       
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //Converting to Upper Case on the fly.
        private void deptID_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.deptID.Text = this.deptID.Text.ToUpper();
        }

        //Check the recordCount from the Database.
        private void checkIFDepartmentExist(string agencyIDIN)
        {
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            InvokeOperation op = departmentDomainService.checkDepartment(agencyIDIN);
            op.Completed += new EventHandler(ap_Completed);
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Check the recordCount from the Database.
            if (op.Value.Equals(0))
            {
                this.contactExists.Foreground = new SolidColorBrush(Colors.Green);
                this.contactExists.Text = "Department ID Is OK to Use.";
            }
            else
            {
                this.contactExists.Foreground = new SolidColorBrush(Colors.Red);
                this.contactExists.Text = "Department ID Is Already In Use!";
            }
        }
    }
}