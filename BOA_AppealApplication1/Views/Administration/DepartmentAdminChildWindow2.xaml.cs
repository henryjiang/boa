﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class DepartmentAdminChildWindow2 : ChildWindow
    {
        //Initiate the session variable.
        App app = (App)Application.Current;
        VoteNTransactionDomainService1 _VoteNTransactionDomainService1;
        DomainDataSource dds = new DomainDataSource();

        public DepartmentAdminChildWindow2(ref DomainDataSource dds)
        {
            InitializeComponent();

            this.departmentDataSourceParameter.Value = app.SessionAgencyID;
            checkDepartmentUsage();

            this.dds = dds;
        }

        private void checkDepartmentUsage()
        {
            ApplicationDomainService1 _ApplicationDomainService1 = new ApplicationDomainService1();
            InvokeOperation op = _ApplicationDomainService1.GetApplicationsCountByAgencyID(app.SessionAgencyID);
            op.Completed += new EventHandler(op_Completed);
        }

        void op_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            if (Convert.ToInt16(op.Value) > 0)
            {
                this.OKButton.Visibility = Visibility.Collapsed;
                this.checkMessageTXT.Text = "This Department Is Being Used And Not Allow To Be Removed!";
                this.checkMessageTXT.Foreground = new SolidColorBrush(Colors.Red);
            }
            else
            { 
                this.OKButton.Visibility = Visibility.Visible;
                this.checkMessageTXT.Text = "Retire the following Department?";
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            tblDepartment deleteDepartment = this.removeDeptForm.CurrentItem as tblDepartment;
            _VoteNTransactionDomainService1 = (VoteNTransactionDomainService1)(departmentDataSource1.DomainContext);
            deleteDepartment.Status = "I";
            //_VoteNTransactionDomainService1.tblDepartments.Remove(deleteDepartment);
            _VoteNTransactionDomainService1.SubmitChanges(removeSubmitted, null);
            this.DialogResult = true;
        }

        void removeSubmitted(SubmitOperation so)
        {
            if (!so.HasError && this.dds.CanLoad)
                this.dds.Load();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

