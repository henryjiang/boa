﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;


namespace BOA_AppealApplication1.Views.Administration
{

    public partial class RetireMainUser : ChildWindow
    {
        MainUserAdminDomainContext _MainUserAdminDomainContext;
        DomainDataSource dds = new DomainDataSource();
        tblMainUser _deletedtblMainUser;

        public RetireMainUser(ref DomainDataSource dds)
        {
            InitializeComponent();

            this.dds = dds;
            this.retireMainUserForm.CurrentItem = dds.DataView.CurrentItem;
            _deletedtblMainUser = (tblMainUser)this.retireMainUserForm.CurrentItem;
            SetCheckMessageTXT();
        }

        private void SetCheckMessageTXT()
        {

            switch (_deletedtblMainUser.IsActive)
            {
                case "Active": { this.retireMainUserForm.Header = "Retire the following user?"; break; }
                case "Inactive": { this.retireMainUserForm.Header = "Reactivate the following user?"; break; }
            }
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _MainUserAdminDomainContext = (MainUserAdminDomainContext)(dds.DomainContext);

            switch (_deletedtblMainUser.IsActive)
            {
                case "Active": { _deletedtblMainUser.IsActive = "Inactive"; break; }
                case "Inactive": { _deletedtblMainUser.IsActive = "Active"; break; }
            }
           
            _MainUserAdminDomainContext.SubmitChanges(removeSubmitted, null);
            this.DialogResult = true;
        }

        void removeSubmitted(SubmitOperation so)
        {
            if (!so.HasError && this.dds.CanLoad)
                this.dds.Load();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

