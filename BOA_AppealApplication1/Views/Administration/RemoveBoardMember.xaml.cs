﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class RemoveBoardMember : ChildWindow
    {
        //Initiate the session variable.
        App app = (App)Application.Current;
        VoteNTransactionDomainService1 _VoteNTransactionDomainService1;
        DomainDataSource _boardmemberds;

        public RemoveBoardMember(ref DomainDataSource boardmemberds)
        {
            InitializeComponent();

            this.boardMemberDataSourceParameter.Value = ApplicationStrings.AgencyFor;
            this.boardMemberDataSourceParameter1.Value = app.SessionBoardMemberID;
            //checkDepartmentUsage();

            this.OKButton.Visibility = Visibility.Visible;
            this.checkMessageTXT.Text = "Retire the following Board Member?";

            this._boardmemberds = boardmemberds;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            tblBoardMember deleteDepartment = this.removeDeptForm.CurrentItem as tblBoardMember;
            _VoteNTransactionDomainService1 = (VoteNTransactionDomainService1)(boardMemberDataSource.DomainContext);
            deleteDepartment.Status = "I";
            _VoteNTransactionDomainService1.SubmitChanges(removeMember, null);
            this.DialogResult = true;
        }

        private void removeMember(SubmitOperation so)
        {
            if (!so.HasError && this._boardmemberds.CanLoad)
            {

                this._boardmemberds.Load();
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

