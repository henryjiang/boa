﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class DeletePermitType : ChildWindow
    {
        //Declare a new variable to hold new department information.
        public tblPermitType permitType { get; set; }
        ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
        DomainDataSource _permitType = new DomainDataSource();

        App app = (App)Application.Current;

        public DeletePermitType(ref DomainDataSource pdatasource)
        {
            InitializeComponent();

            this.permitTypeDataSourceParameter.Value = app.SessionPermitType;
            this.permitTypeDataSourceParameter1.Value = ApplicationStrings.AgencyFor;

            this._permitType = pdatasource;
        }

        private void OKRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            this.permitType = this.removePermitTypeForm.CurrentItem as tblPermitType;
            this.removePermitTypeForm.CommitEdit(true);
            if (this.permitType != null)
            {
                appDomainService = (ApplicationDomainService1)this.permitTypeDataSource.DomainContext;
                appDomainService.tblPermitTypes.Remove(this.permitType);
                appDomainService.SubmitChanges(addedPermit, null);
                this.DialogResult = true;
            }
        }

        private void addedPermit(SubmitOperation so)
        {
            if (!so.HasError)
            {
                while (this.appDomainService.HasChanges)
                    ;
                if(this._permitType.CanLoad)
                    this._permitType.Load();
            }
        }

        private void CancelRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}