﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class DepartmentAdminChildWindow1 : ChildWindow
    {
        //Declare a new variable to hold new department information.
        public tblDepartment newDepartment { get; set; }
        public DomainDataSource departmentDataSource1;

        public DepartmentAdminChildWindow1(ref DomainDataSource dds)
        {
            InitializeComponent();
            this.Title = ApplicationStrings.DepartmentAdminPage1Title;
            this.departmentDataSource1 = dds;

            //Create a new dataform for a new Department.
            newDepartment = new tblDepartment();
            newDepartment.Status = "A";
            this.newDeptForm.CurrentItem = newDepartment;
            this.newDeptForm.BeginEdit();

            this.deptName.LostFocus += new RoutedEventHandler(deptName_LostFocus);
            this.deptID.LostFocus += new RoutedEventHandler(deptID_LostFocus);

            this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        void deptID_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.deptID.Text != "")
            {
                this.deptID.Text = this.deptID.Text.ToUpper();
                checkIFDepartmentExist(this.deptID.Text.ToUpper());
            }
        }

        void deptName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.deptName.Text != "")
            {
                this.deptName.Text = this.deptName.Text.ToUpper();
                this.OKButton.Visibility = System.Windows.Visibility.Visible;
            }
        }
        void deptNameKeyUp(object sender, KeyEventArgs e)
        {
            if (this.deptName.Text != "")
                this.OKButton.Visibility = System.Windows.Visibility.Visible;
            else
                this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
        }
        //Create a new department.
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 departmentDomainService = (VoteNTransactionDomainService1)this.departmentDataSource1.DomainContext;
            newDepartment.Related_Agency = ApplicationStrings.AgencyFor;
            newDepartment.UserID = WebContext.Current.User.DisplayName.ToString();
            this.newDeptForm.CommitEdit();
            departmentDomainService.tblDepartments.Add(newDepartment);
            this.departmentDataSource1.DomainContext.SubmitChanges(deptAdded, null);

            this.DialogResult = true;
        }

        private void deptAdded(SubmitOperation so)
        {
            if (!so.HasError)
            {
                VoteNTransactionDomainService1 departmentDomainService = (VoteNTransactionDomainService1)this.departmentDataSource1.DomainContext;
                tblDeptContact deptContact = new tblDeptContact();
                deptContact.AgencyID = this.deptID.Text;
                deptContact.Related_Agency = ApplicationStrings.AgencyFor;

                departmentDomainService.tblDeptContacts.Add(deptContact);
                departmentDomainService.SubmitChanges(deptContactAdded, null);
            }
        }

        private void deptContactAdded(SubmitOperation so)
        {
            if (!so.HasError)
            {
                this.departmentDataSource1.Load();
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //Check the recordCount from the Database.
        private void checkIFDepartmentExist(string agencyIDIN)
        {
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            InvokeOperation op = departmentDomainService.checkDepartment(agencyIDIN);
            op.Completed += new EventHandler(ap_Completed);
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Check the recordCount from the Database.
            if (op.Value.Equals(0))
            {
                this.agencyStatus.Foreground = new SolidColorBrush(Colors.Green);
                this.agencyStatus.Text = "Department ID Is OK to Use.";
                if(this.deptName.Text != "")
                    this.OKButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.agencyStatus.Foreground = new SolidColorBrush(Colors.Red);
                this.agencyStatus.Text = "Department ID Is Already In Use!";
                this.OKButton.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

    }
}