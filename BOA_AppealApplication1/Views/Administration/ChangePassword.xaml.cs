﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.ComponentModel.DataAnnotations;

namespace BOA_AppealApplication1.Views.Administration
{

    public partial class ChangePassword : ChildWindow
    {
        ///<summary>
        ///This window allows the user to change anyones password
        ///if they have AccessGroup1. If they do not, they can 
        ///only change their own password
        ///</summary>

        public tblMainUser emptyUser { get; set; }
        private TextBox _userStatusMsgs; 

        App app = (App)Application.Current;
        MainUserAdminDomainContext userDC = new MainUserAdminDomainContext();

        public ChangePassword(ref TextBox userStatusMsgs)
        {

            InitializeComponent();
            this.confirmPassword.GotFocus += new RoutedEventHandler(confirmPassword_GetFocus);
            this.Password.GotFocus += new RoutedEventHandler(Password_GetFocus);
            this.GotFocus += new RoutedEventHandler(ChildWindow_GotFocus);
            this.KeyDown += new KeyEventHandler(AddMainUser_KeyDown);

            _userStatusMsgs = userStatusMsgs;

            emptyUser = new tblMainUser();
            changePasswordForm.CurrentItem = emptyUser;

            //Get Current User
            String currentUserName = WebContext.Current.User.DisplayName;
            EntityQuery<tblMainUser> query =
                from u in userDC.GetTblMainUserByUserIdQuery(currentUserName)
                select u;
            LoadOperation<tblMainUser> loadOp = userDC.Load(query, currentUserLoaded, null);

        }

        void ChildWindow_GotFocus(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(() => { Password.Focus(); });
            this.GotFocus -= new RoutedEventHandler(ChildWindow_GotFocus);
        }

        void AddMainUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) SavePassword();
        }

        void currentUserLoaded(LoadOperation so)
        {
            EntitySet<tblMainUser> currentUser = userDC.tblMainUsers;

            Int32  userID = currentUser.First().MainUserID;
            Int32 accessGroup = (Int32)currentUser.First().AccessGroupID;

            if (accessGroup != 10) 
            {
                //insufficient rights to change other user's passwords. Edit logged in user's password
                
                this.userNameLabel.Text = currentUser.First().UserID;
            }
            else
            {
                //get user selected on the adming grid
                userDC.tblMainUsers.Clear();
                Int32 mainUserId = Convert.ToInt32(app.SessionMainUserID);
                EntityQuery<tblMainUser> query =
                    from u in userDC.GetTblMainUserQuery(mainUserId)
                    select u;
                LoadOperation<tblMainUser> loadOp = userDC.Load(query, selectedUserLoaded, null);
            }

        }

        void selectedUserLoaded(LoadOperation so)
        {
            EntitySet<tblMainUser> user = userDC.tblMainUsers;
            if (app.SessionMainUserID != user.First().UserID)
            {

                this.userNameLabel.Text = user.First().UserID;
            }
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            SavePassword();
        }

        private void SavePassword()
        {
            String pwd = ((PasswordBox)this.changePasswordForm.FindName("Password")).Password;
            String confirmPwd = ((PasswordBox)this.changePasswordForm.FindName("confirmPassword")).Password;

            //Set value used in custom validation
            if (pwd != confirmPwd)
            {
                ((PasswordBox)this.changePasswordForm.FindName("Password")).Password = "/**NO***/";
            }

            this.changePasswordForm.ValidateItem();
            ICollection<ValidationResult> validationResults = emptyUser.ValidationErrors;

            if (validationResults.Count == 0)
            {
                EntitySet<tblMainUser> users = userDC.tblMainUsers;
                users.First().Password = emptyUser.Password;
                userDC.UpdatePassword(users.First());
                userDC.SubmitChanges(PwdChanged, null);
            }
            else
            {
                ErroSourceSetFocus(validationResults);
                ((PasswordBox)this.changePasswordForm.FindName("Password")).Password = pwd;
                ((PasswordBox)this.changePasswordForm.FindName("confirmPassword")).Password = confirmPwd;
            }
        }

        private void ErroSourceSetFocus(ICollection<ValidationResult> validationResults)
        {
            if (validationResults.Count == 0) return;
            var controlNames = validationResults.Last().MemberNames;

            if (controlNames != null)
            {
                string controlName = controlNames.First().ToString();
                if (controlName == "Password")
                {
                    PasswordBox pwdBox = (PasswordBox)this.changePasswordForm.FindName(controlName);
                    pwdBox.Focus();
                }
            }
        }

        private void PwdChanged(SubmitOperation so)
        {
            _userStatusMsgs.Text = "Password successfully changed";
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        void confirmPassword_GetFocus(object sender, RoutedEventArgs e)
        {
            this.confirmPassword.SelectAll();
        }

        void Password_GetFocus(object sender, RoutedEventArgs e)
        {
            this.Password.SelectAll();
        }

    }
}

