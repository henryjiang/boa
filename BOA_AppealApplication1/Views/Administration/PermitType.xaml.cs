﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class PermitTypeAdminChildWindow : ChildWindow
    {
        //Declare a new variable to hold new department information.
        public tblPermitType permitType { get; set; }
        ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
        DomainDataSource _permitType = new DomainDataSource();

        App app = (App)Application.Current;

        public PermitTypeAdminChildWindow(ref DomainDataSource pdatasource)
        {
            InitializeComponent();

            this.departmentParameter.Value = ApplicationStrings.AgencyFor;
            this.Title = "Add New Permit Type";

            //Create a new dataform for a new Department.
            permitType = new tblPermitType();
            this.newPermitTypeForm.CurrentItem = permitType;
            this.newPermitTypeForm.BeginEdit();

            this.permitTypeName.LostFocus += new RoutedEventHandler(deptID_TextChanged);

            this._permitType = pdatasource;
        }

        private void departmentDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
        }

        private void OKAddButton_Click(object sender, RoutedEventArgs e)
        {
            this.permitType.AgencyID = ApplicationStrings.AgencyFor;

            string temp = "";
            if (this.deptSelection.SelectedItem != null)
                temp = this.deptSelection.SelectedValue.ToString();

            this.permitType.DeptID = temp;
            this.newPermitTypeForm.CommitEdit(true);
            
            appDomainService.tblPermitTypes.Add(this.permitType);
            appDomainService.SubmitChanges(addedPermit, null);

            this.DialogResult = true;
        }

        private void addedPermit(SubmitOperation so)
        {
            if (!so.HasError)
                this._permitType.Load();
        }
        private void CancelAddButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //Converting to Upper Case on the fly.
        private void deptID_TextChanged(object sender, RoutedEventArgs e)
        {
            this.permitTypeName.Text = this.permitTypeName.Text.ToUpper();
            checkIFDepartmentExist(ApplicationStrings.AgencyFor);
        }

        //Check the recordCount from the Database.
        private void checkIFDepartmentExist(string agencyIDIN)
        {
            InvokeOperation op = appDomainService.checkPermityType(this.permitTypeName.Text, agencyIDIN);
            op.Completed += new EventHandler(ap_Completed);
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Check the recordCount from the Database.
            if (op.Value.Equals(0))
            {
                this.permitTypeExists.Foreground = new SolidColorBrush(Colors.Green);
                this.permitTypeExists.Text = "Permit Type Is OK to Use.";
                this.OKAddButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.permitTypeExists.Foreground = new SolidColorBrush(Colors.Red);
                this.permitTypeExists.Text = "Permit Type Is Already In Use!";
                this.OKAddButton.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}