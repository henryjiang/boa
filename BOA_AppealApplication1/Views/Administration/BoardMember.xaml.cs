﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class BoardMemberAdminChildWindow : ChildWindow
    {
        //Declare a new variable to hold new department information.
        public tblBoardMember newBoardMember { get; set; }
        DomainDataSource _boardmemberds;

        public BoardMemberAdminChildWindow(ref DomainDataSource boardmemberds)
        {
            InitializeComponent();
            this.Title = "Add New Board Member";

            //Create a new dataform for a new Department.
            newBoardMember = new tblBoardMember();
            newBoardMember.MiddleName = "";
            newBoardMember.Status = "A";
            this.newBoardMemberForm.CurrentItem = newBoardMember;

            this.MemberID.LostFocus += new RoutedEventHandler(MemberID_LostFocus);

            this._boardmemberds = boardmemberds;
        }

        void MemberID_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.MemberID.Text != "")
            {
                this.MemberID.Text = this.MemberID.Text.ToUpper();
                checkIFDepartmentExist(ApplicationStrings.AgencyFor);
            }
        }

        //Create a new department.
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            newBoardMember.AgencyID = ApplicationStrings.AgencyFor;
            this.newBoardMemberForm.CommitEdit();
            departmentDomainService.tblBoardMembers.Add(newBoardMember);
            departmentDomainService.SubmitChanges(addedBoardMember, null);
            
            this.DialogResult = true;
        }
        private void addedBoardMember(SubmitOperation so)
        {
            if (!so.HasError)
                this._boardmemberds.Load();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        //Check the recordCount from the Database.
        private void checkIFDepartmentExist(string agencyIDIN)
        {
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            InvokeOperation op = departmentDomainService.checkBoardMember(this.MemberID.Text, agencyIDIN);
            op.Completed += new EventHandler(ap_Completed);
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Check the recordCount from the Database.
            if (op.Value.Equals(0))
            {
                this.memberExists.Foreground = new SolidColorBrush(Colors.Green);
                this.memberExists.Text = "Member ID Is OK to Use.";
                this.OKButton.IsEnabled = true;
            }
            else
            {
                this.memberExists.Foreground = new SolidColorBrush(Colors.Red);
                this.memberExists.Text = "Member ID Is Already In Use!";
                this.OKButton.IsEnabled = false;
            }
        }
    }
}