﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Threading;

namespace BOA_AppealApplication1.Views
{
    public partial class NewAppeal : Page
    {
        public tblApplication newApplication { get; set; }
        public tblItemDetail newItemDetail { get; set; }
        public tblNameField newOwner { get; set; }        
        public tblNameField newAppellant { get; set; }
        //public tblDateField newDateEntry { get; set; }

        private string source { get; set; }
        private Boolean isVoteSheetupdated { get; set; }
        private string applicationIDstr { get; set; }
        const string otherDepartmentalAction = "OTHER ACTION";
        const string otherPermitTypeText = "Other Permit Type";
        
        FileDownload fileDownloadWindow;

        //Initiate the session variable.
        App app = (App)Application.Current;
        DateTime tempDate;
        bool hasErrors = false;
        

        VoteNTransactionDomainService1 vtd;

        public NewAppeal()
        {
            InitializeComponent();
            
            //Initialize values for domain service call parameters.
            this.typeParameter.Value = ApplicationStrings.AgencyFor;
            this.actionParameter.Value = ApplicationStrings.AgencyFor;
            this.BoardMemberParameter.Value = ApplicationStrings.AgencyFor;

            newApplication = new tblApplication();
            if ("REG" == app.SessionRequestType)
            {
                this.Title = "New Appeal";
                newApplication.ApplicationType = "REG";
            }
            else
            {
                this.Title = "New Jurisdiction Request";
                newApplication.ApplicationType = "JR";
                this.JRGrantedText.Visibility = System.Windows.Visibility.Collapsed;
                this.JRLabel.Visibility = System.Windows.Visibility.Collapsed;
                this.CaseStatementLabel.Visibility = System.Windows.Visibility.Collapsed;
                this.ConditionsLabel.Margin = new Thickness(0, 0, 0, 0);
                this.ConditionsText.Margin = new Thickness(10, 0, 0, 0);
                this.PostCardsMailed.Visibility = System.Windows.Visibility.Collapsed;
                this.PostCardMailedLabel.Visibility = System.Windows.Visibility.Collapsed;
            }
            this.HeaderText.Text = this.Title;
            //Bind a new Application record to be ready for data entry.
            newAppealDataForm.CurrentItem = newApplication;

            newApplication.AgencyID = ApplicationStrings.AgencyFor;
            newApplication.Conditions = false;
            newApplication.JRGranted = false;
            //newApplication.ExpDate = DateTime.Today.AddDays(-1);
            newApplication.FileDate = DateTime.Today;
            
            //New Item details
            newItemDetail = new tblItemDetail();
            newItemDetail.Related_City = "San Francisco";
            newItemDetail.Related_State = "CA";
            detailDataForm.CurrentItem = newItemDetail;
            newItemDetail.ModDate = DateTime.Now;
            newItemDetail.ApplicationID = 0;
            newItemDetail.AgencyID = ApplicationStrings.AgencyFor;

            //new owner
            newOwner = new tblNameField();
            newOwnerDataForm.CurrentItem = newOwner;
            newOwner.NameDescription = "new owner";
            newOwner.ApplicationID = 0;
            
            //New appellant
            newAppellant = new tblNameField();
            newAppellantDataForm.CurrentItem = newAppellant;
            newAppellant.NameDescription = "new appellant";
            newAppellant.ApplicationID = 0;
            newAppellant.city = "San Francisco";
            newAppellant.state = "CA";

            //validation events
            detailDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(detailDataForm_ValidatingItem);
            newAppealDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newAppealDataForm_ValidatingItem);
            newAppellantDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newAppellantDataForm_ValidatingItem);
            newOwnerDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newOwnerDataForm_ValidatingItem);

            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick_loading);
            myDispatcherTimer.Start();
        }

        // Detects changes if navigating away from page
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {

            //if (departmentDataSource1.DomainContext.HasChanges || applicationDataSource.DomainContext.HasChanges || AppellantDataSource1.DomainContext.HasChanges
                //|| typeDataSource1.DomainContext.HasChanges || actionDataSource1.DomainContext.HasChanges || OwnerDataSource1.DomainContext.HasChanges)
            //if(this.newApplication.HasChanges || this.newItemDetail.HasChanges || this.newOwner.HasChanges || this.newAppellant.HasChanges)
            if(hasErrors)
            {
                var result = MessageBox.Show("You have unsaved changes, press OK to continue or press Cancel to stay on this page. ", "", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.Cancel)
                    e.Cancel = true;
                base.OnNavigatingFrom(e);
            }
        }

        private void newOwnerDataForm_ValidatingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void newAppealDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.newAppealDataForm.ValidationSummary.Errors.Clear();
            if (this.RespDeptText.Text.Length < 1)
                this.newAppealDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Respondent Department is required!", "Respondent Department is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("agencySelection"), this.newAppealDataForm.CurrentItem));
            if (this.DeptActionText.Text.Length < 1)
                this.newAppealDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Department Action is required!", "Department action is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("actionSelection"), this.newAppealDataForm.CurrentItem));
        }
        private void newAppellantDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void detailDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.detailDataForm.ValidationSummary.Errors.Clear();
            if (this.hDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Hearing Date required!", "Appeal Hearing Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("hDate"), this.detailDataForm.CurrentItem));
            if(this.fDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("File Date required!", "File Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("fDate"), this.detailDataForm.CurrentItem));
            if(this.efDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Effective Date required!", "Effective Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("efDate"), this.detailDataForm.CurrentItem));
            if (this.typeText.Text.Length < 1)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Permit Type is required!", "Permit Type is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("typeSelection"), this.newAppealDataForm.CurrentItem));
        }

        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            if (app.SessionRequestType == "JR")
                this.source = "/EditAppealJR";
            else
                this.source = "/EditAppeal";
            InsertNewAppeal();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "/AdditionalNewContactsPage1";
            InsertNewAppeal();
        }

        void client_GetNameCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url + ":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }

                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
             }
        }
        
        private void InsertNewAppeal()
        {
            if (this.newAppealDataForm.CurrentItem != null && this.newAppealDataForm.ValidateItem() && this.newAppellantDataForm.ValidateItem() && detailDataForm.ValidateItem())
            {
                hasErrors = false;
                if (this.fDate.SelectedDate.Value != null)
                {
                    newApplication.FileDate = new DateTime(fDate.SelectedDate.Value.Year, fDate.SelectedDate.Value.Month, fDate.SelectedDate.Value.Day);
                }
                if (this.efDate.SelectedDate.Value != null)
                {
                    newApplication.ExpDate = new DateTime(efDate.SelectedDate.Value.Year, efDate.SelectedDate.Value.Month, efDate.SelectedDate.Value.Day);
                }
                if (this.hDate.SelectedDate.Value != null)
                {
                    newApplication.HearingDate = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 17, 0, 0);
                    newApplication.Status = "Hearing Pending";
                }
                //newApplication.PermitType = this.typeSelection.SelectedValue.ToString();
                newAppealDataForm.CommitEdit();
                ApplicationDomainService1 _AppealApplicationDomainServices1 = (ApplicationDomainService1)(applicationDataSource.DomainContext);
                _AppealApplicationDomainServices1.RejectChanges();

                tblApplications_JR newJR = new tblApplications_JR();
                // copy newApplication to JR
                newJR.AgencyID = newApplication.AgencyID;
                newJR.ApplicationID = newApplication.ApplicationID;
                newJR.AssocAppeal = newApplication.AssocAppeal;
                newJR.ApplicationType = newApplication.ApplicationType;
                newJR.Block = newApplication.Block;
                newJR.CaseType = newApplication.CaseType;
                newJR.Conditions = newApplication.Conditions;
                newJR.General_Comments = newApplication.General_Comments;
                newJR.ConditionsText = newApplication.ConditionsText;
                newJR.ExpDate = newApplication.ExpDate;
                newJR.FileDate = newApplication.FileDate;
                newJR.HearingDate = newApplication.HearingDate;
                newJR.JRGranted = newApplication.JRGranted;
                newJR.JRGrantedText = newApplication.JRGrantedText;
                newJR.Lot = newApplication.Lot;
                newJR.PermitNumber = newApplication.PermitNumber;
                newJR.PermitType = newApplication.PermitType;
                newJR.PlanningDept = newApplication.PlanningDept;
                newJR.Dept_Agency = newApplication.Dept_Agency;
                newJR.Status = newApplication.Status;

                if (newJR.ApplicationType == "JR")
                    _AppealApplicationDomainServices1.tblApplications_JRs.Add(newJR);
                else
                    _AppealApplicationDomainServices1.tblApplications.Add(newApplication);

                SubmitOperation so = _AppealApplicationDomainServices1.SubmitChanges(OnSubmitCompleted_InsertNewAppeal, null);
            }
            else
                hasErrors = true;
        }
        private void OnSubmitCompleted_InsertNewAppeal(SubmitOperation so)
        {
            if (so.HasError)
            {
                MessageBox.Show(string.Format("Submit Failed: {0}. Please make neccessary changes in Edit screen.", so.Error.Message));
                so.MarkErrorAsHandled();
            }
            else
            {
                ApplicationDomainService1 _AppealApplicationDomainServices1 = (ApplicationDomainService1)(applicationDataSource.DomainContext);
                InvokeOperation<int> query;
                tblApplication reg = new tblApplication();
                tblApplications_JR jr = new tblApplications_JR();
                if (app.SessionRequestType == "REG")
                {
                    reg = (tblApplication)so.ChangeSet.AddedEntities.First();
                    app.SessionApplicationID = reg.ApplicationID;
                    query = _AppealApplicationDomainServices1.GetApplicationIDstr(reg.HearingDate.Year.ToString());
                }
                else
                {
                    jr = (tblApplications_JR)so.ChangeSet.AddedEntities.First();
                    app.SessionApplicationID = jr.ApplicationID;
                    query = _AppealApplicationDomainServices1.GetApplicationIDstr_JR(jr.HearingDate.Year.ToString());
                }
                query.Completed += (s, e) =>
                {
                    this.applicationIDstr = hDate.SelectedDate.Value.Year.ToString() + "-";
                    int appstr = query.Value + 1;
                    if (appstr < 10)
                        this.applicationIDstr += "00" + appstr.ToString();
                    else if (appstr < 100)
                        this.applicationIDstr += "0" + appstr.ToString();
                    else
                        this.applicationIDstr += appstr.ToString();

                    if (app.SessionRequestType == "REG")
                        reg.ApplicationIDstr = this.applicationIDstr;
                    else
                        jr.ApplicationIDstr = this.applicationIDstr;
                    _AppealApplicationDomainServices1.SubmitChanges(InsertNewRecords, null);
                };
                //InsertNewRecords();
            }
        }

        private void InsertNewRecords(SubmitOperation so)
        {
            if (so.HasError)
            {
                MessageBox.Show(string.Format("Error creating new appeal, please contact support with this message: ", so.Error.Message));
                so.MarkErrorAsHandled();
            }
            else
            {
                isVoteSheetupdated = false;
                FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);
                _FieldTableDomainServices1.RejectChanges();
                this.SavingData.Visibility = System.Windows.Visibility.Visible;

                System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
                myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
                myDispatcherTimer.Start();

                //if (this.fDate.SelectedDate.Value != null && this.fTime.Value.Value != null)
                if (this.fDate.SelectedDate.Value != null)
                {
                    DateEntry(this.fDate, "Filed Date", "FiledDate", "New appeal application.");
                }

                if (this.hDate.SelectedDate.Value != null)
                {
                    DateEntry(this.hDate, "Hearing Date", "HearingDate", "New appeal application.");

                    if (this.briefDate.SelectedDate != null || this.appellantReplyDate.SelectedDate != null || this.othersBriefDate.SelectedDate != null)
                    {
                        DateEntry(this.briefDate, "Appellant's Brief Date", "AppellantBrief", "New appeal application.");
                    }

                    if (this.appellantReplyDate.SelectedDate != null)
                    {
                        DateEntry(this.appellantReplyDate, "Appellant's Reply Date", "AppellantReply", "New appeal application.");
                    }

                    if (this.othersBriefDate.SelectedDate != null)
                    {
                        DateEntry(this.othersBriefDate, "Respondent's / Other Parties' Brief Date", "OthersBrief", "New appeal application.");
                    }
                }
                if (this.PostCardsMailed.SelectedDate != null)
                {
                    DateEntry(this.PostCardsMailed, "Post Cards Mailed Date", "PostCardsMailed", "New appeal application.");
                }
                if (this.IndexCardsFiled.SelectedDate != null)
                {
                    DateEntry(this.IndexCardsFiled, "Index Cards Filed Date", "IndexCardsFiled", "New appeal application.");
                }
                if (this.LetterNoticesMailed.SelectedDate != null)
                {
                    DateEntry(this.LetterNoticesMailed, "Letter Notices Mailed Date", "LetterNoticesMailed", "New appeal application.");
                }

                try
                {
                    _FieldTableDomainServices1.SubmitChanges();

                    ApplicationDomainService1 _AppealApplicationDomainServices1 = (ApplicationDomainService1)(applicationDataSource.DomainContext);

                    //Insert a new record to TblItemDetail table if REQUIRED Application information had been entered.
                    if (this.detailDataForm.CurrentItem != null && this.detailDataForm.ValidateItem())
                    {
                        newItemDetail.ApplicationID = app.SessionApplicationID;
                        newItemDetail.AgencyID = ApplicationStrings.AgencyFor;
                        newItemDetail.ModDate = DateTime.Now;
                        //newItemDetail.Related_Agency = newApplication.Dept_Agency;
                        newItemDetail.UserID = WebContext.Current.User.DisplayName.ToString();
                        detailDataForm.CommitEdit();
                        _AppealApplicationDomainServices1.tblItemDetails.Add(newItemDetail);
                        _AppealApplicationDomainServices1.SubmitChanges();
                    }

                    //Insert Determination Holder Information
                    if (this.newOwnerDataForm.CurrentItem != null)
                    {
                        if (this.newOwnerDataForm.ValidateItem())
                        {
                            //newOwner.NameDescription = newOwner.FirstName + newOwner.MiddleName + newOwner.LastName;
                            if (newOwner.NameDescription == "")
                                newOwner.NameDescription = "N/A";
                            newOwner.Type = "Respondent";
                            newOwner.ApplicationID = app.SessionApplicationID;
                            newOwnerDataForm.CommitEdit();
                            OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(OwnerDataSource1.DomainContext);
                            _OwnerDomainServer1.tblNameFields.Add(newOwner);
                            //OwnerDataSource1.SubmitChanges();
                            _OwnerDomainServer1.SubmitChanges();
                        }
                    }

                    //Insert Appellant Information
                    if (this.newAppellantDataForm.CurrentItem != null)
                    {
                        if (this.newAppellantDataForm.ValidateItem())
                        {
                            //newAppellant.NameDescription = newAppellant.FirstName + newAppellant.MiddleName + newAppellant.LastName;
                            if (newAppellant.NameDescription == "")
                                newAppellant.NameDescription = "N/A";
                            newAppellant.Type = "Primary Appellant";
                            newAppellant.ApplicationID = app.SessionApplicationID;
                            newAppellantDataForm.CommitEdit();
                            OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                            _OwnerDomainServer1.tblNameFields.Add(newAppellant);
                            //AppellantDataSource1.SubmitChanges();
                            _OwnerDomainServer1.SubmitChanges(OnSubmitCompleted_owner, null);
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to commit changes for the reason of: " + e.Message);
                }

                // update VoteSheet entries.
                updateVoteSheet();
            }
        }
        // insert record into votesheet table
        private void updateVoteSheet()
        {
            vtd = new VoteNTransactionDomainService1();

            var query = vtd.GetTblVoteResultsQuery(this.hDate.SelectedDate.Value);
            vtd.Load(query, voteResultDataSource_LoadedData, null);
        }
        private void voteResultDataSource_LoadedData(LoadOperation<tblVoteResult> lo)
        {
            //VoteNTransactionDomainService1 vtd = new VoteNTransactionDomainService1();
            tblVoteResult tvr = new tblVoteResult();

            string boardmembers = "";
            for (int i = 0; i < this.VoteNTransactionDataSource1.DataView.Count; i++)
            {
                tblBoardMember tbm1 = (tblBoardMember)this.VoteNTransactionDataSource1.DataView.GetItemAt(i);
                boardmembers += tbm1.MemberID + ((i + 1 < this.VoteNTransactionDataSource1.DataView.Count) ? "," : "");
            }

            tvr.AgencyID = ApplicationStrings.AgencyFor;
            /*string appstr = app.SessionApplicationID.ToString();
            string temp = "";
            if ("REG" == app.SessionRequestType)
            {
                temp = appstr.Substring(3, appstr.Length - 3);
                appstr = (this.hDate.SelectedDate.Value.Year.ToString().Substring(2, 2) + "-" + temp);
            }
            else
                appstr = ("JR-" + this.hDate.SelectedDate.Value.Year.ToString().Substring(2, 2));
            */
            tvr.AppealDescription = this.applicationIDstr + "\n"
                + this.aFName.Text + " " + this.aLName.Text + " vs. \n"
                + ((tblDepartment)this.agencySelection.SelectedItem).DeptName + ", " 
                + ((this.planningDeptDropDown.SelectedValue == null) ? "" : this.planningDeptDropDown.SelectedValue.ToString()) + "\n"
                + this.aAddress.Text;
            tvr.ApplicationID = app.SessionApplicationID;
            tvr.HearingDate = this.hDate.SelectedDate.Value;
            tvr.ModDate = DateTime.Now;
            tvr.BoardMembers = boardmembers;
            tvr.SortOrder = 1000;
            vtd.tblVoteResults.Add(tvr);

            if (lo.Entities.Count() < 1)// vote sheet table is empty, populate common items among all votesheets.
            {
                tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "PUBLIC COMMENT";
                
                tvr.DeptRepresentatives = "";
                tvr.ApplicationID = 0;
                tvr.Type = "item_1";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.BoardMembers = boardmembers;
                tvr.SortOrder = 0;
                vtd.tblVoteResults.Add(tvr);

                tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "COMMISSIONER COMMENTS & QUESTIONS";
                tvr.ApplicationID = 0;
                tvr.Type = "item_2";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.BoardMembers = boardmembers;
                tvr.SortOrder = 1;
                vtd.tblVoteResults.Add(tvr);


                tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "ADOPTION OF MINUTES";
                tvr.ApplicationID = 0;
                tvr.Type = "item_3";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.BoardMembers = boardmembers;
                tvr.SortOrder = 2;
                vtd.tblVoteResults.Add(tvr);
            }
            try
            {
                vtd.SubmitChanges(VoteSheetUpdated, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong, please contact support immediately: " + ex.Message);
            }
        }
        private void VoteSheetUpdated(SubmitOperation so)
        {
            if (so.HasError)
                so.MarkErrorAsHandled();
            else
                isVoteSheetupdated = true;
        }
        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;

            if (!FieldDataSource1.IsBusy && !this.OwnerDataSource1.IsBusy && isVoteSheetupdated &&
                    !this.AppellantDataSource1.IsBusy && !this.applicationDataSource.IsBusy && !this.MemberResultDataSource.IsBusy && 
                    !this.VoteNTransactionDataSource1.IsBusy)
            {
                this.SavingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
                if (this.newAppealDataForm.CurrentItem != null && this.newAppealDataForm.ValidateItem() && this.newAppellantDataForm.ValidateItem() && detailDataForm.ValidateItem())
                {
                    GoToNext(this.source);
                }
            }

        }
        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick_loading(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;

            if (!departmentDataSource1.IsBusy && !OwnerDataSource1.IsBusy && !actionDataSource1.IsBusy 
                && !FieldDataSource1.IsBusy && !typeDataSource1.IsBusy && !AppellantDataSource1.IsBusy 
                && !ApplicationStatusTypesDataSource1.IsBusy)
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }

        }
        private void OnSubmitCompleted_owner(SubmitOperation so)
        {
            if (!so.HasError)
            {
                if (this.source == "PSOA")
                {
                    //this.source = "/Home";
                    //generate PSOA word document based on template
                    if (fileDownloadWindow == null)
                        fileDownloadWindow = new FileDownload();
                    fileDownloadWindow.Show();
                    DocumentGeneration dg = new DocumentGeneration();
                    InvokeOperation op = dg.getPSOADoc(app.SessionApplicationID,"PSOA");
                    op.Completed += new EventHandler(client_GetNameCompleted);
                }
                //GoToNext(this.source);
            }
        }
        private void GoToNext(string uri)
        {
           if(this.source != "PSOA")
                this.NavigationService.Navigate(new Uri(String.Format(uri), UriKind.Relative));
        }
        private void DateEntry(DatePicker dateObject, string description, string dateType, string comments)
        {
            FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);

            tblDateField OthersBrief = new tblDateField();
            OthersBrief.DateValue = new DateTime(dateObject.SelectedDate.Value.Year, dateObject.SelectedDate.Value.Month, dateObject.SelectedDate.Value.Day, 16, 0, 0);
            OthersBrief.ApplicationID = app.SessionApplicationID;
            OthersBrief.ModDate = DateTime.Now;
            OthersBrief.DateDescription = description;
            OthersBrief.DateType = dateType;
            OthersBrief.Comments = comments;
            OthersBrief.UserID = WebContext.Current.User.DisplayName.ToString();
            _FieldTableDomainServices1.tblDateFields.Add(OthersBrief);
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.newAppealDataForm.CancelEdit();
            this.detailDataForm.CancelEdit();
            this.newAppellantDataForm.CancelEdit();
            this.newOwnerDataForm.CancelEdit();
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }
               
        //Avoid user from selecting invalid hearing date.
        private void fDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            this.hDate.DisplayDateStart = this.fDate.SelectedDate;
            if ("JR" == app.SessionRequestType)
            {
                //this.briefDate.SelectedDate = this.fDate.SelectedDate.Value.AddDays(10);
                this.briefDate.SelectedDate = this.fDate.SelectedDate;
                this.othersBriefDate.SelectedDate = this.fDate.SelectedDate.Value.AddDays(10);
                if (this.othersBriefDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday)
                {
                    this.othersBriefDate.SelectedDate = this.othersBriefDate.SelectedDate.Value.AddDays(2);
                    //this.briefDate.SelectedDate = this.briefDate.SelectedDate.Value.AddDays(2);
                }
                if (this.othersBriefDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    this.othersBriefDate.SelectedDate = this.othersBriefDate.SelectedDate.Value.AddDays(1);
                    //this.briefDate.SelectedDate = this.briefDate.SelectedDate.Value.AddDays(1);
                }
            }
        }

        private void AppellantDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Appellant Information HAS NOT Been Entered!", "No Appellant Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

       
        private void typeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.typeSelection.SelectedValue != null)
            {
                this.typeText.Text = this.typeSelection.SelectedValue.ToString();
                this.newItemDetail.Related_Agency = this.RespDeptText.Text;  
                if (this.typeText.Text == otherPermitTypeText)
                {
                    this.permitTypeText2.UpdateLayout();
                    this.otherPermitType.Visibility = System.Windows.Visibility.Visible;
                    //this.permitTypeText2.Text = otherPermitTypeText;
                    this.permitTypeText2.Focus();
                    this.permitTypeText2.SelectAll();
                }
                else
                {
                    this.otherPermitType.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        private void ownerDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Owner Information HAS NOT Been Entered!", "No Owner Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        private void agencySelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.RespDeptText.Text = this.agencySelection.SelectedValue.ToString();
            this.newItemDetail.Related_Agency = this.RespDeptText.Text;
            VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
            InvokeOperation<IEnumerable<string>> dept_info = departmentDomainService.GetDepartmentContactInfo(this.RespDeptText.Text, ApplicationStrings.AgencyFor);
            dept_info.Completed += new EventHandler(dept_info_Completed);
        }

        //Update the Dept address info
        void dept_info_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            string[] info = (string[]) op.Value;
            if (info.Length > 0)
            {
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Visible;
                this.deptContactName.Text = (info[0] == null) ? "" : info[0];
                this.deptAddress.Text = (info[1] == null) ? "" : info[1];
                this.deptPhone.Text = (info[2] == null) ? "" : info[2];
            }
            else
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void departmentDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
        }

        private void applicationDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show(e.Error.ToString(), app.SessionApplicationID.ToString(), MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            else
            {
                this.applicationParameter.Value = ApplicationStrings.AgencyFor;
                this.applicationDataSource.Load();
                ApplicationDomainService1 _AppealApplicationDomainServices1 = (ApplicationDomainService1)(applicationDataSource.DomainContext);

                try
                {
                    _AppealApplicationDomainServices1.SubmitChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to commit changes for the reason of: " + ex.Message);
                }
            }
        }

        private void hDate_SelectedDateChanged(object sender, RoutedEventArgs e)
        {
            //dbnull got converted to 01/01/0001 
            if (hDate.SelectedDate != null && hDate.SelectedDate.Value.Year > 1)
            {
                tempDate = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 16, 0, 0);
                if ("REG" == app.SessionRequestType)
                {
                    this.briefDate.SelectedDate = tempDate.AddDays(-20);
                    this.othersBriefDate.SelectedDate = tempDate.AddDays(-6);
                }
                this.hearing_date_text.Text = tempDate.ToShortDateString();
                //this.appellantReplyDate.SelectedDate = tempDate.AddDays(-6);
            }
            else
                this.hDate.Text = " ";
        }

        private void actionSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.actionText.Text = this.actionSelection.SelectedValue.ToString();
            this.DeptActionText.Text = (this.actionSelection.SelectedValue == null) ? "" : this.actionSelection.SelectedValue.ToString();

            if (this.DeptActionText.Text.ToString() == otherDepartmentalAction)
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
                this.typeText2.UpdateLayout();
                this.typeText2.SelectAll();
                this.typeText2.Focus();
            }
            else
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void otherAction_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if other action, use the other action text rather than the value from dropdown box.
            if (this.typeText.Text.ToString() == otherDepartmentalAction)
            {
                this.DeptActionText.Text = this.typeText2.Text;
            }
        }

        private void permitType_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.typeText.Text = this.permitTypeText2.Text;
        }

        private void efDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime? selected = this.efDate.SelectedDate;
            if (selected.Value != null && selected.Value.Year < 2)
                this.efDate.Text = " ";
            /*if (selected != null && selected >= DateTime.Today)
            {
                if (this.ef_date_text.Text != selected.Value.ToShortDateString())
                {
                    MessageBox.Show("Effective day must be less than " + DateTime.Today.ToShortDateString(), "Invalid date selected", MessageBoxButton.OK);
                }
            }
            else
            {*/
                if (selected != null && selected < DateTime.Today)
                {
                    DateTime tempDate = new DateTime(selected.Value.Year, selected.Value.Month, selected.Value.Day, 16, 0, 0);
                    this.ef_date_text.Text = tempDate.ToShortDateString();
                }
            //}                
        }

        private void ContactsPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ContactsPanel.Visibility == Visibility.Collapsed)
                this.ContactsPanel.Visibility = Visibility.Visible;
            else
                this.ContactsPanel.Visibility = Visibility.Collapsed;
        }

        private void PermitPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PermitPanel.Visibility == Visibility.Collapsed)
                this.PermitPanel.Visibility = Visibility.Visible;
            else
                this.PermitPanel.Visibility = Visibility.Collapsed;
        }

        private void DatePanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DatePanel.Visibility == Visibility.Collapsed)
                this.DatePanel.Visibility = Visibility.Visible;
            else
                this.DatePanel.Visibility = Visibility.Collapsed;
        }

        private void DeptPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DeptPanel.Visibility == Visibility.Collapsed)
                this.DeptPanel.Visibility = Visibility.Visible;
            else
                this.DeptPanel.Visibility = Visibility.Collapsed;
        }

        private void Conditions_Checked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = true;
        }
        private void Conditions_UnChecked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = false;
        }

        private void JRGranted_Checked(object sender, RoutedEventArgs e)
        {
            this.JRGrantedText.IsEnabled = true;
        }
        private void JRGranted_UnChecked(object sender, RoutedEventArgs e)
        {
            this.JRGrantedText.IsEnabled = false;
        }

        private void statusSelection_Loaded(object sender, RoutedEventArgs e)
        {
            this.statusSelection.IsEnabled = false;
        }

        private void efDate_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }
        //Scroll page vertically when focus changes to a control that is outside the viewable area.
        private void AutoScrollViewer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                FrameworkElement element = FocusManager.GetFocusedElement() as FrameworkElement;

                if (element != null)
                {
                    //ScrollViewer scrollViewer = sender as ScrollViewer;
                    ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
                    try
                    {
                        scrollViewer.ScrollToVerticalOffset(GetVerticalOffset(element, scrollViewer));
                    }
                    catch
                    {
                        //if there's an error, don't scroll.
                    }
                }
            }
        }
        private double GetVerticalOffset(FrameworkElement child, ScrollViewer scrollViewer)
        {
            // Ensure the control is scrolled into view in the ScrollViewer.
            System.Windows.Media.GeneralTransform focusedVisualTransform = child.TransformToVisual(scrollViewer);
            Point topLeft = focusedVisualTransform.Transform(new Point(child.Margin.Left, child.Margin.Top));
            Rect rectangle = new Rect(topLeft, child.RenderSize);
            //If the control is taller than the viewport, don't scroll down further than the top of the control.
            double controlRectangleBottom = rectangle.Bottom - scrollViewer.ViewportHeight > scrollViewer.ViewportHeight ? scrollViewer.ViewportHeight : rectangle.Bottom;
            double newOffset = scrollViewer.VerticalOffset + (controlRectangleBottom - scrollViewer.ViewportHeight);
            return newOffset < 0 ? 0 : newOffset + 200; // no use returning negative offset
        }

        /*private void Popup_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Popup.Height = 400;
            this.ShowPopup.Begin();
        }

        private void Popup_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Popup.Height = 80;
            this.ShowPopup.Stop();
        }*/
    }

    public class PlanningDepartment
    {
        public List<string> PlanningDepartmentList
        {
            get
            {
                return new List<string> { "", "Planning Department Approval", "Planning Department Disapproval"};
            }

        }
    }
    public class StatusTypeProvider
    {
        public List<string> StatusTypeList
        {
            get
            {
                return new List<string> { "Pending", "Decided", "Withdrawn", "Dismissed", "Rejected", "Call of the Chair", "NOD REL", "Pending - AOF", "Rehearing" };
            }

        }
    }

    public class DateConverter : IValueConverter
    {
        public object Convert(object value,
                           Type targetType,
                           object parameter,
                           CultureInfo culture)
        {
            if (value != null)
            {
                DateTime date = (DateTime)value;
                return date.ToString("M/d/yyyy");
            }
            return "";
        }

        public object ConvertBack(object value,
                                  Type targetType,
                                  object parameter,
                                  CultureInfo culture)
        {
            if (value != null)
            {
                string strValue = value.ToString();

                DateTime result;

                if (DateTime.TryParse(strValue, out result))
                    return result;

                return value;
            }
            return null;
        }
    }
}