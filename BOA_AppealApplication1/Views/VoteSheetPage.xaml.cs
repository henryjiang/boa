﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Printing;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class VoteSheetPage1 : Page
    {
        //App app = (App)Application.Current;
        FileDownload fileDownloadWindow;

        private ObservableCollection<VoteData> _source = new ObservableCollection<VoteData>();
        public ObservableCollection<VoteData> source {
            get { return _source; }
        }

        // number of board members, this number cannot change!
        public const int NUMBER_OF_BOARDMEMBERS = 5;
        private string boardmembers = "";

        public VoteSheetPage1()
        {
            InitializeComponent();
            
            //Initialize remote services with session variable.
            this.BoardMemberParameter.Value = ApplicationStrings.AgencyFor;
            this.applicationParameter2.Value = ApplicationStrings.AgencyFor;
            this.voteGrid.DataContext = this.applicationDataSource;
            this.deptRepLabel.Text = "\nDepartment Representatives:";
            //this.applicationDataSource.Load();
            this.VoteNTransactionDataSource1.Load();
            // default text for department reps.
            //this.deptRepText.Text = "President Chris Hwang, Vice President Frank Fung, Commissioner Arcelia Hurtado and Commissioner Ann Lazarus.\r\n\r\nRobert Bryan, Deputy City Attorney, Office of the City Attorney (OCA); Scott Sanchez, Zoning Administrator (ZA); Joseph Duffy, Senior Building Inspector, Department of Building Inspection (DBI); Patrick O’Riordan, Senior Building Inspector, DBI; John Kwong, Manager, Department of Public Works Bureau of Street Use and Mapping; Cynthia Goldstein, Executive Director; Victor Pacheco, Legal Assistant.";
            this.deptRepText.Text = "";
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void applicationDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
        }

        private void voteResultDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
            else
            {
                if (this.voteResultDataSource.DataView.Count > 0 )
                    retrieveVoteSheet();  // retrieve votesheet
                else
                    populateVoteSheet(); // populate votesheet 
            }
        }

        #region //deprecated
        private void MemberResultDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (this.MemberResultDataSource.DataView.Count > 0)
            {
                int counter = 0;
                for (int j = 0; j < this.source.Count; j++)
                {
                    string[] tmp = new string[NUMBER_OF_BOARDMEMBERS];
                    int no_board_members = 0;
                    while ((counter < this.MemberResultDataSource.DataView.Count) && ((tblVoteMemberResult)this.MemberResultDataSource.DataView[counter]).VoteID == this.source[j].VoteID)
                    {
                        tmp[no_board_members] = ((tblVoteMemberResult)this.MemberResultDataSource.DataView[counter]).Decision;
                        if(j < 1)
                            this.voteGrid.Columns[no_board_members+1].Header = ((tblVoteMemberResult)this.MemberResultDataSource.DataView[counter]).MemberID;
                        no_board_members++;
                        counter++;
                    }
                    this.source[j].MemberDecision = tmp;
                }
                if (this.voteGrid.ItemsSource != null)
                    this.voteGrid.ItemsSource = null;
               this.voteGrid.ItemsSource = this.source;
            }
        
        }
        #endregion

        private void applicationDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
            else
            {
                // this is a hack for statically add 3 items to every selected hearing date
                if (this.voteResultDataSource.DataView.Count > 0 && this.voteResultDataSource.DataView.Count >= (this.applicationDataSource.DataView.Count + 3))
                    retrieveVoteSheet();
                else if(this.voteResultDataSource.DataView.Count > 0 && this.voteResultDataSource.DataView.Count < (this.applicationDataSource.DataView.Count + 3))
                    populateVoteSheet(true);    
                else
                    populateVoteSheet();    
            }
        }

        private void retrieveVoteSheet()
        {
                if(((tblVoteResult)this.voteResultDataSource.DataView[0]).StartTime != null)
                    this.startTime.Value = DateTime.Parse(((tblVoteResult)this.voteResultDataSource.DataView[0]).StartTime.Value.ToString());
                if (((tblVoteResult)this.voteResultDataSource.DataView[0]).EndTime != null)
                    this.endTime.Value = DateTime.Parse(((tblVoteResult)this.voteResultDataSource.DataView[0]).EndTime.Value.ToString());
                
                foreach (tblVoteResult tvr in this.voteResultDataSource.DataView)
                {
                    //bind board member vote result headers
                    string[] tmp = new string[10]; // 10 is max of board members;
                    string[] members = new string[10];
                    if (tvr.BoardMembers != null && tvr.BoardMembers != "")
                    {
                        // Header = board member initials
                        members = tvr.BoardMembers.Split(new Char[] { ',' });
                        if (this.boardmembers == "")
                        {
                            for (int i = 0; i < members.Count(); i++)
                            {
                                this.voteGrid.Columns[i + 1].Header = members[i];
                                this.voteGrid.Columns[i + 1].CanUserReorder = false;
                            }
                            this.boardmembers = tvr.BoardMembers;
                        }
                        // populate board member vote results
                        if (tvr.BoardMemberDecisions != null && tvr.BoardMemberDecisions != "")
                        {
                            string[] decisions = tvr.BoardMemberDecisions.Split(new Char[] { ',' });
                            for (int i = 0; i < decisions.Count(); i++)
                            {
                                tmp[i] = decisions[i];
                            }
                        }
                    }
                    this._source.Add(new VoteData
                    {
                        AppealDescription = tvr.AppealDescription,
                        Action = tvr.Action,
                        Speakers = tvr.Speakers,
                        Comments = tvr.Comments,
                        Decision = tvr.Decision,
                        VoteID = tvr.VoteID,
                        StartTime = this.startTime.Value.Value,
                        EndTime = this.endTime.Value == null ? DateTime.Now : this.endTime.Value.Value,
                        hearingDate = tvr.HearingDate,
                        DeptRepresentatives = tvr.DeptRepresentatives,
                        MemberDecision = tmp,
                    });
                    
                }
                this.deptRepText.UpdateLayout();
                this.deptRepText.Text = this._source[0].DeptRepresentatives == null ? "" : this._source[0].DeptRepresentatives;
            
            if (this.voteGrid.ItemsSource != null)
                this.voteGrid.ItemsSource = null;
            this.voteGrid.ItemsSource = this._source;
         
        }
        private void populateVoteSheet()
        {
            //this.startTime.Value = DateTime.Parse(DateTime.Now.TimeOfDay.ToString());
            this.startTime.Value = DateTime.Parse("5:00 pm");
            VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.VoteNTransactionDataSource1.DomainContext;

            // if there are dirty changes, reject them.
            if (vtd.HasChanges)
                vtd.RejectChanges();

            // get boardmember IDs
            // give 5 secs for board members to load
            if (this.VoteNTransactionDataSource1.DataView.Count < 1)
                System.Threading.Thread.Sleep(5000);
            if (this.VoteNTransactionDataSource1.DataView.Count < 1)
            {
                MessageBox.Show("Unable to load data, please refresh your browser. If problem persist, please contact technical support.");
                throw new Exception("Unable to load board member data");
            }
            for (int i = 0; i < this.VoteNTransactionDataSource1.DataView.Count; i++)
            {
                tblBoardMember tbm1 = (tblBoardMember)this.VoteNTransactionDataSource1.DataView.GetItemAt(i);
                this.boardmembers += tbm1.MemberID + ((i + 1 < this.VoteNTransactionDataSource1.DataView.Count) ? "," : "");
                this.voteGrid.Columns[i + 1].Header = tbm1.MemberID;
                this.voteGrid.Columns[i + 1].CanUserReorder = false;
            }
            
            // Add default item, for generate minute purposes
            tblVoteResult tvr = new tblVoteResult();
            #region have data?
            if (!hasRecords)
            {
                // Add Public Comment
                this._source.Add(new VoteData
                {
                    ApplicationID = 0,
                    AgencyID = ApplicationStrings.AgencyFor,
                    AppealDescription = "PUBLIC COMMENT",
                    hearingDate = hearingDate.SelectedDate.Value,
                    // Default text for list of board members
                    //DeptRepresentatives = "President Chris Hwang, Vice President Frank Fung, Commissioner Arcelia Hurtado and Commissioner Ann Lazarus.\r\n\r\nRobert Bryan, Deputy City Attorney, Office of the City Attorney (OCA); Scott Sanchez, Zoning Administrator (ZA); Joseph Duffy, Senior Building Inspector, Department of Building Inspection (DBI); Patrick O’Riordan, Senior Building Inspector, DBI; John Kwong, Manager, Department of Public Works Bureau of Street Use and Mapping; Cynthia Goldstein, Executive Director; Victor Pacheco, Legal Assistant.",
                    DeptRepresentatives = "",
                });

                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "PUBLIC COMMENT";
                //tvr.DeptRepresentatives = "President Chris Hwang, Vice President Frank Fung, Commissioner Arcelia Hurtado and Commissioner Ann Lazarus.\r\n\r\nRobert Bryan, Deputy City Attorney, Office of the City Attorney (OCA); Scott Sanchez, Zoning Administrator (ZA); Joseph Duffy, Senior Building Inspector, Department of Building Inspection (DBI); Patrick O’Riordan, Senior Building Inspector, DBI; John Kwong, Manager, Department of Public Works Bureau of Street Use and Mapping; Cynthia Goldstein, Executive Director; Victor Pacheco, Legal Assistant.";
                tvr.DeptRepresentatives = "";
                tvr.ApplicationID = 0;
                tvr.Type = "item_1";
                tvr.HearingDate = hearingDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.StartTime = this.startTime.Value;
                tvr.BoardMembers = this.boardmembers;
                vtd.tblVoteResults.Add(tvr);

                // Add Commissioner comments
                tvr = new tblVoteResult();
                this._source.Add(new VoteData
                {
                    ApplicationID = 0,
                    AgencyID = ApplicationStrings.AgencyFor,
                    AppealDescription = "COMMISSIONER COMMENTS & QUESTIONS",
                    hearingDate = hearingDate.SelectedDate.Value,
                });

                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "COMMISSIONER COMMENTS & QUESTIONS";
                tvr.ApplicationID = 0;
                tvr.Type = "item_2";
                tvr.HearingDate = hearingDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.StartTime = this.startTime.Value;
                tvr.BoardMembers = this.boardmembers;
                vtd.tblVoteResults.Add(tvr);

                // Add Adoption of minutes
                tvr = new tblVoteResult();
                this._source.Add(new VoteData
                {
                    ApplicationID = 0,
                    AgencyID = ApplicationStrings.AgencyFor,
                    AppealDescription = "ADOPTION OF MINUTES",
                    hearingDate = hearingDate.SelectedDate.Value,
                });

                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "ADOPTION OF MINUTES";
                tvr.ApplicationID = 0;
                tvr.Type = "item_3";
                tvr.HearingDate = hearingDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.StartTime = this.startTime.Value;
                tvr.BoardMembers = this.boardmembers;
                vtd.tblVoteResults.Add(tvr);
            }
            #endregion

            /*for (int i = 0; i < this.applicationDataSource.DataView.Count; i++)
            {
                view_applications va = (view_applications)this.applicationDataSource.DataView.GetItemAt(i);

                // if applicationID already in voteResultDataSource, don't add
                bool application_exists = false;
                for (int j = 0; j < this.voteResultDataSource.DataView.Count; j++)
                {
                    tvr = (tblVoteResult)voteResultDataSource.DataView.GetItemAt(j);
                    if (tvr.ApplicationID == va.ApplicationID)
                        application_exists = true;
                }
                if (application_exists)
                    continue;
                else
                    tvr = new tblVoteResult();
                        
                string appstr = va.ApplicationID.ToString();
                string temp = "";
                if (va.ApplicationType == "REG")
                {
                    temp = appstr.Substring(3, appstr.Length - 3);
                    appstr = (va.HearingDate.Year.ToString().Substring(2, 2) + "-" + temp);
                }
                else
                    appstr = ("JR-" + va.HearingDate.Year.ToString().Substring(2, 2) );

                this.source.Add(new VoteData
                {
                    AppealDescription = appstr + "\n" +  va.General_Comments  + "\n vs \n" + va.Dept_Agency + ", " + va.PlanningDept
                });
                tvr.ApplicationID = va.ApplicationID;
                tvr.AppealDescription = appstr + "\n" + va.General_Comments + "\n vs \n" + va.Dept_Agency + ", " + va.PlanningDept;
                tvr.HearingDate = va.HearingDate;
                tvr.AgencyID = va.AgencyID;
                tvr.ModDate = DateTime.Now;
                tvr.StartTime = this.startTime.Value == null ? DateTime.Now : this.startTime.Value;
                tvr.EndTime = this.endTime.Value == null ? DateTime.Now : this.endTime.Value;
                tvr.Type = va.ApplicationType;
                tvr.BoardMembers = this.boardmembers;

                vtd.tblVoteResults.Add(tvr);
            }
             */
            if (vtd.HasChanges)
                vtd.SubmitChanges(OnSubmitCompleted_populateVoteSheet, null);
        }

        private void OnSubmitCompleted_populateVoteSheet(SubmitOperation so)
        {
            if (so.HasError)
            {
                so.MarkErrorAsHandled();
            }
            else
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Visible;
                System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); // 100 Milliseconds 
                myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick1);
                myDispatcherTimer.Start();

                if (this.voteResultDataSource.CanLoad)
                    this.voteResultDataSource.Load();
            }
        }
       
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;

            tblVoteResult tvr = new tblVoteResult();
            tvr.ModDate = DateTime.Now;
            tvr.AppealDescription = "";
            tvr.ApplicationID = 0;
            tvr.HearingDate = this.source[0].hearingDate;
            tvr.StartTime = this.startTime.Value;
            tvr.AgencyID = ApplicationStrings.AgencyFor;
            tvr.Type = "item_added";
            if (this.boardmembers != "")
                tvr.BoardMembers = this.boardmembers;

            vtd.tblVoteResults.Add(tvr);
            //this.voteResultDataSource.DataView.Add(tvr);
            if (vtd.HasChanges)
                vtd.SubmitChanges(OnSubmitCompleted_vtd_addButton, null);
        }

        private void OnSubmitCompleted_vtd_addButton(SubmitOperation so)
        {
            if (so.HasError)
            {
                so.MarkErrorAsHandled();
            }
            else
            {
                VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.MemberResultDataSource.DomainContext;
                foreach (tblVoteResult tvr in so.ChangeSet.AddedEntities)
                {
                    if (tvr.VoteID > 0)
                    {
                        this.source.Add(new VoteData
                        {
                            AppealDescription = "",
                            AgencyID = ApplicationStrings.AgencyFor,
                            ModDate = DateTime.Now,
                            ApplicationID = 0,
                            hearingDate = tvr.HearingDate,
                            VoteID = tvr.VoteID,
                            MemberDecision = new string[10],
                        });
                    }
                }
                //this.voteGrid.ItemsSource = source;
                if(vtd.HasChanges)
                    vtd.SubmitChanges();
            }
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            this.SavingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
            myDispatcherTimer.Start();

            VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;
            //VoteNTransactionDomainService1 memberds = (VoteNTransactionDomainService1)this.MemberResultDataSource.DomainContext;
            for (int i = 0; i < this.source.Count; i++)
            {
               for (int k = 0; k < vtd.tblVoteResults.Count; k++)
               {
                   //tblVoteResult tvr1 = vtd.tblVoteResults.ElementAt(k);
                   if (vtd.tblVoteResults.ElementAt(k).VoteID == this.source[i].VoteID)
                   {
                       //vtd.tblVoteResults.ElementAt(k).VoteID = this.source[i].VoteID;
                       vtd.tblVoteResults.ElementAt(k).Action = this.source[i].Action;
                       vtd.tblVoteResults.ElementAt(k).ModDate = DateTime.Now;
                       vtd.tblVoteResults.ElementAt(k).DeptRepresentatives = this.deptRepText.Text;
                       vtd.tblVoteResults.ElementAt(k).Speakers = this.source[i].Speakers;
                       vtd.tblVoteResults.ElementAt(k).Comments = this.source[i].Comments;
                       vtd.tblVoteResults.ElementAt(k).AppealDescription = this.source[i].AppealDescription;
                       vtd.tblVoteResults.ElementAt(k).Decision = this.source[i].Decision;
                       if (this.startTime.Value == null)
                           vtd.tblVoteResults.ElementAt(k).StartTime = DateTime.Parse("5:00 pm");
                       else
                           vtd.tblVoteResults.ElementAt(k).StartTime = this.startTime.Value;
                       if (this.endTime.Value == null)
                           vtd.tblVoteResults.ElementAt(k).EndTime = DateTime.Now;
                       else
                           vtd.tblVoteResults.ElementAt(k).EndTime = this.endTime.Value;
                       vtd.tblVoteResults.ElementAt(k).BoardMembers = this.boardmembers;
                       string memberDecisions = "";
                       foreach (string memberDecision in this.source[i].MemberDecision)
                       {
                            memberDecisions += memberDecision + ",";
                       }
                       vtd.tblVoteResults.ElementAt(k).BoardMemberDecisions = memberDecisions.TrimEnd(new char[] { ',' }); ;
                       vtd.tblVoteResults.ElementAt(k).AgencyID = ApplicationStrings.AgencyFor;
                       break;
                   }
               }
            }

            if (vtd.HasChanges)
                vtd.SubmitChanges(voteResult_Completed, null);
        }

        void voteResult_Completed(SubmitOperation so)
        {
            if (so.HasError)
            {
                MessageBox.Show("Failed to save vote results!", "", MessageBoxButton.OK);
            }
            else
            {
                
            }
        }

        private void hearingDate_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (hearingDate.SelectedDate != null)
            {
                this.source.Clear();
                this.populated = false;
                this.LoadingData.Visibility = System.Windows.Visibility.Visible;
                System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); // 100 Milliseconds 
                myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
                myDispatcherTimer.Start();

                this.applicationParameter.Value = new DateTime(hearingDate.SelectedDate.Value.Year, hearingDate.SelectedDate.Value.Month, hearingDate.SelectedDate.Value.Day);
                this.voteResultParameter.Value = this.applicationParameter.Value;
                this.voteSheetPanel.Visibility = System.Windows.Visibility.Visible;

                this.startTime.Value = DateTime.Parse("5:00 pm");
                this.endTime.Value = null;

                if (this.voteResultDataSource.CanLoad)
                    this.voteResultDataSource.Load();
            }
        }

        //give some time for changes to make it through domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!MemberResultDataSource.IsBusy && !voteResultDataSource.IsBusy && !VoteNTransactionDataSource1.IsBusy && !applicationDataSource.IsBusy)
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                this.SavingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }

        //give some time for votesheet to populate
        private void myDispatcherTimer_Tick1(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!MemberResultDataSource.IsBusy && !voteResultDataSource.IsBusy && !VoteNTransactionDataSource1.IsBusy && !applicationDataSource.IsBusy)
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }
       
        private void gmButton_Click(object sender, RoutedEventArgs e)
        {
            //generate minutes word document based on template
            if (fileDownloadWindow == null)
                fileDownloadWindow = new FileDownload();
            fileDownloadWindow.Show();
           
            DocumentGeneration dg = new DocumentGeneration();
            InvokeOperation op = dg.getVoteSheetMinutes(this.hearingDate.SelectedDate.Value);
            op.Completed += new EventHandler(client_gmCompleted);
        }
        private void client_gmCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
             
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url + ":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }
                //System.Windows.Browser.HtmlPage.PopupWindow(new Uri(String.Format(url), UriKind.Absolute), "_blank", null);
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
             }
             
        }
        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument document = new PrintDocument();

            document.PrintPage += (s, args) =>
            {
                args.PageVisual = this.LayoutRoot;
            };
            document.Print("Vote Sheet");
        }
        
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            VoteData current = new VoteData();
            VoteNTransactionDomainService1 vtds = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;
            //VoteNTransactionDomainService1 vtds1 = (VoteNTransactionDomainService1)this.MemberResultDataSource.DomainContext;

            if (this.voteGrid.SelectedItem != null)
            {
                current = (VoteData)this.voteGrid.SelectedItem;
                for (int i = 0; i < this.voteResultDataSource.DataView.Count; i++)
                {
                    tblVoteResult tvr = (tblVoteResult)this.voteResultDataSource.DataView[i];
                    if (current.VoteID == tvr.VoteID)
                        vtds.tblVoteResults.Remove(tvr);
                }
                /*for (int j = 0; j < this.MemberResultDataSource.DataView.Count; j++)
                {
                    tblVoteMemberResult tvmr = (tblVoteMemberResult)this.MemberResultDataSource.DataView[j];
                    if (tvmr.VoteID == current.VoteID)
                    {
                        vtds1.tblVoteMemberResults.Remove(tvmr);
                        j--;
                    }
                }*/
                this.source.Remove(current);
                if(vtds.HasChanges)
                    vtds.SubmitChanges();
                /*if(vtds1.HasChanges)
                    vtds1.SubmitChanges();
                 * */
                this.voteGrid.SelectedItem = null;
            }
            else
                MessageBox.Show("Please select an item to delete", "", MessageBoxButton.OK);
        }
    }

    public class VoteTypeProvider
    {
        public List<string> VoteTypeList
        {
            get
            {
                return new List<string> { "Yes", "No", "Motion", "Absent", "Abstain" };
            }

        }
    }

    public class VoteData
    {
        public int ApplicationID { get; set; }
        public int VoteID { get; set; }
        public DateTime hearingDate { get; set; }
        public string AgencyID { get; set; }
        public string UserID { get; set; }

        public string Comments { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ModDate { get; set; }
        public DateTime EndTime { get; set; }
        public string DeptRepresentatives { get; set; }

        public string Action { get; set; }
        public string Speakers { get; set; }
        public string Decision { get; set; }

        public string AppealDescription { get; set; }
        public string[] MemberDecision {get;set;}
    }

}
