﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Windows.Browser;

namespace BOA_AppealApplication1.Views
{
    public partial class ReportPage : Page
    {
        HtmlElement m;
        public ReportPage()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.ReportPageTitle;
            this.m = HtmlPage.Document.GetElementById("ReportIFrame");
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (this.m != null)
            {
                this.m.SetAttribute("src", "ReportPage.aspx");
                this.m.SetStyleAttribute("visibility", "visible");
            }
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (this.m != null)
            {
                this.m.SetStyleAttribute("visibility", "hidden");
            }

        }

    }
}
