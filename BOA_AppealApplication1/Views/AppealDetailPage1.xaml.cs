﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.IO;
using System.Windows.Browser;
using System.Windows.Printing;

namespace BOA_AppealApplication1.Views
{
    public partial class AppealDetailPage1 : Page
    {
        App app = (App)Application.Current;
        FeesViewDomainService1 context = new FeesViewDomainService1();
        FieldTablesDomainService1 _FieldTablesDomainServices1 = new FieldTablesDomainService1();
        FileDownload fileDownloadWindow;

        public AppealDetailPage1()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.AppealDetailPageTitle;

            //Initialize all data source parameters.
            this.applicationParameter.Value = app.SessionApplicationID;
            this.appellantParameter.Value = app.SessionApplicationID;
            this.applicationParameter2.Value = ApplicationStrings.AgencyFor;
            this.ownerParameter.Value = app.SessionApplicationID;
            this.OtherContactsParameter.Value = app.SessionApplicationID;
            this.FeesViewParameter.Value = app.SessionApplicationID;
            
            //Check if payment records exist.
            InvokeOperation op = context.GetPaymentCount(app.SessionApplicationID);
            op.Completed += new EventHandler(op_Completed);

            //Get the latest Appeal Fee Paid total.
            getAppealFeeTotal();

            Fill_BriefDates(app.SessionApplicationID);
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Fill_BriefDates(int curApplicationID)
        {
            //Get the latest Appellant's Brief Date.
            EntityQuery<tblDateField> query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(curApplicationID, "AppellantBrief");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                this.appellantBriefDate.Text = dateList[0].DateValue.ToShortDateString();
            };

            //Get the latest Appellant's Reply Date.
            query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(curApplicationID, "AppellantReply");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                this.appellantReplyDate.Text = dateList[0].DateValue.ToShortDateString();
            };

            //Get the latest Appellant's / Other Parties Brief Date.
            query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(curApplicationID, "OthersBrief");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                this.respondentBriefDate.Text = dateList[0].DateValue.ToShortDateString();
            };
        }

        private void editDetailButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(String.Format("/EditAppealPage1"), UriKind.Relative));
        }

        void op_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            if (op.Value.ToString().Equals("0"))
            {
                this.paymentStatusText.Text += "NO PAYMENT RECORD!";
            }
        }

        private void getAppealFeeTotal()
        {
            FeesViewDomainService1 appDomainService = new FeesViewDomainService1();
            InvokeOperation op = appDomainService.GetPaymentTotal(app.SessionApplicationID);
            op.Completed += new EventHandler(ap_Completed);
        }

        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Get the latest Appeal Fee from the system.
            if (op.Value != null)
            { this.feePaidTXT.Text = Convert.ToDouble(op.Value).ToString("C"); }
            else
            { this.feePaidTXT.Text = "NO Payment Had Been Received."; }
        }

        private void FeesViewDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                this.paymentStatusText.Text += "NO PAYEE!";
                e.MarkErrorAsHandled();
            }
        }

        private void logButton_Click(object sender, RoutedEventArgs e)
        {
            DateLogChildWindow1 dateLogChileWindow1 = new DateLogChildWindow1();
            dateLogChileWindow1.Show();
        }
        private void PSOAButton_Click(object sender, RoutedEventArgs e)
        {
            //generate PSOA word document based on template
            
            if(fileDownloadWindow == null)
                fileDownloadWindow = new FileDownload();
            fileDownloadWindow.Show();
            DocumentGeneration dg = new DocumentGeneration();
            InvokeOperation op = dg.getPSOADoc(Int32.Parse(this.applicationParameter.Value.ToString()),"PSOA");
            op.Completed += new EventHandler(client_GetNameCompleted);
        }
        void client_GetNameCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url +":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }

                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
                //pop up file download window.
                //HtmlPage.Window.Invoke("handleWordDocGen", virtualLocalFilePath);
            }
        }
        
        private void prtDetailButton_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement elementToPrint;
            PrintDocument document = new PrintDocument();
            document.PrintPage += (s, args) =>
            {
                elementToPrint = this.LayoutRoot;
                elementToPrint.Width = args.PrintableArea.Width;
                elementToPrint.Height = args.PrintableArea.Height;
                args.PageVisual = elementToPrint;
            };
            document.Print("Printing Appeal Detail");
        }

        //generate a case report document
        private void ReceiptButton_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
