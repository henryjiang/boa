﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;


namespace BOA_AppealApplication1.Views
{
    public partial class AdminPage : Page
    {


        // number of board members, this number cannot change!
        public const int NUMBER_OF_BOARDMEMBERS = 5;
        private int _CurrentUserAccessID;
        MainUserAdminDomainContext userDC = new MainUserAdminDomainContext();
        App app = (App)Application.Current;

        public AdminPage()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.AdminPageTitle;

            //Auto-refreshing the Agency List.
            //this.departmentDataSource1.RefreshInterval = TimeSpan.FromSeconds(5);
            this.departmentParameter.Value = ApplicationStrings.AgencyFor;
            this.departmentParameter1.Value = "I"; // default to active status
            this.boardMemberParameter.Value = ApplicationStrings.AgencyFor;
            this.deptcontactarameter.Value = ApplicationStrings.AgencyFor;
            this.permitTypeParameter.Value = ApplicationStrings.AgencyFor;

            this.userStatusMsgs.LostFocus += new RoutedEventHandler(userStatusMsgs_LostFocus);
            this.mainUserDataSource.LoadedData += new EventHandler<LoadedDataEventArgs>(MainUserDataSource_Loaded);
            GetCurrentUsersAccess();
            
        }

        // Detects changes if navigating away from page
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            if (this.departmentDataSource1.HasChanges || this.permitTypeDataSource.HasChanges || this.boardMemberDataSource.HasChanges
                || deptContactDataSource1.HasChanges)
            {
                var result = MessageBox.Show("You have unsaved changes, do you want to save those changes?", "", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                    this.departmentDataSource1.SubmitChanges();
            }
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void agencySelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.agencySelection.SelectedItem != null)
                app.SessionAgencyID = (this.agencySelection.SelectedItem as tblDepartment).AgencyID.ToString();
        }

        private void boardMemberSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.BoardMembers.SelectedItem  != null)
                app.SessionBoardMemberID = (this.BoardMembers.SelectedItem as tblBoardMember).MemberID.ToString();
        }
        private void permitTypeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.PermitTypes.SelectedItem != null)
            app.SessionPermitType = (this.PermitTypes.SelectedItem as tblPermitType).PermitType.ToString();
        }
        private void addAgency_Click(object sender, RoutedEventArgs e)
        {
            Administration.DepartmentAdminChildWindow1 AddAgencyWindow = new Administration.DepartmentAdminChildWindow1(ref this.departmentDataSource1);
            AddAgencyWindow.Show();
            
            AddAgencyWindow.Closed += (s, eargs) =>
            {
                if (AddAgencyWindow.DialogResult ?? true)
                {
                    ;
                }
                    
            };
        }

        private void saveAgency_Click(object sender, RoutedEventArgs e)
        {
            if (this.departmentDataSource1.HasChanges)
            {
                //this.departmentDataSource1.SubmitChanges();
                this.departmentDataSource1.DomainContext.SubmitChanges(AdminPage_Completed, null);
            }
        }
        private void removeAgency_Click(object sender, RoutedEventArgs e)
        {
            Administration.DepartmentAdminChildWindow2 removeAgencyWindow = new Administration.DepartmentAdminChildWindow2(ref this.departmentDataSource1);
            removeAgencyWindow.Show();
        }
        private void allStatusAgency_Click(object sender, RoutedEventArgs e)
        {
            this.departmentParameter1.Value = ""; // All status
            if(this.departmentDataSource1.CanLoad)
                this.departmentDataSource1.Load();
        }
        private void activeStatusAgency_Click(object sender, RoutedEventArgs e)
        {
            this.departmentParameter1.Value = "I"; // Active status
            if (this.departmentDataSource1.CanLoad)
                this.departmentDataSource1.Load();
        }

        private void permitTypeDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                this.PermitTypes.Columns[2].IsReadOnly = true;
            }
        }
        private void departmentDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                //this.agencySelection.ItemsSource = this.departmentDataSource1.DataView;
                this.agencySelection.Columns[1].IsReadOnly = true;
            }
        }


        private void deptContactDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                this.DeptContacts.Columns[0].IsReadOnly = true;
            }
        }

        private void boardMemberDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                this.BoardMembers.Columns[0].IsReadOnly = true;
            }
        }
        private void saveBoardMember_Click(object sender, RoutedEventArgs e)
        {
            if (this.boardMemberDataSource.HasChanges)
            {
                List<string> members = new List<string>();
                for( int i=0;i<this.boardMemberDataSource.DataView.Count;i++)
                {
                    tblBoardMember tbm = (tblBoardMember)this.boardMemberDataSource.DataView[i];
                    members.Add(tbm.MemberID);
                }
                if (members.Distinct().Count() == this.boardMemberDataSource.DataView.Count)
                    this.boardMemberDataSource.DomainContext.SubmitChanges(AdminPage_Completed, null);
                //this.boardMemberDataSource.SubmitChanges();
                else
                {
                    MessageBox.Show("Member ID must be unique, please fix and save again.", "", MessageBoxButton.OK);
                }
            }
            
        }

        void AdminPage_Completed(SubmitOperation so)
        {
            if (!so.HasError)
            {
                MessageBox.Show("All changes are saved.");    
            }
            else
            {
                MessageBox.Show(string.Format("Submit Failed: {0}. Please refresh and try again.", so.Error.Message));
                so.MarkErrorAsHandled();
            }
        }
        
        private void removeBoardMember_Click(object sender, RoutedEventArgs e)
        {

            VoteNTransactionDomainService1 vtds = (VoteNTransactionDomainService1)this.boardMemberDataSource.DomainContext;
            if (this.BoardMembers.SelectedItem != null)
            {
                if ((vtds.tblBoardMembers.Count - 1) < NUMBER_OF_BOARDMEMBERS)
                {
                    MessageBox.Show("At least 5 board members is required!");
                }
                else
                {
                    Administration.RemoveBoardMember removeBoardMemberWindow = new Administration.RemoveBoardMember(ref this.boardMemberDataSource);
                    removeBoardMemberWindow.Show();
                }
            }
         
        }

        private void addBoardMember_Click(object sender, RoutedEventArgs e)
        {
            Administration.BoardMemberAdminChildWindow AddBoardMemberWindow = new Administration.BoardMemberAdminChildWindow(ref this.boardMemberDataSource);
            AddBoardMemberWindow.Show();
        }

        private void saveDeptContact_Click(object sender, RoutedEventArgs e)
        {
            if (this.deptContactDataSource1.HasChanges)
            {
                bool canSubmit = true;
                for (int i = 0; i < this.deptContactDataSource1.DataView.Count; i++)
                {
                    tblDeptContact tbm = (tblDeptContact)this.deptContactDataSource1.DataView[i];
                    if (tbm.AgencyID == "" )
                    {
                        canSubmit = false;
                    }
                }
                if (!canSubmit)
                    MessageBox.Show("Agency ID is required , please fix and save again.", "", MessageBoxButton.OK);
                else
                    //this.deptContactDataSource1.SubmitChanges();
                    this.deptContactDataSource1.DomainContext.SubmitChanges(AdminPage_Completed,null);
            }
            
        }
      
        private void removeDeptContact_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 vtds = (VoteNTransactionDomainService1)this.deptContactDataSource1.DomainContext;
            if (this.DeptContacts.SelectedItem != null)
            {
                tblDeptContact current = (tblDeptContact)this.DeptContacts.SelectedItem;
                vtds.tblDeptContacts.Remove(current);
                vtds.SubmitChanges();
            }
        }

        private void addDeptContact_Click(object sender, RoutedEventArgs e)
        {
            VoteNTransactionDomainService1 vtds = (VoteNTransactionDomainService1)this.deptContactDataSource1.DomainContext;
           
            Administration.DeptContact AddDeptContactWindow = new Administration.DeptContact();
            AddDeptContactWindow.Show();
            //this.deptContactDataSource1.DataView.Add(newDeptContact);
        }

        private void addPermitType_Click(object sender, RoutedEventArgs e)
        {
            Administration.PermitTypeAdminChildWindow addPermitTypeWindow = new Administration.PermitTypeAdminChildWindow(ref this.permitTypeDataSource);
            addPermitTypeWindow.Show();
        }
        private void savePermitType_Click(object sender, RoutedEventArgs e)
        {
            if (this.permitTypeDataSource.HasChanges)
            {
                this.permitTypeDataSource.DomainContext.SubmitChanges(AdminPage_Completed, null);
            }
        }
        private void removePermitType_Click(object sender, RoutedEventArgs e)
        {
            Administration.DeletePermitType removePermitTypeWindow = new Administration.DeletePermitType(ref this.permitTypeDataSource);
            removePermitTypeWindow.Show();
        }

        private void UserAdmin_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.UserAdminPanel.Visibility == Visibility.Collapsed)
            {
                this.UserAdminPanel.Visibility = Visibility.Visible;
                this.AgencyAdminPanel.Visibility = Visibility.Collapsed;
                this.DeptContactAdminPanel.Visibility = Visibility.Collapsed;
                this.BoardMemberAdminPanel.Visibility = Visibility.Collapsed;
                this.PermitTypeAdminPanel.Visibility = Visibility.Collapsed;
            }
            else { this.UserAdminPanel.Visibility = Visibility.Collapsed; }
        }

        private void AgencyAdmin_HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.AgencyAdminPanel.Visibility == Visibility.Collapsed)
            {
                this.UserAdminPanel.Visibility = Visibility.Collapsed;
                this.AgencyAdminPanel.Visibility = Visibility.Visible;
                this.DeptContactAdminPanel.Visibility = Visibility.Collapsed;
                this.BoardMemberAdminPanel.Visibility = Visibility.Collapsed;
                this.PermitTypeAdminPanel.Visibility = Visibility.Collapsed;
            }
            else { this.AgencyAdminPanel.Visibility = Visibility.Collapsed; }
        }

        private void boardmember_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.BoardMemberAdminPanel.Visibility == Visibility.Collapsed)
            {
                this.UserAdminPanel.Visibility = Visibility.Collapsed;
                this.AgencyAdminPanel.Visibility = Visibility.Collapsed;
                this.DeptContactAdminPanel.Visibility = Visibility.Collapsed;
                this.PermitTypeAdminPanel.Visibility = Visibility.Collapsed;
                this.BoardMemberAdminPanel.Visibility = Visibility.Visible;
            }
            else
            {
                this.BoardMemberAdminPanel.Visibility = Visibility.Collapsed;
            }

        }
        private void deptcontact_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DeptContactAdminPanel.Visibility == Visibility.Collapsed)
            {
                this.UserAdminPanel.Visibility = Visibility.Collapsed;
                this.AgencyAdminPanel.Visibility = Visibility.Collapsed;
                this.BoardMemberAdminPanel.Visibility = Visibility.Collapsed;
                this.PermitTypeAdminPanel.Visibility = Visibility.Collapsed;
                this.DeptContactAdminPanel.Visibility = Visibility.Visible;
            }
            else
            {
                this.DeptContactAdminPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void permittype_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PermitTypeAdminPanel.Visibility == Visibility.Collapsed)
            {
                this.UserAdminPanel.Visibility = Visibility.Collapsed;
                this.AgencyAdminPanel.Visibility = Visibility.Collapsed;
                this.BoardMemberAdminPanel.Visibility = Visibility.Collapsed;
                this.DeptContactAdminPanel.Visibility = Visibility.Collapsed;
                this.PermitTypeAdminPanel.Visibility = Visibility.Visible;
            }
            else
            {
                this.PermitTypeAdminPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void mainUserDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                //this.agencySelection.ItemsSource = this.departmentDataSource1.DataView;
            }  
        }

        private void mainUserSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tblMainUser currentRow = (tblMainUser)this.userSelection.SelectedItem;

            if (currentRow != null)
            {
                app.SessionMainUserID = currentRow.MainUserID.ToString();

                switch (currentRow.IsActive)
                {
                    case "Active": { this.removeMainUser.Content = "Retire"; break; }
                    case "Inactive": { this.removeMainUser.Content = "Reactivate"; break; }
                }

            }
        }


       private void removeMainUser_Click(object sender, RoutedEventArgs e)
        {

            tblMainUser currentRow = (tblMainUser)this.userSelection.SelectedItem;
            if (currentRow.UserID == "Admin")
            {
                DisplayUserAdminMessage("The Admin account cannot be retired.");
                userStatusMsgs.Visibility = System.Windows.Visibility.Visible;
                userStatusMsgs.Focus();
                return;
            }

            userStatusMsgs.Visibility = System.Windows.Visibility.Collapsed;
            Administration.RetireMainUser removeMainUserWindow = new Administration.RetireMainUser(ref this.mainUserDataSource);
            removeMainUserWindow.Show();
        }

        private void saveMainUser_Click(object sender, RoutedEventArgs e)
        {

            if (this.mainUserDataSource.HasChanges)
            {
                this.mainUserDataSource.DomainContext.SubmitChanges(AdminPage_Completed, null);
            }
        }

        private void addMainUser_Click(object sender, RoutedEventArgs e)
        {
            Administration.AddMainUser AddMainUserWindow = new Administration.AddMainUser(ref this.mainUserDataSource);
            AddMainUserWindow.Show();

            AddMainUserWindow.Closed += (s, eargs) =>
            {
                if (AddMainUserWindow.DialogResult ?? true)
                {
                    ;
                }

            };

        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            tblMainUser currentRow = (tblMainUser)this.userSelection.SelectedItem;
            String selectedUserID = currentRow.UserID;

            String loggedInUser = WebContext.Current.User.Name;

            if (!selectedUserID.Equals(loggedInUser,StringComparison.OrdinalIgnoreCase))
            {
                if (_CurrentUserAccessID != 10)
                {
                    DisplayUserAdminMessage("You cannot change another user's password. Please select your account and click Change Password again.");
                    return;
                }
            }

            if (currentRow.UserID == "Admin")
            {
                DisplayUserAdminMessage("The Admin password cannot be changed");
                return;
            }
             
            userStatusMsgs.Visibility = System.Windows.Visibility.Collapsed;
            Administration.ChangePassword changePasswordWindow = new Administration.ChangePassword(ref this.userStatusMsgs);
            changePasswordWindow.Closed += new EventHandler(changePasswordWindow_Closed);
            changePasswordWindow.Show();
        }

        private void DisplayUserAdminMessage(String message)
        {
            userStatusMsgs.Text = message;
            userStatusMsgs.Visibility = System.Windows.Visibility.Visible;
            userStatusMsgs.Focus();
        }
        void userStatusMsgs_LostFocus(object sender, RoutedEventArgs e)
        {
            if (userStatusMsgs.Text.Length > 0)
            {
                userStatusMsgs.Visibility = Visibility.Collapsed;
            }
        }

        void MainUserDataSource_Loaded(object sender, LoadedDataEventArgs e) 
        {
            tblMainUser currentUser = (tblMainUser)userSelection.SelectedItem;
            if (currentUser == null) return;

            switch (currentUser.IsActive)
            {
                case "Active": { this.removeMainUser.Content = "Retire"; break; }
                case "Inactive": { this.removeMainUser.Content = "Reactivate"; break; }
            }

        }

        void changePasswordWindow_Closed(object sender, EventArgs e)
        {
            if (userStatusMsgs.Text.Length > 0)
            {
                userStatusMsgs.Visibility = Visibility.Visible;
                userStatusMsgs.Focus();
            }
        }
        private void statusMainUser_Click(object sender, RoutedEventArgs e)
        {
            String status = this.statusMainUser.Content.ToString();

            switch (status)
            {
                case "Active Only": 
                { 
                    this.statusMainUser.Content = "Show All";
                    this.statusParameter.Value = "Active"; // All active users
                    if (this.mainUserDataSource.CanLoad)this.mainUserDataSource.Load();                    
                    break; 
                }
                case "Show All": 
                { 
                    this.statusMainUser.Content = "Active Only";
                    this.statusParameter.Value = "Active,Inactive"; // All status
                    if (this.mainUserDataSource.CanLoad) this.mainUserDataSource.Load(); 
                    break;
                }
            }
        }

        private void GetCurrentUsersAccess()
        {
            String currentUserName = WebContext.Current.User.DisplayName;
            EntityQuery<tblMainUser> query =
                from u in userDC.GetTblMainUserByUserIdQuery(currentUserName)
                select u;
            LoadOperation<tblMainUser> loadOp = userDC.Load(query, currentUserLoaded, null);
        
        }

        void currentUserLoaded(LoadOperation so)
        {
            EntitySet<tblMainUser> currentUser = userDC.tblMainUsers;
            _CurrentUserAccessID = (Int32)currentUser.First().AccessGroupID;
        }
    }



    public class BoardMembersData
    {
        string MemberID { set; get; }
        string FirstName { set; get; }
        string LastName { set; get; }
        string Title { set; get; }
    }

    public class permityTypeData
    {
        string PermitName { set; get; }
        string PermitType { set; get; }
        string DeptID { set; get; }
    }
    public class departmentsData
    {
        string DeptName { set; get; }
        string AgencyID { set; get; }
    }
    public class deptContactsData
    {
        string AgencyID { set; get; }
        string NameDescription { set; get; }
        string FirstName { set; get; }
        string LastName { set; get; }
        string AddressLine1 { set; get; }
        string AddressLine2 { set; get; }
        string PhoneNumber { set; get; }
    }

   


}
