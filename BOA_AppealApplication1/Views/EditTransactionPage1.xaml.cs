﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;

namespace BOA_AppealApplication1.Views
{
    public partial class EditTransactionPage1 : Page
    {
        public tblTransHistory newPayment { get; set; }
        
        App app = (App)Application.Current;
        
        public EditTransactionPage1()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.EditTransactionPageTitle;
            this.FeesViewParameter.Value = app.SessionApplicationID;
            this.namesParameter.Value = app.SessionApplicationID;
            //this.applicationParameter.Value = app.SessionApplicationID;
            //this.applicationParameter2.Value = ApplicationStrings.AgencyFor;

            //Open a blank transaction form for new data entry.
            newPayment = new tblTransHistory();
            PaymentDataForm.CurrentItem = newPayment;
            this.PaymentDataForm.BeginEdit();

            //Get the latest Appeal Fee Paid total.
            getAppealFeeTotal();
            getPaymentTotal();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void op_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            if (op.Value.ToString().Equals("0"))
            {
                this.paymentStatusText.Text += "NO PAYMENT RECORD!";
            }
        }

        private void FeesViewDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                this.payeeStatusText.Text += "NO PAYEE!";
                e.MarkErrorAsHandled();
            }
        }

        private void TransactionDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                this.paymentStatusText.Text += "NO PAYMENT RECORD!";
                e.MarkErrorAsHandled();
            }
        }

        private void namesDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                this.payeeStatusText.Text += "NO PAYEE!";
                e.MarkErrorAsHandled();
            }
        }

        //Add a new payment record.
        private void OKButton_Click(object sender, EventArgs e)
        {
            newPayment.ApplicationID = app.SessionApplicationID;
            newPayment.Date = DateTime.Now;
            newPayment.ModDate = DateTime.Now;
            newPayment.PayeeID = (this.namesGrid.SelectedItem as tblNameField).NameDescription.ToString();
            newPayment.UserID = WebContext.Current.User.DisplayName.ToString();
            this.PaymentDataForm.CommitEdit();
            VoteNTransactionDomainService1 _VoteNTransactionDomainService1 = (VoteNTransactionDomainService1)(TransactionDataSource.DomainContext);
            _VoteNTransactionDomainService1.tblTransHistories.Add(newPayment);
            TransactionDataSource.SubmitChanges();
            this.NavigationService.Navigate(new Uri(String.Format("/AppealDetailPage1"), UriKind.Relative));
        }

        private void SkipButton_Click(object sender, RoutedEventArgs e)
        {
            this.PaymentDataForm.CancelEdit();
            MessageBox.Show("Non-saved Payment Record is Disregarded!", "Skip Payment Record", MessageBoxButton.OK);
            this.NavigationService.Navigate(new Uri(String.Format("/AppealDetailPage1"), UriKind.Relative));
        }

        private void ReceiptButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void TransactionDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show(e.Error.ToString(), "Error", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        //Set Payee information to the transaction record from the tblNameField table to reserve consistency.
        private void namesGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //newPayment.PayeeID = (this.namesGrid.SelectedItem as tblNameField).NameDescription.ToString();
        }

        private void applicationDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show(e.Error.ToString(), "Error", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            else
            {
                this.paymentAmountTXT.Text = this.feeRequiresTXT.Text.ToString().Replace('$', ' ');
            }
        }

        private void getAppealFeeTotal()
        {
            FeesViewDomainService1 appDomainService = new FeesViewDomainService1();
            InvokeOperation op = appDomainService.GetPaymentTotal(app.SessionApplicationID);
            op.Completed += new EventHandler(ap_Completed);
        }

        private void getPaymentTotal()
        {
            //Get the latest Appeal Fee.
            ApplicationDomainService1 _applicationDomainService1 = new ApplicationDomainService1();
            EntityQuery<view_applications> query = _applicationDomainService1.Getview_applicationsDetailsQuery(app.SessionApplicationID, ApplicationStrings.AgencyFor);
            _applicationDomainService1.Load<view_applications>(query).Completed += (sender, args) =>
            {
                List<view_applications> applicationList = ((LoadOperation<view_applications>)sender).Entities.ToList();
                if (applicationList[0].InitialFee != null)
                {
                    this.paymentAmountTXT.Text = Convert.ToDouble(applicationList[0].InitialFee).ToString();
                    this.feeRequiresTXT.Text = Convert.ToDouble(applicationList[0].InitialFee).ToString("C");
                }
            };
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Get the latest Appeal Fee from the system.
            if (op.Value != null)
            {
                this.feePaidTXT.Text = Convert.ToDouble(op.Value).ToString("C");
                //this.paymentAmountTXT.Text = this.feePaidTXT.Text.Replace('$', ' ');
            }
            else
            { this.feePaidTXT.Text = "NO Payment Had Been Received."; }
        }


    }

    public class PaymentTypeProvider
    {
        public List<string> PaymentTypeList
        {
            get
            {
                return new List<string> { "Appeal Fee", "Adjustment", "Wavier" };
            }

        }
    }
}
