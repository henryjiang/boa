﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;


namespace BOA_AppealApplication1.Views
{
    public partial class EditAppealPage1 : Page
    {
        //Initiating all objects
        //public tblApplication newApplication { get; set; }
        public tblNameField newOwner { get; set; }
        public tblNameField newAppellant { get; set; }
        public tblItemDetail newItemDetail { get; set; }

        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        bool noPriOwner = false;
        bool noPriAppellant = false;
        bool noItemDetail = false;
        string tempBriefDate1, tempBriefDate2, tempBriefDate3;
        const string otherDepartmentalAction = "OTHER ACTION";
        const string NOD_REL = "NOD REL";
        const string otherPermitTypeText = "Other Permit Type";

                
        App app = (App)Application.Current;
        //public tblNameField tempName { get; set; }
        ApplicationDomainService1 applicationContext = new ApplicationDomainService1();
        OwnerDomainService1 context = new OwnerDomainService1();
        FieldTablesDomainService1 _FieldTablesDomainServices1 = new FieldTablesDomainService1();

        public EditAppealPage1()
        {
            InitializeComponent();

            this.applicationParameter.Value = app.SessionApplicationID;
            this.appellantParameter.Value = app.SessionApplicationID;
            this.ownerParameter.Value = app.SessionApplicationID;
            this.typeParameter.Value = ApplicationStrings.AgencyFor;
            this.OutcomeParameter.Value = ApplicationStrings.AgencyFor;
            this.statusParameter.Value = ApplicationStrings.AgencyFor;
            this.applicationParameter2.Value = ApplicationStrings.AgencyFor;
            this.actionParameter.Value = ApplicationStrings.AgencyFor;
            this.applicationDetailsParameter.Value = app.SessionApplicationID;
            this.applicationDetailsParameter2.Value = ApplicationStrings.AgencyFor;
            //this.Title = ApplicationStrings.EditAppealPageTitle;

            if ("REG" == app.SessionRequestType)
            {
                this.Title = "Editing Appeal NO.: " + app.SessionApplicationID;
            }
            else
            {
                this.Title = "Editing Jurisdiction Request NO.: " + app.SessionApplicationID;            
            }
            this.HeaderText.Text = this.Title;
            InvokeOperation op = context.GetOwnerCount(app.SessionApplicationID);
            op.Completed += new EventHandler(op_Completed);

            InvokeOperation op2 = context.GetAppellantCount(app.SessionApplicationID);
            op2.Completed += new EventHandler(op2_Completed);

            InvokeOperation op3 = applicationContext.checkExistingDetails(app.SessionApplicationID, ApplicationStrings.AgencyFor);
            op3.Completed += new EventHandler(op3_Completed);

            this.applicationDetailsDataSource.SubmittedChanges +=new EventHandler<SubmittedChangesEventArgs>(applicationDetailsDataSource_SubmittedChanges);

            Fill_BriefDates();
            
            //Set brief date counter if Hearing was not entered.
            if (this.hDate.Text.ToString() == "")
            { count4 = 1; }
        }

        void applicationDetailsDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //MessageBox.Show("Owner Information HAS NOT Been Entered!", "No Owner Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Fill_BriefDates()
        {
            //Get the latest Appellant's Brief Date.
            EntityQuery<tblDateField> query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(app.SessionApplicationID, "AppellantBrief");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                if (((LoadOperation<tblDateField>)sender).Entities.Count() > 0 )
                {
                    List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                    this.briefDate.Text = dateList[0].DateValue.ToShortDateString();
                    tempBriefDate1 = dateList[0].DateValue.ToShortDateString();
                }
            };
                        
            //Get the latest Appellant's Reply Date.
            query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(app.SessionApplicationID, "AppellantReply");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                if (((LoadOperation<tblDateField>)sender).Entities.Count() > 0)
                {
                    List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                    this.appellantReplyDate.Text = dateList[0].DateValue.ToShortDateString();
                    tempBriefDate2 = dateList[0].DateValue.ToShortDateString();
                }
            };

            //Get the latest Appellant's / Other Parties Brief Date.
            query = _FieldTablesDomainServices1.GetSpecificTblDateFieldsQuery(app.SessionApplicationID, "OthersBrief");
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                if (((LoadOperation<tblDateField>)sender).Entities.Count() > 0)
                {
                    List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                    this.othersBriefDate.Text = dateList[0].DateValue.ToShortDateString();
                    tempBriefDate3 = dateList[0].DateValue.ToShortDateString();
                }
            };
        }

        //Update all required Brief and Reply dates.
        private void Update_BriefDates(int sessionApplicationID)
        {
            FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);
            //Checking File Date modification and perform logging if it is needed.
            if (this.fDate.SelectedDate.ToString() != this.oldFiledString.Text.ToString())
            {
                tblDateField newFileDate = new tblDateField();
                //newFileDate.DateValue = new DateTime(fDate.SelectedDate.Value.Year, fDate.SelectedDate.Value.Month, fDate.SelectedDate.Value.Day, fTime.Value.Value.Hour, fTime.Value.Value.Minute, fTime.Value.Value.Second);
                newFileDate.DateValue = new DateTime(fDate.SelectedDate.Value.Year, fDate.SelectedDate.Value.Month, fDate.SelectedDate.Value.Day);
                newFileDate.ApplicationID = sessionApplicationID;
                newFileDate.ModDate = DateTime.Now;
                newFileDate.DateDescription = "Modified File Date";
                newFileDate.DateType = "FiledDate";
                newFileDate.Comments = "Through appeal editing.";
                newFileDate.UserID = WebContext.Current.User.DisplayName.ToString();
                _FieldTableDomainServices1.tblDateFields.Add(newFileDate);
            }

            //Checking Hearing Date modification and perform logging if it is needed.
            if (this.hDate.SelectedDate.ToString() != this.oldHearingString.Text.ToString())
            {
                tblDateField newHearingDate = new tblDateField();
                newHearingDate.DateValue = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 17, 0, 0);
                newHearingDate.ApplicationID = sessionApplicationID;
                newHearingDate.ModDate = DateTime.Now;
                newHearingDate.DateDescription = "Modified Hearing Date";
                newHearingDate.DateType = "HearingDate";
                newHearingDate.Comments = "Through appeal editing.";
                newHearingDate.UserID = WebContext.Current.User.DisplayName.ToString();
                _FieldTableDomainServices1.tblDateFields.Add(newHearingDate);
            }

            if (this.briefDate.SelectedDate != null && ((DateTime)this.briefDate.SelectedDate).ToShortDateString() != tempBriefDate1)
            {
                tblDateField DateBrief = new tblDateField();
                DateBrief.DateValue = new DateTime(briefDate.SelectedDate.Value.Year, briefDate.SelectedDate.Value.Month, briefDate.SelectedDate.Value.Day, 16, 0, 0);
                DateBrief.ApplicationID = sessionApplicationID;
                DateBrief.ModDate = DateTime.Now;
                DateBrief.DateDescription = "Modified Appellant's Brief Date";
                DateBrief.DateType = "AppellantBrief";
                DateBrief.Comments = "Through appeal editing.";
                DateBrief.UserID = WebContext.Current.User.DisplayName.ToString();
                _FieldTableDomainServices1.tblDateFields.Add(DateBrief);
            }

            if (this.appellantReplyDate.SelectedDate != null && ((DateTime)this.appellantReplyDate.SelectedDate).ToShortDateString() != tempBriefDate2)
            {
                tblDateField AppellantReply = new tblDateField();
                AppellantReply.DateValue = new DateTime(appellantReplyDate.SelectedDate.Value.Year, appellantReplyDate.SelectedDate.Value.Month, appellantReplyDate.SelectedDate.Value.Day, 16, 0, 0);
                AppellantReply.ApplicationID = sessionApplicationID;
                AppellantReply.ModDate = DateTime.Now;
                AppellantReply.DateDescription = "Modified Appellant's Reply Date";
                AppellantReply.DateType = "AppellantReply";
                AppellantReply.Comments = "Through appeal editing.";
                AppellantReply.UserID = WebContext.Current.User.DisplayName.ToString();
                _FieldTableDomainServices1.tblDateFields.Add(AppellantReply);
            }

            if (this.othersBriefDate.SelectedDate != null && ((DateTime)this.othersBriefDate.SelectedDate).ToShortDateString() != tempBriefDate3)
            {
                tblDateField OthersBrief = new tblDateField();
                OthersBrief.DateValue = new DateTime(othersBriefDate.SelectedDate.Value.Year, othersBriefDate.SelectedDate.Value.Month, othersBriefDate.SelectedDate.Value.Day, 16, 0, 0);
                OthersBrief.ApplicationID = sessionApplicationID;
                OthersBrief.ModDate = DateTime.Now;
                OthersBrief.DateDescription = "Modified Respondent's / Other Parties' Brief Date";
                OthersBrief.DateType = "OthersBrief";
                OthersBrief.Comments = "Through appeal editing.";
                OthersBrief.UserID = WebContext.Current.User.DisplayName.ToString();
                _FieldTableDomainServices1.tblDateFields.Add(OthersBrief);
            }

            FieldDataSource1.SubmitChanges();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            //Update Appeal application information.
            if (this.newAppealDataForm.CurrentItem != null)
            {
                if (this.newAppealDataForm.IsItemValid)
                {
                    newAppealDataForm.CommitEdit();

                    //Update all Briefing Dates
                    Update_BriefDates(app.SessionApplicationID);

                    //Insert Appeal Item Details Information
                    if (this.detailDataForm.CurrentItem != null)
                    {
                        if (this.detailDataForm.IsItemValid)
                        {
                            //detailDataForm.CommitEdit();
                            if (noItemDetail)
                            {
                                newItemDetail.ModDate = DateTime.Now;
                                newItemDetail.AgencyID = ApplicationStrings.AgencyFor;
                                newItemDetail.Related_Agency = agencyText.Text;
                                newItemDetail.ApplicationID = app.SessionApplicationID;
                                newItemDetail.Action = this.actionTXT.Text;

                                detailDataForm.CommitEdit();
                                ApplicationDomainService1 _ItemDetailDomainServer1 = (ApplicationDomainService1)(applicationDetailsDataSource.DomainContext);
                                _ItemDetailDomainServer1.tblItemDetails.Add(newItemDetail);
                            }
                            else
                            {
                                this.applicationDetailsDataSource.SubmitChanges();
                            }
                        }
                    }
                    try
                    {
                        applicationDataSource.SubmitChanges();
                    }
                    catch (Exception ex1)
                    {
                        string message = ex1.InnerException.ToString();
                    }
                }
            }
            //Insert Owner Information
            if (this.newOwnerDataForm.CurrentItem != null && this.oFName.Text != "" && this.oLName.Text != "")
            {
                if (this.newOwnerDataForm.IsItemValid)
                {
                    newOwnerDataForm.CommitEdit();
                    if (noPriOwner)
                    {
                        newOwner.NameDescription = newOwner.FirstName + newOwner.MiddleName + newOwner.LastName;
                        newOwner.Type = "Respondent";
                        newOwner.ApplicationID = app.SessionApplicationID;
                        newOwnerDataForm.CommitEdit();
                        OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(OwnerDataSource1.DomainContext);
                        _OwnerDomainServer1.tblNameFields.Add(newOwner);
                    }
                    OwnerDataSource1.SubmitChanges();
                }
                else { this.newOwnerDataForm.CancelEdit(); }
            }
            else { this.newOwnerDataForm.CancelEdit(); }

            //Insert Appellant Information
            if (this.newAppellantDataForm.CurrentItem != null && this.aFName.Text != "" && this.aLName.Text != "")
            {
                if (this.newAppellantDataForm.IsItemValid)
                {
                    newAppellantDataForm.CommitEdit();
                    if (noPriAppellant)
                    {
                        newAppellant.NameDescription = newAppellant.FirstName + newAppellant.MiddleName + newAppellant.LastName;
                        newAppellant.Type = "Primary Appellant";
                        newAppellant.ApplicationID = app.SessionApplicationID;
                        newAppellantDataForm.CommitEdit();
                        OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                        _OwnerDomainServer1.tblNameFields.Add(newAppellant);
                    }
                    AppellantDataSource1.SubmitChanges();
                }
                else { this.newAppellantDataForm.CancelEdit(); }
            }
            else { this.newAppellantDataForm.CancelEdit(); }
            
            if (this.newAppellantDataForm.IsItemValid && this.newOwnerDataForm.IsItemValid && this.newAppealDataForm.IsItemValid && this.detailDataForm.IsItemValid)
            {
                this.NavigationService.Navigate(new Uri(String.Format("/AdditionalNewContactsPage1"), UriKind.Relative));
            }
           
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.newAppealDataForm.CancelEdit();
            this.newAppellantDataForm.CancelEdit();
            this.newOwnerDataForm.CancelEdit();
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }

        private void getAppealFee(string appealType)
        {
            ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
            InvokeOperation op = appDomainService.GetPermitFee(ApplicationStrings.AgencyFor, appealType);
            op.Completed += new EventHandler(ap_Completed);
        }

        //Update the textbox to store the initial fee from the selected case type.
        void ap_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            //Get the latest Appeal Fee from the system.
            this.initialFeeTXT.Text = Convert.ToDouble(op.Value).ToString(",0.00");
        }

        private void caseTypeComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            count1++;
        }
       
        private void statusComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            count2++;
        }

        private void statusSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
             if (count2++ > 1 && this.statusSelection.SelectedValue != null) {
                 this.statusText.Text = this.statusSelection.SelectedValue.ToString(); 
             }
             this.outcomePanel.UpdateLayout();
            if (this.statusText.Text == NOD_REL)
             {
                 //this.outcomeSelection.Visibility = System.Windows.Visibility.Visible;
                 //this.outcomeText.Visibility = System.Windows.Visibility.Visible;
                 this.outcomePanel.Visibility = System.Windows.Visibility.Visible;
                 //this.outcomeSelection.SelectedValue = this._outcomeText.Text;
             }
             else
             {
                 //this.outcomeSelection.Visibility = System.Windows.Visibility.Collapsed;
                 //this.outcomeText.Visibility = System.Windows.Visibility.Collapsed;
                 this.outcomePanel.Visibility = System.Windows.Visibility.Collapsed;
             }
        }

        private void typeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.tempText.Text = this.typeSelection.SelectedValue.ToString() + "-" + this.typeSelection.SelectedItem.GetType().ToString() + "-" + something.ToString() + "-" + count1++;
            if (count1++ > 1)
            {
                this.typeText.Text = (this.typeSelection.SelectedValue == null) ? "" : this.typeSelection.SelectedValue.ToString();
                //getAppealFee(this.typeSelection.SelectedValue.ToString());
            }
            if (this.typeText.Text.ToString() == otherDepartmentalAction)
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
                this.typeText.UpdateLayout();
                this.typeText2.Focus();
                this.typeText2.SelectAll();
            }
            else
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void OwnerDataSource1_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void testButton_Click(object sender, RoutedEventArgs e)
        {
            this.newOwnerDataForm.CancelEdit();
            newOwner = new tblNameField();
            newOwnerDataForm.CurrentItem = newOwner;
        }

        void op_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            
            if (op.Value.ToString().Equals("0"))
            {
                this.newOwnerDataForm.CancelEdit();
                this.newOwnerDataForm.ItemsSource = null;
                newOwner = new tblNameField();
                newOwnerDataForm.CurrentItem = newOwner;
                noPriOwner = true;
            }
        }


        void op2_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            if (op.Value.ToString().Equals("0"))
            {
                this.newAppellantDataForm.CancelEdit();
                this.newAppellantDataForm.ItemsSource = null;
                newAppellant = new tblNameField();
                newAppellantDataForm.CurrentItem = newAppellant;
                noPriAppellant = true;
            }
        }

        //Open a new ItemDetail DataForm if there is no record found.
        void op3_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;

            if (op.Value.Equals(false))
            {
                this.detailDataForm.CancelEdit();
                this.detailDataForm.ItemsSource = null;
                newItemDetail = new tblItemDetail();
                detailDataForm.CurrentItem = newItemDetail;
                noItemDetail = true;
            }
        }

        private void AppellantDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Appellant Information HAS NOT Been Entered!", "No Appellant Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        private void OwnerDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Owner Information HAS NOT Been Entered!", "No Owner Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        //Avoid user from selecting invalid hearing date.
        private void fDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            this.hDate.DisplayDateStart = this.fDate.SelectedDate;
            //if (fTime.Value != null && fDate.SelectedDate != null)
            if (fDate.SelectedDate != null)
            {
                this.filedTempString.Text = fDate.SelectedDate.Value.Month + "/" + fDate.SelectedDate.Value.Day + "/" + fDate.SelectedDate.Value.Year;
            }
        }

        //private void fTime_ValueChanged(object sender, RoutedEventArgs e)
        //{
        //    if (fTime.Value != null && fDate.SelectedDate != null)
        //    {
        //        this.filedTempString.Text = fDate.SelectedDate.Value.Month + "/" + fDate.SelectedDate.Value.Day + "/" + fDate.SelectedDate.Value.Year + " " + fTime.Value.Value.Hour + ":" + fTime.Value.Value.Minute + ":" + fTime.Value.Value.Second;
        //    }
        //    else if (fDate.SelectedDate == null)
        //    { MessageBox.Show("File Date HAS NOT Been Selected!", "File Date HAS NOT Been Selected!", MessageBoxButton.OK); }
        //}

        private void hDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime tempDate;

            if (hDate.SelectedDate != null)
            {
                this.hearingTempString.Text = hDate.SelectedDate.Value.Month + "/" + hDate.SelectedDate.Value.Day + "/" + hDate.SelectedDate.Value.Year + " " + "17:00";
            }

            //if (this.hDate.SelectedDate == null)
            //{
            //    count4++;
            //}

            //count4 is used to prevent orginal value is modified when application is loaded.
            if (count4++ > 0)
            {
                tempDate = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 16, 0, 0);
                this.briefDate.SelectedDate = tempDate.AddDays(-27);
                this.othersBriefDate.SelectedDate = tempDate.AddDays(-13);
                this.appellantReplyDate.SelectedDate = tempDate.AddDays(-6);
            }
            
        }

        //private void hTime_ValueChanged(object sender, RoutedEventArgs e)
        //{
        //    if (hTime.Value != null && hDate.SelectedDate != null)
        //    {
        //        this.hearingTempString.Text = hDate.SelectedDate.Value.Month + "/" + hDate.SelectedDate.Value.Day + "/" + hDate.SelectedDate.Value.Year + " " + hTime.Value.Value.Hour + ":" + hTime.Value.Value.Minute + ":" + hTime.Value.Value.Second;
        //    } else if (hDate.SelectedDate == null)
        //    { MessageBox.Show("Hearing Date HAS NOT Been Selected!", "Hearing Date HAS NOT Been Selected!", MessageBoxButton.OK); }
        //}

        private void agencySelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (count3++ > 1)
            {
                this.agencyText.Text = this.agencySelection.SelectedValue.ToString();
            }
        }

        private void agencySelection_Loaded(object sender, RoutedEventArgs e)
        {
            count3++;
        }

        private void departmentDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
        }

        private void applicationDetailsDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
        }

        private void applicationDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
        }

        private void logButton_Click(object sender, RoutedEventArgs e)
        {
            DateLogChildWindow1 dateLogChileWindow1 = new DateLogChildWindow1();
            dateLogChileWindow1.Show();
        }

        private void actionSelection_DropDownClosed(object sender, EventArgs e)
        {
            if (this.actionSelection.SelectedValue != null)
            {
                this.actionTXT.Text = this.actionSelection.SelectedValue.ToString();
                getAppealFee(this.actionSelection.SelectedValue.ToString());
                this.permitTypeText.Text = this.actionSelection.SelectedValue.ToString();

                if (this.permitTypeText.Text == otherPermitTypeText)
                {
                    this.otherPermitType.Visibility = System.Windows.Visibility.Visible;
                    this.permitTypeText2.Text = "";
                }
                else
                {
                    this.otherPermitType.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        
        private void otherAction_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if other action, use the other action text rather than the value from dropdown box.
            if (this.typeText.Text.ToString() == otherDepartmentalAction)
            {
                this.typeText.Text  = this.typeText2.Text;
            }
        }

        private void outcomeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.outcomeSelection.SelectedValue != null && this.outcomeSelection.SelectedValue.ToString() != "")
                this._outcomeText.Text = this.outcomeSelection.SelectedValue.ToString();
        }

        private void permitType_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.permitTypeText.Text = this.actionTXT.Text = this.permitTypeText2.Text;
        }

        private void ContactsPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ContactsPanel.Visibility == Visibility.Collapsed)
                this.ContactsPanel.Visibility = Visibility.Visible;
            else
                this.ContactsPanel.Visibility = Visibility.Collapsed;
        }

        private void PermitPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PermitPanel.Visibility == Visibility.Collapsed)
                this.PermitPanel.Visibility = Visibility.Visible;
            else
                this.PermitPanel.Visibility = Visibility.Collapsed;
        }

        private void StatusPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.StatusPanel.Visibility == Visibility.Collapsed)
                this.StatusPanel.Visibility = Visibility.Visible;
            else
                this.StatusPanel.Visibility = Visibility.Collapsed;
        }

        private void DeptPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DeptPanel.Visibility == Visibility.Collapsed)
                this.DeptPanel.Visibility = Visibility.Visible;
            else
                this.DeptPanel.Visibility = Visibility.Collapsed;
        }
    }
}