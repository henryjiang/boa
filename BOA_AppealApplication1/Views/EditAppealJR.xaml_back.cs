﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Printing;

namespace BOA_AppealApplication1.Views
{
    public partial class EditAppealJR : Page
    {
        /// <summary>
        /// data entities
        /// </summary>
        public tblApplications_JR newApplication_jR { get; set; }
        public tblItemDetail newItemDetail { get; set; }
        public tblNameField newOwner { get; set; }
        public tblNameField newAppellant { get; set; }
        public tblDateField newDateEntry { get; set; }

        /// <summary>
        /// misc
        /// </summary>
        private string source { get; set; }
        const string otherDepartmentalAction = "OTHER ACTION";
        const string otherPermitTypeText = "Other Permit Type";
        const string NOD_REL = "NOD REL";
        private Boolean init_date { get; set; }

        private DateTime oHDate { get; set; }
        private DateTime oFDate { get; set; }
        private DateTime oEDate { get; set; }

        private DateTime oIcfDate { get; set; }
        private DateTime oPcmDate { get; set; }
        private DateTime oLnmDate { get; set; }

        FileDownload fileDownloadWindow;

        //Initiate the session variable.
        App app = (App)Application.Current;
        DateTime tempDate;

        public EditAppealJR()
        {
            InitializeComponent();

            string username = WebContext.Current.User.DisplayName;
            if (username.ToLower() == "guest")
            {
                this.EditAppealJRDataForm.IsEnabled = false;
                this.detailDataForm.IsEnabled = false;
                this.newAppellantDataForm.IsEnabled = false;
                this.newOwnerDataForm.IsEnabled = false;
                this.toolkitButtons.Visibility = System.Windows.Visibility.Collapsed;
                this.logButton.IsEnabled = false;
            }

            //Initialize values for domain service call parameters.
            this.applicationParameter.Value = ApplicationStrings.AgencyFor;
            this.applicationParameter1.Value = app.SessionApplicationID;

            this.applicationDetailsParameter.Value = app.SessionApplicationID;
            this.PrimaryDataSourceParameter.Value = app.SessionApplicationID;
            this.RespondentDataSourceParameter.Value = app.SessionApplicationID;
            this.applicationDetailsParameter2.Value = ApplicationStrings.AgencyFor;
            this.init_date = false;

            //determine the appeal type
            //ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
            if ("REG" == app.SessionRequestType)
                this.Title = "Edit Appeal";
            else
                this.Title = "Edit Jurisdiction Request";
            this.HeaderText.Text = this.Title + " NO.: " + app.SessionApplicationID;

            //validation events
            detailDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(detailDataForm_ValidatingItem);
            EditAppealJRDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(EditAppealJRDataForm_ValidatingItem);
            newAppellantDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newAppellantDataForm_ValidatingItem);
            newOwnerDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newOwnerDataForm_ValidatingItem);

            //UI element event handlers
            this.efDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(efDate_SelectedDateChanged);
            this.fDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(fDate_SelectedDateChanged);
            this.hDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(hDate_SelectedDateChanged);

            //this.typeSelection.ItemsSource = new ObservableCollection<string>() {""};
            this.statusSelection.ItemsSource = new ObservableCollection<string>() { "" };
            this.actionSelection.ItemsSource = new ObservableCollection<string>() { "" };
            this.agencySelection.ItemsSource = new ObservableCollection<tblDepartment>() { };
            this.outComeSelection.ItemsSource = new ObservableCollection<string>() { "" };

            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick_loading);
            myDispatcherTimer.Start();
        }

        #region // Combobox selection events
        private void typeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.typeSelection.SelectedValue != null)
            {
                this.typeText.Text = this.typeSelection.SelectedValue.ToString();
                //this.newItemDetail.Related_Agency = this.RespDeptText.Text;
                if (this.typeText.Text == otherPermitTypeText)
                {
                    this.permitTypeText2.UpdateLayout();
                    this.otherPermitType.Visibility = System.Windows.Visibility.Visible;
                    //this.permitTypeText2.Text = otherPermitTypeText;
                    this.permitTypeText2.Focus();
                    this.permitTypeText2.SelectAll();
                }
                else
                {
                    this.otherPermitType.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        private void actionSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.DeptActionText.Text = (this.actionSelection.SelectedValue == null) ? "" : this.actionSelection.SelectedValue.ToString();

            if (this.DeptActionText.Text.ToString() == otherDepartmentalAction)
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
                this.typeText2.UpdateLayout();
                this.typeText2.SelectAll();
                this.typeText2.Focus();
            }
            else
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void agencySelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.agencySelection.SelectedValue != null)
            {
                //this.RespDeptText.Text = this.agencySelection.SelectedValue.ToString();
                this.RespDeptText.Text = ((tblDepartment)this.agencySelection.SelectedValue).AgencyID;

                VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
                InvokeOperation<IEnumerable<string>> dept_info = departmentDomainService.GetDepartmentContactInfo(this.RespDeptText.Text, ApplicationStrings.AgencyFor);
                dept_info.Completed += new EventHandler(dept_info_Completed);

                ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
                InvokeOperation<IEnumerable<tblPermitType>> permitList = appDomainService.GetTblPermitTypes1(ApplicationStrings.AgencyFor, this.RespDeptText.Text);
                permitList.Completed += new EventHandler(permitList_Completed);
            }
        }
        private void statusSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string status = "";
            if (this.statusSelection.SelectedValue != null)
            {
                status = this.statusSelection.SelectedValue.ToString();
            }
            this.statusText.Text = status;
            /*if (status == NOD_REL)
            {
                this.outComeSelection.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.outComeSelection.Visibility = System.Windows.Visibility.Collapsed;
            }*/
        }
        private void outComeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.outComeSelection.SelectedValue != null)
                this.outcomeText.Text = this.outComeSelection.SelectedValue.ToString();
        }

        private void planningDeptDropDown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.planningDeptDropDown.SelectedValue != null)
                this.PlanningDeptText.Text = this.planningDeptDropDown.SelectedValue.ToString();
        }
        #endregion

        #region // completed events

        void load_editForm(string deptid)
        {
            if (deptid != "")
            {
                ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
                InvokeOperation<IEnumerable<tblPermitType>> permitList = appDomainService.GetTblPermitTypes1(ApplicationStrings.AgencyFor, deptid);
                permitList.Completed += new EventHandler(permitList_Completed);
                InvokeOperation<IEnumerable<tblStatusType>> statusList = appDomainService.GetTblStatusTypes1(ApplicationStrings.AgencyFor, 1);
                statusList.Completed += new EventHandler(statusList_Completed);
                InvokeOperation<IEnumerable<tblActionType>> deptActionList = appDomainService.GetDepartmentTblActionTypes1(ApplicationStrings.AgencyFor);
                deptActionList.Completed += new EventHandler(deptActionList_Completed);

                VoteNTransactionDomainService1 voteDomainService = new VoteNTransactionDomainService1();
                InvokeOperation<IEnumerable<tblDepartment>> deptList = voteDomainService.GetTblDepartments1();
                deptList.Completed += new EventHandler(deptList_Completed);
            }
        }
        void dept_info_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            string[] info = (string[])op.Value;
            if (info.Length > 0)
            {
                this.deptInfoPanel.UpdateLayout();
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Visible;
                this.deptContactName.Text = (info[0] == null) ? "" : info[0];
                this.deptAddress.Text = (info[1] == null) ? "" : info[1];
                this.deptPhone.Text = (info[2] == null) ? "" : info[2];
            }
            else
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Collapsed;
        }
        void outcomeList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblOutcomeType[] info = (tblOutcomeType[])op.Value;

            foreach (tblOutcomeType outcome in info)
            {
                (this.outComeSelection.ItemsSource as ObservableCollection<string>).Add(outcome.OutcomeName);
            }

            this.outComeSelection.UpdateLayout();
            string selectedItem = "";
            if (this.newApplication_jR != null)
                selectedItem = this.newApplication_jR.OutcomeType;
            else
                selectedItem = this.newApplication_jR.OutcomeType;

            if (selectedItem != null)
            {
                //this.outComeSelection.Visibility = System.Windows.Visibility.Visible;
                this.outComeSelection.SelectedItem = selectedItem;
            }
            /*else
                this.outComeSelection.Visibility = System.Windows.Visibility.Collapsed;
             */
        }

        void deptActionList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblActionType[] info = (tblActionType[])op.Value;

            foreach (tblActionType action in info)
            {
                (this.actionSelection.ItemsSource as ObservableCollection<string>).Add(action.ActionName);
            }
            this.actionSelection.UpdateLayout();
            string selectedItem;
            if (this.newApplication_jR != null)
                selectedItem = this.newApplication_jR.CaseType;
            else
                selectedItem = this.newApplication_jR.CaseType;
            this.actionSelection.SelectedItem = selectedItem;

            if (selectedItem != "" && (this.actionSelection.SelectedItem == null || this.actionSelection.SelectedItem.ToString() == otherDepartmentalAction))
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
                this.actionSelection.SelectedItem = otherDepartmentalAction;
                this.typeText2.Text = selectedItem;
            }
            else
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;
        }

        void deptList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDepartment[] info = (tblDepartment[])op.Value;

            this.agencySelection.UpdateLayout();
            tblDepartment selectedItem = new tblDepartment();
            string selectedValue;
            if (this.newApplication_jR != null)
                selectedValue = this.newApplication_jR.Dept_Agency;
            else
                selectedValue = this.newApplication_jR.Dept_Agency;

            foreach (tblDepartment dept in info)
            {
                (this.agencySelection.ItemsSource as ObservableCollection<tblDepartment>).Add(dept);
                if (dept.AgencyID == selectedValue)
                    selectedItem = dept;
            }

            this.agencySelection.SelectedItem = selectedItem;
        }

        void statusList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblStatusType[] info = (tblStatusType[])op.Value;

            foreach (tblStatusType status in info)
            {
                (this.statusSelection.ItemsSource as ObservableCollection<string>).Add(status.StatusName);
            }
            this.statusSelection.UpdateLayout();
            string selectedItem;
            if (this.newApplication_jR != null)
                selectedItem = this.newApplication_jR.Status;
            else
                selectedItem = this.newApplication_jR.Status;
            this.statusSelection.SelectedItem = selectedItem;

            ApplicationDomainService1 appDomainServer = new ApplicationDomainService1();
            InvokeOperation<IEnumerable<tblOutcomeType>> outcomeList = appDomainServer.GetTblOutcomeType1(ApplicationStrings.AgencyFor);
            outcomeList.Completed += new EventHandler(outcomeList_Completed);
        }

        void permitList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblPermitType[] info = (tblPermitType[])op.Value;

            this.typeSelection.ItemsSource = new ObservableCollection<string>() { "" };
            foreach (tblPermitType permit in info)
            {
                (this.typeSelection.ItemsSource as ObservableCollection<string>).Add(permit.PermitName);
            }
            this.typeSelection.UpdateLayout();
            string selectedItem;
            if (this.newApplication_jR != null)
                selectedItem = this.newApplication_jR.PermitType;
            else
                selectedItem = this.newApplication_jR.PermitType;
            this.typeSelection.SelectedItem = selectedItem;

            if (this.typeSelection.SelectedItem == null)
                this.typeText.Text = "";
        }


        void lnm_date_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField info = (tblDateField)op.Value;

            if (info != null)
            {
                this.LetterNoticesMailed.Text = info.DateValue.ToShortDateString();
                this.oLnmDate = info.DateValue;
            }
        }

        void pcm_date_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField info = (tblDateField)op.Value;

            if (info != null)
            {
                this.PostCardsMailed.Text = info.DateValue.ToShortDateString();
                this.oPcmDate = info.DateValue;
            }
        }

        void icf_date_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField info = (tblDateField)op.Value;

            if (info != null)
            {
                this.IndexCardsFiled.Text = info.DateValue.ToShortDateString();
                this.oIcfDate = info.DateValue;
            }
        }
        void brief_date_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField info = (tblDateField)op.Value;

            if (info != null)
            {
                this.briefDate.Text = info.DateValue.ToShortDateString();
                //this.oIcfDate = info.DateValue;
            }
        }

        void obrief_date_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField info = (tblDateField)op.Value;

            if (info != null)
            {
                this.othersBriefDate.Text = info.DateValue.ToShortDateString();
                //this.oIcfDate = info.DateValue;
            }
        }
        void invoke_ods_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            Boolean info = (Boolean)op.Value;

            newOwner = (tblNameField)this.newOwnerDataForm.CurrentItem;
            //newOwner.NameDescription = newOwner.FirstName + newOwner.MiddleName + newOwner.LastName;
            if (newOwner.NameDescription == "")
                newOwner.NameDescription = "N/A";
            newOwner.Type = "Respondent";
            newOwner.ApplicationID = app.SessionApplicationID;
            this.newOwnerDataForm.CommitEdit();
            if (this.newOwnerDataForm.ValidateItem())
            {
                if (info)
                {
                    this.RespondentDataSource.SubmitChanges();
                }
                else
                {
                    OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(RespondentDataSource.DomainContext);
                    _OwnerDomainServer1.EntityContainer.Clear();
                    _OwnerDomainServer1.tblNameFields.Add(newOwner);
                    _OwnerDomainServer1.SubmitChanges();
                }
                if (this.source != "PSOA")
                    GoToNext(this.source);
                else
                    this.handle_psoa();
            }
        }

        #endregion

        #region // loading and submitChange events

        private void applicationDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (!e.HasError)
            {
                DomainDataSourceView dvs = ((DomainDataSource)sender).DataView;
                if (dvs.Count > 0)
                {
                    this.newApplication_jR = (tblApplications_JR)dvs[0];
                    this.load_editForm(newApplication_jR.Dept_Agency);
                    this.planningDeptDropDown.ItemsSource = new ObservableCollection<string>() { "", "Planning Department Approval", "Planning Department Disapproval"};
                    this.planningDeptDropDown.SelectedItem = this.newApplication_jR.PlanningDept;

                    //save date information
                    this.oEDate = this.newApplication_jR.ExpDate;
                    this.oHDate = this.newApplication_jR.HearingDate;
                    this.oFDate = this.newApplication_jR.FileDate;

                    //populate indexcard, postcard, and letter notice dates
                    FieldTablesDomainService1 _fieldDomainService = (FieldTablesDomainService1)this.FieldDataSource1.DomainContext;
                    InvokeOperation icf_date = _fieldDomainService.GetDateFieldByDateType("IndexCardsFiled", app.SessionApplicationID);
                    icf_date.Completed += new EventHandler(icf_date_Completed);

                    InvokeOperation pcm_date = _fieldDomainService.GetDateFieldByDateType("PostCardsMailed", app.SessionApplicationID);
                    pcm_date.Completed += new EventHandler(pcm_date_Completed);

                    InvokeOperation lnm_date = _fieldDomainService.GetDateFieldByDateType("LetterNoticesMailed", app.SessionApplicationID);
                    lnm_date.Completed += new EventHandler(lnm_date_Completed);

                    InvokeOperation brief_date = _fieldDomainService.GetDateFieldByDateType("AppellantBrief", app.SessionApplicationID);
                    brief_date.Completed += new EventHandler(brief_date_Completed);

                    InvokeOperation obrief_date = _fieldDomainService.GetDateFieldByDateType("OthersBrief", app.SessionApplicationID);
                    obrief_date.Completed += new EventHandler(obrief_date_Completed);

                    //set header 
                    string appstr = app.SessionApplicationID.ToString();
                    appstr = ("JR-" + this.oHDate.Year.ToString().Substring(2, 2) + "-" + appstr);

                    this.HeaderText.Text = this.Title + " NO.: " + appstr;
                }
            }
            else
            {
                e.MarkErrorAsHandled();
            }
        }

        private void AppellantDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Appellant Information HAS NOT Been Entered!", "No Appellant Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }
        private void ownerDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Owner Information HAS NOT Been Entered!", "No Owner Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        private void applicationDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (!e.HasError)
                GoToNext(this.source);
            else
                e.MarkErrorAsHandled();
        }
        #endregion

        #region // Date selection and Text changed events
        private void fDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            this.hDate.DisplayDateStart = this.fDate.SelectedDate;
        }

        private void hDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //dbnull got converted to 01/0/0001 
            if (hDate.SelectedDate.Value.Year > 1)
            {
                tempDate = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 16, 0, 0);
                //this.briefDate.SelectedDate = tempDate.AddDays(-20);
                //this.othersBriefDate.SelectedDate = tempDate.AddDays(-6);
                this.hearing_date_text.Text = tempDate.ToShortDateString();
                //this.appellantReplyDate.SelectedDate = tempDate.AddDays(-6);
            }
            else
                this.hDate.Text = " ";
        }

        private void otherAction_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if other action, use the other action text rather than the value from dropdown box.
            if (this.typeText.Text.ToString() == otherDepartmentalAction)
            {
                this.DeptActionText.Text = this.typeText2.Text;
            }
        }

        private void permitType_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.typeText.Text = this.permitTypeText2.Text;
        }

        private void efDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            /*if (this.init_date)
            {
                DateTime selected = this.efDate.SelectedDate.Value;
                if (selected < DateTime.Today)
                {
                    MessageBox.Show("Must be greater than " + DateTime.Today.AddDays(-1).ToShortDateString(), "Invalid date selected", MessageBoxButton.OK);
                    this.efDate.SelectedDate = DateTime.Now;
                }
            }
            else*/
                this.init_date = true;

        }
        #endregion

        #region //Button clicks
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.EditAppealJRDataForm.CancelEdit();
            this.detailDataForm.CancelEdit();
            this.newAppellantDataForm.CancelEdit();
            this.newOwnerDataForm.CancelEdit();
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }
        private void logButton_Click(object sender, RoutedEventArgs e)
        {
            DateLogChildWindow1 dateLogChileWindow1 = new DateLogChildWindow1();
            dateLogChileWindow1.Show();
        }
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "/Home";
            GoToNext(this.source);
        }
        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "";
            saveChanges();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "/AdditionalNewContactsPage1";
            saveChanges();

        }
        private void PsoaButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "PSOA";
            saveChanges();
        }
        void handle_psoa()
        {
            //this.source = "/Home";
            GoToNext(this.source);
            //generate PSOA word document based on template
            if (fileDownloadWindow == null)
                fileDownloadWindow = new FileDownload();
            fileDownloadWindow.Show();
            DocumentGeneration dg = new DocumentGeneration();
            InvokeOperation op = dg.getPSOADoc(app.SessionApplicationID,"PSOA");
            op.Completed += new EventHandler(client_GetNameCompleted);
        }
        void client_GetNameCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url + ":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }

                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
            }
        }

        private void saveChanges()
        {
            this.SavingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
            myDispatcherTimer.Start();

            if (this.EditAppealJRDataForm.ValidateItem() && this.detailDataForm.ValidateItem() && this.newAppellantDataForm.ValidateItem())
            {
                //date fields, not bind.
                FieldTablesDomainService1 _fieldTableDomainService = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);
                _fieldTableDomainService.RejectChanges();
                if (this.IndexCardsFiled.SelectedDate.HasValue && this.IndexCardsFiled.SelectedDate != this.oIcfDate)
                {
                    DateEntry(this.IndexCardsFiled, "Modified Index Card Filed Date", "IndexCardsFiled", "appeal application editing");
                }
                if (this.PostCardsMailed.SelectedDate.HasValue && this.PostCardsMailed.SelectedDate != this.oPcmDate)
                {
                    DateEntry(this.PostCardsMailed, "Modified Post Cards Mailed Date", "PostCardsMailed", "appeal application editing");
                }
                if (this.LetterNoticesMailed.SelectedDate.HasValue && this.oLnmDate != this.LetterNoticesMailed.SelectedDate)
                {
                    DateEntry(this.LetterNoticesMailed, "Letter Notices Mailed Date", "LetterNoticesMailed", "appeal application editing");
                }

                if (this.fDate.SelectedDate.Value != null && this.fDate.SelectedDate.Value != this.oFDate)
                {
                    DateEntry(this.fDate, "Modified Filed Date", "FiledDate", "appeal application editing.");
                }
                if (this.hDate.SelectedDate.Value != null && this.hDate.SelectedDate.Value != this.oHDate)
                {
                    DateEntry(this.hDate, "Modified Hearing Date", "HearingDate", "appeal application editing.");
                }
                    //if (this.briefDate.SelectedDate != null || this.appellantReplyDate.SelectedDate != null || this.othersBriefDate.SelectedDate != null)
                    if (this.briefDate.SelectedDate.HasValue)
                    {
                        DateEntry(this.briefDate, "Modified Appellant's Brief Date", "AppellantBrief", "appeal application editing.");
                    }

                    if (this.appellantReplyDate.SelectedDate.HasValue)
                    {
                        DateEntry(this.appellantReplyDate, "Modified Appellant's Reply Date", "AppellantReply", "appeal application editing.");
                    }

                    if (this.othersBriefDate.SelectedDate.HasValue)
                    {
                        DateEntry(this.othersBriefDate, "Modified Respondent's / Other Parties' Brief Date", "OthersBrief", "appeal application editing.");
                    }

                _fieldTableDomainService.SubmitChanges();

                if (this.applicationDataSource1.HasChanges)
                    this.applicationDataSource1.SubmitChanges();
                if (this.PrimaryDataSource.HasChanges)
                    this.PrimaryDataSource.SubmitChanges();
                if (this.applicationDetailsDataSource.HasChanges)
                    this.applicationDetailsDataSource.SubmitChanges();

                if (this.RespondentDataSource.HasChanges)
                {
                    OwnerDomainService1 ods = new OwnerDomainService1();
                    InvokeOperation invoke_ods = ods.checkExistingNames(app.SessionApplicationID, "Respondent");
                    invoke_ods.Completed += new EventHandler(invoke_ods_Completed);
                }
                else
                {
                    if (this.source != "PSOA")
                        GoToNext(this.source);
                    else
                        this.handle_psoa();
                }
            }
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument document = new PrintDocument();

            document.PrintPage += (s, args) =>
            {
                args.PageVisual = this.ContactsPanel;
            };
            document.Print("Contacts");
        }

        #endregion

        #region //Checkbox, Hyperlink action handlers
        private void ContactsPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ContactsPanel.Visibility == Visibility.Collapsed)
                this.ContactsPanel.Visibility = Visibility.Visible;
            else
                this.ContactsPanel.Visibility = Visibility.Collapsed;
        }

        private void PermitPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PermitPanel.Visibility == Visibility.Collapsed)
                this.PermitPanel.Visibility = Visibility.Visible;
            else
                this.PermitPanel.Visibility = Visibility.Collapsed;
        }

        private void DatePanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DatePanel.Visibility == Visibility.Collapsed)
                this.DatePanel.Visibility = Visibility.Visible;
            else
                this.DatePanel.Visibility = Visibility.Collapsed;
        }

        private void DeptPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DeptPanel.Visibility == Visibility.Collapsed)
                this.DeptPanel.Visibility = Visibility.Visible;
            else
                this.DeptPanel.Visibility = Visibility.Collapsed;
        }

        private void Conditions_Checked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = true;
        }
        private void Conditions_UnChecked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = false;
        }

       
        #endregion

        #region //Validating Events
        void newOwnerDataForm_ValidatingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void EditAppealJRDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.EditAppealJRDataForm.ValidationSummary.Errors.Clear();
            /*if (this.aPermit.Text.Length < 1)
                this.EditAppealJRDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Permit Number is required!", "Permit Number is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("aPermit"), this.EditAppealJRDataForm.CurrentItem));
             * */
            if (this.RespDeptText.Text.Length < 1)
                this.EditAppealJRDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Respondent Department is required!", "Respondent Department is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("agencySelection"), this.EditAppealJRDataForm.CurrentItem));
            if (this.DeptActionText.Text.Length < 1)
                this.EditAppealJRDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Department Action is required!", "Department action is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("actionSelection"), this.EditAppealJRDataForm.CurrentItem));
        }
        private void newAppellantDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void detailDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.detailDataForm.ValidationSummary.Errors.Clear();
            if (this.hDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Hearing Date required!", "Appeal Hearing Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("hDate"), this.detailDataForm.CurrentItem));
            if (this.fDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("File Date required!", "File Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("fDate"), this.detailDataForm.CurrentItem));
            if (this.efDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Effective Date required!", "Effective Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("efDate"), this.detailDataForm.CurrentItem));
            if (this.typeText.Text.Length < 1)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Permit Type is required!", "Permit Type is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("typeSelection"), this.EditAppealJRDataForm.CurrentItem));
        }
        #endregion

        #region //Helper functions
        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!FieldDataSource1.IsBusy && !this.applicationDataSource1.IsBusy &&
                    !this.PrimaryDataSource.IsBusy && !this.applicationDetailsDataSource.IsBusy && !this.RespondentDataSource.IsBusy)
            {
                this.SavingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }

        }
        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick_loading(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;

            if (!PrimaryDataSource.IsBusy && !RespondentDataSource.IsBusy && !FieldDataSource1.IsBusy 
                && !applicationDetailsDataSource.IsBusy)
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }
        private void OnSubmitCompleted_owner(SubmitOperation so)
        {
            if (!so.HasError)
            {
                if (this.source == "PSOA")
                {
                    this.source = "/Home";
                    //generate PSOA word document based on template
                    if (fileDownloadWindow == null)
                        fileDownloadWindow = new FileDownload();
                    fileDownloadWindow.Show();
                    DocumentGeneration dg = new DocumentGeneration();
                    InvokeOperation op = dg.getPSOADoc(app.SessionApplicationID,"PSOA");
                    op.Completed += new EventHandler(client_GetNameCompleted);
                }
                GoToNext(this.source);
            }
        }
        private void GoToNext(string uri)
        {
            if (this.source != "" && this.source != "PSOA")
                this.NavigationService.Navigate(new Uri(String.Format(uri), UriKind.Relative));
        }
        private void DateEntry(DatePicker dateObject, string description, string dateType, string comments)
        {
            FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);

            tblDateField OthersBrief = new tblDateField();
            OthersBrief.DateValue = new DateTime(dateObject.SelectedDate.Value.Year, dateObject.SelectedDate.Value.Month, dateObject.SelectedDate.Value.Day, 16, 0, 0);
            OthersBrief.ApplicationID = app.SessionApplicationID;
            OthersBrief.ModDate = DateTime.Now;
            OthersBrief.DateDescription = description;
            OthersBrief.DateType = dateType;
            OthersBrief.Comments = comments;
            OthersBrief.UserID = WebContext.Current.User.DisplayName.ToString();
            _FieldTableDomainServices1.tblDateFields.Add(OthersBrief);
        }
        //Scroll page vertically when focus changes to a control that is outside the viewable area.
        private void AutoScrollViewer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                FrameworkElement element = FocusManager.GetFocusedElement() as FrameworkElement;

                if (element != null)
                {
                    //ScrollViewer scrollViewer = sender as ScrollViewer;
                    ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
                    try
                    {
                        scrollViewer.ScrollToVerticalOffset(GetVerticalOffset(element, scrollViewer));
                    }
                    catch
                    {
                        //if there's an error, don't scroll.
                    }
                }
            }
        }
        private double GetVerticalOffset(FrameworkElement child, ScrollViewer scrollViewer)
        {
            // Ensure the control is scrolled into view in the ScrollViewer.
            System.Windows.Media.GeneralTransform focusedVisualTransform = child.TransformToVisual(scrollViewer);
            Point topLeft = focusedVisualTransform.Transform(new Point(child.Margin.Left, child.Margin.Top));
            Rect rectangle = new Rect(topLeft, child.RenderSize);
            //If the control is taller than the viewport, don't scroll down further than the top of the control.
            double controlRectangleBottom = rectangle.Bottom - scrollViewer.ViewportHeight > scrollViewer.ViewportHeight ? scrollViewer.ViewportHeight : rectangle.Bottom;
            double newOffset = scrollViewer.VerticalOffset + (controlRectangleBottom - scrollViewer.ViewportHeight);
            return newOffset < 0 ? 0 : newOffset; 
        }
        #endregion
    }

}