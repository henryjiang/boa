﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;

namespace BOA_AppealApplication1.Views
{
    public partial class RemoveContactChildWindow1 : ChildWindow
    {
        public tblNameField newContact { get; set; }
        App app = (App)Application.Current;
        OwnerDomainService1 _OwnerDomainServices1;
        
        public RemoveContactChildWindow1()
        {
            InitializeComponent();

            //this.contactFirstName.Text = ((tblNameField)(app.SessionObject)).FirstName.ToString();
            //this.contactMiddleName.Text = ((tblNameField)(app.SessionObject)).MiddleName.ToString();
            //this.contactLastName.Text = ((tblNameField)(app.SessionObject)).LastName.ToString();
            if(app.SessionObject != null)
                this.contactParameter.Value = ((tblNameField)(app.SessionObject)).NameDescription.ToString();
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            //tblNameField deleteContact = this.removeContactForm.CurrentItem as tblNameField;
            //_OwnerDomainServices1 = (OwnerDomainService1)(contactDataSource1.DomainContext);
            //_OwnerDomainServices1.tblNameFields.Remove(deleteContact);
            //_OwnerDomainServices1.SubmitChanges();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

