﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace BOA_AppealApplication1.Views
{
    public partial class statusLogWindow : ChildWindow
    {
        App app = (App)Application.Current;
        FieldTablesDomainService1 _FieldTablesDomainServices1 = new FieldTablesDomainService1();

        public statusLogWindow()
        {
            InitializeComponent();
            //this.dateParameter.Value = app.SessionApplicationID;

            //Call the related domain services to retrieve records.
            PagedCollectionView dateListView;
            EntityQuery<tblStatusField> query = _FieldTablesDomainServices1.GetApplicationTblStatusFieldsQuery(app.SessionApplicationID);
            _FieldTablesDomainServices1.Load<tblStatusField>(query).Completed += (sender, args) =>
            {
                List<tblStatusField> dateList = ((LoadOperation<tblStatusField>)sender).Entities.ToList();
                List<statusFieldList> statusList = new List<statusFieldList>();

                for(int i = 0; i < dateList.Count; i++)
                {
                    statusFieldList sfl = new statusFieldList();
                    sfl.ApplicationID = dateList[i].ApplicationID;
                    sfl.Comments = dateList[i].Comments;
                    sfl.DateComments = dateList[i].DateComments;
                    sfl.Status = dateList[i].Status;
                    sfl.ModDate = dateList[i].ModDate.ToShortDateString();
                    sfl.UserID = dateList[i].UserID;
                    statusList.Add(sfl);
                }
                dateListView = new PagedCollectionView(statusList);
                this.dateLogGrid.ItemsSource = dateListView;

                if (dateListView.CanGroup == true)
                {
                    //Group dates by their types.
                    dateListView.GroupDescriptions.Add(new PropertyGroupDescription("ModDate"));

                    //collapse group view
                    PagedCollectionView pcv = dateLogGrid.ItemsSource as PagedCollectionView;
                    try
                    {
                        foreach (CollectionViewGroup group in pcv.Groups)
                        {
                            dateLogGrid.CollapseRowGroup(group, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Could not collapse group.
                        MessageBox.Show(ex.Message);
                    }
                }
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void FieldDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                //collapse group view
                PagedCollectionView pcv = dateLogGrid.ItemsSource as PagedCollectionView;
                try
                {
                    foreach (CollectionViewGroup group in pcv.Groups)
                    {
                        dateLogGrid.ScrollIntoView(group, null);
                        dateLogGrid.CollapseRowGroup(group, true);
                    }
                }
                catch (Exception ex)
                {
                    // Could not collapse group.
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }

    public class statusFieldList
    {
        public int ApplicationID { get; set; }

        public string Comments { get; set; }

        public string ModDate { get; set; }

        public string Status { get; set; }

        public string DateComments { get; set; }

        public string UserID { get; set; }
    }
}

