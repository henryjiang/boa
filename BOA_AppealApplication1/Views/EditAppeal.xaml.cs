﻿using System;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Printing;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace BOA_AppealApplication1.Views
{
    public partial class EditAppeal : Page
    {
        /// <summary>
        /// data entities
        
        public tblApplication newApplication { get; set; }
        public tblApplications_JR newApplication_jR { get; set; }
        public tblItemDetail newItemDetail { get; set; }
        public tblNameField newOwner { get; set; }
        public tblNameField newAppellant { get; set; }
        public tblDateField newDateEntry { get; set; }
        /// </summary>
        
        /// <summary>
        /// misc
        
        private string source { get; set; }
        const string otherDepartmentalAction = "OTHER ACTION";
        const string otherPermitTypeText = "Other Permit Type";
        const string NOD_REL = "NOD REL";
        private Boolean init_date { get; set; }
        /// </summary>
        
        /// <summary>
        /// set of fields to keep for tracking purpose
        
        private DateTime oHDate { get; set; }
        private DateTime oFDate { get; set; }
        private DateTime oEDate { get; set; }
        private DateTime oADate { get; set; }
        private DateTime oBDate { get; set; }
        private DateTime ofindingAdoptedDate { get; set; }
        private DateTime ostatusDate { get; set; }

        private DateTime oIcfDate { get; set; }
        private DateTime oPcmDate { get; set; }
        private DateTime oLnmDate { get; set; }
        
        private string oStatus { get; set; }
        private Boolean oFindingAdopted { get; set; }
        private Boolean isVoteSheetupdated { get; set; }
        /// </summary>
       
        FileDownload fileDownloadWindow;
        public OpenFileDialog fileDialog = null;

        //Initiate the session variable.
        App app = (App)Application.Current;
        DateTime tempDate;

        VoteNTransactionDomainService1 vtd;
        
        public EditAppeal()
        {
            InitializeComponent();
            string username = WebContext.Current.User.DisplayName;
            if (username.ToLower() == "guest")
            {
                this.EditAppealDataForm.IsEnabled = false;
                this.detailDataForm.IsEnabled = false;
                this.newAppellantDataForm.IsEnabled = false;
                this.newOwnerDataForm.IsEnabled = false;
                this.toolkitButtons.Visibility = System.Windows.Visibility.Collapsed;
                this.logButton.IsEnabled = false;
            }

            //Initialize values for domain service call parameters.
            this.applicationParameter.Value = ApplicationStrings.AgencyFor;
            this.applicationParameter1.Value = app.SessionApplicationID;
            this.BoardMemberParameter.Value = ApplicationStrings.AgencyFor;

            this.applicationDetailsParameter.Value = app.SessionApplicationID;
            this.PrimaryDataSourceParameter.Value = app.SessionApplicationID;
            this.RespondentDataSourceParameter.Value = app.SessionApplicationID;
            this.applicationDetailsParameter2.Value = ApplicationStrings.AgencyFor;
            this.init_date = false;

            //determine the appeal type
            //ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
            if ("REG" == app.SessionRequestType)
                this.Title = "Edit Appeal";
            else
                this.Title = "Edit Jurisdiction Request";

            //validation events
            detailDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(detailDataForm_ValidatingItem);
            EditAppealDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(EditAppealDataForm_ValidatingItem);
            newAppellantDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newAppellantDataForm_ValidatingItem);
            newOwnerDataForm.ValidatingItem += new EventHandler<System.ComponentModel.CancelEventArgs>(newOwnerDataForm_ValidatingItem);

            //UI element event handlers
            this.efDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(efDate_SelectedDateChanged);
            this.fDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(fDate_SelectedDateChanged);
            this.hDate.SelectedDateChanged += new EventHandler<SelectionChangedEventArgs>(hDate_SelectedDateChanged);

            //this.typeSelection.ItemsSource = new ObservableCollection<string>() {""};
            this.statusSelection.ItemsSource = new ObservableCollection<string>() { "" };
            this.actionSelection.ItemsSource = new ObservableCollection<string>() { "" };
            this.agencySelection.ItemsSource = new ObservableCollection<tblDepartment>() { };
            this.outComeSelection.ItemsSource = new ObservableCollection<string>() { "" };

            // Wait for data to load
            this.LoadingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick_loading);
            myDispatcherTimer.Start();

            this.PageScrollViewer.KeyUp += new KeyEventHandler(AutoScrollViewer_KeyUp);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
            scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
            scrollViewer.ScrollToTop();
        }

        #region // Combobox selection events
        private void typeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.typeSelection.SelectedValue != null)
            {
                this.typeText.Text = this.typeSelection.SelectedValue.ToString();
                //this.newItemDetail.Related_Agency = this.RespDeptText.Text;
                if (this.typeText.Text == otherPermitTypeText)
                {
                    this.permitTypeText2.UpdateLayout();
                    this.otherPermitType.Visibility = System.Windows.Visibility.Visible;
                    //this.permitTypeText2.Text = otherPermitTypeText;
                    //this.permitTypeText2.Focus();
                    this.permitTypeText2.SelectAll();
                    this.typeText.Text = this.permitTypeText2.Text;
                }
                else
                {
                    this.otherPermitType.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        private void actionSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.DeptActionText.Text = (this.actionSelection.SelectedValue == null) ? "" : this.actionSelection.SelectedValue.ToString();

            if (this.DeptActionText.Text.ToString() == otherDepartmentalAction)
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
                this.typeText2.UpdateLayout();
                this.typeText2.SelectAll();
                //this.typeText2.Focus();
                this.DeptActionText.Text = this.typeText2.Text;
            }
            else
            {
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void agencySelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.agencySelection.SelectedValue != null)
            {
                //this.RespDeptText.Text = this.agencySelection.SelectedValue.ToString();
                this.RespDeptText.Text = ((tblDepartment)this.agencySelection.SelectedValue).AgencyID;
                
                VoteNTransactionDomainService1 departmentDomainService = new VoteNTransactionDomainService1();
                InvokeOperation<IEnumerable<string>> dept_info = departmentDomainService.GetDepartmentContactInfo(this.RespDeptText.Text, ApplicationStrings.AgencyFor);
                dept_info.Completed += new EventHandler(dept_info_Completed);

                ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
                InvokeOperation<IEnumerable<tblPermitType>> permitList = appDomainService.GetTblPermitTypes1(ApplicationStrings.AgencyFor, this.RespDeptText.Text);
                permitList.Completed += new EventHandler(permitList_Completed);
            }
        }
        private void statusSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string status = "";
            if (this.statusSelection.SelectedValue != null)
            {
                status = this.statusSelection.SelectedValue.ToString();
                this.statusDate.SelectedDate = null;
            }
            this.statusText.Text = status;

            // IF status in (NOD REL, Dismissed, Rejected, or Withdrawn) show datepicker.
            //if(status == "Rejected" || status == "Dismissed" || status == "NOD REL" || status == "Withdrawn"){
                this.statusDate.Visibility = System.Windows.Visibility.Visible;
            //}
            //else{
              //  this.statusDate.Visibility = System.Windows.Visibility.Collapsed;
            //}

        }
        private void outComeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.outComeSelection.SelectedValue != null)
                this.outcomeText.Text = this.outComeSelection.SelectedValue.ToString();
        }

        private void planningDeptDropDown_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.planningDeptDropDown.SelectedValue != null)
                this.PlanningDeptText.Text = this.planningDeptDropDown.SelectedValue.ToString();
        }
        #endregion

        #region // completed events

        void load_editForm(string deptid)
        {
            if (deptid != "")
            {
                ApplicationDomainService1 appDomainService = new ApplicationDomainService1();
                InvokeOperation<IEnumerable<tblPermitType>> permitList = appDomainService.GetTblPermitTypes1(ApplicationStrings.AgencyFor, deptid);
                permitList.Completed += new EventHandler(permitList_Completed);
                InvokeOperation<IEnumerable<tblStatusType>> statusList = appDomainService.GetTblStatusTypes1(ApplicationStrings.AgencyFor, "REG");
                statusList.Completed += new EventHandler(statusList_Completed);
                InvokeOperation<IEnumerable<tblActionType>> deptActionList = appDomainService.GetDepartmentTblActionTypes1(ApplicationStrings.AgencyFor);
                deptActionList.Completed += new EventHandler(deptActionList_Completed);
                
                VoteNTransactionDomainService1 voteDomainService = new VoteNTransactionDomainService1();
                InvokeOperation<IEnumerable<tblDepartment>> deptList = voteDomainService.GetTblDepartments1();
                deptList.Completed += new EventHandler(deptList_Completed);
            }
        }
        void dept_info_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            string[] info = (string[])op.Value;
            if (info.Length > 0)
            {
                this.deptInfoPanel.UpdateLayout();
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Visible;
                this.deptContactName.Text = (info[0] == null) ? "" : info[0];
                this.deptAddress.Text = (info[1] == null) ? "" : info[1];
                this.deptPhone.Text = (info[2] == null) ? "" : info[2];
            }
            else
                this.deptInfoPanel.Visibility = System.Windows.Visibility.Collapsed;
        }
        void outcomeList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblOutcomeType[] info = (tblOutcomeType[])op.Value;

            foreach (tblOutcomeType outcome in info)
            {
                (this.outComeSelection.ItemsSource as ObservableCollection<string>).Add(outcome.OutcomeName);
            }

            this.outComeSelection.UpdateLayout();
            string selectedItem = "";
            if (this.newApplication != null)
                selectedItem = this.newApplication.OutcomeType;
            else
                selectedItem = this.newApplication_jR.OutcomeType;

            if (selectedItem != null)
            {
            //    this.outComeSelection.Visibility = System.Windows.Visibility.Visible;
                this.outComeSelection.SelectedItem = selectedItem;
            }
            /*else
                this.outComeSelection.Visibility = System.Windows.Visibility.Collapsed;
             */
        }

        void deptActionList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblActionType[] info = (tblActionType[])op.Value;

            foreach (tblActionType action in info)
            {
                (this.actionSelection.ItemsSource as ObservableCollection<string>).Add(action.ActionName);
            }
            this.actionSelection.UpdateLayout();
            string selectedItem = "";
            if (this.newApplication != null)
                selectedItem = this.newApplication.CaseType;

            if (!this.actionSelection.Items.Contains(selectedItem))
            {
                selectedItem = otherDepartmentalAction;
                this.typeText2.Text = this.newApplication.CaseType;
                this.OtherAction.Visibility = System.Windows.Visibility.Visible;
            }
            else
                this.OtherAction.Visibility = System.Windows.Visibility.Collapsed;

            this.actionSelection.SelectedItem = selectedItem;
        }

        void deptList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDepartment[] info = (tblDepartment[])op.Value;

            this.agencySelection.UpdateLayout();
            tblDepartment selectedItem = new tblDepartment();
            string selectedValue;
            if (this.newApplication != null)
                selectedValue = this.newApplication.Dept_Agency;
            else
                selectedValue = this.newApplication_jR.Dept_Agency;

            foreach (tblDepartment dept in info)
            {
                (this.agencySelection.ItemsSource as ObservableCollection<tblDepartment>).Add(dept);
                if (dept.AgencyID == selectedValue)
                    selectedItem = dept;
            }

            this.agencySelection.SelectedItem = selectedItem;
        }

        void statusList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblStatusType[] info = (tblStatusType[])op.Value;

            foreach (tblStatusType status in info)
            {
                (this.statusSelection.ItemsSource as ObservableCollection<string>).Add(status.StatusName);
            }
            this.statusSelection.UpdateLayout();
            string selectedItem = "";
            if (this.newApplication != null)
                selectedItem = this.newApplication.Status;
            //else
                //selectedItem = this.newApplication_jR.Status;
            this.statusSelection.SelectedItem = selectedItem;

            ApplicationDomainService1 appDomainServer = new ApplicationDomainService1();
            InvokeOperation<IEnumerable<tblOutcomeType>> outcomeList = appDomainServer.GetTblOutcomeType1(ApplicationStrings.AgencyFor);
            outcomeList.Completed += new EventHandler(outcomeList_Completed);
        }

        void permitList_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblPermitType[] info = (tblPermitType[])op.Value;

            this.typeSelection.ItemsSource = new ObservableCollection<string>() { "" };
            foreach (tblPermitType permit in info)
            {
                (this.typeSelection.ItemsSource as ObservableCollection<string>).Add(permit.PermitName);
            }
            this.typeSelection.UpdateLayout();
            string selectedItem = "";
            if (this.newApplication != null)
                selectedItem = this.newApplication.PermitType;

            if (!this.typeSelection.Items.Contains(selectedItem))
            {
                selectedItem = "Other Permit Type";
                this.permitTypeText2.Text = this.newApplication.PermitType;
            }
            
            this.typeSelection.SelectedItem = selectedItem;

            if (this.typeSelection.SelectedItem == null)
                this.typeText.Text = "";
        }


        void get_dates_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            tblDateField[] infos = (tblDateField[])op.Value;

            IEnumerable<IGrouping<string, DateTime>> dates = infos.OrderByDescending(x => x.DateValue).GroupBy(x => x.DateType, x => x.DateValue);

            //foreach(tblDateField info in infos){
            foreach(IGrouping<string, DateTime> info in dates){
                DateTime DateValue = (DateTime)info.FirstOrDefault();
                if (info.Key == "LetterNoticesMailed")
                {
                    this.LetterNoticesMailed.Text = DateValue.ToShortDateString();
                    this.oLnmDate = DateValue;
                }
                else if (info.Key == "PostCardsMailed")
                {
                    this.PostCardsMailed.Text = DateValue.ToShortDateString();
                    this.oPcmDate = DateValue;
                }
                else if (info.Key == "IndexCardsFiled")
                {
                    this.IndexCardsFiled.Text = DateValue.ToShortDateString();
                    this.oIcfDate = DateValue;
                }
                else if (info.Key == "AppellantBrief")
                {
                    this.briefDate.Text = DateValue.ToShortDateString();
                    this.oADate = DateValue;
                }
                else if (info.Key == "OthersBrief")
                {
                    this.othersBriefDate.Text = DateValue.ToShortDateString();
                    this.oBDate = DateValue;
                }
                else if (info.Key == "FindingAdpotedDate")
                {
                    this.findingAdoptedDate.Text = DateValue.ToShortDateString();
                    this.ofindingAdoptedDate = DateValue;
                }
                else
                {
                    string tempStatus = info.Key;
                    if (tempStatus == "DismissedDate" || tempStatus == "RejectedDate" || tempStatus == "WithdrawnDate" || tempStatus == "NODRELDate" || tempStatus == "DeniedDate" || tempStatus == "ContinuedDate" || tempStatus == "RehearingDate"
                        || tempStatus == "CalloftheChairDate" || tempStatus == "Pending-AOFDate" || tempStatus == "HearingPendingDate" || tempStatus == "DecidedDate")
                    {
                        this.statusDate.Text = DateValue.ToShortDateString();
                        this.ostatusDate = DateValue;
                        break;
                    }
                }
            }            
        }

        void invoke_ods_Completed(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            Boolean info = (Boolean)op.Value;

            newOwner = (tblNameField)this.newOwnerDataForm.CurrentItem;
            if (newOwner.NameDescription == "")
                newOwner.NameDescription = "N/A";
            newOwner.Type = "Respondent";
            newOwner.ApplicationID = app.SessionApplicationID;
            this.newOwnerDataForm.CommitEdit();
            if (this.newOwnerDataForm.ValidateItem())
            {
                if (info)
                {
                    try
                    {
                        this.RespondentDataSource.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to add Respondent for the reason of: " + ex.Message);
                    }
                }
                else
                {
                    OwnerDomainService1 _OwnerDomainServer1 = (OwnerDomainService1)(RespondentDataSource.DomainContext);
                    _OwnerDomainServer1.EntityContainer.Clear();
                    _OwnerDomainServer1.tblNameFields.Add(newOwner);
                    try
                    {
                        _OwnerDomainServer1.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to add Respondent for the reason of: " + ex.Message);
                    }
                }
                if (this.source != "PSOA")
                    GoToNext(this.source);
                else
                    this.handle_psoa();
            }
        }

        #endregion

        #region // loading and submitChange events

        private void applicationDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (!e.HasError)
            {
                DomainDataSourceView dvs = ((DomainDataSource)sender).DataView;
                if(dvs.Count > 0)
                {
                    this.newApplication = (tblApplication)dvs[0];
                    this.load_editForm(newApplication.Dept_Agency);
                    this.planningDeptDropDown.ItemsSource = new ObservableCollection<string>() { "", "Planning Department Approval", "Planning Department Disapproval"};
                    this.planningDeptDropDown.SelectedItem = this.newApplication.PlanningDept;

                    //save date information
                    this.oEDate = this.newApplication.ExpDate;
                    this.oHDate = this.newApplication.HearingDate;
                    this.oFDate = this.newApplication.FileDate;
                    this.oStatus = this.newApplication.Status;

                    if (this.oStatus == "Rejected" || this.oStatus == "Dismissed" || this.oStatus == "NOD REL" || this.oStatus == "Withdrawn")
                    {
                        this.statusDate.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        this.statusDate.Visibility = System.Windows.Visibility.Collapsed;
                    }

                    if (this.newApplication.FindingAdopted.HasValue)
                    {
                        this.oFindingAdopted = (Boolean)this.newApplication.FindingAdopted.Value;
                        this.findingAdoptedDate.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        this.oFindingAdopted = false;
                        this.findingAdoptedDate.Visibility = System.Windows.Visibility.Collapsed;
                        this.FindingAdpoted.IsChecked = false;
                    }

                    // consolidate this
                    FieldTablesDomainService1 _fieldDomainService = (FieldTablesDomainService1)this.FieldDataSource1.DomainContext;
                    InvokeOperation get_dates = _fieldDomainService.GetSpecificTblDateFields2(app.SessionApplicationID);
                    get_dates.Completed += new EventHandler(get_dates_Completed);

                    //set header 
                    /*string appstr = app.SessionApplicationID.ToString();
                    string temp = "";
                    temp = appstr.Substring(3, appstr.Length - 3);
                    appstr = (this.oHDate.Year.ToString().Substring(2, 2) + "-" + temp);
                    */
                    this.HeaderText.Text = this.Title + " NO.: " + newApplication.ApplicationIDstr;
                }
            }
            else
            {
                e.MarkErrorAsHandled();
            }
        }
        private void PrimaryDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (PrimaryDataSource.DataView.Count < 1)
            {
                tblNameField primary = new tblNameField();
                PrimaryDataSource.DataView.Add(primary);
                //PrimaryDataSource.Load();
            }

        }
        private void AppellantDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Appellant Information HAS NOT Been Entered!", "No Appellant Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }
        private void ownerDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("Owner Information HAS NOT Been Entered!", "No Owner Information!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }
        private void applicationDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (!e.HasError)
                GoToNext(this.source);
            else
                e.MarkErrorAsHandled();
        }
        #endregion

        #region // Date selection and Text changed events
        private void fDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            this.hDate.DisplayDateStart = this.fDate.SelectedDate;
        }

        private void hDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //dbnull got converted to 01/0/0001 
            if (hDate.SelectedDate.Value.Year > 1)
            {
                tempDate = new DateTime(hDate.SelectedDate.Value.Year, hDate.SelectedDate.Value.Month, hDate.SelectedDate.Value.Day, 16, 0, 0);
                //this.briefDate.SelectedDate = tempDate.AddDays(-20);
                //this.othersBriefDate.SelectedDate = tempDate.AddDays(-6);
                this.hearing_date_text.Text = tempDate.ToShortDateString();
                //this.appellantReplyDate.SelectedDate = tempDate.AddDays(-6);
            }
            else
                this.hDate.Text = " ";
        }

        private void otherAction_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if other action, use the other action text rather than the value from dropdown box.
            //if (this.typeText.Text.ToString() == otherDepartmentalAction)
            //{
                this.DeptActionText.Text = this.typeText2.Text;
            //}
        }

        private void permitType_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.typeText.Text = this.permitTypeText2.Text;
        }

        private void efDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            /*if (this.init_date)
            {
                DateTime selected = this.efDate.SelectedDate.Value;
                if (selected < DateTime.Today)
                {
                    MessageBox.Show("Must be greater than " + DateTime.Today.AddDays(-1).ToShortDateString(), "Invalid date selected", MessageBoxButton.OK);
                    this.efDate.SelectedDate = DateTime.Now;
                }
            }
            else */
            this.init_date = true;
        }

        private void formatPhoneNumber(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text != "")
            {
                string pattern = @"[-_\s\(\)]";
                string replacement = "";
                Regex ex = new Regex(pattern);
                string phone = ex.Replace(tb.Text, replacement);
                try
                {
                    tb.Text = Convert.ToDouble(phone).ToString("(###) ###-####");
                }
                catch (FormatException fe)
                {
                    MessageBox.Show("Invalid phone number format, please try again.", "", MessageBoxButton.OK);
                }
            }
        }
        #endregion

        #region //Button clicks
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.EditAppealDataForm.CancelEdit();
            this.detailDataForm.CancelEdit();
            this.newAppellantDataForm.CancelEdit();
            this.newOwnerDataForm.CancelEdit();
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }
        private void logButton_Click(object sender, RoutedEventArgs e)
        {
            DateLogChildWindow1 dateLogChileWindow1 = new DateLogChildWindow1();
            dateLogChileWindow1.Show();
        }
        private void statusLogButton_Click(object sender, RoutedEventArgs e)
        {
            statusLogWindow statusLog = new statusLogWindow();
            statusLog.Show();
        }
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "/Home";
            saveChanges();
            //GoToNext(this.source);
        }
        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "";
            saveChanges();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "/AdditionalNewContactsPage1";
            saveChanges();
                
        }
        private void PsoaButton_Click(object sender, RoutedEventArgs e)
        {
            this.source = "PSOA";
            saveChanges();   
        }

        private void DocumentSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.DocumentSelection != null && this.DocumentSelection.SelectedValue != null)
            {
                string type = ((ComboBoxItem)this.DocumentSelection.SelectedValue).Content.ToString();
                if (type != "Select Document")
                {
                    if (type == "PSOA")
                        this.source = "PSOA";
                    else if (type == "Suspension Letter")
                        this.source = "SL";
                    else if (type == "Notice of Decision")
                        this.source = "NOD";
                    else if (type == "Notice of Appeal")
                        this.source = "NOP";
                    else if (type == "Case Report")
                        this.source = "CR";
                    else if (type == "Affidavit(Withdrawal)")
                        this.source = "AW";
                    else if (type == "Affidavit(Decided Appeal)")
                        this.source = "AD";
                    else
                        this.source = "";
                    saveChanges();
                }
            }
        }
        void handle_psoa()
        {
            DocumentGeneration dg = new DocumentGeneration();
            InvokeOperation op = null;
            if (this.source == "PSOA" || this.source == "SL" || this.source == "NOD"
                || this.source == "NOP" || this.source == "CR" || this.source == "AW"
                || this.source == "AD")
            {
                op = dg.getPSOADoc(app.SessionApplicationID, this.source);
                //generate PSOA word document based on template
                if (fileDownloadWindow == null)
                    fileDownloadWindow = new FileDownload();
                fileDownloadWindow.Show();
            }
            else
                GoToNext(this.source);
            if( op != null)
                op.Completed += new EventHandler(client_GetNameCompleted);
        }

        void client_GetNameCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
             
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url + ":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
                this.EditAppealDataForm.Focus();
            }
        }
        
        private void fileUploadButton_Click(object sender, RoutedEventArgs e)
        {
            FileUploadService.FileUploadClient service = new FileUploadService.FileUploadClient();
            fileDialog = new OpenFileDialog();
            fileDialog.Filter = "All Files|*.*";
            bool? result = fileDialog.ShowDialog();
            if (result != null && result == true)
            {
             try
             {
                 Stream strm = fileDialog.File.OpenRead();
                 byte[] Buffer = new byte[strm.Length]; strm.Read(Buffer, 0, (int)strm.Length);
                 strm.Dispose();
                 strm.Close();

                 //Accessing DataContract...

                 FileUploadService.FileUploadFileUploadData file = new FileUploadService.FileUploadFileUploadData();
                 file.FileName = fileDialog.File.Name;
                 file.File = Buffer;

                 FileUploadService.FileUploadFileUploadID ID = new FileUploadService.FileUploadFileUploadID();
                 ID.ApplicationID = app.SessionApplicationID;
                 service.SaveFileAsync(file, ID);
                 
                 service.SaveFileCompleted +=new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(service_SaveFileCompleted);
             }
             catch (Exception fileException)
             {
                 MessageBox.Show("File upload error!!! " + fileException.ToString());
             }
            }
         
        }
        public void service_SaveFileCompleted(object sender,    System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
                MessageBox.Show(fileDialog.File.Name + " Successfully Saved");
            else
                MessageBox.Show("File upload error!!!" + e.ToString());
        }

        private void fileListButton_Click(object sender, RoutedEventArgs e)
        {
            string url = System.Windows.Browser.HtmlPage.Document.DocumentUri.Scheme + "://" + System.Windows.Browser.HtmlPage.Document.DocumentUri.Host + "/tempfiles/" + app.SessionApplicationID;
            System.Windows.Browser.HtmlPage.PopupWindow(new Uri(String.Format(url), UriKind.Absolute),"",null);
        }
        private void saveChanges()
        {
            isVoteSheetupdated = false;
            this.SavingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
            myDispatcherTimer.Start();

            if (this.EditAppealDataForm.ValidateItem() && this.detailDataForm.ValidateItem() && this.newAppellantDataForm.ValidateItem() )
            {
                //date fields, not bind.
                FieldTablesDomainService1 _fieldTableDomainService = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);
                _fieldTableDomainService.RejectChanges();
                if (this.IndexCardsFiled.SelectedDate.HasValue && this.IndexCardsFiled.SelectedDate != this.oIcfDate)
                {
                    DateEntry(this.IndexCardsFiled, "Modified Index Card Filed Date", "IndexCardsFiled", "appeal application editing", this.oIcfDate);
                    this.oIcfDate = this.IndexCardsFiled.SelectedDate.Value;
                }
                if (this.PostCardsMailed.SelectedDate.HasValue && this.PostCardsMailed.SelectedDate != this.oPcmDate)
                {
                    DateEntry(this.PostCardsMailed, "Modified Post Cards Mailed Date", "PostCardsMailed", "appeal application editing", this.oPcmDate);
                    this.oPcmDate = this.PostCardsMailed.SelectedDate.Value;
                }
                if (this.LetterNoticesMailed.SelectedDate.HasValue && this.oLnmDate != this.LetterNoticesMailed.SelectedDate)
                {
                    DateEntry(this.LetterNoticesMailed, "Letter Notices Mailed Date", "LetterNoticesMailed", "appeal application editing", this.oLnmDate);
                    this.oLnmDate = this.LetterNoticesMailed.SelectedDate.Value;
                }
                if (this.fDate.SelectedDate.HasValue && this.fDate.SelectedDate != this.oFDate)
                {
                    DateEntry(this.fDate, "Modified Filed Date", "FiledDate", "appeal application editing.", this.oFDate);
                    this.oFDate = this.fDate.SelectedDate.Value;
                }
                if (this.hDate.SelectedDate.HasValue && this.hDate.SelectedDate != this.oHDate)
                {
                    DateEntry(this.hDate, "Modified Hearing Date", "HearingDate", "appeal application editing.", this.oHDate);
                    //UpdateVoteSheet(this.oHDate);

                    this.oHDate = this.hDate.SelectedDate.Value;
                }
                if (this.efDate.SelectedDate.HasValue && this.efDate.SelectedDate != this.oEDate)
                {
                    DateEntry(this.efDate, "Modified effective Date", "ExpDate", "appeal application editing.", this.oEDate);
                    this.oEDate = this.efDate.SelectedDate.Value;
                }
                if (this.briefDate.SelectedDate.HasValue && this.briefDate.SelectedDate != this.oADate)
                {
                    DateEntry(this.briefDate, "Modified Appellant's Brief Date", "AppellantBrief", "appeal application editing", this.oADate);
                    this.oADate = this.briefDate.SelectedDate.Value;
                }
                if (this.othersBriefDate.SelectedDate.HasValue && this.othersBriefDate.SelectedDate != this.oBDate)
                {
                    DateEntry(this.othersBriefDate, "Modified Other's Brief Date", "OthersBrief", "appeal application editing", this.oBDate);
                    this.oBDate = this.othersBriefDate.SelectedDate.Value;
                }
                if (this.findingAdoptedDate.SelectedDate.HasValue && this.findingAdoptedDate.SelectedDate != this.ofindingAdoptedDate)
                {
                    DateEntry(this.findingAdoptedDate, "Modified FindingAdpoted Date", "FindingAdpotedDate", "appeal application editing", this.ofindingAdoptedDate);
                    this.ofindingAdoptedDate = this.findingAdoptedDate.SelectedDate.Value;
                }
                if (this.statusDate.SelectedDate.HasValue && this.statusDate.SelectedDate != this.ostatusDate)
                {
                    //string comments = this.statusText.Text + "'s date has been set or updated to " + this.statusDate.SelectedDate.Value.ToLongDateString();
                    DateEntry(this.statusDate, "Modified " + this.statusText.Text + " Date", this.statusText.Text.Replace(" ", "") + "Date", "Update Status date", this.ostatusDate);
                    this.ostatusDate = this.statusDate.SelectedDate.Value;
                }
                // track status and FindingAdpoted changes
                if (this.statusText.Text != this.oStatus || this.oFindingAdopted != this.FindingAdpoted.IsChecked)
                {
                    saveStatusChanges();
                }
                try
                {
                    if (_fieldTableDomainService.HasChanges)
                        _fieldTableDomainService.SubmitChanges();
                    if (this.applicationDataSource1.HasChanges)
                        this.applicationDataSource1.SubmitChanges();
                    if (this.PrimaryDataSource.HasChanges)
                        this.PrimaryDataSource.SubmitChanges();
                    if (this.applicationDetailsDataSource.HasChanges)
                        this.applicationDetailsDataSource.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to commit changes for the reason of: " + e.Message);
                }
                if (this.RespondentDataSource.HasChanges)
                {
                    OwnerDomainService1 ods = new OwnerDomainService1();
                    InvokeOperation invoke_ods = ods.checkExistingNames(app.SessionApplicationID, "Respondent");
                    invoke_ods.Completed += new EventHandler(invoke_ods_Completed);
                }
                
                UpdateVoteSheet(this.oHDate);
            }
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument document = new PrintDocument();

            document.PrintPage += (s, args) =>
            {

                args.PageVisual = this.ContactsPanel;
            };
            document.Print("Contacts");
        }

        #endregion 

        #region //Checkbox, Hyperlink action handlers
        
        private void ContactsPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ContactsPanel.Visibility == Visibility.Collapsed)
                this.ContactsPanel.Visibility = Visibility.Visible;
            else
                this.ContactsPanel.Visibility = Visibility.Collapsed;
        }

        private void PermitPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PermitPanel.Visibility == Visibility.Collapsed)
                this.PermitPanel.Visibility = Visibility.Visible;
            else
                this.PermitPanel.Visibility = Visibility.Collapsed;
        }

        private void DatePanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DatePanel.Visibility == Visibility.Collapsed)
                this.DatePanel.Visibility = Visibility.Visible;
            else
                this.DatePanel.Visibility = Visibility.Collapsed;
        }

        private void DeptPanel_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DeptPanel.Visibility == Visibility.Collapsed)
                this.DeptPanel.Visibility = Visibility.Visible;
            else
                this.DeptPanel.Visibility = Visibility.Collapsed;
        }

        private void Conditions_Checked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = true;
        }
        private void Conditions_UnChecked(object sender, RoutedEventArgs e)
        {
            this.ConditionsText.IsEnabled = false;
        }

        private void JRGranted_Checked(object sender, RoutedEventArgs e)
        {
            this.JRGrantedText.IsEnabled = true;
        }
        private void JRGranted_UnChecked(object sender, RoutedEventArgs e)
        {
            this.JRGrantedText.IsEnabled = false;
        }

        private void HandleCheck(object sender, RoutedEventArgs e)
        {
            this.findingAdoptedDate.Visibility = System.Windows.Visibility.Visible;
        }

        private void HandleUnchecked(object sender, RoutedEventArgs e)
        {
            this.findingAdoptedDate.Visibility = System.Windows.Visibility.Collapsed;
        }
        #endregion

        #region //Validating Events
        void newOwnerDataForm_ValidatingItem(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void EditAppealDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.EditAppealDataForm.ValidationSummary.Errors.Clear();
            /*if (this.aPermit.Text.Length < 1)
                this.EditAppealDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Permit Number is required!", "Permit Number is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("aPermit"), this.EditAppealDataForm.CurrentItem));
             * */
            if (this.RespDeptText.Text.Length < 1)
                this.EditAppealDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Respondent Department is required!", "Respondent Department is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("agencySelection"), this.EditAppealDataForm.CurrentItem));
            if (this.DeptActionText.Text.Length < 1)
                this.EditAppealDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Department Action is required!", "Department action is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("actionSelection"), this.EditAppealDataForm.CurrentItem));
        }

        private void newAppellantDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            this.ContactsPanel.Visibility = System.Windows.Visibility.Visible;
            this.ContactsPanel.UpdateLayout();
        }
        private void detailDataForm_ValidatingItem(object obj, System.ComponentModel.CancelEventArgs e)
        {
            //clear error
            this.detailDataForm.ValidationSummary.Errors.Clear();
            if (this.hDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Hearing Date required!", "Appeal Hearing Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("hDate"), this.detailDataForm.CurrentItem));
            if (this.fDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("File Date required!", "File Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("fDate"), this.detailDataForm.CurrentItem));
            if (this.efDate.SelectedDate == null)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Effective Date required!", "Effective Date", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("efDate"), this.detailDataForm.CurrentItem));
            if (this.typeText.Text.Length < 1)
                this.detailDataForm.ValidationSummary.Errors.Add(new ValidationSummaryItem("Permit Type is required!", "Permit Type is missing", ValidationSummaryItemType.PropertyError, new ValidationSummaryItemSource("typeSelection"), this.EditAppealDataForm.CurrentItem));
        }
        #endregion 

        #region //Helper functions

        //update votesheet entries
        private void UpdateVoteSheet(DateTime dateObject)
        {
            vtd = new VoteNTransactionDomainService1();
            var query = vtd.GetOneTblVoteResultQuery(this.newApplication.ApplicationID, this.hDate.SelectedDate.Value);
            vtd.Load(query, voteResultDataSource_LoadedData, null);

            //var query = vtd.GetTblVoteResultsQuery(this.hDate.SelectedDate.Value);
            //vtd.Load(query, newVoteResults_LoadedData, null);
        }
        private void newVoteResults_LoadedData(LoadOperation<tblVoteResult> lo)
        {
            if (lo.Entities.Count() < 2)
            {
                string boardmembers = "";
                for (int i = 0; i < this.VoteNTransactionDataSource1.DataView.Count; i++)
                {
                    tblBoardMember tbm1 = (tblBoardMember)this.VoteNTransactionDataSource1.DataView.GetItemAt(i);
                    boardmembers += tbm1.MemberID + ((i + 1 < this.VoteNTransactionDataSource1.DataView.Count) ? "," : "");
                }

                tblVoteResult tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "PUBLIC COMMENT";
                tvr.DeptRepresentatives = "";
                tvr.ApplicationID = 0;
                tvr.Type = "item_1";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.SortOrder = 0;
                tvr.BoardMembers = boardmembers;
                vtd.tblVoteResults.Add(tvr);

                tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "COMMISSIONER COMMENTS & QUESTIONS";
                tvr.ApplicationID = 0;
                tvr.Type = "item_2";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.SortOrder = 1;
                tvr.BoardMembers = boardmembers;
                vtd.tblVoteResults.Add(tvr);


                tvr = new tblVoteResult();
                tvr.AgencyID = ApplicationStrings.AgencyFor;
                tvr.AppealDescription = "ADOPTION OF MINUTES";
                tvr.ApplicationID = 0;
                tvr.Type = "item_3";
                tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.ModDate = DateTime.Now;
                tvr.SortOrder = 2;
                tvr.BoardMembers = boardmembers;
                vtd.tblVoteResults.Add(tvr);
            }

            var query = vtd.GetOneTblVoteResultQuery(this.newApplication.ApplicationID, this.hDate.SelectedDate.Value);
            vtd.Load(query, voteResultDataSource_LoadedData, null);
        }

        private void voteResultDataSource_LoadedData(LoadOperation<tblVoteResult> lo)
        {
            tblVoteResult tvr = null;
            bool oktoadd = false;
            if (lo.Entities.Count() > 0)
            { // appeal exists in vote results, update it
                tvr = lo.Entities.First();
                if (this.newApplication.Status.Trim() == "Withdrawn" || this.newApplication.Status.Trim() == "Dismissed" || this.newApplication.Status.Trim() == "Rejected" || this.newApplication.Status.Trim() == "Call of the Chair")
                    vtd.tblVoteResults.Remove(tvr);
            }
            else
            {   // appeals doesn't exist in vote results, add new
                tvr = new tblVoteResult();
                tvr.HearingDate = this.hDate.SelectedDate.Value;
            }
            if (! (this.newApplication.Status.Trim() == "Withdrawn" || this.newApplication.Status.Trim() == "Dismissed" || this.newApplication.Status.Trim() == "Rejected" || this.newApplication.Status.Trim() == "Call of the Chair") )
            {
                string appstr = this.newApplication.ApplicationID.ToString();
                string temp = "";
                if (this.newApplication.ApplicationType.Trim() == "REG")
                {
                    temp = appstr.Substring(3, appstr.Length - 3);
                    appstr = (this.hDate.SelectedDate.Value.Year.ToString().Substring(2, 2) + "-" + temp);
                }
                else
                    appstr = ("JR-" + this.hDate.SelectedDate.Value.Year.ToString().Substring(2, 2));

                tvr.ApplicationID = app.SessionApplicationID;
                tvr.AppealDescription = appstr + "\n" + this.aFName.Text + " " + this.aLName.Text + " vs. \n"
                    + ((tblDepartment)this.agencySelection.SelectedItem).DeptName + ", " + this.newApplication.PlanningDept + "\n"
                    + this.aAddress.Text;
                //tvr.HearingDate = this.hDate.SelectedDate.Value;
                tvr.AgencyID = this.newApplication.AgencyID;
                tvr.ModDate = DateTime.Now;
                tvr.Type = this.newApplication.ApplicationType;
                oktoadd = true;
            }
            if (lo.Entities.Count()  < 1 && oktoadd)
                vtd.tblVoteResults.Add(tvr);
            
            try
            {
                vtd.SubmitChanges(VoteSheetUpdated, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to update hearing date for the votesheet due to " + ex.Message);
            }
        }
        private void VoteSheetUpdated(SubmitOperation so)
        {
            if (so.HasError)
                so.MarkErrorAsHandled();
            else
                isVoteSheetupdated = true;
        }
        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
           
                System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;

                if (!FieldDataSource1.IsBusy && !this.applicationDataSource1.IsBusy && isVoteSheetupdated &&
                        !this.PrimaryDataSource.IsBusy && !this.applicationDetailsDataSource.IsBusy && !this.RespondentDataSource.IsBusy)
                {
                    s.Stop();
                    this.SavingData.Visibility = System.Windows.Visibility.Collapsed;
                    if (this.source != "")
                        this.handle_psoa();
                }
        }

        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick_loading(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;

            if (!PrimaryDataSource.IsBusy && !RespondentDataSource.IsBusy && !FieldDataSource1.IsBusy
                && !applicationDetailsDataSource.IsBusy)
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }

        }
      
        // Navigate to next screen
        private void GoToNext(string uri)
        {
            if (this.source != "" && this.source != "PSOA" && this.source != "SL" && this.source != "NOD"
                && this.source != "NOP" && this.source != "CR" && this.source != "AW"
                && this.source != "AD")
                this.NavigationService.Navigate(new Uri(String.Format(uri), UriKind.Relative));
        }

        // track date field changes
        private void DateEntry(DatePicker dateObject, string description, string dateType, string comments, DateTime oldValue)
        {
            FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);
            
            tblDateField OthersBrief = new tblDateField();
            //OthersBrief.DateValue = new DateTime(dateObject.SelectedDate.Value.Year, dateObject.SelectedDate.Value.Month, dateObject.SelectedDate.Value.Day, 16, 0, 0);
            OthersBrief.DateValue = dateObject.SelectedDate.Value;
            OthersBrief.ApplicationID = app.SessionApplicationID;
            OthersBrief.ModDate = DateTime.Now;
            if(oldValue != DateTime.MinValue)
                OthersBrief.OldValue = oldValue;
            OthersBrief.DateDescription = description;
            OthersBrief.DateType = dateType;
            OthersBrief.Comments = comments;
            OthersBrief.UserID = WebContext.Current.User.DisplayName.ToString();
            _FieldTableDomainServices1.tblDateFields.Add(OthersBrief);
        }

        // track status and findingadopted changes
        private void saveStatusChanges()
        {
            FieldTablesDomainService1 _FieldTableDomainServices1 = (FieldTablesDomainService1)(FieldDataSource1.DomainContext);

            tblStatusField tbs = new tblStatusField();

            tbs.ApplicationID = this.newApplication.ApplicationID;
            tbs.ModDate = DateTime.Now;
            tbs.UserID = WebContext.Current.User.DisplayName.ToString();

            if (this.statusText.Text != this.oStatus)
            {
                tbs.Status = this.statusText.Text;
                tbs.Comments = "Changed from " + this.oStatus + " to " + tbs.Status;
               
                //if (tbs.Status == "Rejected" || tbs.Status == "Dismissed" || tbs.Status == "NOD REL" || tbs.Status == "Withdrawn")
                //{
                    if (this.statusDate.SelectedDate.HasValue)
                    {
                        tbs.DateComments = tbs.Status + " changed to " + this.statusDate.SelectedDate.Value.ToShortDateString();
                       // DateEntry(this.statusDate, "Modified " + tbs.Status + " Date", tbs.Status.Replace(" ", "") + "Date", "Update Status date", this.ostatusDate);
                    }
                //}
                _FieldTableDomainServices1.tblStatusFields.Add(tbs);
                this.oStatus = tbs.Status;
            }
            if (this.FindingAdpoted.IsChecked != null && this.oFindingAdopted != this.FindingAdpoted.IsChecked)
            {
                tbs = new tblStatusField();
                tbs.ApplicationID = this.newApplication.ApplicationID;
                tbs.ModDate = DateTime.Now;
                tbs.UserID = WebContext.Current.User.DisplayName.ToString();
                tbs.Status = "FindingAdopted";
                tbs.Comments = "FindingAdopted changed to " + this.FindingAdpoted.IsChecked.ToString();
                _FieldTableDomainServices1.tblStatusFields.Add(tbs);
                this.oFindingAdopted = (Boolean)this.FindingAdpoted.IsChecked;
            }
        }
        //Scroll page vertically when focus changes to a control that is outside the viewable area.
        private void AutoScrollViewer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                FrameworkElement element = FocusManager.GetFocusedElement() as FrameworkElement;

                if (element != null)
                {
                    //ScrollViewer scrollViewer = sender as ScrollViewer;
                    ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
                    try
                    {
                        scrollViewer.ScrollToVerticalOffset(GetVerticalOffset(element, scrollViewer));
                    }
                    catch
                    {
                        //if there's an error, don't scroll.
                    }
                }
            }
        }
       
        private double GetVerticalOffset(FrameworkElement child, ScrollViewer scrollViewer)
        {
            // Ensure the control is scrolled into view in the ScrollViewer.
            System.Windows.Media.GeneralTransform focusedVisualTransform = child.TransformToVisual(scrollViewer);
            Point topLeft = focusedVisualTransform.Transform(new Point(child.Margin.Left, child.Margin.Top));
            Rect rectangle = new Rect(topLeft, child.RenderSize);
            //If the control is taller than the viewport, don't scroll down further than the top of the control.
            double controlRectangleBottom = rectangle.Bottom - scrollViewer.ViewportHeight > scrollViewer.ViewportHeight ? scrollViewer.ViewportHeight : rectangle.Bottom;
            double newOffset = scrollViewer.VerticalOffset + (controlRectangleBottom - scrollViewer.ViewportHeight);
            return newOffset < 0 ? 0 : newOffset + 200; 
        }


        #endregion
    }

    public class MyDatePicker : System.Windows.Controls.DatePicker
    {
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            //base.OnMouseWheel(e);
            e.Handled = true;
        }
    }
  }