﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace BOA_AppealApplication1.Views
{
    public partial class DateLogChildWindow1 : ChildWindow
    {
        App app = (App)Application.Current;
        FieldTablesDomainService1 _FieldTablesDomainServices1 = new FieldTablesDomainService1();

        public DateLogChildWindow1()
        {
            InitializeComponent();
            //this.dateParameter.Value = app.SessionApplicationID;

            //Call the related domain services to retrieve records.
            PagedCollectionView dateListView;
            EntityQuery<tblDateField> query = _FieldTablesDomainServices1.GetApplicationTblDateFieldsQuery(app.SessionApplicationID);
            _FieldTablesDomainServices1.Load<tblDateField>(query).Completed += (sender, args) =>
            {
                List<tblDateField> dateList = ((LoadOperation<tblDateField>)sender).Entities.ToList();
                dateListView = new PagedCollectionView(dateList);
                List<dateFieldList> dateFieldList = new List<dateFieldList>();

                for (int i = 0; i < dateList.Count; i++)
                {
                    dateFieldList sfl = new dateFieldList();
                    if (dateList[i].OldValue == null)
                        continue;
                    sfl.OldValue = ((DateTime)dateList[i].OldValue).ToShortDateString();
                    sfl.ApplicationID = dateList[i].ApplicationID;
                    sfl.Comments = dateList[i].Comments;
                    sfl.ModDate = dateList[i].ModDate.ToShortDateString();
                    sfl.DateType = dateList[i].DateType;
                    sfl.DateValue = dateList[i].DateValue;
                    sfl.OldValue = dateList[i].OldValue == null ? "" : ((DateTime)dateList[i].OldValue).ToShortDateString();
                    sfl.Decision = dateList[i].Decision;
                    sfl.UserID = dateList[i].UserID;
                    
                    sfl.DateDescription = dateList[i].DateDescription;
                    dateFieldList.Add(sfl);
                }
                dateListView = new PagedCollectionView(dateFieldList);
                this.dateLogGrid.ItemsSource = dateListView;
                if (dateListView.CanGroup == true)
                {
                    //Group dates by their types.
                    dateListView.GroupDescriptions.Add(new PropertyGroupDescription("ModDate"));

                    //collapse group view
                    PagedCollectionView pcv = dateLogGrid.ItemsSource as PagedCollectionView;
                    try
                    {
                        foreach (CollectionViewGroup group in pcv.Groups)
                        {
                            //dateLogGrid.ScrollIntoView(group, null);
                            dateLogGrid.CollapseRowGroup(group, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Could not collapse group.
                        MessageBox.Show(ex.Message);
                    }
                }
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void FieldDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                e.MarkErrorAsHandled();
            }
            else
            {
                //collapse group view
                PagedCollectionView pcv = dateLogGrid.ItemsSource as PagedCollectionView;
                try
                {
                    foreach (CollectionViewGroup group in pcv.Groups)
                    {
                        dateLogGrid.ScrollIntoView(group, null);
                        dateLogGrid.CollapseRowGroup(group, true);
                    }
                }
                catch (Exception ex)
                {
                    // Could not collapse group.
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }

    public class dateFieldList
    {
        public int ApplicationID { get; set; }

        public string Comments { get; set; }

        public string DateDescription { get; set; }

        public string DateType { get; set; }

        public DateTime DateValue { get; set; }

        public string OldValue { get; set; }

        public string Decision { get; set; }

        public string ModDate { get; set; }

        public string UserID { get; set; }
    }
}

