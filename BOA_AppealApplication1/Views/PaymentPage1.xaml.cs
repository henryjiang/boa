﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace BOA_AppealApplication1.Views
{
    public partial class PaymentPage1 : Page
    {
        public PaymentPage1()
        {
            InitializeComponent();
            //Initiating all session variables.
            App app = (App)Application.Current;
            this.Title = ApplicationStrings.PaymentPage1;
            this.contactParameter.Value = app.SessionApplicationID;
            this.feeParameter.Value = app.SessionApplicationID;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void ContactDataSource1_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("No Associating Contacts are found!", "No Associating Contacts are found!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

        private void ContactDataSource1_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("No Data Changes!", "No Data Changes!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
        }

    }
}
