﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Printing;

namespace BOA_AppealApplication1.Views.Administration
{
    public partial class VoteSheetPage1 : Page
    {
        //App app = (App)Application.Current;
        FileDownload fileDownloadWindow;
        public bool HasScrolledToNewItem { get; set; }
        App app = (App)Application.Current;

        private ObservableCollection<VoteData> _source = new ObservableCollection<VoteData>();
        public ObservableCollection<VoteData> source {
            get { return _source; }
        }

        // number of board members, this number cannot change!
        public const int NUMBER_OF_BOARDMEMBERS = 5;
        private string boardmembers = "";
        public int RowIndex = 0;

        public VoteSheetPage1()
        {
            InitializeComponent();
            
            //Initialize remote services with session variable.
            this.BoardMemberParameter.Value = ApplicationStrings.AgencyFor;
            this.deptRepLabel.Text = "\nDepartment Representatives:";
            
            // default text for department reps.
            this.deptRepText.Text = "";

            this.voteGrid.LayoutUpdated += new EventHandler(voteGrid_LayoutUpdated);
        }
        
        void dataGrid1_RowsMoved(object sender, RowsMovedEventArgs args)
        {
            // args.StartIndex - The start index of the set of rows that are being moved
            // args.Count - The number of adjacent rows below the start index that are being moved.
            // args.DestinationIndex - The destination for this move

            // Interpreting this move varies by the application.
            // Here we will adjust the SortOrder of the bound objects and then rebind them with the grid.

            IList<VoteData> list = this.voteGrid.ItemsSource as IList<VoteData>;
            this.MoveInList(list, args.StartIndex, args.Count, args.DestinationIndex);
        }
        private void dataGrid1_BeforeMovingRows(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.FlattenSelection();
        }
        public int MoveInList(IList<VoteData> list, int start, int count, int dest)
        {
            int from;
            int to;
            int newDestOfStart;

            if (dest > start)
            {
                from = start;
                to = dest - 1;
                for (int i = 0; i < count; i++)
                {
                    VoteData view = list[start];

                    list.RemoveAt(start);
                    list.Insert(dest - 1, view);
                }
                newDestOfStart = dest - count;
            }
            else
            {
                from = dest;
                to = start + count - 1;
                for (int i = 0; i < count; i++)
                {
                    VoteData view = list[start + i];
                    list.RemoveAt(start + i);
                    list.Insert(dest + i, view);
                }
                newDestOfStart = dest;
            }

            for (int i = from; i <= to; i++)
            {
                ((VoteData)list[i]).SortOrder = i;
            }
            return newDestOfStart;

        }
        // If the user selected row 2 and row 5 then this method will also select row 3 and 4.
        // This is necessary because the logic here assumes that all selected objects are adjacent to each other.
        private void FlattenSelection()
        {
            int topRowIndex = Int32.MaxValue;
            int botRowIndex = Int32.MinValue;

            IList<VoteData> list = this.voteGrid.ItemsSource as IList<VoteData>;
            foreach (VoteData item in this.voteGrid.SelectedItems)
            {
                int index = list.IndexOf(item);
                if (index < topRowIndex)
                    topRowIndex = index;
                if (index > botRowIndex)
                    botRowIndex = index;
            }
            if (this.voteGrid.SelectedItems.Count < (botRowIndex - topRowIndex + 1))
            {
                for (int i = topRowIndex; i <= botRowIndex; i++)
                {
                    VoteData o = list[i];
                    if (this.voteGrid.SelectedItems.Contains(o) == false)
                        this.voteGrid.SelectedItems.Add(o);
                }
            }
        }
        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        void voteGrid_LayoutUpdated(object sender, EventArgs e)
        {
            if (!HasScrolledToNewItem)
            {
                this.voteGrid.Focus();
                this.voteGrid.SelectedIndex = this._source.Count - 1;
                this.voteGrid.ScrollIntoView(this.voteGrid.SelectedItem, this.voteGrid.Columns.First());
                ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
                //scrollViewer.ScrollIntoView(this.voteGrid);
                scrollViewer.ScrollToBottom();
                HasScrolledToNewItem = true;
            }
        }

        private void voteResultDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
            else
            {
                if (this.voteResultDataSource.DataView.Count > 0 )
                    retrieveVoteSheet();  // retrieve votesheet
            }
        }
        private void boarMemberDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (this.boardmembers == "")
            {
                for (int i = 0; i < this.VoteNTransactionDataSource1.DataView.Count && i < NUMBER_OF_BOARDMEMBERS; i++)
                {
                    tblBoardMember tbm1 = (tblBoardMember)this.VoteNTransactionDataSource1.DataView.GetItemAt(i);
                    this.boardmembers += tbm1.MemberID + ((i + 1 < this.VoteNTransactionDataSource1.DataView.Count) ? "," : "");
                    this.voteGrid.Columns[i + 1].Header = tbm1.MemberID;
                    this.voteGrid.Columns[i + 1].CanUserReorder = false;
                }
            }
        }
        private void retrieveVoteSheet()
        {
            if(((tblVoteResult)this.voteResultDataSource.DataView[0]).StartTime != null)
                this.startTime.Value = DateTime.Parse(((tblVoteResult)this.voteResultDataSource.DataView[0]).StartTime.Value.ToString());
            if (((tblVoteResult)this.voteResultDataSource.DataView[0]).EndTime != null)
                this.endTime.Value = DateTime.Parse(((tblVoteResult)this.voteResultDataSource.DataView[0]).EndTime.Value.ToString());
            
            //manually add combobox items
            ComboBoxItem item = new ComboBoxItem();
            ComboBoxItem item_1 = new ComboBoxItem();
            item_1.Content = item.Content = "Select type to ADD";
            item.IsSelected = true;
            item_1.IsSelected = true;
            this.addSelection.Items.Add(item);
            this.addSelection1.Items.Add(item_1);

            ComboBoxItem item1 = new ComboBoxItem();
            ComboBoxItem item1_1 = new ComboBoxItem();
            item1_1.Content = item1.Content = "Create New";
            this.addSelection.Items.Add(item1);
            this.addSelection1.Items.Add(item1_1);

            ComboBoxItem item2 = new ComboBoxItem();
            ComboBoxItem item2_1 = new ComboBoxItem();
            item2_1.Content = item2.Content = "--- Or Copy From ---";
            this.addSelection.Items.Add(item2);
            this.addSelection1.Items.Add(item2_1);

            foreach (tblVoteResult tvr in this.voteResultDataSource.DataView)
            {
                //bind board member vote result headers
                string[] tmp = new string[10]; // 10 is max of board members;
                string[] members = new string[10];
                if (tvr.BoardMembers != null && tvr.BoardMembers != "")
                {
                    // Header = board member initials
                    members = tvr.BoardMembers.Split(new Char[] { ',' });
                    if (this.boardmembers == "")
                    {
                        for (int i = 0; i < members.Count() && i < NUMBER_OF_BOARDMEMBERS; i++)
                        {
                            this.voteGrid.Columns[i + 1].Header = members[i];
                            this.voteGrid.Columns[i + 1].CanUserReorder = false;
                        }
                        this.boardmembers = tvr.BoardMembers;
                    }
                    // populate board member vote results
                    if (tvr.BoardMemberDecisions != null && tvr.BoardMemberDecisions != "")
                    {
                        string[] decisions = tvr.BoardMemberDecisions.Split(new Char[] { ',' });
                        for (int i = 0; i < decisions.Count(); i++)
                        {
                            tmp[i] = decisions[i];
                        }
                    }
                }
                this._source.Add(new VoteData
                {
                    AppealDescription = tvr.AppealDescription,
                    Action = tvr.Action,
                    Speakers = tvr.Speakers,
                    Comments = tvr.Comments,
                    Decision = tvr.Decision,
                    VoteID = tvr.VoteID,
                    StartTime = this.startTime.Value.Value,
                    EndTime = this.endTime.Value == null ? DateTime.Now : this.endTime.Value.Value,
                    hearingDate = tvr.HearingDate,
                    DeptRepresentatives = tvr.DeptRepresentatives,
                    MemberDecision = tmp,
                    SortOrder = tvr.SortOrder.HasValue == false ? 0 : tvr.SortOrder.Value,
                });
                if (tvr.AppealDescription != null && tvr.AppealDescription.IndexOf('\n') > 1)
                {
                    string[] lines = tvr.AppealDescription.Split('\n');
                    ComboBoxItem itemx = new ComboBoxItem();
                    ComboBoxItem itemx_1 = new ComboBoxItem();
                    itemx.Content = lines[0];
                    itemx_1.Content = lines[0];
                    this.addSelection.Items.Add(itemx);
                    this.addSelection1.Items.Add(itemx_1);
                }
            }
                
            this.deptRepText.UpdateLayout();
            this.deptRepText.Text = this._source[0].DeptRepresentatives == null ? "" : this._source[0].DeptRepresentatives;
            
            if (this.voteGrid.ItemsSource != null)
                this.voteGrid.ItemsSource = null;
            this.voteGrid.ItemsSource = this._source;

            // load board members
            if(this.boardmembers == "")
                this.VoteNTransactionDataSource1.Load();

            this.addSelection.SelectionChanged += new SelectionChangedEventHandler(addSelection_SelectionChanged);
            this.addSelection1.SelectionChanged += new SelectionChangedEventHandler(addSelection_SelectionChanged);
        }
      
       
        private void addSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cb != null && cb.SelectedItem != null)
            {
                string type = (cb.SelectedItem as ComboBoxItem).Content.ToString();
                if (type != "--- Or Copy From ---" && type != "Select type to ADD")
                {
                    VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;
                    tblVoteResult tvr = new tblVoteResult();
                    tvr.ModDate = DateTime.Now;
                    tvr.ApplicationID = 0;
                    tvr.HearingDate = this.hearingDate.SelectedDate.Value;
                    tvr.StartTime = this.startTime.Value;
                    tvr.AgencyID = ApplicationStrings.AgencyFor;
                    tvr.Type = "item_added";
                    if (this.boardmembers != "")
                        tvr.BoardMembers = this.boardmembers;

                    if (type == "Create New")
                    {
                        tvr.AppealDescription = "";
                    }
                    else
                    {
                        for (int i = 0; i < this._source.Count; i++)
                        {
                            if (this._source[i].AppealDescription != "" && this._source[i].AppealDescription.IndexOf(type) > -1)
                            {
                                tvr.AppealDescription = this._source[i].AppealDescription;
                            }
                        }
                    }
                    vtd.tblVoteResults.Add(tvr);

                    if (vtd.HasChanges)
                        vtd.SubmitChanges(OnSubmitCompleted_vtd_addButton, null);
                }
            }
        }
        private void OnSubmitCompleted_vtd_addButton(SubmitOperation so)
        {
            if (so.HasError)
            {
                so.MarkErrorAsHandled();
            }
            else
            {
                foreach (tblVoteResult tvr in so.ChangeSet.AddedEntities)
                {
                    if (tvr.VoteID > 0)
                    {
                        this.source.Add(new VoteData
                        {
                            AppealDescription = tvr.AppealDescription,
                            AgencyID = ApplicationStrings.AgencyFor,
                            ModDate = DateTime.Now,
                            ApplicationID = 0,
                            hearingDate = tvr.HearingDate,
                            VoteID = tvr.VoteID,
                            MemberDecision = new string[10],
                            SortOrder = this.source.Count + 1,
                        });
                    }
                }
                if (this.voteGrid.ItemsSource != null)
                    this.voteGrid.ItemsSource = null;
                this.voteGrid.ItemsSource = this._source;
                // load board members
                this.VoteNTransactionDataSource1.Load();

                HasScrolledToNewItem = false;
                this.voteGrid.UpdateLayout();
            }
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            this.SavingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
            myDispatcherTimer.Start();

            VoteNTransactionDomainService1 vtd = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;
            vtd.RejectChanges(); //reject dirty changes;
            //VoteNTransactionDomainService1 memberds = (VoteNTransactionDomainService1)this.MemberResultDataSource.DomainContext;
            for (int i = 0; i < this.source.Count; i++)
            {
               for (int k = 0; k < vtd.tblVoteResults.Count; k++)
               {
                   //tblVoteResult tvr1 = vtd.tblVoteResults.ElementAt(k);
                   if (vtd.tblVoteResults.ElementAt(k).VoteID == this.source[i].VoteID)
                   {
                       //vtd.tblVoteResults.ElementAt(k).VoteID = this.source[i].VoteID;
                       vtd.tblVoteResults.ElementAt(k).Action = this.source[i].Action;
                       vtd.tblVoteResults.ElementAt(k).ModDate = DateTime.Now;
                       vtd.tblVoteResults.ElementAt(k).DeptRepresentatives = this.deptRepText.Text;
                       vtd.tblVoteResults.ElementAt(k).Speakers = this.source[i].Speakers;
                       vtd.tblVoteResults.ElementAt(k).Comments = this.source[i].Comments;
                       vtd.tblVoteResults.ElementAt(k).AppealDescription = this.source[i].AppealDescription;
                       vtd.tblVoteResults.ElementAt(k).Decision = this.source[i].Decision;
                       if (this.startTime.Value == null)
                           vtd.tblVoteResults.ElementAt(k).StartTime = DateTime.Parse("5:00 pm");
                       else
                           vtd.tblVoteResults.ElementAt(k).StartTime = this.startTime.Value;
                       if (this.endTime.Value == null)
                           vtd.tblVoteResults.ElementAt(k).EndTime = DateTime.Now;
                       else
                           vtd.tblVoteResults.ElementAt(k).EndTime = this.endTime.Value;
                       vtd.tblVoteResults.ElementAt(k).BoardMembers = this.boardmembers;
                       string memberDecisions = "";
                       foreach (string memberDecision in this.source[i].MemberDecision)
                       {
                            memberDecisions += memberDecision + ",";
                       }
                       vtd.tblVoteResults.ElementAt(k).BoardMemberDecisions = memberDecisions.TrimEnd(new char[] { ',' });
                       vtd.tblVoteResults.ElementAt(k).AgencyID = ApplicationStrings.AgencyFor;
                       vtd.tblVoteResults.ElementAt(k).SortOrder = i;
                       break;
                   }
               }
            }

            if (vtd.HasChanges)
                vtd.SubmitChanges(voteResult_Completed, null);
        }

        void voteResult_Completed(SubmitOperation so)
        {
            if (so.HasError)
            {
                MessageBox.Show("Failed to save vote results! Error: " + so.Error.Message, "", MessageBoxButton.OK);
            }
            else
            {
                
            }
        }

        private void hearingDate_CalendarClosed(object sender, RoutedEventArgs e)
        {
            if (hearingDate.SelectedDate != null)
            {
                this.source.Clear();
                this.boardmembers = "";
                this.LoadingData.Visibility = System.Windows.Visibility.Visible;
                System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500); // 100 Milliseconds 
                myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
                myDispatcherTimer.Start();

                this.voteResultParameter.Value = new DateTime(hearingDate.SelectedDate.Value.Year, hearingDate.SelectedDate.Value.Month, hearingDate.SelectedDate.Value.Day);
                this.voteSheetPanel.Visibility = System.Windows.Visibility.Visible;

                this.startTime.Value = DateTime.Parse("5:00 pm");
                this.endTime.Value = null;

                if (this.voteResultDataSource.CanLoad)
                    this.voteResultDataSource.Load();
            }
        }

        //give some time for changes to make it through domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!voteResultDataSource.IsBusy && !VoteNTransactionDataSource1.IsBusy )
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                this.SavingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }

        //give some time for votesheet to populate
        private void myDispatcherTimer_Tick1(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!voteResultDataSource.IsBusy && !VoteNTransactionDataSource1.IsBusy )
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }
       
        private void gmButton_Click(object sender, RoutedEventArgs e)
        {
            //generate minutes word document based on template
            if (fileDownloadWindow == null)
                fileDownloadWindow = new FileDownload();
            fileDownloadWindow.Show();
           
            DocumentGeneration dg = new DocumentGeneration();
            InvokeOperation op = dg.getVoteSheetMinutes(this.hearingDate.SelectedDate.Value);
            op.Completed += new EventHandler(client_gmCompleted);
        }
        private void client_gmCompleted(object sender, EventArgs e)
        {
            InvokeOperation op = sender as InvokeOperation;
            if (fileDownloadWindow != null)
                fileDownloadWindow.Close();
             
            //After document is generated, give user option to download.
            if (op.Value != null)
            {
                //construct file url
                string[] strFile = op.Value.ToString().Split('\\');
                string virtualLocalFilePath = "/" + strFile[strFile.Length - 2] + "/" + strFile[strFile.Length - 1];
                Uri uri = System.Windows.Browser.HtmlPage.Document.DocumentUri;
                string url = "";
                if (uri != null)
                {
                    url = uri.Scheme + "://" + uri.Host;
                    if (uri.Port.ToString() != "")
                        url = url + ":" + uri.Port;
                    url = url + virtualLocalFilePath;
                }
                //System.Windows.Browser.HtmlPage.PopupWindow(new Uri(String.Format(url), UriKind.Absolute), "_blank", null);
                System.Windows.Browser.HtmlPage.Window.Navigate(new Uri(String.Format(url), UriKind.Absolute));
             }
             
        }
        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument document = new PrintDocument();

            document.PrintPage += (s, args) =>
            {
                args.PageVisual = this.LayoutRoot;
            };
            document.Print("Vote Sheet");
        }
        
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(String.Format("/Home"), UriKind.Relative));
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            VoteData current = new VoteData();
            VoteNTransactionDomainService1 vtds = (VoteNTransactionDomainService1)this.voteResultDataSource.DomainContext;
            
            if (this.voteGrid.SelectedItem != null)
            {
                current = (VoteData)this.voteGrid.SelectedItem;
                for (int i = 0; i < vtds.tblVoteResults.Count; i++)
                {
                    tblVoteResult tvr = vtds.tblVoteResults.ElementAt(i);
                    if (current.VoteID == tvr.VoteID)
                        vtds.tblVoteResults.Remove(tvr);
                }
               
                this.source.Remove(current);
                if(vtds.HasChanges)
                    vtds.SubmitChanges(voteResult_Completed,null);
                this.voteGrid.SelectedItem = null;
            }
            else
                MessageBox.Show("Please select an item to delete", "", MessageBoxButton.OK);
        }
    }

    public class VoteTypeProvider
    {
        public List<string> VoteTypeList
        {
            get
            {
                return new List<string> { "Yes", "No", "Motion", "Absent", "Abstain" };
            }

        }
    }

    public class VoteData
    {
        public int SortOrder { get; set; }
        public int ApplicationID { get; set; }
        public int VoteID { get; set; }
        public DateTime hearingDate { get; set; }
        public string AgencyID { get; set; }
        public string UserID { get; set; }

        public string Comments { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ModDate { get; set; }
        public DateTime EndTime { get; set; }
        public string DeptRepresentatives { get; set; }

        public string Action { get; set; }
        public string Speakers { get; set; }
        public string Decision { get; set; }

        public string AppealDescription { get; set; }
        public string[] MemberDecision {get;set;}
    }

}
