﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;
using System.ServiceModel.DomainServices.Client;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Printing;


namespace BOA_AppealApplication1.Views
{
    public partial class PrintContacts : ChildWindow
    {
        App app = (App)Application.Current;
        OwnerDomainService1 _ods = new OwnerDomainService1();
        public ObservableCollection<PrintFields> source = new ObservableCollection<PrintFields>();

        public PrintContacts()
        {
            InitializeComponent();
            //this.dateParameter.Value = app.SessionApplicationID;

            //Call the related domain services to retrieve records.
            //PagedCollectionView dateListView;
            this.appellantParameter1.Value = app.SessionApplicationID;
        }

        private void AppellantDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                MessageBox.Show("No Results!", "No Results!", MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            else
            {
                //List<tblNameField> dateList = ((LoadOperation<tblNameField>)sender).Entities.ToList();
                
                for (int i = 0; i < this.AppellantDataSource.DataView.Count; i++)
                {
                    tblNameField tnf = (tblNameField)this.AppellantDataSource.DataView.GetItemAt(i);
                    this.source.Add(new PrintFields
                    {
                        Type = tnf.Type,
                        Name = tnf.FirstName + " " + tnf.LastName,
                        Address = tnf.AddressLine1 + " " + tnf.AddressLine2 + " " + tnf.city + " " + tnf.state + " " + tnf.zip,
                        Email = tnf.email,
                        Phone = tnf.PhoneNumber
                    });
                }
                this.dateLogGrid.ItemsSource = this.source; //dateListView;
            }
            
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument p = new PrintDocument();
            Panel printableParent = dateLogGrid.Parent as Panel;
            bool okToPrint = false;

            System.Windows.Controls.Panel tmp = new System.Windows.Controls.Canvas();

            p.BeginPrint += (object sender1, BeginPrintEventArgs e1) =>
            {
                ((Panel)dateLogGrid.Parent).Children.Remove(dateLogGrid);
                tmp.Children.Add(dateLogGrid);
                tmp.Children.Add(new TextBlock
                {
                    VerticalAlignment = System.Windows.VerticalAlignment.Bottom,
                    Text = "Contact Information",
                    FontSize = 8
                });
            };

            p.EndPrint += (s2, e2) =>
            {
                tmp.Children.Remove(dateLogGrid);
                printableParent.Children.Add(dateLogGrid);
                dateLogGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                dateLogGrid.VerticalAlignment = VerticalAlignment.Stretch;
                dateLogGrid.Margin = new Thickness(0);
            };


            p.PrintPage += (object s, PrintPageEventArgs e1) =>
            {
                // Rotate to landscape if necessary
                if (e1.PrintableArea.Height > e1.PrintableArea.Width)
                {
                    double scale = e1.PrintableArea.Height / e1.PrintableArea.Width;
                    scale = 1.0;

                    tmp.Width = e1.PrintableArea.Height;
                    tmp.Height = e1.PrintableArea.Width * scale;

                    CompositeTransform transform = new CompositeTransform
                    {
                        Rotation = 90,
                        TranslateX = tmp.Height * scale,
                        ScaleX = scale,
                        ScaleY = scale
                    };
                    tmp.RenderTransform = transform;
                    tmp.RenderTransformOrigin = new Point(0.5, 0.5);
                    dateLogGrid.Margin = new Thickness(48 / scale, 96 / scale, 48 / scale, 0);
                    dateLogGrid.Width = tmp.Width - 96 / scale;
                    dateLogGrid.Height = tmp.Height - 96 / scale;

                }
                else
                {
                    tmp.Width = e1.PrintableArea.Width;
                    tmp.Height = e1.PrintableArea.Height;
                    dateLogGrid.Margin = new Thickness(48, 96, 0, 0);
                    dateLogGrid.Width = tmp.Width - 96;
                    dateLogGrid.Height = tmp.Height - 96;
                }
                dateLogGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                dateLogGrid.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                tmp.InvalidateArrange();
                tmp.InvalidateMeasure();
                okToPrint = true;

                //tmp.SizeChanged += (s2, e2) => { okToPrint = true; };
                //PrintableElement.SizeChanged += (s2, e2) => { okToPrint = true; };

                if (okToPrint)
                {
                    e1.PageVisual = tmp;
                    e1.HasMorePages = false;
                }
                else
                {
                    e1.PageVisual = null;
                    e1.HasMorePages = true;
                }
            };

            p.Print("Contact Information");

            p.EndPrint += new EventHandler<EndPrintEventArgs>(document_EndPrint);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void document_EndPrint(Object sender, System.Windows.Printing.EndPrintEventArgs evt)
        {
            this.Close();
        }
        
    }
    public class PrintFields
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    
}

