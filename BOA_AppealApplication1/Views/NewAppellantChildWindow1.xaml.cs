﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;

namespace BOA_AppealApplication1.Views
{
    public partial class NewAppellantChildWindow1 : ChildWindow
    {
        public tblNameField newAppellant { get; set; }

        public NewAppellantChildWindow1()
        {
            InitializeComponent();
            newAppellant = new tblNameField();
            this.newAppellantDataForm.CurrentItem = newAppellant;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (this.newAppellantDataForm.CurrentItem != null)
            {
                if (this.newAppellantDataForm.IsItemValid)
                {
                    newAppellantDataForm.CommitEdit();
                    OwnerDomainService1 _OwnerDomainServices1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                    _OwnerDomainServices1.tblNameFields.Add(newAppellant);
                    AppellantDataSource1.SubmitChanges();
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            newAppellantDataForm.CancelEdit();
        }
    }
}

