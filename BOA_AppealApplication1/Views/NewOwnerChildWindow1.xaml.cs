﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;

namespace BOA_AppealApplication1.Views
{
    public partial class NewOwnerChildWindow1 : ChildWindow
    {
        public tblNameField newOwner { get; set; }

        public NewOwnerChildWindow1()
        {
            InitializeComponent();
            newOwner = new tblNameField();
            this.newOwnerDataForm.CurrentItem = newOwner;
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (this.newOwnerDataForm.CurrentItem != null)
            {
                if (this.newOwnerDataForm.IsItemValid)
                {
                    newOwner.Type = "Co-Owner";
                    newOwner.ApplicationID = 0;
                    newOwnerDataForm.CommitEdit();                    
                    OwnerDomainService1 _OwnerDomainServices1 = (OwnerDomainService1)(OwnerDataSource1.DomainContext);
                    _OwnerDomainServices1.tblNameFields.Add(newOwner);
                    OwnerDataSource1.SubmitChanges();
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.newOwnerDataForm.CancelEdit();
        }
    }
}

