﻿namespace BOA_AppealApplication1
{
    using System.Windows.Controls;
    using System.Windows.Navigation;
    using System;
   
    using BOA_AppealApplication1.Web.Services;
    using BOA_AppealApplication1.Web;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Data;
    using System.Diagnostics;
    using System.Threading;
    using System.Windows.Interactivity;
    using System.Collections.ObjectModel;

    
    /// <summary>
    /// Home page for the application.
    /// </summary>
    public partial class Home : Page
    {
        /// <summary>
        /// Creates a new <see cref="Home"/> instance.
        /// </summary>
        App app = (App)Application.Current;
 
        public Home()
        {
            InitializeComponent();
            this.Title = ApplicationStrings.HomePageTitle;
            this.applicationParameter.Value = ApplicationStrings.AgencyFor;

            string username = WebContext.Current.User.DisplayName;
            if (username.ToLower() == "guest")
            {
                this.newAppealButton.IsEnabled = false;
                this.newJRButton.IsEnabled = false;
            }
            this.LoadingData.Visibility = System.Windows.Visibility.Visible;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000); // 100 Milliseconds 
            myDispatcherTimer.Tick += new EventHandler(myDispatcherTimer_Tick);
            myDispatcherTimer.Start();

//            this.dataPager1.PageIndexChanged +=new EventHandler<EventArgs>(dataPager_PageIndexChanged);
//            this.dataPager2.PageIndexChanged += new EventHandler<EventArgs>(dataPager_PageIndexChanged);

            this.applicationGrid.Focus();
         }

        /// <summary>
        /// Executes when the user navigates to this page.
        /// </summary>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //ApplicationDomainService1 context = new ApplicationDomainService1();
            //this.applicationGrid.ItemsSource = context.tblApplications;
            //context.Load(context.GetTblApplicationsQuery());
            ScrollViewer scrollViewer = (ScrollViewer)this.app.RootVisual;
            scrollViewer.ScrollToTop();
        }

        // save sort order before navigating away from current page
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            PagedCollectionView itemListView = (PagedCollectionView) this.applicationGrid.ItemsSource;
            if (itemListView.SortDescriptions.Count > 0)
            {
                System.ComponentModel.SortDescription sd = itemListView.SortDescriptions[0];
                app.SessionSortHeader = sd.PropertyName;
                app.SessionSortOrder = sd.Direction;
                app.SessionSortPageIndex = this.dataPager1.PageIndex;
            }
            base.OnNavigatingFrom(e);
        }
        private void newAppealButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            app.SessionRequestType = "REG";
            this.NavigationService.Navigate(new Uri(String.Format("/NewAppeal"), UriKind.Relative));
        }

        private void newJR_Click(object sender, RoutedEventArgs e)
        {
            app.SessionRequestType = "JR";
            this.NavigationService.Navigate(new Uri(String.Format("/NewAppeal"), UriKind.Relative));
        }
        private void applicationDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
            DomainDataSource dds = (DomainDataSource)sender;
            this.applicationGrid.UpdateLayout();
            ObservableCollection < MyProjection >  myList = new ObservableCollection<MyProjection>() { };
            //this.applicationGrid.ItemsSource = new ObservableCollection<MyProjection>() { };
            foreach (MyProjection myApp in dds.DataView)
            {
                if (myApp.ApplicationIDstr.IndexOf(this.appealText.Text) > -1 || this.appealText.Text == "")
                {
                    //(this.applicationGrid.ItemsSource as ObservableCollection<MyProjection>).Add(myApp);
                    myList.Add(myApp);
                }
            }
            PagedCollectionView itemListView = new PagedCollectionView(myList);
            this.dataPager1.Source = itemListView;
            this.dataPager2.Source = itemListView;
            if (app.SessionSortHeader != null)
            {
                itemListView.SortDescriptions.Add(new System.ComponentModel.SortDescription(app.SessionSortHeader,app.SessionSortOrder));
                this.dataPager1.PageIndex = app.SessionSortPageIndex;
            }
            this.applicationGrid.ItemsSource = itemListView;
           
        }
        private void JR_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            DomainDataSourceView dds = this.applicationDataSource.DataView;
            ObservableCollection<MyProjection> myList = new ObservableCollection<MyProjection>() { };
            foreach (MyProjection myApp in dds)
            {
                if (myApp.ApplicationIDstr.IndexOf(this.appealText.Text) > -1 || this.appealText.Text == "")
                {
                    if(myApp.ApplicationType.Trim() == "JR")
                        myList.Add(myApp);
                }
            }
            PagedCollectionView itemListView = new PagedCollectionView(myList);
            this.applicationGrid.ItemsSource = itemListView;
            this.dataPager1.Source = itemListView;
            this.dataPager2.Source = itemListView;
        }
        private void Reg_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            DomainDataSourceView dds = this.applicationDataSource.DataView;
            ObservableCollection<MyProjection> myList = new ObservableCollection<MyProjection>() { };
            foreach (MyProjection myApp in dds)
            {
                if (myApp.ApplicationIDstr.IndexOf(this.appealText.Text) > -1 || this.appealText.Text == "")
                {
                    if (myApp.ApplicationType.Trim() == "REG")
                        myList.Add(myApp);
                }
            }
            PagedCollectionView itemListView = new PagedCollectionView(myList);
            this.applicationGrid.ItemsSource = itemListView;
            this.dataPager1.Source = itemListView;
            this.dataPager2.Source = itemListView;
        }
        private void ALL_HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            DomainDataSourceView dds = this.applicationDataSource.DataView;
            ObservableCollection<MyProjection> myList = new ObservableCollection<MyProjection>() { };
            foreach (MyProjection myApp in dds)
            {
                if (myApp.ApplicationIDstr.IndexOf(this.appealText.Text) > -1 || this.appealText.Text == "")
                    myList.Add(myApp);
            }
            PagedCollectionView itemListView = new PagedCollectionView(myList);
            this.applicationGrid.ItemsSource = itemListView;
            this.dataPager1.Source = itemListView;
            this.dataPager2.Source = itemListView;
        }
        private void appealDetailButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(String.Format("/AppealDetailPage1"), UriKind.Relative));
        }

        private void applicationGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid currentGrid = (DataGrid)sender;
            if (currentGrid.SelectedItem != null)
            {
                app.SessionApplicationID = ((MyProjection)currentGrid.SelectedItem).ApplicationID;
            }
        }

        private void editDetailButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri(String.Format("/EditAppeal"), UriKind.Relative));
        }

        private void applicationDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (e.HasError)
            {
                //Database return NULL value and ERROR is handled here.
                e.MarkErrorAsHandled();
            }
        }

        private void DataGridDoubleClickBehavior_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            MyProjection currentGrid = this.applicationGrid.SelectedItem as MyProjection;
            app.SessionApplicationID = currentGrid.ApplicationID;
            //ID 1 - 10000 are jurisdiction requests
            if (currentGrid.ApplicationType.Trim() == "JR")
            {
                app.SessionRequestType = "JR";
                this.NavigationService.Navigate(new Uri(String.Format("/EditAppealJR"), UriKind.Relative));
            }
            else
            {
                app.SessionRequestType = "REG";
                this.NavigationService.Navigate(new Uri(String.Format("/EditAppeal"), UriKind.Relative));
            }
        }

        //give some time for changes to make it to domainservices.
        private void myDispatcherTimer_Tick(object o, EventArgs sender)
        {
            System.Windows.Threading.DispatcherTimer s = (System.Windows.Threading.DispatcherTimer)o;
            if (!applicationDataSource.IsBusy)        
            {
                this.LoadingData.Visibility = System.Windows.Visibility.Collapsed;
                s.Stop();
            }
        }
    }

    /// <summary>

/// Class to attach mouse click events to UIElements

/// </summary>

    public class MouseClickManager
    {
        #region Private members

        private event MouseButtonEventHandler _click;
        private event MouseButtonEventHandler _doubleClick;

        #endregion
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseClickManager"/> class.
        /// </summary>
        /// <param name="control">The control.</param>
        public MouseClickManager(int doubleClickTimeout)
        {
            this.Clicked = false;
            this.DoubleClickTimeout = doubleClickTimeout;
        }

        #endregion
        #region Events

        public event MouseButtonEventHandler Click
        {
            add { _click += value; }
            remove { _click -= value; }
        }

        public event MouseButtonEventHandler DoubleClick
        {
            add { _doubleClick += value; }
            remove { _doubleClick -= value; }
        }

        /// <summary>
        /// Called when [click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void OnClick(object sender, MouseButtonEventArgs e)
        {
            if (_click != null)
            {
                Debug.Assert(sender is Control);
                (sender as Control).Dispatcher.BeginInvoke(_click, sender, e);
            }
        }

        /// <summary>
        /// Called when [double click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void OnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_doubleClick != null)
            {
                _doubleClick(sender, e);
            }
        }

        /// <summary>

        /// Handles the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void HandleClick(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                if (this.Clicked)
                {
                    this.Clicked = false;
                    OnDoubleClick(sender, e);
                }
                else
                {
                    this.Clicked = true;

                    ParameterizedThreadStart threadStart = new ParameterizedThreadStart(ResetThread);
                    Thread thread = new Thread(threadStart);
                    thread.Start(e);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MouseClickManager"/> is clicked.
        /// </summary>
        /// <value><c>true</c> if clicked; otherwise, <c>false</c>.</value>
        private bool Clicked { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>The timeout.</value>
        public int DoubleClickTimeout { get; set; }

        #endregion

        #region Methods

        #region Private
        /// <summary>
        /// Resets the thread.
        /// </summary>
        /// <param name="state">The state.</param>
        private void ResetThread(object state)
        {
            Thread.Sleep(this.DoubleClickTimeout);

            lock (this)
            {
                if (this.Clicked)
                {
                    this.Clicked = false;
                    OnClick(this, (MouseButtonEventArgs)state);
                }
            }
        }
        #endregion
        #endregion
    }
    
    public class DataGridDoubleClickBehavior : Behavior<DataGrid>
    {
        private readonly MouseClickManager _gridClickManager;

        public event EventHandler<MouseButtonEventArgs> DoubleClick;

        public DataGridDoubleClickBehavior()
        {
            _gridClickManager = new MouseClickManager(300);
            _gridClickManager.DoubleClick += new MouseButtonEventHandler(_gridClickManager_DoubleClick);
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.LoadingRow += OnLoadingRow;
            AssociatedObject.UnloadingRow += OnUnloadingRow;
        }

        void OnUnloadingRow(object sender, DataGridRowEventArgs e)
        {
            //row is no longer visible so remove double click event otherwise
            //row events will miss fire
            e.Row.MouseLeftButtonUp -= _gridClickManager.HandleClick;
        }

        void OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            //row is visible in grid, wire up double click event
            e.Row.MouseLeftButtonUp += _gridClickManager.HandleClick;
        }
        
        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.LoadingRow -= OnLoadingRow;
            AssociatedObject.UnloadingRow -= OnUnloadingRow;
        }

        void _gridClickManager_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DoubleClick != null)
                DoubleClick(sender, e);
        }

    }
}
