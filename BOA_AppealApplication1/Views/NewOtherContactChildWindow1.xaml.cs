﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BOA_AppealApplication1.Web;
using BOA_AppealApplication1.Web.Services;

namespace BOA_AppealApplication1.Views
{
    public partial class NewOtherContactChildWindow1 : ChildWindow
    {
        public tblNameField newContact { get; set; }

        public NewOtherContactChildWindow1()
        {
            InitializeComponent();
            newContact = new tblNameField();
            this.newContactDataForm.CurrentItem = newContact;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (this.newContactDataForm.CurrentItem != null)
            {
                if (this.newContactDataForm.IsItemValid)
                {
                    newContactDataForm.CommitEdit();
                    OwnerDomainService1 _OwnerDomainServices1 = (OwnerDomainService1)(AppellantDataSource1.DomainContext);
                    _OwnerDomainServices1.tblNameFields.Add(newContact);
                    AppellantDataSource1.SubmitChanges();
                }
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            newContactDataForm.CancelEdit();
        }
    }

    public class ContactTypeProvider
    {
        public List<string> ContactTypeList
        {
            get
            {
                return new List<string> { "Co-Owner", "Co-Appellant", "Interested Party" };
            }

        }
    }
}

