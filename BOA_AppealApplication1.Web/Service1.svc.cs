﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Text;
using System.Web.Hosting;

using Microsoft.Office.Interop.Word;

namespace BOA_AppealApplication1.Web
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Service1
    {

        [OperationContract]
        public string getPSOADoc(string ApplicationID)
        {
            string templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\BOARD_OF_APPEALS.doc";
            //create a temp file, these temp files are to be delete daily
            string[] tempFile = Path.GetTempFileName().Split('\\');
            string resultFile = HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + tempFile[tempFile.Length - 1] + ".doc";
            string localFilePath = getWordDocument(ApplicationID, resultFile, templateFile);
            return localFilePath;
        }

        //non operational contracts, helper functions
        //create PSOA word document
        public string getWordDocument(string app_id, string resultFile, string templateFile)
        {
            string names = " ";
            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;
            ldb.tblApplicationsFields = "HearingDate,FileDate";
            ldb.tblNameFields = "FirstName, LastName";
            XDocument doc = ldb.createQuery();

            if (doc.Element("ERROR") != null)
            {
                return doc.Element("ERROR").Value;
            }
            //XDocument doc = XDocument.Load("C:\\Documents and Settings\\hjiang\\My Documents\\Visual Studio 2010\\Projects\\ConsoleApplication1\\ConsoleApplication1\\bin\\Debug\\sample.xml");
            //create missing ref
            object missing = System.Reflection.Missing.Value;

            //use microsoft word 12.0 library
            Application wordApp = new Application();
            Microsoft.Office.Interop.Word._Document adoc = new Microsoft.Office.Interop.Word.Document();
            adoc = wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);

            wordApp.Visible = false;
            object readOnly = false;
            object isVisible = false;
            //open word template
            try
            {
                //load template doc
                //adoc = wordApp.Documents.Open("C:\\Documents and Settings\\hjiang\\My Documents\\Visual Studio 2010\\Projects\\ConsoleApplication1\\ConsoleApplication1\\bin\\Debug\\BOARD OF APPEALS.doc", ref missing,
                adoc = wordApp.Documents.Open(templateFile,
                                                            ref readOnly, ref missing, ref missing, ref missing,
                                                            ref missing, ref missing, ref missing, ref missing,
                                                            ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);
                //activate document
                adoc.Activate();

                //get & fill values from tblApplications
                DateTime HearingDate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                bookmarkReplace("hearing_date", HearingDate.ToShortDateString(), adoc);
                if (doc.Element("Document").Element("Applications").Element("FileDate").Value != "1/1/0001")
                    bookmarkReplace("file_date", DateTime.Parse(doc.Element("Document").Element("Applications").Element("FileDate").Value).ToShortDateString(), adoc);
                //thursdays prior to hearing 
                bookmarkReplace("thursdays_4", HearingDate.AddDays(-27).ToShortDateString(), adoc);
                bookmarkReplace("thursdays_2", HearingDate.AddDays(-13).ToShortDateString(), adoc);
                bookmarkReplace("thursdays_1", HearingDate.AddDays(-6).ToShortDateString(), adoc);

                //get & fill values from tblNameField
                foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                {
                    names = String.Concat(e.Element("LastName").Value, " ", e.Element("FirstName").Value, ",", names);
                }

                names = names.Remove(names.LastIndexOf(','), 1);
                bookmarkReplace("all_names", names, adoc);

                //save to a different document, leave the original document untouch.
                object saveas = resultFile;
                adoc.SaveAs(saveas, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                Microsoft.Office.Interop.Word._Document rDoc = adoc;
                //release winword process
                adoc.Close(false, ref missing, ref missing);
                ((Microsoft.Office.Interop.Word._Application)wordApp).Quit(false, ref missing, ref missing);
                return saveas.ToString();
            }
            catch (Exception e)
            {
                adoc.Close(false, ref missing, ref missing);
                ((Microsoft.Office.Interop.Word._Application)wordApp).Quit(false, ref missing, ref missing);
                return e.ToString();
            }
        }

        //using MS word bookmarks, fill in the document with values from DB.
        static void bookmarkReplace(string bookmarkName, string value, Microsoft.Office.Interop.Word._Document adoc)
        {
            //create bookmarks from bookmark names; bookmark names are created in the word template
            object bookmarkStart = (object)string.Concat(bookmarkName, "_start");
            object bookmarkEnd = (object)string.Concat(bookmarkName, "_end");

            object ibkStart = adoc.Bookmarks.get_Item(ref bookmarkStart).Range.Start;
            object ibkEnd = adoc.Bookmarks.get_Item(ref bookmarkEnd).Range.Start;
            Microsoft.Office.Interop.Word.Range rng = adoc.Range(ref ibkStart, ref ibkEnd);

            //this is to calculate the length of the input field, tabs are counted as 1 but it's actually 15 white spaces.
            int rngLength = 0;
            foreach (char c in rng.Text.ToCharArray())
            {
                if (c.Equals('\t'))
                    rngLength += 15;
                else
                    rngLength++;
            }

            //if there are more names than the current field can hold, split the names into 2 or more lines.
            if (bookmarkName.Equals("all_names") && (value.Length > rngLength / 2))
            {

                string[] names = value.Split(',');
                string extra = "";
                string orgValue = "";
                foreach (string name in names)
                {
                    if (string.Concat(orgValue, name).Length > (rngLength / 2))
                        extra = string.Concat(extra, name, ",");
                    else
                        orgValue = string.Concat(orgValue, name, ",");
                }
                bookmarkReplace("all_names", orgValue.Remove(orgValue.LastIndexOf(','), 1).Trim(), adoc);
                bookmarkReplace("all_names_ext1", extra.Remove(extra.LastIndexOf(','), 1).Trim(), adoc);

            }
            else
            {   //pad the input with white spaces, this is because the word document template wants a specific format.
                if ((rngLength > (value.Length * 2)) && (bookmarkName.IndexOf("all_names") < 0))
                {
                    int padLength = (rngLength - value.Length) / 2;
                    string dummy = "";
                    int padleft = padLength > 10 ? (padLength - 10) : 0;
                    value = string.Concat(dummy.PadLeft(padleft, ' '), value, dummy.PadRight(padleft, ' '));
                }
                rng.Text = value;
                //create a new bookmark
                object oRng = rng;
                adoc.Bookmarks.Add(bookmarkName, ref oRng);
            }
        }
    }

    public class LicenseDB2Word
    {
        private string application_id;
        private string app_fields;
        private string name_fields;
        private tblApplication tbl_a;
        private tblNameField tbl_n;

        public LicenseDB2Word()
        {
            this.tbl_a = tblApplication.CreatetblApplication("", "");
            this.tbl_n = tblNameField.CreatetblNameField("", "");
        }
        public string ApplicationID
        {
            set
            {
                this.application_id = value;
            }
        }
        public string tblApplicationsFields
        {
            get
            {
                return this.app_fields;
            }
            set
            {
                this.app_fields = value;
            }
        }
        public string tblNameFields
        {
            get
            {
                return this.name_fields;
            }
            set
            {
                this.name_fields = value;
            }
        }
        public XDocument createQuery()
        {
            LicenseDBEntities dbcontext = new LicenseDBEntities();

            string[] nameFields = this.tblNameFields.Split(',');
            string[] appFields = this.tblApplicationsFields.Split(',');

            try
            {
                var query = (from tbl_n in dbcontext.tblNameFields
                             join tbl_a in dbcontext.tblApplications on tbl_n.ApplicationID equals tbl_a.ApplicationID into g
                             from tbl_a in g.DefaultIfEmpty()
                             where tbl_n.ApplicationID  == (this.application_id)
                             select new { tbl_n, tbl_a }).ToList();

                //select new { tbl_n.FirstName, tbl_n.LastName, tbl_a.HearingDate, tbl_a.FileDate }).ToList();
                if (query.Count > 0)
                {
                    this.tbl_a = query[0].tbl_a;

                    //get the dates
                    var dates = (from tbl_d in dbcontext.tblDateFields
                                 orderby tbl_d.ModDate descending
                                 where tbl_d.ApplicationID == this.application_id
                                 select new { tbl_d}).ToList();

                    XElement apps_xml = new XElement("Applications");
                    foreach (string s in appFields)
                    {
                        apps_xml.Add(new XElement(s.Trim(), tblaSwitcher(s.Trim().ToLower())));
                    }
                    //insert dates
                    for(int i=0;i <dates.Count();i++)
                    //foreach(var w in dates[1].tbl_d)
                    {
                        tblDateField w = (tblDateField) dates[i].tbl_d;
                        
                        if (apps_xml.XPathSelectElement(w.DateType.ToString().Trim()) == null)
                            apps_xml.Add(new XElement(w.DateType.ToString().Trim(), w.DateValue));
                    }
                    XElement names_xml = new XElement("Names");
                    foreach (var i in query)
                    {
                        this.tbl_n = i.tbl_n;
                        XElement name_xml = new XElement("Name");
                        foreach (string s in nameFields)
                        {
                            name_xml.Add(new XElement(s.Trim(), tblnSwitcher(s.Trim().ToLower())));
                        }
                        names_xml.Add(name_xml);
                    }
                    XDocument doc = new XDocument(
                                            new XElement("Document",
                                                            new XElement(apps_xml),
                                                            new XElement(names_xml)
                                                            ));
                    return doc;
                }
                else
                {
                    return null;
                }
            }
            catch (XmlException xe)
            {
                XDocument doc = new XDocument(new XElement("ERROR", xe.ToString()));
                return doc;
            }
        }

        public string tblaSwitcher(string s)
        {
            switch (s)
            {
                case "application": return this.tbl_a.ApplicationID;
                case "filedate": return this.tbl_a.FileDate.GetValueOrDefault().ToString();
                case "status": return this.tbl_a.Status;
                case "expdate": return this.tbl_a.ExpDate.GetValueOrDefault().ToString();
                case "permitnumber": return this.tbl_a.PermitNumber;
                case "routenumber": return this.tbl_a.RouteNumber;
                case "hearingdate": return this.tbl_a.HearingDate.GetValueOrDefault().ToString();
                case "agencyid": return this.tbl_a.AgencyID;
                case "dba": return this.tbl_a.DBA;
                case "public_comments": return this.tbl_a.Public_Comments;
                case "comm_comments": return this.tbl_a.Comm_Comments;
                case "casenum": return this.tbl_a.CaseNum;
                case "casetype": return this.tbl_a.CaseType;
                case "related_agency": return this.tbl_a.Related_Agency;
                default: return "";
            }
        }
        public string tblnSwitcher(string s)
        {
            switch (s)
            {
                case "namedescription": return this.tbl_n.NameDescription;
                case "lastname": return this.tbl_n.LastName;
                case "middlename": return this.tbl_n.MiddleName;
                case "firstname": return this.tbl_n.FirstName;
                case "addressline1": return this.tbl_n.AddressLine1;
                case "addressline2": return this.tbl_n.AddressLine2;
                case "phonenumber": return this.tbl_n.PhoneNumber;
                case "type": return this.tbl_n.Type;
                case "comments": return this.tbl_n.Comments;
                case "applicationid": return this.tbl_n.ApplicationID;
                case "moddate": return this.tbl_n.ModDate.GetValueOrDefault().ToString();
                case "type2": return this.tbl_n.Type2;
                default: return "";
            }
        }
    }

}
