﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Hosting;

namespace BOA_AppealApplication1.Web
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FileUpload
    {
        [OperationContract]
        public void SaveFile(FileUploadData uploadfile, FileUploadID Application)
        {
            FileStream filestream = null;
            int ApplicationID = Application.ApplicationID;
            if (Directory.Exists(HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID))
            {
                filestream = new FileStream(HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID + "\\" + uploadfile.FileName, FileMode.Create);
            }
            else
            {
                DirectoryInfo directory = Directory.CreateDirectory(HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID + "\\");
                if (directory.Exists)
                    filestream = new FileStream(HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID + "\\" + uploadfile.FileName, FileMode.Create);
                else
                    throw new Exception("Failed to create directory: " + HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID + "\\");
            }
            if (filestream != null)
            {
                filestream.Write(uploadfile.File, 0, uploadfile.File.Length);
                filestream.Close();
                filestream.Dispose();
            }
            else
                throw new Exception("Failed to create directory: " + HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + ApplicationID + "\\");
        }


        [DataContract]
        public class FileUploadData
        {
            [DataMember]
            public string FileName;
            [DataMember]
            public byte[] File;
        }
        public class FileUploadID
        {
            [DataMember]
            public int ApplicationID;
        }
    }
}
