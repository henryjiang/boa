﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies tblNameFieldMetadata as the class
    // that carries additional metadata for the tblNameField class.
    [MetadataTypeAttribute(typeof(tblNameField.tblNameFieldMetadata))]
    public partial class tblNameField
    {

        // This class allows you to attach custom attributes to properties
        // of the tblNameField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblNameFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblNameFieldMetadata()
            {
            }

            [Display(Name="Address:")]
            public string AddressLine1 { get; set; }

            [Display(Name = "City, State, Zip Code:")]
            public string AddressLine2 { get; set; }

            [Display(Name = "Appeal NO:")]
            public string ApplicationID { get; set; }

            public string Comments { get; set; }

            [Display(Name = "First Name:")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name:")]
            
            public string LastName { get; set; }

            [Display(Name = "Middle Name:")]
            public string MiddleName { get; set; }

            public Nullable<DateTime> ModDate { get; set; }

            [Editable(true)]
            [ReadOnly(false)]
            public string NameDescription { get; set; }

            [Display(Name = "Phone Number:")]
            public string PhoneNumber { get; set; }

            public string cellphone { get; set; }

            public string city { get; set; }

            public string state { get; set; }

            public string zip { get; set; }

            public string fax { get; set; }

            public string email { get; set; }

            public string Type { get; set; }

            public string Type2 { get; set; }

            public string UserID { get; set; }
        }
    }
}
