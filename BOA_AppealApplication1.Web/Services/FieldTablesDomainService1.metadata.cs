﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies tblDateFieldMetadata as the class
    // that carries additional metadata for the tblDateField class.
    [MetadataTypeAttribute(typeof(tblDateField.tblDateFieldMetadata))]
    public partial class tblDateField
    {

        // This class allows you to attach custom attributes to properties
        // of the tblDateField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblDateFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblDateFieldMetadata()
            {
            }

            public int ApplicationID { get; set; }

            public string Comments { get; set; }

            public string DateDescription { get; set; }

            public string DateType { get; set; }

            public DateTime DateValue { get; set; }

            public DateTime OldValue { get; set; }

            public string Decision { get; set; }

            public DateTime ModDate { get; set; }

            public string UserID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblStatusFieldMetadata as the class
    // that carries additional metadata for the tblStatusField class.
    [MetadataTypeAttribute(typeof(tblStatusField.tblStatusFieldMetadata))]
    public partial class tblStatusField
    {

        // This class allows you to attach custom attributes to properties
        // of the tblStatusField class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblStatusFieldMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblStatusFieldMetadata()
            {
            }

            public int ApplicationID { get; set; }

            public string Comments { get; set; }

            public DateTime ModDate
            {
                get;
                set;
            }
            
            public string Status { get; set; }

            public string DateComments { get; set; }

            public string UserID { get; set; }
        }
    }
}
