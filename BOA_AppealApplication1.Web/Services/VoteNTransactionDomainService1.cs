﻿
namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;


    // Implements application logic using the LicenseDBEntities3 context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class VoteNTransactionDomainService1 : LinqToEntitiesDomainService<LicenseDBEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblBoardMembers' query.
        public IQueryable<tblBoardMember> GetTblBoardMembers()
        {
            return this.ObjectContext.tblBoardMembers;
        }

        public IQueryable<tblBoardMember> GetTblAgencyBoardMembers(string agencyID)
        {
            return this.ObjectContext.tblBoardMembers.Where(e => e.AgencyID == agencyID && e.Status == "A");
        }
        public IQueryable<tblBoardMember> GetTblBoardMember(string memberID, string agencyID)
        {
            return this.ObjectContext.tblBoardMembers.Where(e => e.AgencyID == agencyID && e.MemberID == memberID);
        }

        public int checkBoardMember(string MemberID, string agencyID)
        {
            return this.ObjectContext.tblBoardMembers.Where(e => e.AgencyID == agencyID && e.MemberID == MemberID).Count();
        }

        public void InsertTblBoardMember(tblBoardMember tblBoardMember)
        {
            if ((tblBoardMember.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblBoardMember, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblBoardMembers.AddObject(tblBoardMember);
            }
        }

        public void UpdateTblBoardMember(tblBoardMember currenttblBoardMember)
        {
            this.ObjectContext.tblBoardMembers.AttachAsModified(currenttblBoardMember, this.ChangeSet.GetOriginal(currenttblBoardMember));
        }

        public void DeleteTblBoardMember(tblBoardMember tblBoardMember)
        {
            if ((tblBoardMember.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblBoardMembers.Attach(tblBoardMember);
            }
            this.ObjectContext.tblBoardMembers.DeleteObject(tblBoardMember);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblTransHistories' query.
        public IQueryable<tblTransHistory> GetTblTransHistories()
        {
            return this.ObjectContext.tblTransHistories;
        }

        public IQueryable<tblTransHistory> GetAppealTblTransHistories(int applicationID)
        {
            return this.ObjectContext.tblTransHistories.Where(e => e.ApplicationID == applicationID);
        }

        public void InsertTblTransHistory(tblTransHistory tblTransHistory)
        {
            if ((tblTransHistory.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblTransHistory, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblTransHistories.AddObject(tblTransHistory);
            }
        }

        public void UpdateTblTransHistory(tblTransHistory currenttblTransHistory)
        {
            this.ObjectContext.tblTransHistories.AttachAsModified(currenttblTransHistory, this.ChangeSet.GetOriginal(currenttblTransHistory));
        }

        public void DeleteTblTransHistory(tblTransHistory tblTransHistory)
        {
            if ((tblTransHistory.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblTransHistories.Attach(tblTransHistory);
            }
            this.ObjectContext.tblTransHistories.DeleteObject(tblTransHistory);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblVoteResults' query.
        
        public IQueryable<tblVoteMemberResult> GetTblVoteMemberResult(DateTime hearingDate)
        {
            return this.ObjectContext.tblVoteMemberResults.Where(e => e.HearingDate.Value.Year == hearingDate.Year
                    && e.HearingDate.Value.Month == hearingDate.Month
                    && e.HearingDate.Value.Day == hearingDate.Day);
        }
        public void InsertTblVoteMemberResult(tblVoteMemberResult tblVoteResultItems)
        {
            if (tblVoteResultItems.EntityState != EntityState.Detached)
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblVoteResultItems, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblVoteMemberResults.AddObject(tblVoteResultItems);
            }
        }
        public void UpdateTblVoteMemberResult(tblVoteMemberResult votememberresult)
        {
            this.ObjectContext.tblVoteMemberResults.AttachAsModified(votememberresult, this.ChangeSet.GetOriginal(votememberresult));

        }
        public void DeleteTblVoteMemberResult(tblVoteMemberResult tblVoteMemberResult)
        {
            if ((tblVoteMemberResult.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblVoteMemberResults.Attach(tblVoteMemberResult);
            }
            this.ObjectContext.tblVoteMemberResults.DeleteObject(tblVoteMemberResult);
        }
        public IQueryable<tblVoteResult> GetTblVoteResults(DateTime hearingDate)
        {
            return this.ObjectContext.tblVoteResults.Where(e => e.HearingDate.Year == hearingDate.Year
                    && e.HearingDate.Month == hearingDate.Month
                    && e.HearingDate.Day == hearingDate.Day).OrderBy(x => x.SortOrder);
        }
        // by hearing date and applicationID.
        public IQueryable<tblVoteResult> GetOneTblVoteResult(int ApplicationID, DateTime hearingDate)
        {
            return this.ObjectContext.tblVoteResults.Where(e => e.HearingDate.Year == hearingDate.Year
                     && e.HearingDate.Month == hearingDate.Month
                     && e.HearingDate.Day == hearingDate.Day && e.ApplicationID == ApplicationID);
        }
        public void InsertTblVoteResult(tblVoteResult tblVoteResult)
        {
            
             if ((tblVoteResult.EntityState != EntityState.Detached))
                {
                    this.ObjectContext.ObjectStateManager.ChangeObjectState(tblVoteResult, EntityState.Added);
                }
                else
                {
                    this.ObjectContext.tblVoteResults.AddObject(tblVoteResult);
                }
            
        }
       
        public void UpdateTblVoteResult(tblVoteResult currenttblVoteResult)
        {
            this.ObjectContext.tblVoteResults.AttachAsModified(currenttblVoteResult, this.ChangeSet.GetOriginal(currenttblVoteResult));
        }

        public void DeleteTblVoteResult(tblVoteResult tblVoteResult)
        {
            if ((tblVoteResult.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblVoteResults.Attach(tblVoteResult);
            }
            this.ObjectContext.tblVoteResults.DeleteObject(tblVoteResult);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'TblDepartments' query.
        public IQueryable<tblDepartment> GetTblDepartments()
        {
            var result = this.ObjectContext.tblDepartments.Where(e => e.Status != "I");
            List<tblDepartment> newList = result.ToList();
            newList.Insert(0, new tblDepartment { AgencyID ="", Comments ="", DeptName ="", Related_Agency ="", UserID ="" });
            return newList.AsQueryable();
        }
        [Invoke]
        public IEnumerable<tblDepartment> GetTblDepartments1()
        {
            var result = this.ObjectContext.tblDepartments.Where(e => e.Status != "I");
            List<tblDepartment> newList = result.ToList();
            newList.Insert(0, new tblDepartment { AgencyID = "", Comments = "", DeptName = "", Related_Agency = "", UserID = "" });
            return newList.AsEnumerable();
        }
        public IQueryable<tblDepartment> GetFilteredTblDepartments(string agencyID)
        {
            return this.ObjectContext.tblDepartments.Where(e => e.AgencyID != agencyID && e.Related_Agency == agencyID && e.Status != "I");
        }
        public IQueryable<tblDepartment> GetDeptsForAdminPanel(string agencyID, string status)
        {
            return this.ObjectContext.tblDepartments.Where(e => e.AgencyID != agencyID && e.Related_Agency == agencyID && e.Status != status);
        }
        public IQueryable<tblDepartment> GetOneTblDepartments(string agencyID)
        {
            return this.ObjectContext.tblDepartments.Where(e => e.AgencyID == agencyID);
        }

        public IQueryable<tblDeptContact> GetDepartmentContacts(string agencyID)
        {
            return this.ObjectContext.tblDeptContacts.Where(e => e.Related_Agency == agencyID);
        }

        [Invoke]
        public IEnumerable<string> GetDepartmentInfo(string agencyID)
        {
            var result = this.ObjectContext.tblDepartments.Where(e => e.AgencyID == agencyID);
            List<tblDepartment> ll = result.ToList();
            List<string> newList = new List<string>();
            if (ll.Count > 0)
            {
                string street = (ll[0].street == null) ? "" : ll[0].street;
                string city = (ll[0].city == null) ? "" : ll[0].city;
                string state = (ll[0].state == null) ? "" : ll[0].state;
                string zip = (ll[0].zip == null) ? "" : ll[0].zip;
                newList.Insert(0, street);
                newList.Insert(1, city);
                newList.Insert(2, state);
                newList.Insert(3, zip);
            }
            return newList.AsEnumerable();
        }
        [Invoke]
        public IEnumerable<string> GetDepartmentContactInfo(string agencyID, string related_agencyID)
        {
            var result = this.ObjectContext.tblDeptContacts.Where(e => e.AgencyID == agencyID).Where(a => a.Related_Agency == related_agencyID);
            List<tblDeptContact> ll = result.ToList();
            List<string> newList = new List<string>();
            if (ll.Count > 0)
            {
                string FirstName= (ll[0].FirstName == null) ? "" : ll[0].FirstName;
                string MiddleName= (ll[0].MiddleName == null) ? "" : ll[0].MiddleName;
                string LastName= (ll[0].LastName == null) ? "" : ll[0].LastName;
                string AddressLine1 = (ll[0].AddressLine1 == null) ? "" : ll[0].AddressLine1;
                string AddressLine2 = (ll[0].AddressLine2 == null) ? "" : ll[0].AddressLine2;
                string PhoneNumber = (ll[0].PhoneNumber == null) ? "" : ll[0].PhoneNumber;
                newList.Insert(0, String.Concat(FirstName, " ",  MiddleName, " ", LastName));
                newList.Insert(1, String.Concat(AddressLine1, " ", AddressLine2));
                newList.Insert(2, PhoneNumber);
            }
            return newList.AsEnumerable();
        }
        public int checkDepartment(string agencyID)
        {
            return this.ObjectContext.tblDepartments.Where(e => e.AgencyID == agencyID).Count();
        }

        public void InsertTblDepartment(tblDepartment tblDepartment)
        {
            if ((tblDepartment.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblDepartment, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblDepartments.AddObject(tblDepartment);
            }
        }

        public void UpdateTblDepartment(tblDepartment currenttblDepartment)
        {
            this.ObjectContext.tblDepartments.AttachAsModified(currenttblDepartment, this.ChangeSet.GetOriginal(currenttblDepartment));
        }

        public void DeleteTblDepartment(tblDepartment tblDepartment)
        {
            if ((tblDepartment.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblDepartments.Attach(tblDepartment);
            }
            this.ObjectContext.tblDepartments.DeleteObject(tblDepartment);
        }

        public void InsertTblDeptContact(tblDeptContact tblDepartmentContact)
        {
            if ((tblDepartmentContact.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblDepartmentContact, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblDeptContacts.AddObject(tblDepartmentContact);
            }
        }

        public void UpdateTblDeptContact(tblDeptContact currenttblDeptContact)
        {
            this.ObjectContext.tblDeptContacts.AttachAsModified(currenttblDeptContact, this.ChangeSet.GetOriginal(currenttblDeptContact));
        }

        public void DeleteTblDeptContact(tblDeptContact tblDepartmentContact)
        {
            if ((tblDepartmentContact.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblDeptContacts.Attach(tblDepartmentContact);
            }
            this.ObjectContext.tblDeptContacts.DeleteObject(tblDepartmentContact);
        }
    }
}


