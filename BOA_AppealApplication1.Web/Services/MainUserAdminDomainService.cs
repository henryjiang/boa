﻿
namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;
    using BOA_AppealApplication1.Web.Resources;
    using BOA_AppealApplication1.Web.Models.Shared;
    using BOA_AppealApplication1.Web.Resources;
    using BOA_AppealApplication1.Web.Models;


    // Implements application logic using the LicenseDBEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class MainUserAdminDomainService : LinqToEntitiesDomainService<LicenseDBEntities>
    {
        
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblMainUsers' query.
        public IQueryable<tblMainUser> GetTblMainUsers()
        {

            return this.ObjectContext.tblMainUsers;
        }

        public IQueryable<tblMainUser> GetTblMainUser(Int32 mainUserId)
        {
            return this.ObjectContext.tblMainUsers.Where(e => e.MainUserID == mainUserId);
        }

        public IQueryable<tblMainUser> GetTblMainUserByUserId(String userId)
        {
            return this.ObjectContext.tblMainUsers.Where(e => e.UserID == userId);
        }

        public IQueryable<tblMainUser> GetTblMainUsersByStatus(string status)
        {//This method is passed a comma delimited string of statuses
            string[] statuses = status.Split(',');
            return this.ObjectContext.tblMainUsers.Where(e => statuses.Contains(e.IsActive));
        }

        [CustomValidation(typeof(CustomServerSideValidation), "DuplicateUser")]
        public void InsertTblMainUser(tblMainUser tblMainUser)
        {
            if ((tblMainUser.EntityState != EntityState.Detached))
            {
                
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblMainUser, EntityState.Added);
            }
            else
            {
                tblMainUser.Password = Crypto.CreateHash(tblMainUser.Password);
                this.ObjectContext.tblMainUsers.AddObject(tblMainUser);
            }
        }

        public void UpdateTblMainUser(tblMainUser currenttblMainUser)
        {   
            this.ObjectContext.tblMainUsers.AttachAsModified(currenttblMainUser, this.ChangeSet.GetOriginal(currenttblMainUser));
        }

        [Update(UsingCustomMethod=true)]
        public void UpdatePassword(tblMainUser currenttblMainUser)
        {
            currenttblMainUser.Password = Crypto.CreateHash(currenttblMainUser.Password);
            this.ObjectContext.tblMainUsers.AttachAsModified(currenttblMainUser, this.ChangeSet.GetOriginal(currenttblMainUser));
        }

        public void DeleteTblMainUser(tblMainUser tblMainUser)
        {
            if ((tblMainUser.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblMainUsers.Attach(tblMainUser);
            }
            this.ObjectContext.tblMainUsers.DeleteObject(tblMainUser);
        }

    }

}


