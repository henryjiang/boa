﻿
namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;


    // Implements application logic using the LicenseDBEntities4 context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class FeesViewDomainService1 : LinqToEntitiesDomainService<LicenseDBEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'vwAppellantApplicationFees' query.
        public IQueryable<vwAppellantApplicationFee> GetVwAppellantApplicationFees()
        {
            return this.ObjectContext.vwAppellantApplicationFees;
        }

        public IQueryable<vwAppellantApplicationFee> GetVwAppellantOneApplicationFees(int applicationID)
        {
            return this.ObjectContext.vwAppellantApplicationFees.Where(e => e.ApplicationID == applicationID);
        }

        public int GetPaymentCount(int applicationID)
        {
            return this.ObjectContext.vwAppellantApplicationFees.Where(e => e.ApplicationID == applicationID).Count();
        }

        public decimal GetPaymentTotal(int applicationID)
        {
            if (this.ObjectContext.vwAppellantApplicationFees.Where(e => e.ApplicationID == applicationID).Count() > 0)
            {
                return this.ObjectContext.vwAppellantApplicationFees.Where(e => e.ApplicationID == applicationID).Sum(s => s.Amount);
            }
            else return 0;
        }

        public void InsertVwAppellantApplicationFee(vwAppellantApplicationFee vwAppellantApplicationFee)
        {
            if ((vwAppellantApplicationFee.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(vwAppellantApplicationFee, EntityState.Added);
            }
            else
            {
                this.ObjectContext.vwAppellantApplicationFees.AddObject(vwAppellantApplicationFee);
            }
        }

        public void UpdateVwAppellantApplicationFee(vwAppellantApplicationFee currentvwAppellantApplicationFee)
        {
            this.ObjectContext.vwAppellantApplicationFees.AttachAsModified(currentvwAppellantApplicationFee, this.ChangeSet.GetOriginal(currentvwAppellantApplicationFee));
        }

        public void DeleteVwAppellantApplicationFee(vwAppellantApplicationFee vwAppellantApplicationFee)
        {
            if ((vwAppellantApplicationFee.EntityState == EntityState.Detached))
            {
                this.ObjectContext.vwAppellantApplicationFees.Attach(vwAppellantApplicationFee);
            }
            this.ObjectContext.vwAppellantApplicationFees.DeleteObject(vwAppellantApplicationFee);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'vwBOAAppeals' query.
        public IQueryable<vwBOAAppeal> GetVwBOAAppeals()
        {
            return this.ObjectContext.vwBOAAppeals;
        }

        public string GetMAXVWApplicationsID(string agencyID)
        {
            return this.ObjectContext.vwBOAAppeals.Where(e => e.AgencyID == agencyID).Max(a => a.ApplicationID).ToString();
        }

        public void InsertVwBOAAppeal(vwBOAAppeal vwBOAAppeal)
        {
            if ((vwBOAAppeal.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(vwBOAAppeal, EntityState.Added);
            }
            else
            {
                this.ObjectContext.vwBOAAppeals.AddObject(vwBOAAppeal);
            }
        }

        public void UpdateVwBOAAppeal(vwBOAAppeal currentvwBOAAppeal)
        {
            this.ObjectContext.vwBOAAppeals.AttachAsModified(currentvwBOAAppeal, this.ChangeSet.GetOriginal(currentvwBOAAppeal));
        }

        public void DeleteVwBOAAppeal(vwBOAAppeal vwBOAAppeal)
        {
            if ((vwBOAAppeal.EntityState == EntityState.Detached))
            {
                this.ObjectContext.vwBOAAppeals.Attach(vwBOAAppeal);
            }
            this.ObjectContext.vwBOAAppeals.DeleteObject(vwBOAAppeal);
        }
    }
}


