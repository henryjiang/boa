﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies tblBoardMemberMetadata as the class
    // that carries additional metadata for the tblBoardMember class.
    [MetadataTypeAttribute(typeof(tblBoardMember.tblBoardMemberMetadata))]
    public partial class tblBoardMember
    {

        // This class allows you to attach custom attributes to properties
        // of the tblBoardMember class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblBoardMemberMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblBoardMemberMetadata()
            {
            }

            public string AgencyID { get; set; }

            public DateTime EndDate { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            [Editable(true)]
            public string MemberID
            {
                get;
                set;
            }

            public string MiddleName { get; set; }

            public Nullable<DateTime> StartDate { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblTransHistoryMetadata as the class
    // that carries additional metadata for the tblTransHistory class.
    [MetadataTypeAttribute(typeof(tblTransHistory.tblTransHistoryMetadata))]
    public partial class tblTransHistory
    {

        // This class allows you to attach custom attributes to properties
        // of the tblTransHistory class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblTransHistoryMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblTransHistoryMetadata()
            {
            }

            public decimal Amount { get; set; }

            public string ApplicationID { get; set; }

            public string Comments { get; set; }

            public DateTime Date { get; set; }

            public Nullable<decimal> Fee { get; set; }

            public string FeeStatus { get; set; }

            public DateTime ModDate { get; set; }

            public string PayeeID { get; set; }

            public string Type { get; set; }

            public string UserID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblVoteResultMetadata as the class
    // that carries additional metadata for the tblVoteResult class.
    [MetadataTypeAttribute(typeof(tblVoteResult.tblVoteResultMetadata))]
    public partial class tblVoteResult
    {

        // This class allows you to attach custom attributes to properties
        // of the tblVoteResult class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblVoteResultMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblVoteResultMetadata()
            {
            }

            public string AgencyID { get; set; }

            public string ApplicationID { get; set; }

            public string Comments { get; set; }

            public DateTime HearingDate { get; set; }

            public DateTime ModDate { get; set; }

            public string UserID { get; set; }
            
        }
    }

    // The MetadataTypeAttribute identifies tblDepartmentMetadata as the class
    // that carries additional metadata for the tblDepartment class.
    [MetadataTypeAttribute(typeof(tblDepartment.tblDepartmentMetadata))]
    public partial class tblDepartment
    {

        // This class allows you to attach custom attributes to properties
        // of the tblVoteResult class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblDepartmentMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblDepartmentMetadata()
            {
            }

            [Editable(true)]
            [Required(ErrorMessage = "Required Field")]
            public string AgencyID { get; set; }

            public string DeptName { get; set; }

            public string Comments { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblDepartmentMetadata as the class
    // that carries additional metadata for the tblDepartment class.
    [MetadataTypeAttribute(typeof(tblDeptContact.tblDeptContactMetadata))]
    public partial class tblDeptContact
    {
        internal sealed class tblDeptContactMetadata
        {
            // Metadata classes are not meant to be instantiated.
            private tblDeptContactMetadata()
            {
            }

            [Editable(true)]
            [Required(ErrorMessage = "Required Field")]
            public string AgencyID { get; set; }
        }
    }
}
