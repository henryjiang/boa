﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies vwBOAAppealMetadata as the class
    // that carries additional metadata for the vwBOAAppeal class.
    [MetadataTypeAttribute(typeof(vwBOAAppeal.vwBOAAppealMetadata))]
    public partial class vwBOAAppeal
    {

        // This class allows you to attach custom attributes to properties
        // of the vwBOAAppeal class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class vwBOAAppealMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private vwBOAAppealMetadata()
            {
            }

            public string AgencyID { get; set; }

            public string ApplicationID { get; set; }
        }
    }
}
