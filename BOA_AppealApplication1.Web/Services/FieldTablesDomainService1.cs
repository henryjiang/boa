﻿namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;


    // Implements application logic using the LicenseDBEntities2 context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class FieldTablesDomainService1 : LinqToEntitiesDomainService<LicenseDBEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblDateFields' query.
        [Query(IsDefault = true)]
        public IQueryable<tblDateField> GetTblDateFields()
        {
            return this.ObjectContext.tblDateFields;
        }

        public IQueryable<tblDateField> GetSpecificTblDateFields(int applicationID, string dateType)
        {
            return this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID && e.DateType == dateType).OrderByDescending(e => e.ModDate);
        }

        [Invoke]
        public IEnumerable<tblDateField> GetSpecificTblDateFields1(int applicationID, string dateType)
        {
            return this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID && e.DateType == dateType).OrderByDescending(e => e.ModDate).AsEnumerable();
        }

        [Invoke]
        public IEnumerable<tblDateField> GetSpecificTblDateFields2(int applicationID)
        {
            return this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ModDate).AsEnumerable();
        }
        [Invoke]
        public bool GetDateFieldByDateTime(int applicationID, string dateType, string currentdate)
        {
            var query = (this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID && e.DateType == dateType)).OrderByDescending(e => e.ModDate);

            tblDateField date = query.FirstOrDefault();
            if (date.DateValue.ToShortDateString() == currentdate)
                return true;
            return false;
        }

        [Invoke]
        public tblDateField GetDateFieldByDateType(string dateType, int applicationID)
        {
            return this.ObjectContext.tblDateFields.Where(e => e.DateType == dateType && e.ApplicationID == applicationID).OrderByDescending(e => e.ModDate).FirstOrDefault();
        }

        public IQueryable<tblDateField> GetApplicationTblDateFields(int applicationID)
        {
            return this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ModDate);
        }

        public IQueryable<tblStatusField> GetApplicationTblStatusFields(int applicationID)
        {
            return this.ObjectContext.tblStatusFields.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ModDate);
        }

        //public IQueryable<tblDateField> GetTblDateFieldsLatest(string applicationID, string dateType)
        //{
        //    return this.ObjectContext.tblDateFields.Where(e => e.ApplicationID == applicationID && e.DateType == dateType).OrderByDescending(e => e.ModDate);
        //}

        public void InsertTblDateField(tblDateField tblDateField)
        {
            if ((tblDateField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblDateField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblDateFields.AddObject(tblDateField);
            }
        }

        public void UpdateTblDateField(tblDateField currenttblDateField)
        {
            this.ObjectContext.tblDateFields.AttachAsModified(currenttblDateField, this.ChangeSet.GetOriginal(currenttblDateField));
        }

        public void DeleteTblDateField(tblDateField tblDateField)
        {
            if ((tblDateField.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblDateFields.Attach(tblDateField);
            }
            this.ObjectContext.tblDateFields.DeleteObject(tblDateField);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblStatusFields' query.
        [Query(IsDefault = true)]
        public IQueryable<tblStatusField> GetTblStatusFields()
        {
            return this.ObjectContext.tblStatusFields;
        }

        public IQueryable<tblStatusField> GetTblStatusFieldsLatest(int applicationID)
        {
            return this.ObjectContext.tblStatusFields.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ModDate);
        }

        public void InsertTblStatusField(tblStatusField tblStatusField)
        {
            if ((tblStatusField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblStatusField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblStatusFields.AddObject(tblStatusField);
            }
        }

        public void UpdateTblStatusField(tblStatusField currenttblStatusField)
        {
            this.ObjectContext.tblStatusFields.AttachAsModified(currenttblStatusField, this.ChangeSet.GetOriginal(currenttblStatusField));
        }

        public void DeleteTblStatusField(tblStatusField tblStatusField)
        {
            if ((tblStatusField.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblStatusFields.Attach(tblStatusField);
            }
            this.ObjectContext.tblStatusFields.DeleteObject(tblStatusField);
        }
    }
}