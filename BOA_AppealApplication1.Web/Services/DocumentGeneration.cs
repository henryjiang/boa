﻿
using BOA_AppealApplication1.Web;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Text.RegularExpressions;
//using Microsoft.Office.Interop.Word;
using DocumentFormat.OpenXml.Packaging;

namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using DocumentFormat.OpenXml.Wordprocessing;
    using DocumentFormat.OpenXml.Vml;
    using DocumentFormat.OpenXml.Vml.Office;
    using DocumentFormat.OpenXml.Vml.Wordprocessing;
    using HorizontalAnchorValues = DocumentFormat.OpenXml.Vml.Wordprocessing.HorizontalAnchorValues;
    using Lock = DocumentFormat.OpenXml.Vml.Office.Lock;
    using VerticalAnchorValues = DocumentFormat.OpenXml.Vml.Wordprocessing.VerticalAnchorValues;


    // Implements application logic using the LicenseDBEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class DocumentGeneration : LinqToEntitiesDomainService<LicenseDBEntities>
    {

        public string getPSOADoc(int ApplicationID, string type)
        {
            string localFilePath = "";
            string templateFile = "";
            string[] tempFile = System.IO.Path.GetTempFileName().Split('\\');
            string resultFile = HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\";

            try
            {
                if (type == "SL")
                {
                    //templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Suspension Letter Template.html";
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Suspension Letter Template.docx";
                    localFilePath = getSuspensionLetterDoc(ApplicationID, resultFile + "Suspension_Letter_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "NOD")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Notice of Decision Template.docx";
                    localFilePath = getNotifceDecisionDoc(ApplicationID, resultFile + "Notice_Decision_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "NOP")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Notice of Appeal Template.docx";
                    localFilePath = getNotifceAppealDoc(ApplicationID, resultFile + "Notice_Appeal_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "CR")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Case Report Template.docx";
                    localFilePath = getCaseReportDoc(ApplicationID, resultFile + "Case_Report_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "AW")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Affidavit Template (Withdrawal).docm";
                    localFilePath = getAffidavitWithdrawalDoc(ApplicationID, resultFile + "Aff_withdrawal_" + tempFile[tempFile.Length - 1] + ".docm", templateFile);
                }
                else if (type == "AW_JR")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Affidavit Template JR(Withdrawal).docm";
                    localFilePath = getAffidavitWithdrawalJRDoc(ApplicationID, resultFile + "Aff_withdrawal_" + tempFile[tempFile.Length - 1] + ".docm", templateFile);
                }
                else if (type == "AD")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Affidavit Template (Decided Appeal).docm";
                    localFilePath = getAffidavitDecidedDoc(ApplicationID, resultFile + "Aff_Decided_" + tempFile[tempFile.Length - 1] + ".docm", templateFile);
                }
                else if (type == "JR Receipt")
                {
                    //templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Template_Appeals_JR.htm";
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Template_Appeals_JR.docx";
                    localFilePath = getWordDocumentJR(ApplicationID, resultFile + "JR_Receipt_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "PSOA")
                {
                    //templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Template_Appeals.html";
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Template_Appeals.docx";
                    localFilePath = getWordDocument(ApplicationID, resultFile + "PSOA_" + tempFile[tempFile.Length - 1] + ".docx", templateFile);
                }
                else if (type == "Granted Letter")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\JR Granted Letter.docx";
                    localFilePath = getJRGrantedDoc(ApplicationID, resultFile + "JRGranted_" + tempFile[tempFile.Length - 1] + ".docx", templateFile, "GRANTED");
                }
                else if (type == "Denied Letter")
                {
                    templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\JR Denied Letter.docx";
                    localFilePath = getJRGrantedDoc(ApplicationID, resultFile + "JRDenied_" + tempFile[tempFile.Length - 1] + ".docx", templateFile, "DENIED");
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            return localFilePath;
        }
       
        public string getSuspensionLetterDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            string lastnames = "";
            string dnames = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");

                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                {
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                    lastnames = String.Concat(lastnames, ", ", lastname);
                                }
                                else
                                {
                                    names = String.Concat(firstname, " ", lastname, names);
                                    lastnames = lastnames + lastname;
                                }
                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{a_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");

                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                            if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                dnames = firstname + lastname;
                                if (dnames != "")
                                {
                                    regexText = new Regex("{Determination_LastName}");
                                    docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                }
                                regexText = new Regex("{Determination_FirstName}");
                                docText = regexText.Replace(docText, e.Element("FirstName") == null ? "" : e.Element("FirstName").Value);

                                regexText = new Regex("{if_exists_d}");
                                if (e.Element("FirstName").Value != "" || e.Element("LastName").Value != "")
                                    docText = regexText.Replace(docText, ", Permit Holder");
                                else
                                    docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");

                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }

                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                            
                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names + ", Appellants";
                            else
                                names = names + ", Appellant";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        if (lastnames != "")
                        {
                            int place = lastnames.LastIndexOf(", ");
                            if (place > 1)
                                lastnames = lastnames.Remove(place, 2).Insert(place, " and ");
                        }
                        regexText = new Regex("{Appellant_Last}");
                        docText = regexText.Replace(docText, lastnames);


                        if (dnames == "")
                        {
                            regexText = new Regex("{Determination_LastName}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("dept_contact") == null ? "" : doc.Element("Document").Element("Applications").Element("dept_contact").Value);
                        }

                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                        {
                            regexText = new Regex("{Dept_Agency}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        }
                        else
                        {
                            regexText = new Regex("{Dept_Agency}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                        {
                            regexText = new Regex("{ApplicationID}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        }
                        else
                        {
                            regexText = new Regex("{ApplicationID}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        }
                        else
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, "");
                        }
                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getNotifceDecisionDoc(int app_id, string resultFile, string templateFile)
        {
            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            string names = "";

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("HearingDate").Value.ToUpper());
                        }

                        regexText = new Regex("{outcometype}");
                        if (doc.Element("Document").Element("Applications").Element("outcometype") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("outcometype").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{CaseType}");
                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)

                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        #region //notice of appeal template
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();

                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{a_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");

                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                            if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                regexText = new Regex("{Determination_LastName}");
                                docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                regexText = new Regex("{Determination_FirstName}");
                                docText = regexText.Replace(docText, e.Element("FirstName") == null ? "" : e.Element("FirstName").Value);

                                regexText = new Regex("{if_exists_d}");
                                if (e.Element("FirstName").Value != "" || e.Element("LastName").Value != "")
                                    docText = regexText.Replace(docText, ", Permit Holder");
                                else
                                    docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(",");
                            if (place > 1)
                                names = names.Remove(place, 1).Insert(place, " and ").Trim();

                            regexText = new Regex("{appellant_name}");
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(names).ToUpper()));

                            if (names.Contains(",") || names.Contains(" and "))
                                names = names.Trim() + ", Appellants";
                            else
                                names = names.Trim() + ", Appellant";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        if (doc.Element("Document").Element("Applications").Element("ExpDate") != null)
                        {
                            regexText = new Regex("{ExpDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("FileDate") != null)
                        {
                            regexText = new Regex("{FileDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("FileDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("OriginalHearing") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            DateTime hearingdate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("OriginalHearing").Value);
                            docText = regexText.Replace(docText, hearingdate.ToString("MMMM dd, yyyy"));
                        }
                        regexText = new Regex("{HearingDate1}");
                        if (doc.Element("Document").Element("Applications").Element("OriginalHearing") != null)
                        {
                            DateTime hearingdate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("OriginalHearing").Value);
                            docText = regexText.Replace(docText, hearingdate.ToString("MMMM dd, yyyy"));
                        }
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{property_address}");
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{Dept_Agency}");
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("related_agency").Value).ToUpper()));
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{PlanningDept}");
                        if (doc.Element("Document").Element("Applications").Element("PlanningDept") != null)
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("PlanningDept").Value).ToUpper()));
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{permitnumber}");
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{permit_description}");
                        if (doc.Element("Document").Element("Applications").Element("permit_description") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permit_description").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{ApplicationID}");
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        #endregion

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getNotifceAppealDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        Regex regexText1 = new Regex("");
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{a_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");

                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                            if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                regexText = new Regex("{Determination_LastName}");
                                docText = regexText.Replace(docText, lastname);
                                regexText = new Regex("{Determination_FirstName}");
                                docText = regexText.Replace(docText, firstname);

                                regexText = new Regex("{if_exists_d}");
                                regexText1 = new Regex("{Determination_holder}");
                                if (e.Element("FirstName").Value != "" || e.Element("LastName").Value != "")
                                {
                                    docText = regexText.Replace(docText, ", Permit Holder");
                                    docText = regexText1.Replace(docText, ", to " + firstname + lastname);
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, "");
                                    docText = regexText1.Replace(docText, "");
                                }

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ").Trim();

                            regexText = new Regex("{appellant_name}");
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(names).ToUpper()));

                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names.Trim() + ", Appellants";
                            else
                                names = names.Trim() + ", Appellant";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        if (doc.Element("Document").Element("Applications").Element("ExpDate") != null)
                        {
                            regexText = new Regex("{ExpDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("FileDate") != null)
                        {
                            regexText = new Regex("{FileDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("FileDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("OriginalHearing") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            DateTime hearingdate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("OriginalHearing").Value);
                            docText = regexText.Replace(docText, hearingdate.ToString("MMMM dd, yyyy"));
                        }

                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                        {
                            regexText = new Regex("{Dept_Agency}");
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("related_agency").Value).ToUpper()));
                        }
                        else
                        {
                            regexText = new Regex("{Dept_Agency}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("PlanningDept") != null)
                        {
                            regexText = new Regex("{PlanningDept}");
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("PlanningDept").Value).ToUpper()));
                        }
                        else
                        {
                            regexText = new Regex("{PlanningDept}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permit_description") != null)
                        {
                            regexText = new Regex("{permit_description}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permit_description").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permit_description}");
                            docText = regexText.Replace(docText, "");
                        }

                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                        {
                            regexText = new Regex("{ApplicationID}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        }
                        else
                        {
                            regexText = new Regex("{ApplicationID}");
                            docText = regexText.Replace(docText, "");
                        }

                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        }
                        else
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, "");
                        }

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);

                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getCaseReportDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            string appellant_agent = "";
            string appellant_attorney = "";
            string permitholder_first = "";
            string permitholder = "";
            string permitholder_last = "";
            string permitholder_agent = "";
            string permitholder_attorney = "";

            string case_statement = "";
            XDocument doc = new XDocument();

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            Regex regexText = new Regex("");
            Regex regexText1 = new Regex("");

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search for the name block
                    MainDocumentPart mainpart = wordDoc.MainDocumentPart;
                    IEnumerable<DocumentFormat.OpenXml.OpenXmlElement> elems = mainpart.Document.Body.Descendants();
                    DocumentFormat.OpenXml.Wordprocessing.Paragraph p = new DocumentFormat.OpenXml.Wordprocessing.Paragraph();
                    foreach (DocumentFormat.OpenXml.OpenXmlElement elem in elems)
                    {
                        if (elem is DocumentFormat.OpenXml.Wordprocessing.Text && (elem.InnerText.Contains("{Appellant_Agent_Name}")))
                        {
                            DocumentFormat.OpenXml.Wordprocessing.Run run = (DocumentFormat.OpenXml.Wordprocessing.Run)elem.Parent;
                            p = (DocumentFormat.OpenXml.Wordprocessing.Paragraph)run.Parent;
                            p.RemoveAllChildren();
                            break;
                        }
                    }

                    // search and replace placeholders
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2,Comments";
                    doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();

                            if (firstname == "" && lastname == "")
                                continue;
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);
                                if (e.Element("Comments") != null && e.Element("Comments").Value != "" && case_statement == "")
                                    case_statement = e.Element("Comments").Value;
                            }
                            else if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                permitholder_first = firstname;
                                permitholder_last = lastname;
                                permitholder = firstname + " " + lastname;
                            }
                            else if (e.Element("Type") != null && e.Element("Type").Value == "Determination Holder's Attorney")
                            {
                                permitholder_attorney = firstname + " " + lastname;
                            }
                            else if (e.Element("Type") != null && e.Element("Type").Value == "Determination Holder's Agent")
                            {
                                permitholder_agent = firstname + " " + lastname;
                            }
                            else if (e.Element("Type") != null && e.Element("Type").Value == "Appellant's Attorney")
                            {
                                appellant_attorney = firstname + " " + lastname;
                            }
                            else if (e.Element("Type") != null && e.Element("Type").Value == "Appellant's Agent")
                            {
                                appellant_agent = firstname + " " + lastname;
                            }
                        }

                        Run newRun = new Run();
                        Run newRun1 = new Run();
                        Run newRun2 = new Run();
                        Run newRun3 = new Run();

                        p.ParagraphProperties = newParagraphProperties();

                        if (appellant_agent != "" && appellant_agent != " ")
                        {
                            newRun = createRun("Appenllant's Agent ", true, false);
                            p.Append(newRun);
                            newRun2 = createEmptyRun(false);
                            p.Append(newRun2);
                            newRun1 = createRun(appellant_agent, false, true);
                            p.Append(newRun1);
                            newRun3 = createEmptyRun(true);
                            p.Append(newRun3);
                        }
                        if (appellant_attorney != "" && appellant_attorney != " ")
                        {
                            newRun = createRun("Appellant's Attorney ", true, false);
                            p.Append(newRun);
                            newRun2 = createEmptyRun(false);
                            p.Append(newRun2);
                            newRun1 = createRun(appellant_attorney, false, true);
                            p.Append(newRun1);
                            newRun3 = createEmptyRun(true);
                            p.Append(newRun3);
                        }
                        if (permitholder != "" && permitholder != " ")
                        {
                            newRun = createRun("Permit Holder ", true, false);
                            p.Append(newRun);
                            newRun2 = createEmptyRun(false);
                            p.Append(newRun2);
                            newRun1 = createRun(permitholder, false, true);
                            p.Append(newRun1);
                            newRun3 = createEmptyRun(true);
                            p.Append(newRun3);
                        }
                        if (permitholder_agent != "" && permitholder_agent != " ")
                        {
                            newRun = createRun("Permit Holder's Agent ", true, false);
                            p.Append(newRun);
                            newRun2 = createEmptyRun(false);
                            p.Append(newRun2);
                            newRun1 = createRun(permitholder_agent, false, true);
                            p.Append(newRun1);
                            newRun3 = createEmptyRun(true);
                            p.Append(newRun3);
                        }
                        if (permitholder_attorney != "" && permitholder_attorney != " ")
                        {
                            newRun = createRun("Permit Holder's Attorney ", true, false);
                            p.Append(newRun);
                            newRun2 = createEmptyRun(false);
                            p.Append(newRun2);
                            newRun1 = createRun(permitholder_attorney, false, false);
                            p.Append(newRun1);
                        }
                    }
                    catch (Exception e1)
                    {
                        return e1.ToString();
                    }
                    wordDoc.Close();
                    // Save the file with the new name
                    File.WriteAllBytes(resultFile, stream.ToArray());
                    stream.Close();
                }
            }
            byteArray = File.ReadAllBytes(resultFile);
            using (MemoryStream stream1 = new MemoryStream())
            {
                stream1.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream1, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    DateTime hearingdate = new DateTime();
                    DateTime expdate = new DateTime();
                    DateTime filedate = new DateTime();
                    try
                    {
                        if (names != "")
                        {
                            int place = names.LastIndexOf(",");
                            if (place > 1)
                                names = names.Remove(place, 1).Insert(place, " and ");
                        }
                        regexText = new Regex("{APPELLANT_NAMES}");
                        regexText1 = new Regex("{appellant_names}");
                        docText = regexText1.Replace(docText, names);
                        names = HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(names).ToUpper());
                        docText = regexText.Replace(docText, names);

                        regexText = new Regex("{DETERMINATION_FIRSTNAME}");
                        regexText1 = new Regex("{DETERMINATION_LASTNAME}");
                        if (permitholder != "")
                        {
                            docText = regexText.Replace(docText, " to " + permitholder_first);
                            docText = regexText1.Replace(docText, " " + permitholder_last + ",");
                        }
                        else
                        {
                            docText = regexText.Replace(docText, "");
                            docText = regexText1.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("ExpDate") != null)
                        {
                            regexText = new Regex("{EXPDATE}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                            expdate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                            regexText = new Regex("{ExpDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("FileDate") != null)
                        {
                            regexText = new Regex("{FileDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("FileDate").Value);
                            filedate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("FileDate").Value);
                        }
                        regexText = new Regex("{property_address}");
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{JRGrantedText}");
                        regexText1 = new Regex("{JurisdictionGranted}");
                        if (doc.Element("Document").Element("Applications").Element("JRGrantedText") != null)
                        {
                            docText = regexText1.Replace(docText, "Jurisdiction Request Granted: ");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("JRGrantedText").Value);
                        }
                        else
                        {
                            docText = regexText.Replace(docText, "");
                            docText = regexText1.Replace(docText, "");
                        }

                        regexText = new Regex("{DEPT_AGENCY}");
                        regexText1 = new Regex("{Dept_Agency}");
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                        {
                            docText = regexText.Replace(docText, HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("related_agency").Value).ToUpper()));
                            docText = regexText1.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        }
                        else
                        {
                            docText = regexText.Replace(docText, "");
                            docText = regexText1.Replace(docText, "");
                        }
                        regexText = new Regex("{PLANNINGDEPT}");
                        regexText1 = new Regex("{PlanningDept}");
                        if (doc.Element("Document").Element("Applications").Element("PLANNINGDEPT") != null && doc.Element("Document").Element("Applications").Element("PLANNINGDEPT").Value != "")
                        {
                            docText = regexText.Replace(docText, ", " + HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(doc.Element("Document").Element("Applications").Element("PLANNINGDEPT").Value).ToUpper()));
                            docText = regexText1.Replace(docText, ", " + doc.Element("Document").Element("Applications").Element("PLANNINGDEPT").Value);
                        }
                        else
                        {
                            docText = regexText.Replace(docText, "");
                            docText = regexText1.Replace(docText, "");
                        }

                        regexText = new Regex("{PERMIT_DESCRIPTION}");
                        if (doc.Element("Document").Element("Applications").Element("permit_description") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permit_description").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{ApplicationID}");
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{CASETYPE}");
                        if (doc.Element("Document").Element("Applications").Element("CASETYPE") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CASETYPE").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{Block}");
                        if (doc.Element("Document").Element("Applications").Element("Block") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Block").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{Lot}");
                        if (doc.Element("Document").Element("Applications").Element("Lot") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Lot").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{permitnumber}");
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{PERMITTYPE}");
                        regexText1 = new Regex("{permittype}");
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            string permittype = doc.Element("Document").Element("Applications").Element("permittype").Value;
                            if (permittype.StartsWith("A") || permittype.StartsWith("A") || permittype.StartsWith("E") || permittype.StartsWith("I")
                                || permittype.StartsWith("O") || permittype.StartsWith("U"))
                                docText = regexText.Replace(docText, "An " + permittype);
                            else
                                docText = regexText.Replace(docText, "A " + permittype);
                            docText = regexText1.Replace(docText, permittype);
                        }
                        else
                        {
                            docText = regexText1.Replace(docText, "");
                            docText = regexText.Replace(docText, "");
                        }
                        regexText = new Regex("{assoc_appeal}");
                        if (doc.Element("Document").Element("Applications").Element("assoc_appeal") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("assoc_appeal").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{case_statement}");
                        if (case_statement != "")
                            docText = regexText.Replace(docText, case_statement);
                        else
                            docText = regexText.Replace(docText, "");

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);

                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            hearingdate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                            regexText = new Regex("{FinalHearingDate}");
                            docText = regexText.Replace(docText, hearingdate.ToString("MMMM dd, yyyy"));
                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, DateTime.Parse(doc.Element("Document").Element("Applications").Element("OriginalHearing").Value).ToString("MMMM dd, yyyy"));
                        }
                        
                        regexText = new Regex("{HearingPlus60}");
                        docText = regexText.Replace(docText, filedate.AddDays(60).ToString("MMMM dd, yyyy"));
                        regexText = new Regex("{HearingMinusFileDate}");
                        DateTime assignedHearingDate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("OriginalHearing").Value);
                        int dateDiff = (int)(assignedHearingDate - filedate).TotalDays;
                        docText = regexText.Replace(docText, dateDiff.ToString());
                        
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    //AddWatermark(wordDoc);
                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream1.ToArray());
                stream1.Close();
            }
            return resultFile;
        }
        
        public string getAffidavitDecidedDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />" + e.Element("addressline2").Value);
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                                if (e.Element("Type").Value == "Primary Appellant")
                                {
                                    regexText = new Regex("{appellant_last}");
                                    docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                }
                            }
                            if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                regexText = new Regex("{Determination_LastName}");
                                docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                regexText = new Regex("{Determination_FirstName}");
                                docText = regexText.Replace(docText, e.Element("FirstName") == null ? "" : e.Element("FirstName").Value);

                                regexText = new Regex("{if_exists_d}");
                                if (e.Element("FirstName").Value != "" || e.Element("LastName").Value != "")
                                    docText = regexText.Replace(docText, ", Permit Holder");
                                else
                                    docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />" + e.Element("addressline2").Value);
                                else
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                
                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names + ", Appellants";
                            else
                                names = names + ", Appellant";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        regexText = new Regex("{property_address}");
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{Dept_Agency}");
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{ApplicationID}");
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{dept_info}");
                        if (doc.Element("Document").Element("Applications").Element("dept_contact") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("dept_contact").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                        regexText = new Regex("{current_month_year}");
                        docText = regexText.Replace(docText, DateTime.Now.ToString("Y"));
                        regexText = new Regex("{current_date}");
                        docText = regexText.Replace(docText, DateTime.Now.Day.ToString());
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getAffidavitWithdrawalDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />" + e.Element("addressline2").Value);
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                                if (e.Element("Type").Value == "Primary Appellant")
                                {
                                    regexText = new Regex("{appellant_last}");
                                    docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                }
                            }
                            if (e.Element("Type") != null && e.Element("Type").Value == "Respondent")
                            {
                                regexText = new Regex("{Determination_LastName}");
                                docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                regexText = new Regex("{Determination_FirstName}");
                                docText = regexText.Replace(docText, e.Element("FirstName") == null ? "" : e.Element("FirstName").Value);

                                regexText = new Regex("{if_exists_d}");
                                if (e.Element("FirstName").Value != "" || e.Element("LastName").Value != "")
                                    docText = regexText.Replace(docText, ", Permit Holder");
                                else
                                    docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names + ", Appellants";
                            else
                                names = names + ", Appellant";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }

                        regexText = new Regex("{Dept_Agency}");
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{ApplicationID}");
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{dept_info}");
                        if (doc.Element("Document").Element("Applications").Element("dept_contact") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("dept_contact").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                        regexText = new Regex("{current_month_year}");
                        docText = regexText.Replace(docText, DateTime.Now.ToString("Y"));
                        regexText = new Regex("{current_date}");
                        docText = regexText.Replace(docText, DateTime.Now.Day.ToString());
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getAffidavitWithdrawalJRDoc(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            string dnames = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />" + e.Element("addressline2").Value);
                                else
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + "," : "");
                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                                if (e.Element("Type").Value == "Primary Appellant")
                                {
                                    regexText = new Regex("{appellant_last}");
                                    docText = regexText.Replace(docText, e.Element("LastName") == null ? "" : e.Element("LastName").Value);
                                }
                            }
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Respondent" || e.Element("Type").Value == "Co-Respondent"))
                            {
                                if (e.Element("Type").Value == "Co-Respondent")
                                    dnames = String.Concat(dnames, ", ", firstname, " ", lastname);
                                else
                                    dnames = String.Concat(firstname, " ", lastname, dnames);

                                // old code, no longer use but place holder still in template
                                regexText = new Regex("{if_exists_d}");
                                docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + "," : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");

                            regexText = new Regex("{primary_name}");
                            docText = regexText.Replace(docText, names);

                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names + ", Requestors";
                            else
                                names = names + ", Requestor";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        if (dnames != "")
                        {
                            int place = dnames.LastIndexOf(", ");
                            if (place > 1)
                                dnames = dnames.Remove(place, 2).Insert(place, " and ");

                            regexText = new Regex("{determination_name}");
                            docText = regexText.Replace(docText, dnames);

                            if (dnames.Contains(", ") || dnames.Contains(" and "))
                                dnames = dnames + ", Permit Holders";
                            else
                                dnames = dnames + ", Permit Holder";
                        }
                        regexText = new Regex("{Determination_LastName}");
                        docText = regexText.Replace(docText, "");
                        regexText = new Regex("{Determination_FirstName}");
                        docText = regexText.Replace(docText, dnames);


                        regexText = new Regex("{property_address}");
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{Dept_Agency}");
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{permitnumber}");
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{permittype}");
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{CaseType}");
                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{PlanningDept}");
                        if (doc.Element("Document").Element("Applications").Element("PlanningDept") != null && doc.Element("Document").Element("Applications").Element("PlanningDept").ToString() != "")
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("PlanningDept").Value);
                        else
                            docText = regexText.Replace(docText, "");
                        
                        regexText = new Regex("{ApplicationID}");
                        if (doc.Element("Document").Element("Applications").Element("ApplicationID") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ApplicationID").Value);
                        else
                            docText = regexText.Replace(docText, "");
                       
                        regexText = new Regex("{dept_info}");
                        if (doc.Element("Document").Element("Applications").Element("dept_contact") != null)
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("dept_contact").Value);
                        else
                            docText = regexText.Replace(docText, "");

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                        regexText = new Regex("{current_month_year}");
                        docText = regexText.Replace(docText, DateTime.Now.ToString("Y"));
                        regexText = new Regex("{current_date}");
                        docText = regexText.Replace(docText, DateTime.Now.Day.ToString());
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getJRGrantedDoc(int app_id, string resultFile, string templateFile, string type)
        {
            string names = "";
            string dnames = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, city, state, zip, addressline1, addressline2";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }

                    try
                    {
                        Regex regexText = new Regex("");
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();

                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                regexText = new Regex("{a_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + "<w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{a_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{a_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");

                                regexText = new Regex("{a_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{a_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Respondent" || e.Element("Type").Value == "Co-Respondent"))
                            {
                                if (e.Element("Type").Value == "Co-Respondent")
                                    dnames = String.Concat(dnames, ", ", firstname, " ", lastname);
                                else
                                    dnames = String.Concat(firstname, " ", lastname, dnames);

                                // old code, no longer use but place holder still in template
                                regexText = new Regex("{if_exists_d}");
                                docText = regexText.Replace(docText, "");

                                regexText = new Regex("{d_addressLine1}");
                                if (e.Element("addressline2") != null && e.Element("addressline2").Value != "")
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value + " <w:br />");
                                }
                                else
                                {
                                    docText = regexText.Replace(docText, e.Element("addressline1") == null ? "" : e.Element("addressline1").Value);
                                }
                                regexText = new Regex("{d_addressLine2}");
                                docText = regexText.Replace(docText, e.Element("addressline2") == null ? "" : e.Element("addressline2").Value);

                                regexText = new Regex("{d_city}");
                                docText = regexText.Replace(docText, (e.Element("city") != null && e.Element("city").Value != "") ? e.Element("city").Value + ", " : "");
                                regexText = new Regex("{d_state}");
                                docText = regexText.Replace(docText, e.Element("state") == null ? "" : e.Element("state").Value);
                                regexText = new Regex("{d_zip}");
                                docText = regexText.Replace(docText, e.Element("zip") == null ? "" : e.Element("zip").Value);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                            if (names.Contains(", ") || names.Contains(" and "))
                                names = names + ", Requestors";
                            else
                                names = names + ", Requestor";
                        }
                        regexText = new Regex("{appellant_names}");
                        docText = regexText.Replace(docText, names);

                        if (dnames != "")
                        {
                            int place = dnames.LastIndexOf(", ");
                            if (place > 1)
                                dnames = dnames.Remove(place, 2).Insert(place, " and ");

                            regexText = new Regex("{determination_name}");
                            docText = regexText.Replace(docText, dnames);

                            if (dnames.Contains(", ") || dnames.Contains(" and "))
                                dnames = dnames + ", Permit Holders";
                            else
                                dnames = dnames + ", Permit Holder";
                        }
                        regexText = new Regex("{Determination_LastName}");
                        docText = regexText.Replace(docText, "");
                        regexText = new Regex("{Determination_FirstName}");
                        docText = regexText.Replace(docText, dnames);

                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            DateTime hearingDate = (DateTime)doc.Element("Document").Element("Applications").Element("HearingDate");
                            string hearingDate5 = hearingDate.AddDays(5).ToString("MMMM dd, yyyy");

                            regexText = new Regex("{HearingDate_5}");
                            docText = regexText.Replace(docText, hearingDate5);

                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, "");
                        }

                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        }
                        else
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, "");
                        }

                        // Granted or Denied?
                        regexText = new Regex("{jr_type}");
                        docText = regexText.Replace(docText, type);

                        string today = DateTime.Now.ToString("MMMM dd, yyyy");
                        regexText = new Regex("{TodayDate}");
                        docText = regexText.Replace(docText, today);
                        
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getWordDocument(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            string dnames = "";
            string case_statement = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, Comments";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }
                    try
                    {
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value.Trim();
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value.Trim();
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                if (e.Element("Comments") != null && case_statement == "")
                                    case_statement = e.Element("Comments").Value;
                            }
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Respondent" || e.Element("Type").Value == "Co-Respondent"))
                            {
                                if (e.Element("Type").Value == "Co-Respondent")
                                    dnames = String.Concat(dnames, ", ", firstname, " ", lastname);
                                else
                                    dnames = String.Concat(firstname, " ", lastname, dnames);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                        }
                        Regex regexText = new Regex("{names}");
                        docText = regexText.Replace(docText, names);

                        regexText = new Regex("{determination_name}");
                        if (dnames != "")
                        {
                            int place = dnames.LastIndexOf(", ");
                            if (place > 1)
                                dnames = dnames.Remove(place, 2).Insert(place, " and ");
                            docText = regexText.Replace(docText, dnames);
                        }
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{if_exists_d}");
                        if (dnames != "")
                            docText = regexText.Replace(docText, ", to: ");
                        else
                            docText = regexText.Replace(docText, "");

                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("AppellantBrief") != null)
                        {
                            regexText = new Regex("{AppellantBrief}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("AppellantBrief").Value);
                        }
                        else
                        {
                            regexText = new Regex("{AppellantBrief}");
                            docText = regexText.Replace(docText, "[Please fill in]");
                        }
                        if (doc.Element("Document").Element("Applications").Element("OthersBrief") != null)
                        {
                            regexText = new Regex("{OthersBrief}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("OthersBrief").Value);
                        }
                        else
                        {
                            regexText = new Regex("{OthersBrief}");
                            docText = regexText.Replace(docText, "[Please fill in]");
                        }
                        if (doc.Element("Document").Element("Applications").Element("ExpDate") != null)
                        {
                            regexText = new Regex("{ExpDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("FileDate") != null)
                        {
                            regexText = new Regex("{FileDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("FileDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        }
                        else
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                        {
                            regexText = new Regex("{related_agency}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        }
                        else
                        {
                            regexText = new Regex("{related_agency}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (case_statement != "")
                        {
                            regexText = new Regex("{case_statement}");
                            docText = regexText.Replace(docText, case_statement);
                        }
                        else
                        {
                            regexText = new Regex("{case_statement}");
                            docText = regexText.Replace(docText, "");
                        }
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getWordDocumentJR(int app_id, string resultFile, string templateFile)
        {
            string names = "";
            string dnames = "";
            string case_statement = "";

            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            byte[] byteArray = File.ReadAllBytes(templateFile);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    // search and replace placeholders
                    //Regex regexText = new Regex("{AppellantBrief}");
                    //docText = regexText.Replace(docText, "Hi Everyone!");
                    string regex = @"{(\w+)}";
                    Regex r = new Regex(regex);
                    var matches = r.Matches(docText);
                    foreach (Match match in matches)
                    {
                        ldb.tblApplicationsFields += match.Value.Replace("{", "").Replace("}", "") + ",";
                    }
                    ldb.tblApplicationsFields.Remove(ldb.tblApplicationsFields.LastIndexOf(','), 1);

                    //ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
                    ldb.tblNameFields = "FirstName, LastName, Type, Comments";
                    XDocument doc = ldb.createQuery();

                    if (doc != null && doc.Element("ERROR") != null)
                    {
                        return doc.Element("ERROR").Value;
                    }
                    try
                    {
                        //get & fill values from tblNameField
                        foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                        {
                            string firstname = e.Element("FirstName") == null ? "" : e.Element("FirstName").Value;
                            string lastname = e.Element("LastName") == null ? "" : e.Element("LastName").Value;

                            if (e.Element("Type") != null && (e.Element("Type").Value == "Primary Appellant" || e.Element("Type").Value == "Co-Appellant"))
                            {
                                if (e.Element("Type").Value == "Co-Appellant")
                                    names = String.Concat(names, ", ", firstname, " ", lastname);
                                else
                                    names = String.Concat(firstname, " ", lastname, names);

                                if (e.Element("Comments") != null && case_statement == "")
                                    case_statement = e.Element("Comments").Value;
                            }
                            if (e.Element("Type") != null && (e.Element("Type").Value == "Respondent" || e.Element("Type").Value == "Co-Respondent"))
                            {
                                if (e.Element("Type").Value == "Co-Respondent")
                                    dnames = String.Concat(dnames, ", ", firstname, " ", lastname);
                                else
                                    dnames = String.Concat(firstname, " ", lastname, dnames);
                            }
                        }
                        if (names != "")
                        {
                            int place = names.LastIndexOf(", ");
                            if (place > 1)
                                names = names.Remove(place, 2).Insert(place, " and ");
                        }
                        Regex regexText = new Regex("{names}");
                        docText = regexText.Replace(docText, names);

                        regexText = new Regex("{determination_name}");
                        if (dnames != "")
                        {
                            int place = dnames.LastIndexOf(", ");
                            if (place > 1)
                                dnames = dnames.Remove(place, 2).Insert(place, " and ");
                            docText = regexText.Replace(docText, dnames.Trim() + ",");
                        }
                        else
                            docText = regexText.Replace(docText, "");

                        regexText = new Regex("{if_exists_d}");
                        if (dnames != "")
                            docText = regexText.Replace(docText, ", issued to: ");
                        else
                            docText = regexText.Replace(docText, "");

                        if (doc.Element("Document").Element("Applications").Element("HearingDate") != null)
                        {
                            regexText = new Regex("{HearingDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("AppellantBrief") != null)
                        {
                            regexText = new Regex("{AppellantBrief}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("AppellantBrief").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("OthersBrief") != null)
                        {
                            regexText = new Regex("{OthersBrief}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("OthersBrief").Value);
                        }
                        else
                        {
                            regexText = new Regex("{OthersBrief}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("ExpDate") != null)
                        {
                            regexText = new Regex("{ExpDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("ExpDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("FileDate") != null)
                        {
                            regexText = new Regex("{FileDate}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("FileDate").Value);
                        }
                        if (doc.Element("Document").Element("Applications").Element("CaseType") != null)
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("CaseType").Value);
                        }
                        else
                        {
                            regexText = new Regex("{CaseType}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("Property_Address") != null)
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("Property_Address").Value);
                        }
                        else
                        {
                            regexText = new Regex("{property_address}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("related_agency") != null)
                        {
                            regexText = new Regex("{related_agency}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("related_agency").Value);
                        }
                        else
                        {
                            regexText = new Regex("{related_agency}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permitnumber") != null)
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permitnumber").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permitnumber}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (doc.Element("Document").Element("Applications").Element("permittype") != null)
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, doc.Element("Document").Element("Applications").Element("permittype").Value);
                        }
                        else
                        {
                            regexText = new Regex("{permittype}");
                            docText = regexText.Replace(docText, "");
                        }
                        if (case_statement != "")
                        {
                            regexText = new Regex("{case_statement}");
                            docText = regexText.Replace(docText, case_statement);
                        }
                        else
                        {
                            regexText = new Regex("{case_statement}");
                            docText = regexText.Replace(docText, "");
                        }
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                // Save the file with the new name
                File.WriteAllBytes(resultFile, stream.ToArray());
            }
            return resultFile;
        }
        
        public string getVoteSheetMinutes(DateTime HearingDate)
        {
            string localFilePath = "";
            try
            {
                string templateFile = HostingEnvironment.ApplicationPhysicalPath + "\\Template_VoteSheetMinutes.docx";
                string templateFile_jr = HostingEnvironment.ApplicationPhysicalPath + "\\Minutes_JR.docx";
                string templateFile_reg = HostingEnvironment.ApplicationPhysicalPath + "\\Minutes_Regular.docx";
                string templateFile_added = HostingEnvironment.ApplicationPhysicalPath + "\\Minutes_Added.docx";
                //create a temp file, these temp files are to be delete daily
                string[] tempFile = System.IO.Path.GetTempFileName().Split('\\');
                string resultFile = HostingEnvironment.ApplicationPhysicalPath + "tempfiles\\" + "Votesheet_" + tempFile[tempFile.Length - 1] + ".docx";
                localFilePath = getMinutes(HearingDate, resultFile, templateFile, templateFile_jr, templateFile_reg, templateFile_added);
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            return localFilePath;
        }


        private string getMinutes(DateTime HearingDate, string resultFile, string templateFile, string templateFile_jr, string templateFile_reg, string templateFile_added)
        {
            string docText = "";
            string regInnerText = "";
            string jrInnerText = "";
            string addedInnerText = "";
           
            Regex regexText = new Regex("");
            int applicationID = 0;

            if (!File.Exists(templateFile))
                return "Template file not found!!!";

            GenerateMinutes ldb = new GenerateMinutes();
            ldb.HearingDate = HearingDate;

            XDocument doc = ldb.createQuery();
            if (doc != null && doc.Element("ERROR") != null)
            {
                return doc.Element("ERROR").Value;
            }
                       
            byte[] byteArray = File.ReadAllBytes(templateFile);
            File.WriteAllBytes(resultFile, byteArray);
            
            WordprocessingDocument jrDoc = WordprocessingDocument.Open(templateFile_jr, false);
            Body jrBody = jrDoc.MainDocumentPart.Document.Body;
            SectionProperties s1 = jrBody.Descendants<SectionProperties>().FirstOrDefault();
            jrBody.RemoveChild<SectionProperties>(s1);
            WordprocessingDocument regDoc = WordprocessingDocument.Open(templateFile_reg, false);
            Body regBody = regDoc.MainDocumentPart.Document.Body;
            SectionProperties s2 = regBody.Descendants<SectionProperties>().FirstOrDefault();
            regBody.RemoveChild<SectionProperties>(s2);
            WordprocessingDocument addedDoc = WordprocessingDocument.Open(templateFile_added,false);
            Body addedBody = addedDoc.MainDocumentPart.Document.Body;
            SectionProperties s3 = addedBody.Descendants<SectionProperties>().FirstOrDefault();
            addedBody.RemoveChild<SectionProperties>(s3);
            
            //using (MemoryStream stream = new MemoryStream())
            //{
                //stream.Write(byteArray, 0, (int)byteArray.Length);
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(resultFile, true))
                {
                    MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                    SectionProperties s4 = mainPart.Document.Body.Descendants<SectionProperties>().FirstOrDefault();
                    mainPart.Document.Body.RemoveChild<SectionProperties>(s4);
                    docText = mainPart.Document.Body.InnerXml;
                    
                    mainPart.Document.Body.InnerXml = "";
                    string bodyContent = "";
                    string endTime = DateTime.Now.ToString("H:mm tt");
                    try
                    {
                        // fill hearing date in main template
                        int counter = 4;
                        int numVotes = doc.Element("Document").Element("VoteResults").Elements("VoteResult").Count();
                        string AppID = "";
                        
                        List<string> L1 = new List<string> { "{HearingDate}", "{department_reps}", "{Presents}", "{Absents}" };
                        List<string> L2 = new List<string>();

                        L2.Add(HearingDate.ToLongDateString());
                        L2.Add(doc.Element("Document").Element("VoteResults").Element("DeptRepresentatives") == null || doc.Element("Document").Element("VoteResults").Element("DeptRepresentatives").Value == "" ? "" : "<w:br />" + doc.Element("Document").Element("VoteResults").Element("DeptRepresentatives").Value);
                        L2.Add(doc.Element("Document").Element("VoteResults").Element("Presents") == null || doc.Element("Document").Element("VoteResults").Element("Presents").Value == "" ? "NONE" : doc.Element("Document").Element("VoteResults").Element("Presents").Value);
                        L2.Add(doc.Element("Document").Element("VoteResults").Element("Absentees") == null || doc.Element("Document").Element("VoteResults").Element("Absentees").Value  == "" ? "" : "<w:br />" + "ABSENT: " + doc.Element("Document").Element("VoteResults").Element("Absentees").Value);
                        docText = searchReplace(docText, L1, L2);
                        if (doc.Element("Document").Element("VoteResults").Elements("VoteResult").First().Element("EndTime") != null)
                        {
                            endTime = DateTime.Parse(doc.Element("Document").Element("VoteResults").Elements("VoteResult").First().Element("EndTime").Value).ToString("hh:mm tt");
                        }
                        foreach (XElement e in doc.Element("Document").Element("VoteResults").Elements("VoteResult"))
                        {
                            #region Added template
                            try { 
                                applicationID = Int32.Parse(e.Element("ApplicationID").Value);
                                string temp = applicationID.ToString();

                                if (applicationID.ToString().Length > 4)
                                    temp = applicationID.ToString().Substring(3, applicationID.ToString().Length - 3);
                                if (applicationID.ToString().Length > 4)
                                    AppID =  (HearingDate.Year.ToString().Substring(2, 2) + "-" + temp);
                                else
                                    AppID = "JR-" + HearingDate.Year.ToString().Substring(2, 2) + "-" + temp;
                            }
                            catch (Exception any) { applicationID = 0; }
                            //static items on votesheet
                            if (applicationID == 0)
                            {
                                if (e.Element("Type").Value == "item_1")
                                {
                                    List<string> listofelements = new List<string> { "{speakers_1}" };
                                    List<string> listofreplace = new List<string>();

                                    listofreplace.Add(e.Element("Comments").Value == "" ? "NONE" : e.Element("Comments").Value);
                                    docText = searchReplace(docText, listofelements, listofreplace);
                                }
                                else if (e.Element("Type").Value == "item_2")
                                {
                                    List<string> listofelements = new List<string> { "{speakers_2}", "{public_comment_2}" };
                                    List<string> listofreplace = new List<string>();

                                    listofreplace.Add(e.Element("Speakers").Value == "" ? "NONE" : e.Element("Speakers").Value);
                                    listofreplace.Add(e.Element("Comments").Value == "" ? "NONE" : e.Element("Comments").Value);
                                    docText = searchReplace(docText, listofelements, listofreplace);
                                }
                                else if (e.Element("Type").Value == "item_3")
                                {
                                    List<string> listofelements = new List<string> { "{speakers_3}", "{public_comment_3}", "{action_3}" };
                                    List<string> listofreplace = new List<string>();

                                    listofreplace.Add(e.Element("Speakers").Value == "" ? "NONE" : e.Element("Speakers").Value);
                                    listofreplace.Add(e.Element("Comments").Value == "" ? "NONE" : e.Element("Comments").Value);
                                    listofreplace.Add(e.Element("Action").Value == "" ? "NONE" : e.Element("Action").Value);
                                    docText = searchReplace(docText, listofelements, listofreplace);
                                }
                                else if (e.Element("Type").Value == "item_added")
                                {
                                    string title = "Please fill in";
                                    string description = "Please fill in";
                                    string desc = e.Element("AppealDescription") == null ? "NONE" : e.Element("AppealDescription").Value;
                                    int iPos = desc.IndexOf("\r");
                                    if (iPos > 1)
                                    {
                                        title = desc.Substring(0, iPos);
                                        description = desc.Substring(iPos + 1, desc.Length - iPos - 1);
                                    }
                                    else
                                    {
                                        title = "";
                                        description = desc;
                                    }
                                    List<string> listofelements = new List<string> { "{speakers}", "{public_comment}", "{action}", "{appeal_description}", "{counter}", "{appeal_no}", "{votes}", "{decision}" };
                                    List<string> listofreplace = new List<string>();

                                    listofreplace.Add(e.Element("Speakers") == null ? "NONE" : e.Element("Speakers").Value);
                                    listofreplace.Add(e.Element("Comments") == null ? "NONE" : e.Element("Comments").Value);
                                    listofreplace.Add(e.Element("Action") == null ? "NONE" : e.Element("Action").Value);
                                    listofreplace.Add(description);
                                    listofreplace.Add(counter.ToString());
                                    listofreplace.Add(title);

                                    string votes = "";
                                    if (e.Element("Results") != null)
                                    {
                                        string[] bmd = e.Element("Results").Value.Split(',');
                                        string[] ids = e.Element("Members").Value.Split(',');
                                        for (int y = 0; y < bmd.Count(); y++)
                                        {
                                            if(bmd[y] != "Absent")
                                                votes += ids[y] + " voted " + bmd[y] + "<w:br />";
                                        }
                                    }
                                    listofreplace.Add(votes);
                                    listofreplace.Add(e.Element("Decision") == null ? "NONE" : e.Element("Decision").Value);
                                    addedInnerText = addedBody.InnerXml;
                                    addedInnerText = searchReplace(addedInnerText, listofelements, listofreplace);
                                    if (numVotes == counter)
                                    {
                                        addedInnerText = addedInnerText.Replace("{ADJOURNMENT}", "<w:br /><w:br />ADJOURNMENT: <w:br />").Replace("{adjournment_text}", "There being no further business, President Hwang adjourned the meeting at " + endTime);
                                    }
                                    else
                                    {
                                        addedInnerText = addedInnerText.Replace("{ADJOURNMENT}", "").Replace("{adjournment_text}", "");
                                    }
                                    bodyContent += addedInnerText;
                                    counter++;
                                }
                            }
                            #endregion header
                            #region JR template
                            else if (e.Element("Type").Value == "JR")
                            {
                                List<string> listofelements = new List<string> {"{FileDate}","{public_comment}","{speakers}","{action}",
                                    "{ExpDate}","{PermitDescription}","{Determination_FirstName}",
                                    "{Determination_LastName}","{Primary_LastName}","{Primary_FirstName}",
                                    "{PropertyAddress}","{PermitType}","{DepartmentName}","{appeal_no}",
                                    "{DepartmentalAction}", "{PermitNumber}","{counter}"};
                                List<string> listofreplace = new List<string>();

                                string FileDate = e.Element("FileDate") == null ? "" : ((DateTime)e.Element("FileDate")).ToLongDateString();
                                listofreplace.Add(FileDate);
                                listofreplace.Add(e.Element("Comments") == null ? "NONE" : e.Element("Comments").Value);
                                listofreplace.Add(e.Element("Speakers") == null ? "NONE" : e.Element("Speakers").Value);
                                listofreplace.Add(e.Element("Action") == null ? "NONE" : e.Element("Action").Value);
                                string ExpDate = e.Element("ExpDate") == null ? "" : ((DateTime)e.Element("ExpDate")).ToLongDateString();
                                listofreplace.Add(ExpDate);
                                listofreplace.Add(e.Element("PermitDescription") == null ? "" : e.Element("PermitDescription").Value);
                                listofreplace.Add(e.Element("Determination_FirstName") == null ? "" : e.Element("Determination_FirstName").Value);
                                listofreplace.Add(e.Element("Determination_LastName") == null ? "" : e.Element("Determination_LastName").Value);
                                listofreplace.Add(e.Element("Primary_LastName") == null ? "" : e.Element("Primary_LastName").Value);
                                listofreplace.Add(e.Element("Primary_Name") == null ? "" : e.Element("Primary_Name").Value);
                                listofreplace.Add(e.Element("PropertyAddress") == null ? "" : e.Element("PropertyAddress").Value);
                                
                                listofreplace.Add(e.Element("PermitType") == null ? "" : e.Element("PermitType").Value);
                                
                                listofreplace.Add(e.Element("DepartmentName") == null ? "" : e.Element("DepartmentName").Value);
                                listofreplace.Add(AppID);
                                listofreplace.Add(e.Element("DepartmentalAction") == null ? "" : e.Element("DepartmentalAction").Value);
                                listofreplace.Add(e.Element("PermitNumber") == null ? "" : e.Element("PermitNumber").Value);
                                listofreplace.Add(counter.ToString());

                                jrInnerText = jrBody.InnerXml;
                                jrInnerText = searchReplace(jrInnerText, listofelements, listofreplace);
                                if (numVotes == counter)
                                {
                                    jrInnerText = jrInnerText.Replace("{ADJOURNMENT}", "<w:br /><w:br />ADJOURNMENT: <w:br />").Replace("{adjournment_text}", "There being no further business, President Hwang adjourned the meeting at " + endTime);
                                }
                                else
                                {
                                    jrInnerText = jrInnerText.Replace("{ADJOURNMENT}", "").Replace("{adjournment_text}", "");
                                }

                                bodyContent += jrInnerText;
                                counter++;
                            }
                            #endregion JR
                            #region REGULAR template
                            else
                            {
                                List<string> listofelements = new List<string> {"{public_comment}","{speakers}",
                                    "{action}","{Primary_LastName}","{Primary_FirstName}",
                                    "{PropertyAddress}","{DepartmentName}","{appeal_no}",
                                    "{PermitNumber}","{counter}", "{no_determination_holder}","{yes_determination_holder}","{PlanningDept}"};
                                List<string> listofreplace = new List<string>();

                                listofreplace.Add(e.Element("Comments") == null ? "NONE" : e.Element("Comments").Value);
                                listofreplace.Add(e.Element("Speakers") == null ? "NONE" : e.Element("Speakers").Value);
                                listofreplace.Add(e.Element("Action") == null ? "NONE" : e.Element("Action").Value);
                                listofreplace.Add(e.Element("Primary_LastName") == null ? "" : e.Element("Primary_LastName").Value);
                                listofreplace.Add(e.Element("Primary_Name") == null ? "" : e.Element("Primary_Name").Value);
                                listofreplace.Add(e.Element("PropertyAddress") == null ? "" : e.Element("PropertyAddress").Value);
                                listofreplace.Add(e.Element("DepartmentName") == null ? "" : e.Element("DepartmentName").Value);
                                listofreplace.Add(AppID);
                                listofreplace.Add(e.Element("PermitNumber") == null ? "" : e.Element("PermitNumber").Value);
                                listofreplace.Add(counter.ToString());
                                
                                string ExpDate = e.Element("ExpDate") == null ? "" : ((DateTime)e.Element("ExpDate")).ToLongDateString();
                                string noStr = "Appealing the " + e.Element("DepartmentalAction") == null ? "" : e.Element("DepartmentalAction").Value +
                                    " on " + ExpDate + ", of a {PermitType} ({Permit Description}).";
                                string yesStr = "Protesting the " + e.Element("DepartmentalAction") == null ? "" : e.Element("DepartmentalAction").Value +
                                    " on " + ExpDate + ", to ";
                                if (e.Element("Determination_FirstName") != null || e.Element("Determination_LastName") != null)
                                {
                                    string lastname = e.Element("Determination_LastName") == null ? "" : e.Element("Determination_LastName").Value;
                                    string firstname = e.Element("Determination_FirstName") == null ? "" : e.Element("Determination_FirstName").Value;

                                    yesStr += firstname + " " + lastname
                                           + ", of a " + e.Element("PermitType") == null ? "" : e.Element("PermitType").Value
                                           + " (" + e.Element("PermitDescription") == null ? "" : "("+ e.Element("PermitDescription").Value + ").";
                                    noStr = "";
                                }
                                else
                                {
                                    noStr += ", of a " + e.Element("PermitType") == null ? "" : e.Element("PermitType").Value
                                     + " (" + e.Element("PermitDescription") == null ? "" : "(" + e.Element("PermitDescription").Value + ").";
                                    yesStr = "";
                                }
                                listofreplace.Add(noStr);
                                listofreplace.Add(yesStr);
                                listofreplace.Add(e.Element("PlanningDept") == null ? "" : e.Element("PlanningDept").Value);

                                regInnerText = regBody.InnerXml;
                                regInnerText = searchReplace(regInnerText, listofelements, listofreplace);
                                if (numVotes == counter)
                                {
                                    regInnerText = regInnerText.Replace("{ADJOURNMENT}", "<w:br /><w:br />ADJOURNMENT: <w:br />").Replace("{adjournment_text}", "There being no further business, President Hwang adjourned the meeting at " + endTime);
                                }
                                else
                                {
                                    regInnerText = regInnerText.Replace("{ADJOURNMENT}", "").Replace("{adjournment_text}", "");
                                }
                                bodyContent += regInnerText;
                                counter++;
                            }
                            #endregion REGULAR
                        }

                        // get header
                        HeaderPart header = mainPart.HeaderParts.FirstOrDefault();
                        header.Header.InnerXml = header.Header.InnerXml.Replace("{HearingDate}", HearingDate.ToString("MMMM dd, yyyy"));
                   
                        mainPart.Document.Body.InnerXml = docText + bodyContent;
                        mainPart.Document.Body.AppendChild<SectionProperties>(s4);

                        mainPart.Document.Save();
                        jrDoc.Close(); addedDoc.Close(); regDoc.Close(); wordDoc.Close();
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                //}   
            }
            return resultFile;
        }
        
        private string searchReplace(string innerXML, List<string> find, List<string> replace)
        {
            
            for (int i = 0; i < find.Count(); i++)
            {
                innerXML = innerXML.Replace(find[i], replace[i]);
            }  
            return innerXML;
        }
        
        private DocumentFormat.OpenXml.Wordprocessing.Run createRun(string text, bool isUnderline, bool isNewLine)
        {
            Run run1 = new Run();

            RunProperties runProperties1 = new RunProperties();
            RunFonts runFonts1 = new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" };
            FontSize fontSize1 = new FontSize() { Val = "22" };
            Underline underline1 = null;
            if (isUnderline)
                underline1 = new Underline() { Val = UnderlineValues.Single };

            runProperties1.Append(runFonts1);
            runProperties1.Append(fontSize1);
            if (underline1 != null)
                runProperties1.Append(underline1);
            Text text1 = new Text();
            text1.Text = text;
            run1.Append(runProperties1);
            run1.Append(text1);
            if (isNewLine)
            {
                run1.Append(new Break());
            }
            return run1;
        }
        
        private DocumentFormat.OpenXml.Wordprocessing.Run createEmptyRun(bool isNewLine)
        {
            Run run1 = new Run();

            RunProperties runProperties1 = new RunProperties();
            RunFonts runFonts1 = new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" };
            FontSize fontSize1 = null;
            if (isNewLine)
                fontSize1 = new FontSize() { Val = "16" };
            else
                fontSize1 = new FontSize() { Val = "22" };
            runProperties1.Append(runFonts1);
            runProperties1.Append(fontSize1);

            Text text1 = new Text() { Space = DocumentFormat.OpenXml.SpaceProcessingModeValues.Preserve };
            text1.Text = ": ";
            run1.Append(runProperties1);

            if (isNewLine)
                run1.Append(new Break());
            else
                run1.Append(text1);
            return run1;
        }
        
        private ParagraphProperties newParagraphProperties()
        {
            ParagraphProperties paragraphProperties1 = new ParagraphProperties();
            WidowControl widowControl1 = new WidowControl() { Val = false };

            Tabs tabs1 = new Tabs();
            TabStop tabStop1 = new TabStop() { Val = TabStopValues.Right, Position = 9540 };

            tabs1.Append(tabStop1);

            ParagraphMarkRunProperties paragraphMarkRunProperties1 = new ParagraphMarkRunProperties();
            RunFonts runFonts1 = new RunFonts() { Ascii = "Arial", HighAnsi = "Arial" };
            FontSize fontSize1 = new FontSize() { Val = "22" };

            paragraphMarkRunProperties1.Append(runFonts1);
            paragraphMarkRunProperties1.Append(fontSize1);

            paragraphProperties1.Append(widowControl1);
            paragraphProperties1.Append(tabs1);
            paragraphProperties1.Append(paragraphMarkRunProperties1);
            return paragraphProperties1;
        }

        public class GenerateMinutes
        {
            private DateTime _hearingdate;
            public GenerateMinutes()
            {
                //this.tvr = tblVoteResult.CreatetblVoteResult(0, DateTime.Now, "", DateTime.Now, 0);
            }
            public DateTime HearingDate
            {
                set
                {
                    this._hearingdate = value;
                }
                get
                {
                    return this._hearingdate;
                }
            }

            public XDocument createQuery()
            {
                LicenseDBEntities dbcontext = new LicenseDBEntities();
                DocumentGeneration dg = new DocumentGeneration();

                try
                {
                    // get board members
                    IDictionary<string, IDictionary<string, string>> dict = new Dictionary<string, IDictionary<string, string>>();
                    var boardmembers = (from tbl_bm in dbcontext.tblBoardMembers
                                        where tbl_bm.Status == "A"
                                        orderby tbl_bm.FirstName
                                        select new { tbl_bm }).ToList();
                    for (int j = 0; j < boardmembers.Count; j++)
                    {
                        IDictionary<string, string> bmsDict = new Dictionary<string, string>();
                        bmsDict[boardmembers[j].tbl_bm.Title] = boardmembers[j].tbl_bm.FirstName + " " + boardmembers[j].tbl_bm.LastName;
                        dict[boardmembers[j].tbl_bm.MemberID] = bmsDict;
                    }
                    var query = (from tbl_v in dbcontext.tblVoteResults
                                 where (tbl_v.HearingDate.Month == this.HearingDate.Month && tbl_v.HearingDate.Year == this.HearingDate.Year 
                                 && tbl_v.HearingDate.Day == this.HearingDate.Day)
                                 orderby tbl_v.SortOrder
                                 select new { tbl_v }).ToList();
                    if (query.Count > 0)
                    {
                        XElement apps_xml = new XElement("VoteResults");
                        bool done = false;
                        string names = "";
                        for (int i = 0; i < query.Count; i++)
                        {
                            tblVoteResult v = query[i].tbl_v;
                            XElement vote = new XElement("VoteResult");
                            
                            vote.Add(new XElement("Action", HttpUtility.HtmlEncode(v.Action)));
                            vote.Add(new XElement("Comments", HttpUtility.HtmlEncode(v.Comments)));
                            vote.Add(new XElement("Decision", HttpUtility.HtmlEncode(v.Decision)));
                            vote.Add(new XElement("HearingDate", v.HearingDate));
                            vote.Add(new XElement("EndTime", (v.EndTime == null ? DateTime.Now : v.EndTime.Value)));
                            vote.Add(new XElement("StartTime", (v.StartTime == null ? DateTime.Now : v.StartTime.Value)));
                            vote.Add(new XElement("Speakers", HttpUtility.HtmlEncode(v.Speakers)));
                            vote.Add(new XElement("VoteID", v.VoteID));
                            vote.Add(new XElement("ModDate", v.ModDate));
                            vote.Add(new XElement("ApplicationID", v.ApplicationID));
                            vote.Add(new XElement("Type", v.Type == null ? "" : v.Type.Trim()));
                            vote.Add(new XElement("AppealDescription", HttpUtility.HtmlEncode(v.AppealDescription)));
                            
                            // figure out if there are any absents.
                            if ((!string.IsNullOrEmpty(v.BoardMemberDecisions)) &&  (!done))
                            {
                                string[] bmd = v.BoardMemberDecisions.Split(',');
                                string[] ids = v.BoardMembers.Split(',');
                                string absentees = "";
                                string president = "";
                                string vp = "";
                                string others = "";

                                // get board member names in order
                                for (int x = 0; x < bmd.Count(); x++)
                                {
                                    IDictionary<string, string> memberDict = dict[ids[x]];
                                    names += memberDict.Values.FirstOrDefault() + ",";
                                }
                                names = names.TrimEnd(',');
                                if ( v.BoardMemberDecisions.Contains("Absent"))
                                {
                                    for (int k = 0; k < bmd.Count(); k++)
                                    {
                                        if (bmd[k] == "Absent")
                                        {
                                            IDictionary<string, string> memberDict = dict[ids[k]];
                                            absentees += memberDict.Values.FirstOrDefault();
                                            dict.Remove(ids[k]);
                                        }
                                    }
                                    apps_xml.Add(new XElement("Absentees", absentees));
                                }
                                
                                for (int j = 0; j < dict.Count; j++)
                                {
                                    if (dict.ElementAt(j).Value.Keys.FirstOrDefault() == "President")
                                        president = dict.ElementAt(j).Value.Values.FirstOrDefault();
                                    else if (dict.ElementAt(j).Value.Keys.FirstOrDefault() == "Vice President")
                                        vp = dict.ElementAt(j).Value.Values.FirstOrDefault();
                                    else
                                        others += dict.ElementAt(j).Value.Values.FirstOrDefault() + ", ";
                                }
                                others = others.TrimEnd(' ').Trim(',');
                                apps_xml.Add(new XElement("Presents", president + ", " + vp + ", " + others));
                                apps_xml.Add(new XElement("DeptRepresentatives", HttpUtility.HtmlEncode(v.DeptRepresentatives)));
                                done = true;
                            }
                            // add member names and vote result to tree
                            vote.Add(new XElement("Members", names));
                            vote.Add(new XElement("Results", v.BoardMemberDecisions));

                            var tblapps = (from tbl_a in dbcontext.view_applications
                                           where tbl_a.ApplicationID == v.ApplicationID
                                           select new { tbl_a }).ToList();
                            var primary = (from tbl_n in dbcontext.tblNameFields
                                           where tbl_n.ApplicationID == v.ApplicationID && (tbl_n.Type == "Primary Appellant" || tbl_n.Type == "Co-Appellant")
                                           select new { tbl_n }).ToList();
                            var determination = (from tbl_n in dbcontext.tblNameFields
                                                 where tbl_n.ApplicationID == v.ApplicationID && (tbl_n.Type == "Respondent")
                                                 select new { tbl_n }).ToList();
                            var tblitems = (from tbl_i in dbcontext.tblItemDetails
                                            where tbl_i.ApplicationID == v.ApplicationID
                                            select new { tbl_i }).ToList();
                            if (tblapps.Count > 0)
                            {
                                view_applications application = tblapps[0].tbl_a;
                                vote.Add(new XElement("ExpDate", application.ExpDate));
                                vote.Add(new XElement("FileDate", application.FileDate));
                                vote.Add(new XElement("DepartmentalAction", HttpUtility.HtmlEncode(application.CaseType)));
                                vote.Add(new XElement("PermitNumber", HttpUtility.HtmlEncode(application.PermitNumber)));
                                vote.Add(new XElement("PermitType", HttpUtility.HtmlEncode(application.PermitType)));
                                vote.Add(new XElement("PlanningDept", HttpUtility.HtmlEncode(String.IsNullOrEmpty(application.PlanningDept) ? "" : application.PlanningDept.ToUpper())));

                                var tbldept = (from tbl_d in dbcontext.tblDepartments
                                               where tbl_d.AgencyID == application.Dept_Agency
                                               select new { tbl_d }).ToList();
                                if (tbldept.Count > 0)
                                {
                                    tblDepartment tbld = tbldept[0].tbl_d;
                                    vote.Add(new XElement("DepartmentName", HttpUtility.HtmlEncode(tbld.DeptName.ToUpper())));
                                }
                            }
                            if (primary.Count > 0)
                            {
                                string primary_names = "";
                                for (int k = 0; k < primary.Count;k++)
                                {
                                    tblNameField name = primary[k].tbl_n;
                                    string first = (String.IsNullOrEmpty(name.FirstName) ? "" : name.FirstName);
                                    string last = (String.IsNullOrEmpty(name.LastName) ? "" : name.LastName);
                                    string mid = (String.IsNullOrEmpty(name.MiddleName) ? "" : name.MiddleName);

                                    if ( (mid != "") || (last != "" && mid == "" ))
                                        first = first + " ";
                                    if (last != "" && mid != "")
                                        mid = mid + " ";
                                        
                                    if (primary_names != "")
                                        primary_names = String.Concat(primary_names, ", ", first, mid, last);
                                    else
                                        primary_names = String.Concat(first, mid, last);
                                }
                                vote.Add(new XElement("Primary_Name", HttpUtility.HtmlEncode(primary_names.ToUpper())));
                                //vote.Add(new XElement("Primary_LastName", HttpUtility.HtmlEncode(primary_name.LastName)));
                            }
                            if (determination.Count > 0)
                            {
                                tblNameField determination_name = determination[0].tbl_n;
                                vote.Add(new XElement("Determination_FirstName", HttpUtility.HtmlEncode(determination_name.FirstName)));
                                vote.Add(new XElement("Determination_LastName", HttpUtility.HtmlEncode(determination_name.LastName)));
                            }
                            if (tblitems.Count > 0)
                            {
                                tblItemDetail tid = tblitems[0].tbl_i;
                                vote.Add(new XElement("PermitDescription", HttpUtility.HtmlEncode(tid.Description)));
                                vote.Add(new XElement("PropertyAddress", HttpUtility.HtmlEncode(formatAddress(tid.Related_Address, tid.Related_City, tid.Related_State, tid.Related_Zip))));
                            }
                            apps_xml.Add(vote);
                        }
                        XDocument doc = new XDocument(new XElement("Document", new XElement(apps_xml)));
                        return doc;
                    }
                    else
                        return null;
                }
                catch (XmlException xe)
                {
                    XDocument doc = new XDocument(new XElement("ERROR", xe.ToString()));
                    return doc;
                }
                catch (Exception e)
                {
                    XDocument doc = new XDocument(new XElement("ERROR", e.ToString()));
                    return doc;
                }
            }

            protected string formatAddress(string address, string city, string state, string zip)
            {
                string fulladdress = "";
                if (address != "")
                    fulladdress += address;
                if (city != "")
                    fulladdress += ", " + city;
                if (state != "")
                    fulladdress += ", " + state;
                if (zip != "")
                    fulladdress += ", " + zip;
                return fulladdress;
            }
        }

        #region // deprecated, this requires MS Office installed on server

        /*public string getWordDocument(int app_id, string resultFile, string templateFile)
        {
            //create missing ref
            object missing = System.Reflection.Missing.Value;
            //use microsoft word 12.0 library
            //Application wordApp = new Application();
            Microsoft.Office.Interop.Word._Application wordApp = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word._Document adoc = new Microsoft.Office.Interop.Word.Document();
            adoc = wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);

            wordApp.Visible = false;
            object readOnly = false;
            object isVisible = false;
            
            string names = " ";
            
            LicenseDB2Word ldb = new LicenseDB2Word();
            ldb.ApplicationID = app_id;
            ldb.tblApplicationsFields = "HearingDate,FileDate, CaseType";
            ldb.tblNameFields = "FirstName, LastName";
            XDocument doc = ldb.createQuery();

            if (doc != null && doc.Element("ERROR") != null)
            {
                return doc.Element("ERROR").Value;
            }
            
            //open word template
            try{
                //load template doc
                //adoc = wordApp.Documents.Open("C:\\Documents and Settings\\hjiang\\My Documents\\Visual Studio 2010\\Projects\\ConsoleApplication1\\ConsoleApplication1\\bin\\Debug\\BOARD OF APPEALS.doc", ref missing,
                adoc = wordApp.Documents.Open(templateFile,
                                                            ref readOnly, ref missing, ref missing, ref missing,
                                                            ref missing, ref missing, ref missing, ref missing,
                                                            ref missing, ref isVisible, ref missing, ref missing, ref missing, ref missing);
                //activate document
                adoc.Activate();

                //get & fill values from tblApplications
                bookmarkReplace("dept_action", doc.Element("Document").Element("Applications").Element("CaseType").Value, adoc);
                DateTime HearingDate = DateTime.Parse(doc.Element("Document").Element("Applications").Element("HearingDate").Value);
                bookmarkReplace("hearing_date", HearingDate.ToShortDateString(), adoc);
                if (doc.Element("Document").Element("Applications").Element("FileDate").Value != "1/1/0001")
                    bookmarkReplace("file_date", DateTime.Parse(doc.Element("Document").Element("Applications").Element("FileDate").Value).ToShortDateString(), adoc);
                //thursdays prior to hearing 
                bookmarkReplace("thursdays_4", DateTime.Parse(doc.Element("Document").Element("Applications").Element("OthersBrief").Value).ToShortDateString(), adoc);
                bookmarkReplace("thursdays_2", DateTime.Parse(doc.Element("Document").Element("Applications").Element("AppellantBrief").Value).ToShortDateString(), adoc);
                //bookmarkReplace("thursdays_1", DateTime.Parse(doc.Element("Document").Element("Applications").Element("AppellantBrief").Value).ToShortDateString(), adoc);

                //get & fill values from tblNameField
                foreach (XElement e in doc.Element("Document").Element("Names").Elements("Name"))
                {
                    names = String.Concat(e.Element("LastName").Value, " ", e.Element("FirstName").Value, ",", names);
                }

                names = names.Remove(names.LastIndexOf(','), 1);
                bookmarkReplace("all_names", names, adoc);

                //save to a different document, leave the original document untouch.
                object saveas = resultFile;
                adoc.SaveAs(saveas, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                //Microsoft.Office.Interop.Word._Document rDoc = adoc;
                //release winword process
                adoc.Close(false, ref missing, ref missing);
                
               //((Microsoft.Office.Interop.Word._Application)wordApp).Quit(false, ref missing, ref missing);
               wordApp.Quit(false, ref missing, ref missing);
                return saveas.ToString();
            }
            catch (Exception e)
            {
                adoc.Close(false, ref missing, ref missing);
                ((Microsoft.Office.Interop.Word._Application)wordApp).Quit(false, ref missing, ref missing);
                return e.ToString();
            }
        }
        */
        //using MS word bookmarks, fill in the document with values from DB.
        /*static void bookmarkReplace(string bookmarkName, string value, Microsoft.Office.Interop.Word._Document adoc)
        {
            try
            {
                //create bookmarks from bookmark names; bookmark names are created in the word template
                object bookmarkStart = (object)string.Concat(bookmarkName, "_start");
                object bookmarkEnd = (object)string.Concat(bookmarkName, "_end");

                object ibkStart = adoc.Bookmarks.get_Item(ref bookmarkStart).Range.Start;
                object ibkEnd = adoc.Bookmarks.get_Item(ref bookmarkEnd).Range.Start;
                Microsoft.Office.Interop.Word.Range rng = adoc.Range(ref ibkStart, ref ibkEnd);

                //this is to calculate the length of the input field, tabs are counted as 1 but it's actually 15 white spaces.
                int rngLength = 0;
                foreach (char c in rng.Text.ToCharArray())
                {
                    if (c.Equals('\t'))
                        rngLength += 15;
                    else
                        rngLength++;
                }

                //if there are more names than the current field can hold, split the names into 2 or more lines.
                if (bookmarkName.Equals("all_names") && (value.Length > rngLength / 2))
                {

                    string[] names = value.Split(',');
                    string extra = "";
                    string orgValue = "";
                    foreach (string name in names)
                    {
                        if (string.Concat(orgValue, name).Length > (rngLength / 2))
                            extra = string.Concat(extra, name, ",");
                        else
                            orgValue = string.Concat(orgValue, name, ",");
                    }
                    bookmarkReplace("all_names", orgValue.Remove(orgValue.LastIndexOf(','), 1).Trim(), adoc);
                    bookmarkReplace("all_names_ext1", extra.Remove(extra.LastIndexOf(','), 1).Trim(), adoc);

                }
                else
                {   //pad the input with white spaces, this is because the word document template wants a specific format.
                    if ((rngLength > (value.Length * 2)) && (bookmarkName.IndexOf("all_names") < 0))
                    {
                        int padLength = (rngLength - value.Length) / 2;
                        string dummy = "";
                        int padleft = padLength > 10 ? (padLength - 10) : 0;
                        value = string.Concat(dummy.PadLeft(padleft, ' '), value, dummy.PadRight(padleft, ' '));
                    }
                    rng.Text = value;
                    //create a new bookmark
                    object oRng = rng;
                    adoc.Bookmarks.Add(bookmarkName, ref oRng);
                }
            }
            catch (Exception e)
            {
                ;
            }
        }*/
        #endregion

        public class LicenseDB2Word
        {
            private int application_id;
            private string app_fields;
            private string name_fields;
            private view_applications tbl_a;
            private tblNameField tbl_n;

            public LicenseDB2Word()
            {
                this.tbl_a = view_applications.Createview_applications(0, "", DateTime.Now, DateTime.Now, DateTime.Now, "");
                this.tbl_n = tblNameField.CreatetblNameField("", 0, 0);
            }
            public int ApplicationID
            {
                set
                {
                    this.application_id = value;
                }
            }
            public string tblApplicationsFields
            {
                get
                {
                    return this.app_fields;
                }
                set
                {
                    this.app_fields = value;
                }
            }
            public string tblNameFields
            {
                get
                {
                    return this.name_fields;
                }
                set
                {
                    this.name_fields = value;
                }
            }
            public XDocument createQuery()
            {
                LicenseDBEntities dbcontext = new LicenseDBEntities();
                DocumentGeneration dg = new DocumentGeneration();

                string[] nameFields = this.tblNameFields.Split(',');
                string[] appFields = this.tblApplicationsFields.Split(',');

                try
                {

                    var query = (from tbl_n in dbcontext.tblNameFields
                                 join tbl_a in dbcontext.view_applications on tbl_n.ApplicationID equals tbl_a.ApplicationID into g
                                 from tbl_a in g.DefaultIfEmpty()
                                 where tbl_n.ApplicationID == (this.application_id)
                                 select new { tbl_n, tbl_a }).ToList();

                    //select new { tbl_n.FirstName, tbl_n.LastName, tbl_a.HearingDate, tbl_a.FileDate }).ToList();
                    if (query.Count > 0)
                    {
                        this.tbl_a = query[0].tbl_a;
                        //get the dates
                        var dates = (from tbl_d in dbcontext.tblDateFields
                                     orderby tbl_d.ModDate descending
                                     where tbl_d.ApplicationID == this.application_id
                                     select new { tbl_d }).ToList();

                        XElement apps_xml = new XElement("Applications");
                        string elementName = "";
                        foreach (string s in appFields)
                        {
                            elementName = tblaSwitcher(s.Trim().ToLower());
                            if (elementName != "")
                                apps_xml.Add(new XElement(s.Trim(), elementName));
                        }
                        // add appSTR
                        string appType = this.tbl_a.ApplicationType.Trim();

                        if (appType == "REG")
                            apps_xml.Add(new XElement("ApplicationID",
                               this.tbl_a.HearingDate.Year.ToString().Substring(2, 2) + "-" + this.tbl_a.ApplicationID.ToString().Substring(3, this.tbl_a.ApplicationID.ToString().Length - 3)));
                        else
                            apps_xml.Add(new XElement("ApplicationID",
                                "JR-" + this.tbl_a.HearingDate.Year.ToString().Substring(2, 2) + "-" + this.tbl_a.ApplicationID.ToString()));

                        //insert property address
                        var property_address = (from tbl_p in dbcontext.tblItemDetails
                                                where tbl_p.ApplicationID == this.application_id
                                                select new { tbl_p }).ToList();
                        tblItemDetail tid = new tblItemDetail();
                        if (property_address.Count > 0)
                        {
                            tid = (tblItemDetail)property_address[0].tbl_p;
                            apps_xml.Add(new XElement("Property_Address", HttpUtility.HtmlEncode(tid.Related_Address)));
                            apps_xml.Add(new XElement("permit_description", HttpUtility.HtmlEncode(tid.Description)));
                        }
                        //insert related agency name
                        var related_agency = (from tbl_dept in dbcontext.tblDepartments
                                              where tbl_dept.AgencyID == this.tbl_a.Dept_Agency
                                              select new { tbl_dept }).ToList();
                        tblDepartment tbldept = new tblDepartment();
                        if (related_agency.Count > 0)
                            tbldept = (tblDepartment)related_agency[0].tbl_dept;
                        string agency_name = "";
                        if (tbldept.DeptName != "")
                            agency_name = tbldept.DeptName;
                        apps_xml.Add(new XElement("related_agency", agency_name));

                        //insert dept contact info
                        var dept_contact = (from tbl_deptcontact in dbcontext.tblDeptContacts
                                              where tbl_deptcontact.AgencyID == this.tbl_a.Dept_Agency
                                              select new { tbl_deptcontact }).ToList();
                        tblDeptContact contact = new tblDeptContact();
                        if (dept_contact.Count() > 0)
                        {
                            contact = dept_contact[0].tbl_deptcontact;
                            string addressline1 = HttpUtility.HtmlEncode(contact.AddressLine1);
                            string first_name = (!string.IsNullOrEmpty(contact.FirstName)) ? contact.FirstName.Replace("c/o ", "") : "";
                            string last_name = (!string.IsNullOrEmpty(contact.LastName)) ? contact.LastName : "";
                            string info = HttpUtility.HtmlEncode(first_name + " " + last_name) + "<w:br />" + agency_name + "<w:br />" + addressline1.Insert(addressline1.ToLower().IndexOf("san francisco"), "<w:br />");
                            apps_xml.Add(new XElement("dept_contact", info));
                        }

                        //insert dates
                        DateTime originalHearing = new DateTime();
                        for (int i = 0; i < dates.Count(); i++)
                        {
                            tblDateField w = (tblDateField)dates[i].tbl_d;
                            string format = "MMMM dd, yyyy";
                            if (apps_xml.XPathSelectElement(w.DateType.Trim().Replace(" ", string.Empty)) == null)
                                apps_xml.Add(new XElement(w.DateType.Trim().Replace(" ", string.Empty), w.DateValue.ToString(format)));
                            if (w.DateType == "HearingDate" && w.OldValue != null)
                            {
                                if(originalHearing.Ticks == 0 )
                                    originalHearing = (DateTime)w.OldValue;
                                else if(originalHearing.Ticks > ((DateTime)w.OldValue).Ticks )
                                    originalHearing = (DateTime)w.OldValue;
                            }
                        }
                        apps_xml.Add(new XElement("OriginalHearing", originalHearing.ToString()));
                        XElement names_xml = new XElement("Names");
                        foreach (var i in query)
                        {
                            this.tbl_n = i.tbl_n;
                            XElement name_xml = new XElement("Name");
                            string elementName1 = "";
                            foreach (string s in nameFields)
                            {
                                elementName1 = tblnSwitcher(s.Trim().ToLower());
                                if (elementName1 != null)
                                    name_xml.Add(new XElement(s.Trim(), elementName1));
                            }
                            names_xml.Add(name_xml);
                        }

                        XDocument doc = new XDocument(
                            new XElement("Document",
                                new XElement(apps_xml),
                                new XElement(names_xml)
                        ));
                        return doc;
                    }
                    else
                        return null;
                }
                catch (XmlException xe)
                {
                    XDocument doc = new XDocument(new XElement("ERROR", xe.ToString()));
                    return doc;
                }
            }

            public string tblaSwitcher(string s)
            {
                string format = "MMMM dd, yyyy";
                switch (s)
                {
                    //case "applicationid": return this.tbl_a.ApplicationID.ToString();
                    case "filedate": return this.tbl_a.FileDate.ToString(format);
                    case "status": return this.tbl_a.Status;
                    case "expdate": return this.tbl_a.ExpDate.ToString(format);
                    case "permitnumber": return HttpUtility.HtmlEncode(this.tbl_a.PermitNumber);
                    case "permittype": return HttpUtility.HtmlEncode(this.tbl_a.PermitType);
                    case "routenumber": return HttpUtility.HtmlEncode(this.tbl_a.RouteNumber);
                    case "hearingdate": return this.tbl_a.HearingDate.ToString(format);
                    case "agencyid": return HttpUtility.HtmlEncode(this.tbl_a.AgencyID);
                    case "dba": return this.tbl_a.DBA;
                    case "public_comments": return HttpUtility.HtmlEncode(this.tbl_a.OtherDeptContacts); //public_comments not used, so use it for otherdeptcontacts
                    case "comm_comments": return HttpUtility.HtmlEncode(this.tbl_a.General_Comments);
                    case "casenum": return HttpUtility.HtmlEncode(this.tbl_a.CaseNum);
                    case "casetype": return HttpUtility.HtmlEncode(this.tbl_a.CaseType);
                    case "dept_agency": return HttpUtility.HtmlEncode(this.tbl_a.Dept_Agency);
                    case "block": return HttpUtility.HtmlEncode(this.tbl_a.Block);
                    case "lot": return HttpUtility.HtmlEncode(this.tbl_a.Lot);
                    case "planningdept": return HttpUtility.HtmlEncode(this.tbl_a.PlanningDept);
                    case "outcometype": return HttpUtility.HtmlEncode(this.tbl_a.OutcomeType);
                    case "assoc_appeal": return HttpUtility.HtmlEncode(this.tbl_a.AssocAppeal);
                    case "jrgrantedtext": return HttpUtility.HtmlEncode(this.tbl_a.JRGrantedText);
                    default: return "";
                }
            }
            public string tblnSwitcher(string s)
            {
                switch (s)
                {
                    case "namedescription": return HttpUtility.HtmlEncode(this.tbl_n.NameDescription);
                    case "lastname": return HttpUtility.HtmlEncode(this.tbl_n.LastName);
                    case "middlename": return HttpUtility.HtmlEncode(this.tbl_n.MiddleName);
                    case "firstname": return HttpUtility.HtmlEncode(this.tbl_n.FirstName);
                    case "addressline1": return HttpUtility.HtmlEncode(this.tbl_n.AddressLine1);
                    case "addressline2": return HttpUtility.HtmlEncode(this.tbl_n.AddressLine2);
                    case "city": return HttpUtility.HtmlEncode(this.tbl_n.city);
                    case "state": return HttpUtility.HtmlEncode(this.tbl_n.state);
                    case "zip": return this.tbl_n.zip;
                    case "phonenumber": return this.tbl_n.PhoneNumber;
                    case "type": return this.tbl_n.Type;
                    case "comments": return HttpUtility.HtmlEncode(this.tbl_n.Comments);
                    case "applicationid": return this.tbl_n.ApplicationID.ToString();
                    case "moddate": return this.tbl_n.ModDate.GetValueOrDefault().ToString();
                    case "type2": return this.tbl_n.Type2;
                    default: return "";
                }
            }
        }

    }
}


