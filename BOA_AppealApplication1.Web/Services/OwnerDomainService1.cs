﻿
namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;


    // Implements application logic using the LicenseDBEntities1 context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class OwnerDomainService1 : LinqToEntitiesDomainService<LicenseDBEntities>
    {
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblNameFields' query.
        public IQueryable<tblNameField> GetTblNameFields()
        {
            return this.ObjectContext.tblNameFields;
        }

        //Get One Contact
        public IQueryable<tblNameField> GetOneTblNameFields(string nameDescription)
        {
            return this.ObjectContext.tblNameFields.Where(e => e.NameDescription == nameDescription);
        }

        //Get all appeal application related contacts.
        public IQueryable<tblNameField> GetTblNameAllFields(int applicationID)
        {
            return this.ObjectContext.tblNameFields.Where(e => e.ApplicationID == applicationID);
        }

        //Checked if there is already Primary Owner information entered.
        public int GetOwnerCount(int applicationID)
        {
            return this.ObjectContext.tblNameFields.Where(e => e.Type == "Respondent" && e.ApplicationID == applicationID).Count();
        }

        //Check if there is already Primary Appellant information entered.
        public int GetAppellantCount(int applicationID)
        {
            return this.ObjectContext.tblNameFields.Where(e => (e.Type == "Primary Appellant" || e.Type == "Apellant's Attorney") && e.ApplicationID == applicationID).Count();
        }
        
        //Get Primary Owner details.
        public IQueryable<tblNameField> GetTblNameOwnerFields(int applicationID)
        {
            var result =  this.ObjectContext.tblNameFields.Where(e => e.Type == "Respondent" && e.ApplicationID == applicationID);
            if (result.Count() < 1)
            {
                List<tblNameField> nameList = result.ToList();
                nameList.Insert(0, new tblNameField { NameDescription="", AddressLine1="", AddressLine2="", ApplicationID=applicationID, cellphone="", city="", Comments="", email="", fax="", FirstName="", LastName="", MiddleName="", Type="Respondent", state="", zip="", UserID="", PhoneNumber=""  });
                return nameList.AsQueryable();
            }
                
            return result.AsQueryable();
        }

        //Get Primary Appellant details.
        public IQueryable<tblNameField> GetTblNameAppellantFields(int applicationID)
        {
            return this.ObjectContext.tblNameFields.Where(e => (e.Type == "Primary Appellant" || e.Type == "Apellant's Attorney") && e.ApplicationID == applicationID);
        }

        //Get all other contacts details.
        public IQueryable<tblNameField> GetTblOthersNameFields(int applicationID)
        {
            return this.ObjectContext.tblNameFields.Where(e => e.Type != "Respondent" && e.Type != "Primary Appellant" && e.ApplicationID == applicationID);
        }

        public void InsertTblNameFields(tblNameField tblNameField)
        {
            if ((tblNameField.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblNameField, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblNameFields.AddObject(tblNameField);
            }
        }

        public void UpdateTblNameFields(tblNameField currenttblNameField)
        {
            this.ObjectContext.tblNameFields.AttachAsModified(currenttblNameField, this.ChangeSet.GetOriginal(currenttblNameField));
        }

        public void DeleteTblNameFields(tblNameField tblNameField)
        {
            if ((tblNameField.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblNameFields.Attach(tblNameField);
            }
            this.ObjectContext.tblNameFields.DeleteObject(tblNameField);
        }

        [Invoke]
        public bool checkExistingNames(int applicationID, string type)
        {
            if (this.ObjectContext.tblNameFields.Where(e => e.ApplicationID == applicationID && e.Type == type).Count() > 0)
            {
                return true;
            }
            else { return false; }
        }
        
    }
}


