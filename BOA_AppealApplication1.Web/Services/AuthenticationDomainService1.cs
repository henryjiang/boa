﻿namespace BOA_AppealApplication1.Web
{
    using System.Security.Authentication;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using System.ServiceModel.DomainServices.Server.ApplicationServices;
    using System.Threading;
    using System.Linq;

    /// <summary>
    /// RIA Services DomainService responsible for authenticating users when
    /// they try to log on to the application.
    ///
    /// Most of the functionality is already provided by the base class
    /// AuthenticationBase
    /// </summary>
    [EnableClientAccess]
    public class AuthenticationService : AuthenticationBase<User>
    {

        protected override bool ValidateUser(string userName, string password)
        {
            LicenseDBEntities userContext = new LicenseDBEntities();
            // get the pwd for this user
            var users =
                    from u in userContext.tblMainUsers
                    where (u.UserID == userName && u.IsActive == "Active")
                    select u;

            string hash = string.Empty;

            //user id not found
            if (users.Count() == 0) return false;

            //check password
            hash = users.First().Password;
            return Crypto.ValidatePassword(password, hash);
            //return true;

        }
    }
}
