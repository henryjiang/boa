﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web.Resources;
    using BOA_AppealApplication1.Web.Models.Shared;
    using BOA_AppealApplication1.Web.Resources;
    using BOA_AppealApplication1.Web.Models;

    // The MetadataTypeAttribute identifies tblMainUserMetadata as the class
    // that carries additional metadata for the tblMainUser class.
    [MetadataTypeAttribute(typeof(tblMainUser.tblMainUserMetadata))]
    public partial class tblMainUser
    {

        // This class allows you to attach custom attributes to properties
        // of the tblMainUser class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblMainUserMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblMainUserMetadata()
            {
            }

            public Nullable<short> AccessGroupID { get; set; }
            
            [Required(ErrorMessage = "Agency is required")]
            public string AgencyID { get; set; }

            public string IsActive { get; set; }

            public int MainUserID { get; set; }

            [CustomValidation(typeof(CustomValidators), "ConfirmPassword")]
            [Required(ErrorMessage = "Password is required")]
            [RegularExpression("^.*[^a-zA-Z0-9].*$", ErrorMessageResourceName = "ValidationErrorBadPasswordStrength",
                ErrorMessageResourceType = typeof(ValidationErrorResources))]
            [StringLength(200, MinimumLength = 7, ErrorMessageResourceName =
                "ValidationErrorBadPasswordLength", ErrorMessageResourceType = typeof(ValidationErrorResources))]
            public string Password { get; set; }

            [Required(ErrorMessage = "User Name is required")]
            
            public string UserID { get; set; }

        }
    }

}
