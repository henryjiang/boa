﻿
namespace BOA_AppealApplication1.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;


    // The MetadataTypeAttribute identifies view_applicationsMetadata as the class
    // that carries additional metadata for the view_applications class.
    [MetadataTypeAttribute(typeof(view_applications.view_applicationsMetadata))]
    public partial class view_applications
    {

        // This class allows you to attach custom attributes to properties
        // of the view_applications class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class view_applicationsMetadata
        {
            // Metadata classes are not meant to be instantiated.
            private view_applicationsMetadata()
            {
            }

            public string AgencyID { get; set; }

            [Editable(true)]
            public int ApplicationID { get; set; }

            public string CaseNum { get; set; }

            public string CaseType { get; set; }

            public string General_Comments { get; set; }

            [Display(Description = "Doing Business As.")]
            public string DBA { get; set; }

            public DateTime ExpDate { get; set; }

            public Nullable<DateTime> FileDate { get; set; }

            public Nullable<DateTime> HearingDate { get; set; }

            [Display(Description = "The Permit Number Which Is Being Appealed.")]
            public string PermitNumber { get; set; }

            public string OtherDeptContacts { get; set; }

            public string RouteNumber { get; set; }

            public string Status { get; set; }

            [Display(Description = "Agency Where The Permit Was Granted.")]
            public string Dept_Agency { get; set; }

            public decimal InitialFee { get; set; }
            
            public string OutcomeType { get; set; }

            public string PermitType { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies view_applicationsMetadata as the class
    // that carries additional metadata for the view_applications class.
    [MetadataTypeAttribute(typeof(tblApplication_JR.tblApplication_JRMetadata))]
    public partial class tblApplication_JR
    {

        // This class allows you to attach custom attributes to properties
        // of the view_applications class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblApplication_JRMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblApplication_JRMetadata()
            {
            }

            public string AgencyID { get; set; }

            [Editable(true)]
            public int ApplicationID { get; set; }

            public string CaseNum { get; set; }

            public string CaseType { get; set; }

            public string Comm_Comments { get; set; }

            [Display(Description = "Doing Business As.")]
            public string DBA { get; set; }

            [Required(ErrorMessage="Effect Date is required!")]
            public DateTime ExpDate { get; set; }

            [Required(ErrorMessage = "File Date is required!")]
            public Nullable<DateTime> FileDate { get; set; }

            [Required(ErrorMessage = "Hearing Date is required!")]
            public Nullable<DateTime> HearingDate { get; set; }

            [Display(Description = "The Permit Number Which Is Being Appealed.")]
            [Required(ErrorMessage = "Permit Number is required!")]
            public string PermitNumber { get; set; }

            public string OtherDeptContacts { get; set; }

            public string RouteNumber { get; set; }

            public string Status { get; set; }

            [Display(Description = "Agency Where The Permit Was Granted.")]
            public string Dept_Agency { get; set; }

            public decimal InitialFee { get; set; }

            public string OutcomeType { get; set; }

            public string PermitType { get; set; }
        }
    }
    // The MetadataTypeAttribute identifies view_applicationsMetadata as the class
    // that carries additional metadata for the view_applications class.
    [MetadataTypeAttribute(typeof(tblApplications.tblApplicationsMetadata))]
    public partial class tblApplications
    {

        // This class allows you to attach custom attributes to properties
        // of the view_applications class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblApplicationsMetadata
        {
            // Metadata classes are not meant to be instantiated.
            private tblApplicationsMetadata()
            {
            }


            public string AgencyID { get; set; }

            [Editable(true)]
            public int ApplicationID { get; set; }

            public string CaseNum { get; set; }

            public string CaseType { get; set; }

            public string Comm_Comments { get; set; }

            [Display(Description = "Doing Business As.")]
            public string DBA { get; set; }

            [Required(ErrorMessage = "Effect Date is required!")]
            public DateTime ExpDate { get; set; }

            [Required(ErrorMessage = "File Date is required!")]
            public Nullable<DateTime> FileDate { get; set; }

            [Required(ErrorMessage = "Hearing Date is required!")]
            public Nullable<DateTime> HearingDate { get; set; }

            [Display(Description = "The Permit Number Which Is Being Appealed.")]
            [Required(ErrorMessage="Permit Number is required!")]
            public string PermitNumber { get; set; }

            public string OtherDeptContacts { get; set; }

            public string RouteNumber { get; set; }

            public string Status { get; set; }

            [Display(Description = "Agency Where The Permit Was Granted.")]
            public string Dept_Agency { get; set; }

            public decimal InitialFee { get; set; }

            public string OutcomeType { get; set; }

            public string PermitType { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblItemDetailMetadata as the class
    // that carries additional metadata for the tblItemDetail class.
    [MetadataTypeAttribute(typeof(tblItemDetail.tblItemDetailMetadata))]
    public partial class tblItemDetail
    {

        // This class allows you to attach custom attributes to properties
        // of the tblItemDetail class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblItemDetailMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblItemDetailMetadata()
            {
            }

            public string Action { get; set; }

            [Display(Description = "Agency Where The Permit Was Granted.")]
            public string AgencyID { get; set; }

            public int ApplicationID { get; set; }

            public string Comments { get; set; }

            public string Decision { get; set; }

            public DateTime DecisionDate { get; set; }

            public DateTime ModDate { get; set; }

            public string Related_Agency { get; set; }

            public string Related_Address { get; set; }

            public string Related_City { get; set; }

            public string Related_State { get; set; }

            public string Related_Zip { get; set; }

            public string Description { get; set; }

            public string Dept_Contact { get; set; }

            public string UserID { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblPermitTypeMetadata as the class
    // that carries additional metadata for the tblPermitType class.
    [MetadataTypeAttribute(typeof(tblPermitType.tblPermitTypeMetadata))]
    public partial class tblPermitType
    {

        // This class allows you to attach custom attributes to properties
        // of the tblPermitType class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblPermitTypeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblPermitTypeMetadata()
            {
            }

            public string AgencyID { get; set; }

            public string Comments { get; set; }

            public string PermitName { get; set; }

            [Editable(true)]
            public string PermitType { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public decimal Fee { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblStatusTypeMetadata as the class
    // that carries additional metadata for the tblStatusType class.
    [MetadataTypeAttribute(typeof(tblStatusType.tblStatusTypeMetadata))]
    public partial class tblStatusType
    {

        // This class allows you to attach custom attributes to properties
        // of the tblStatusType class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblStatusTypeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblStatusTypeMetadata()
            {
            }

            public string AgencyID { get; set; }

            public string Comments { get; set; }

            public string StatusName { get; set; }

            public string StatusType { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies tblActionTypeMetadata as the class
    // that carries additional metadata for the tblActionType class.
    [MetadataTypeAttribute(typeof(tblActionType.tblActionTypeMetadata))]
    public partial class tblActionType
    {

        // This class allows you to attach custom attributes to properties
        // of the tblActionType class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblActionTypeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblActionTypeMetadata()
            {
            }

            public string ActionName { get; set; }

            public string ActionType { get; set; }

            public string AgencyID { get; set; }

            public string Comments { get; set; }

            public Nullable<DateTime> EndDate { get; set; }

            public Nullable<decimal> Fee { get; set; }

            public string Related_Agency { get; set; }

            public Nullable<DateTime> StartDate { get; set; }
        }
    }

    [MetadataTypeAttribute(typeof(tblOutcomeType.tblOutcomeTypeMetadata))]
    public partial class tblOutcomeType
    {

        // This class allows you to attach custom attributes to properties
        // of the tblActionType class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class tblOutcomeTypeMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private tblOutcomeTypeMetadata()
            {
            }

            public string OutcomeName { get; set; }

            public string OutcomeType { get; set; }

            public string AgencyID { get; set; }

            public string Comments { get; set; }
        }
    }
    
    public class MyProjection
    {
        public string AgencyID { get; set; }
        public string CaseNum { get; set; }

        public string CaseType { get; set; }
        
        public string Comm_Comments { get; set; }

        [Display(Description = "Doing Business As.")]
        public string DBA { get; set; }

        public Nullable<DateTime> ExpDate { get; set; }

        public Nullable<DateTime> FileDate { get; set; }

        public Nullable<DateTime> HearingDate { get; set; }

        [Display(Description = "The Permit Number Which Is Being Appealed.")]
        public string PermitNumber { get; set; }

        public string OtherDeptContacts { get; set; }

        public string PermitType { get; set; }

        public string RouteNumber { get; set; }

        public string Status { get; set; }

        [Display(Description = "Agency Where The Permit Was Granted.")]
        public string Dept_Agency { get; set; }

        
        public Nullable<decimal> InitialFee { get; set; }
        [Display(Name = "Address:")]
        public string AddressLine1 { get; set; }

        [Display(Name = "City, State, Zip Code:")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Appeal NO:")]
        [Key]
        public int ApplicationID { get; set; }

        public string ApplicationType { get; set; }

        [ReadOnly(false)]
        [Editable(true)]
        public string ApplicationIDstr
        {
            get
            {
                if (this.HearingDate.Value != null)
                {
                    string temp = this.ApplicationID.ToString();
                   
                    if(this.ApplicationID.ToString().Length > 4)
                        temp = this.ApplicationID.ToString().Substring(3,this.ApplicationID.ToString().Length - 3);
                    if(this.ApplicationID.ToString().Length > 4)
                        return (this.HearingDate.Value.Year.ToString().Substring(2, 2) + "-" + temp);
                    else
                        return ("JR-" + this.HearingDate.Value.Year.ToString().Substring(2, 2) + "-" + temp);
                }
                else
                    return this.ApplicationID.ToString();
            }
        }

        public string Comments { get; set; }

        [Display(Name = "First Name:")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name:")]
        public string LastName { get; set; }

        [Display(Name = "Middle Name:")]
        public string MiddleName { get; set; }

        public string FullName { get; set; }

        public string FullAddress { get; set; }

        public Nullable<DateTime> ModDate { get; set; }

        public string NameDescription { get; set; }

        [Display(Name = "Phone Number:")]
        public string PhoneNumber { get; set; }

        public string Type { get; set; }

        public string Type2 { get; set; }

        public string UserID { get; set; }

        public string OutcomeType { get; set; }

        public string AssocAppeal { get; set; }

        public string Block { get; set; }
        public string Lot { get; set; }
        public string PlanningDept { get; set; }
        public string JRGrantedText { get; set; }
        public string ConditionsText { get; set; }
        public bool JRGranted { get; set; }
        public bool Conditions { get; set; }
        public string Continuance { get; set; }
    }


}
