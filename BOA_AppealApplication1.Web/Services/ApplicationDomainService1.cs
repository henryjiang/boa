﻿
namespace BOA_AppealApplication1.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.ServiceModel.DomainServices.EntityFramework;
    using System.ServiceModel.DomainServices.Hosting;
    using System.ServiceModel.DomainServices.Server;
    using BOA_AppealApplication1.Web;
    using System.Linq.Expressions;

    // Implements application logic using the LicenseDBEntities context.
    // TODO: Add your application logic to these methods or in additional methods.
    // TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
    // Also consider adding roles to restrict access as appropriate.
    // [RequiresAuthentication]
    [EnableClientAccess()]
    public class ApplicationDomainService1 : LinqToEntitiesDomainService<LicenseDBEntities>
    {

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'view_applications' query.
        public IQueryable<view_applications> Getview_applications(string agencyID)
        {
            return this.ObjectContext.view_applications.Where(e => e.AgencyID == agencyID).OrderByDescending(e => e.ApplicationID);
        }
        public IEnumerable<MyProjection> GetTblApplicationAndNamesQuery(string agencyID)
        {
            var query = (from a in this.ObjectContext.view_applications
                         join b in this.ObjectContext.tblNameFields on a.ApplicationID equals b.ApplicationID 
                         join c in this.ObjectContext.tblItemDetails on a.ApplicationID equals c.ApplicationID orderby a.ApplicationID descending
                         select new MyProjection
                         {
                             ApplicationID = a.ApplicationID,
                             ApplicationIDstr = a.ApplicationIDstr,
                             ApplicationType = a.ApplicationType,
                             FileDate = a.FileDate,
                             Status = a.Status,
                             ExpDate = a.ExpDate,
                             AgencyID = a.AgencyID,
                             DBA = a.DBA,
                             Comm_Comments = a.General_Comments,
                             OtherDeptContacts = a.OtherDeptContacts,
                             InitialFee = (decimal)a.InitialFee,
                             AssocAppeal = a.AssocAppeal,
                             PlanningDept = a.PlanningDept,
                             JRGranted = (bool)(a.JRGranted == null ? false : a.JRGranted),
                             JRGrantedText = a.JRGrantedText,
                             Conditions = (bool)(a.Conditions == null ? false : a.Conditions),
                             ConditionsText = a.ConditionsText,
                             Continuance = a.Continuance,
                             Block = a.Block,
                             Lot = a.Lot,
                             HearingDate = a.HearingDate,
                             FullAddress = c.Related_Address, //+ "," + (string.IsNullOrEmpty(c.Related_City) ? "" : c.Related_City) + "," + (string.IsNullOrEmpty(c.Related_State) ? "" : c.Related_State) + "," + (string.IsNullOrEmpty(c.Related_Zip) ? "" : c.Related_Zip), 
                             PermitNumber = a.PermitNumber,  Dept_Agency = a.Dept_Agency, 
                             FullName=(b.LastName != "" && b.FirstName != "") ? string.Concat(b.LastName, ", ", b.FirstName) : "", 
                             //FullName = this.getFullName(a.ApplicationID),
                             CaseType = a.CaseType, OutcomeType = a.OutcomeType, PermitType = a.PermitType
                         }).ToList();
            List<MyProjection> myProjection = new List<MyProjection>() { };
            foreach (MyProjection p in query)
            {
                p.FullName = getFullName(p.ApplicationID);
                /*string[] fullAddress = p.FullAddress.Split(',');
                if(fullAddress.Count() > 0)
                    p.FullAddress = getFullAddress(fullAddress[0], fullAddress[1], fullAddress[2], fullAddress[3]);
                 */
                myProjection.Add(p);
            }
            return myProjection.AsEnumerable();
        }
        public string getFullAddress(string address1, string city, string state, string zip)
        {
            string fullAddress = "";
            fullAddress += (address1 == "" || address1 == null) ? "" : address1 + ", ";
            fullAddress += (city == "" || city == null) ? "" : city + ", ";
            fullAddress += (state == "" || state == null) ? "" : state;
            fullAddress += (zip == "" || zip == null) ? "" : ", " + zip;
            return fullAddress;
        }
        public string getFullName(int applicationID, string types="")
        {
            var result = new List<tblNameField>();

            if (types.Equals(string.Empty))
                result = this.ObjectContext.tblNameFields.Where(e => e.ApplicationID == applicationID).ToList();
            else
            {
                result = this.ObjectContext.tblNameFields.Where(e => e.ApplicationID == applicationID && (e.Type.Equals("Primary Appellant") || e.Type.Equals("Co-Appellant"))).ToList();
            }

            string fullname = "";
            string PrimaryAppellant = "";
            string Respondent = "";
            string CoRespondent = "";
            string CoAppellant = "";
            string AppellantAgent = "";
            string AppellantAttorney = ""; 
            string DeterminationHolderAgent = "";
            string DeterminationHolderAttorney = "";
            string Other = "";
            foreach (tblNameField names in result)
            {
                if ((names.FirstName == null || names.FirstName =="") && (names.LastName == null || names.LastName == ""))
                    continue;
                /*if (fullname != "")
                    fullname += ", ";
                 */
                fullname += names.FirstName + " ";
                if(names.MiddleName != null && names.MiddleName != "")
                    fullname += names.MiddleName + " ";
                fullname += names.LastName;
                switch (names.Type)
                {
                    case "Primary Appellant": PrimaryAppellant = fullname; break;
                    case "Respondent": Respondent = fullname; break;
                    case "Co-Respondent": 
                        CoRespondent = (CoRespondent == "" ) ? fullname : CoRespondent + ", " + fullname; 
                        break;
                    case "Co-Appellant": 
                        CoAppellant = (CoAppellant == "") ? fullname : CoAppellant + ", " + fullname; 
                        break;
                    case "Appellant's Agent": 
                        AppellantAgent = (AppellantAgent == "") ? fullname : AppellantAgent + ", " + fullname; 
                        break;
                    case "Appellant's Attorney": 
                        AppellantAttorney = (AppellantAttorney == "") ? fullname : AppellantAttorney + ", " + fullname; 
                        break;
                    case "Determination Holder's Agent": 
                        DeterminationHolderAgent = (DeterminationHolderAgent == "") ? fullname : DeterminationHolderAgent + ", " + fullname; 
                        break;
                    case "Determination Holder's Attorney": 
                        DeterminationHolderAttorney = (DeterminationHolderAttorney == "") ? fullname : DeterminationHolderAttorney + ", " + fullname; 
                        break;
                    case "Other": 
                        Other = (Other == "") ? fullname : Other + ", " + fullname; 
                        break;
                    default: fullname = ""; break;
                }
                fullname = "";
            }
            fullname = PrimaryAppellant + ((Respondent == "") ? "" : ", " + Respondent) + ((CoRespondent == "") ? "" : ", " + CoRespondent) + ((CoAppellant == "") ? "" : ", " + CoAppellant)
                + ((AppellantAgent == "") ? "" : ", " + AppellantAgent) + ((AppellantAttorney == "") ? "" : ", " + AppellantAttorney) + ((DeterminationHolderAgent == "") ? "" : ", " + DeterminationHolderAgent)
                + ((DeterminationHolderAttorney == "") ? "" : ", " + DeterminationHolderAttorney) + ((Other == "") ? "" : ", " + Other);
            return fullname;
        }
        public IEnumerable<view_applications> GetAllview_applications()
        {
            return this.ObjectContext.view_applications.ToList();
        }

        public int GetApplicationsCountByAgencyID(string relatedAgencyID)
        {
            return this.ObjectContext.view_applications.Where(e => e.Dept_Agency == relatedAgencyID).Count();
        }

        public IQueryable<view_applications> Getview_applicationsDetails(int applicationID, string agencyID)
        {
            return this.ObjectContext.view_applications.Where(e => e.ApplicationID == applicationID && e.AgencyID == agencyID);
        }

        public IEnumerable<view_applications> Getview_applicationsHearing(DateTime hearingDate, string agencyID)
        {
            var query = this.ObjectContext.view_applications.Where(e => e.HearingDate.Year == hearingDate.Year
                     && e.HearingDate.Month == hearingDate.Month
                     && e.HearingDate.Day == hearingDate.Day
                     && e.AgencyID == agencyID).OrderBy(e => e.ApplicationID).ToList();

            string types = "Co-Appellant, Primary Appellant";
            List<view_applications> myList = new List<view_applications>() { };
            foreach (view_applications va in query)
            {
                va.General_Comments = this.getFullName(va.ApplicationID, types);
                myList.Add(va);
            }
            return myList.AsEnumerable();
        }

        //Regular
        public IQueryable<tblApplication> GetApplications(string agencyID)
        {
            return this.ObjectContext.tblApplications.Where(e => e.AgencyID == agencyID).OrderByDescending(e => e.ApplicationID);
        }
        public IQueryable<tblApplication> GettblApplicationsDetails(string agencyID, int applicationID)
        {
            return this.ObjectContext.tblApplications.Where(e => e.ApplicationID == applicationID && e.AgencyID == agencyID);
        }

        public int GetMAXApplicationsID(string agencyID)
        {
            var result = this.ObjectContext.tblApplications.Where(e => e.AgencyID == agencyID);
            if(result.Count() > 0)
                return result.Max(a => a.ApplicationID);
            return 0;
        }

        public void InserttblApplication(tblApplication view_applications)
        {
            if ((view_applications.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(view_applications, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblApplications.AddObject(view_applications);
            }
        }

        public void UpdatetblApplication(tblApplication currentview_applications)
        {
            this.ObjectContext.tblApplications.AttachAsModified(currentview_applications, this.ChangeSet.GetOriginal(currentview_applications));
        }

        public void DeletetblApplication(tblApplication view_applications)
        {
            if ((view_applications.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblApplications.Attach(view_applications);
            }
            this.ObjectContext.tblApplications.DeleteObject(view_applications);
        }

        //JR
        public IQueryable<tblApplications_JR> GettblApplications_JR(string agencyID)
        {
            return this.ObjectContext.tblApplications_JR.Where(e => e.AgencyID == agencyID).OrderByDescending(e => e.ApplicationID);
        }
        public IQueryable<tblApplications_JR> GettblApplications_JRDetails(string agencyID, int applicationID)
        {
            return this.ObjectContext.tblApplications_JR.Where(e => e.ApplicationID == applicationID && e.AgencyID == agencyID);
        }

        public int GetMAXApplicationsID_JR(string agencyID)
        {
            return this.ObjectContext.tblApplications_JR.Where(e => e.AgencyID == agencyID).Max(a => a.ApplicationID);
        }
        public void InserttblApplication_JR(tblApplications_JR view_applications)
        {
            if ((view_applications.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(view_applications, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblApplications_JR.AddObject(view_applications);
            }
        }

        public void UpdatetblApplication_JR(tblApplications_JR currentview_applications)
        {
            this.ObjectContext.tblApplications_JR.AttachAsModified(currentview_applications, this.ChangeSet.GetOriginal(currentview_applications));
        }

        public void DeletetblApplication_JR(tblApplications_JR view_applications)
        {
            if ((view_applications.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblApplications_JR.Attach(view_applications);
            }
            this.ObjectContext.tblApplications_JR.DeleteObject(view_applications);
        }
        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblItemDetails' query.
        public IQueryable<tblItemDetail> GetTblItemDetails()
        {
            return this.ObjectContext.tblItemDetails;
        }

        public IQueryable<tblItemDetail> GetApplicationTblItemDetails(int applicationID, string agencyID)
        {
            return this.ObjectContext.tblItemDetails.Where(e => e.ApplicationID == applicationID && e.AgencyID == agencyID);
        }

        public bool checkExistingDetails(int applicationID, string agencyID)
        {
            if (this.ObjectContext.tblItemDetails.Where(e => e.ApplicationID == applicationID && e.AgencyID == agencyID).Count() > 0)
            {
                return true;
            }
            else { return false; }

        }

        public void InsertTblItemDetail(tblItemDetail tblItemDetail)
        {
            if ((tblItemDetail.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblItemDetail, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblItemDetails.AddObject(tblItemDetail);
            }
        }

        public void UpdateTblItemDetail(tblItemDetail currenttblItemDetail)
        {
            this.ObjectContext.tblItemDetails.AttachAsModified(currenttblItemDetail, this.ChangeSet.GetOriginal(currenttblItemDetail));
        }

        public void DeleteTblItemDetail(tblItemDetail tblItemDetail)
        {
            if ((tblItemDetail.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblItemDetails.Attach(tblItemDetail);
            }
            this.ObjectContext.tblItemDetails.DeleteObject(tblItemDetail);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblPermitTypes' query.
        public IQueryable<tblPermitType> GetTblPermitTypes(string agencyID)
        {
            return this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID).OrderBy(e => e.PermitName);
        }

        // by permit type and agency
        public IQueryable<tblPermitType> GetOneTblPermitType(string PermitType, string AgencyID)
        {
            return this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == AgencyID && e.PermitType == PermitType);
        }

        //filter by DeptID
        public IQueryable<tblPermitType> GetTblPermitTypesWithDeptID(string agencyID, string DeptID)
        {
            var result =  this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID && e.DeptID== DeptID).OrderBy(e => e.PermitName);
            List<tblPermitType> newList = result.ToList();
            //Add other permit type to the list, this record doesn't need to be in the DB
            //newList.Insert(0, new tblPermitType { PermitName = "Other Permit Type",  PermitType="Other Permit Type", AgencyID="BOA", DeptID=""});
            newList.Insert(newList.Count, new tblPermitType { PermitName = "Other Permit Type", PermitType = "Other Permit Type", AgencyID = "BOA", DeptID = "" });
            //empty selection
            newList.Insert(0, new tblPermitType { PermitName = "", PermitType = "", AgencyID = "BOA", DeptID = "" });
            return newList.AsQueryable();
        }
     
        public decimal GetPermitFee(string agencyID, string permitType)
        {
            //var result = this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID && e.PermitType == permitType && e.StartDate < DateTime.Now && e.EndDate.Value >= DateTime.Now).Select(s => s.Fee).FirstOrDefault();
            return 0;
        }

        public IQueryable<tblPermitType> GetTblPermitDetails(string agencyID, int applicationID)
        {
            view_applications currApplication = Getview_applicationsDetails(applicationID, agencyID).FirstOrDefault();
            
            return this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID && e.PermitType == currApplication.CaseType).OrderBy(e => e.PermitName);
        }

        public void InsertTblPermitType(tblPermitType tblPermitType)
        {
            if ((tblPermitType.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblPermitType, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblPermitTypes.AddObject(tblPermitType);
            }
        }

        public void UpdateTblPermitType(tblPermitType currenttblPermitType)
        {
            this.ObjectContext.tblPermitTypes.AttachAsModified(currenttblPermitType, this.ChangeSet.GetOriginal(currenttblPermitType));
        }

        public void DeleteTblPermitType(tblPermitType tblPermitType)
        {
            if ((tblPermitType.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblPermitTypes.Attach(tblPermitType);
            }
            this.ObjectContext.tblPermitTypes.DeleteObject(tblPermitType);
        }
        public int checkPermityType(string permitType, string agencyID)
        {
            return this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID && e.PermitType == permitType).Count();
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblStatusTypes' query.
        public IQueryable<tblStatusType> GetTblStatusTypes(string agencyID)
        {
            var result = this.ObjectContext.tblStatusTypes.Where(e => e.AgencyID == agencyID).OrderBy(e => e.StatusType);
            List<tblStatusType> newList = result.ToList();
            newList.Insert(0, new tblStatusType { AgencyID = "", Comments = "", StatusName = "", StatusType = "" });
            return newList.AsQueryable();
        }
        
        public void InsertTblStatusType(tblStatusType tblStatusType)
        {
            if ((tblStatusType.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblStatusType, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblStatusTypes.AddObject(tblStatusType);
            }
        }

        public void UpdateTblStatusType(tblStatusType currenttblStatusType)
        {
            this.ObjectContext.tblStatusTypes.AttachAsModified(currenttblStatusType, this.ChangeSet.GetOriginal(currenttblStatusType));
        }

        public void DeleteTblStatusType(tblStatusType tblStatusType)
        {
            if ((tblStatusType.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblStatusTypes.Attach(tblStatusType);
            }
            this.ObjectContext.tblStatusTypes.DeleteObject(tblStatusType);
        }

        // outcome
        public IQueryable<tblOutcomeType> GetTblOutcomeTypes(string agencyID)
        {
            var result = this.ObjectContext.tblOutcomeTypes.Where(e => e.AgencyID == agencyID);
            List<tblOutcomeType> newList = result.ToList();
            newList.Insert(0, new tblOutcomeType { OutcomeName="", OutcomeType="", Comments ="", AgencyID="" });
            return newList.AsQueryable();
        }
        public IQueryable<tblOutcomeType> GetTblOutcomeType(string agencyID)
        {
            return this.ObjectContext.tblOutcomeTypes.Where(e => e.OutcomeType == agencyID);
        }

        // TODO:
        // Consider constraining the results of your query method.  If you need additional input you can
        // add parameters to this method or create additional query methods with different names.
        // To support paging you will need to add ordering to the 'tblActionTypes' query.
        public IQueryable<tblActionType> GetTblActionTypes()
        {
            return this.ObjectContext.tblActionTypes;
        }

        public IQueryable<tblActionType> GetDepartmentTblActionTypes(string agencyID)
        {
            var result = this.ObjectContext.tblActionTypes.Where(e => e.AgencyID == agencyID).OrderBy(e => e.ID);;
            List<tblActionType> newList = result.ToList();
            newList.Insert(0, new tblActionType { ActionName ="", ActionType ="", AgencyID ="", Comments="", Fee= 0, Related_Agency ="", EndDate = null, StartDate = null  });
            return newList.AsQueryable();
        }

        public void InsertTblActionType(tblActionType tblActionType)
        {
            if ((tblActionType.EntityState != EntityState.Detached))
            {
                this.ObjectContext.ObjectStateManager.ChangeObjectState(tblActionType, EntityState.Added);
            }
            else
            {
                this.ObjectContext.tblActionTypes.AddObject(tblActionType);
            }
        }

        public void UpdateTblActionType(tblActionType currenttblActionType)
        {
            this.ObjectContext.tblActionTypes.AttachAsModified(currenttblActionType, this.ChangeSet.GetOriginal(currenttblActionType));
        }

        public void DeleteTblActionType(tblActionType tblActionType)
        {
            if ((tblActionType.EntityState == EntityState.Detached))
            {
                this.ObjectContext.tblActionTypes.Attach(tblActionType);
            }
            this.ObjectContext.tblActionTypes.DeleteObject(tblActionType);
        }

        #region //Invokables for code behind binding
      
        [Invoke]
        public IEnumerable<tblStatusType> GetTblStatusTypes1(string agencyID, string JR)
        {
            List<tblStatusType> newList = new List<tblStatusType>();
            if (JR == "REG")
            {
                newList = this.ObjectContext.tblStatusTypes.Where(e => e.AgencyID == agencyID && (e.JR == "NO" || e.JR == "BOTH" ) ).OrderBy(e => e.StatusType).ToList();
            }
            else
            {
                newList = this.ObjectContext.tblStatusTypes.Where(e => e.AgencyID == agencyID && (e.JR == "YES" || e.JR == "BOTH")).OrderBy(e => e.StatusType).ToList();
            }
            //List<tblStatusType> newList = result.ToList();
            newList.Insert(0, new tblStatusType { AgencyID = "", Comments = "", StatusName = "", StatusType = "" });
            return newList.AsEnumerable();
        }
        
        [Invoke]
        public IEnumerable<tblPermitType> GetTblPermitTypes1(string agencyID, string DeptID)
        {
            var result = this.ObjectContext.tblPermitTypes.Where(e => e.AgencyID == agencyID && e.DeptID == DeptID).OrderBy(e => e.PermitName);
            List<tblPermitType> newList = result.ToList();
            //Add other permit type to the list, this record doesn't need to be in the DB
            newList.Insert(newList.Count, new tblPermitType { PermitName = "Other Permit Type", PermitType = "Other Permit Type", AgencyID = "BOA", DeptID = "" });
            //empty selection
            newList.Insert(0, new tblPermitType { PermitName = "", PermitType = "", AgencyID = "BOA", DeptID = "" });
            return newList.AsEnumerable();
        }

        [Invoke]
        public IEnumerable<tblActionType> GetDepartmentTblActionTypes1(string agencyID)
        {
            var result = this.ObjectContext.tblActionTypes.Where(e => e.AgencyID == agencyID).OrderBy(e => e.ID); ;
            List<tblActionType> newList = result.ToList();
            newList.Insert(0, new tblActionType { ActionName = "", ActionType = "", AgencyID = "", Comments = "", Fee = 0, Related_Agency = "", EndDate = null, StartDate = null });
            return newList.AsEnumerable();
        }
        [Invoke]

        public IEnumerable<tblOutcomeType> GetTblOutcomeType1(string agencyID)
        {
            return this.ObjectContext.tblOutcomeTypes.Where(e => e.AgencyID == agencyID).AsEnumerable();
        }
        [Invoke]
        public IEnumerable<tblApplication> GetApplications1(int applicationID)
        {
            return this.ObjectContext.tblApplications.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ApplicationID).AsEnumerable();
        }
        [Invoke]
        public IEnumerable<tblApplications_JR> GettblApplications_JR1(int applicationID)
        {
            return this.ObjectContext.tblApplications_JR.Where(e => e.ApplicationID == applicationID).OrderByDescending(e => e.ApplicationID).AsEnumerable();
        }
        [Invoke]
        public int GetApplicationIDstr(string year)
        {
            return this.ObjectContext.tblApplications.Where(x => x.ApplicationIDstr.IndexOf(year) > -1).Count();
        }
        [Invoke]
        public int GetApplicationIDstr_JR(string year)
        {
            return this.ObjectContext.tblApplications_JR.Where(x => x.ApplicationIDstr.IndexOf(year) > -1).Count();
        }
        #endregion
    }
}