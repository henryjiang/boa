﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BOA_AppealApplication1.Web.Services.Validator
{
    
    /// <summary>
    /// Validate that a date value is either a Past or Future date
    /// as appropriate.
    /// </summary>
    public class RequiredFieldValidatorAttribute : ValidationAttribute
    {
        /// <summary>
        /// The type of date expected.
        /// </summary>
        public bool ValidatorType { get; private set; }

        /// <summary>
        /// Validate that a date value is either a Past or Future date
        /// as appropriate.
        /// </summary>
        /// <param name="validatorType"></param>
        public RequiredFieldValidatorAttribute(bool validatorType)
        {
            this.ValidatorType = validatorType;
        }

        /// <summary>
        /// Conditionally validate that the date is either in the past
        /// or in the future.
        /// </summary>
        /// <param name="value">The date to validate.</param>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// <see cref="ValidationResult.Success"/> when the date matches the
        /// expected date type, otherwise a <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {

            if (value == null)
            {
                return new ValidationResult(
                    string.Format("Field is required", validationContext.DisplayName),
                    new[] { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
#if !SILVERLIGHT
        /// <summary>
        /// The desktop framework has this property and it must be
        /// overridden when allowing multiple attributes, so that
        /// attribute instances can be disambiguated based on
        /// field values.
        /// </summary>
        public override object TypeId
        {
            get { return this; }
        }
#endif
    }
}