﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using BOA_AppealApplication1.Web.Models;
using System.ComponentModel;
using BOA_AppealApplication1.Web;

namespace BOA_AppealApplication1.Web.Models
{
    public class CustomServerSideValidation
    {

        public static ValidationResult DuplicateUser(tblMainUser tblMainUser, ValidationContext validationContext)
        {
            string[] memberNames = new string[] { "UserID" };
            String userID = tblMainUser.UserID;

            LicenseDBEntities objectContext = new LicenseDBEntities();
            if (objectContext.tblMainUsers.Any(e => e.UserID == userID))
            {
                return new ValidationResult(
                            "User name already exists. Check inactive users and reactivate, or user a different user name.", memberNames);

            }
            return ValidationResult.Success;
        }
    }
}